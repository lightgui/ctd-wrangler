%% Reads in a deck cell text file and returns its control data, datenum data, datestr data, and light intensity data
function[timeCol_datenum timeCol_datestr lightCol] = readDeckCellFile(filePath)
    fid = fopen(filePath,'r');
    cellText = textscan(fid,'%d %s %s %s','Delimiter','\t');
    fclose(fid);

    controlCol = cellText{1};
    timeCol = cellText{2};
    lightCol = cellText{3};

    % check if data looks like deck cell data
    isBadFile = 0;
    if isempty(controlCol) || isempty(timeCol) || isempty(lightCol)
        isBadFile = 1;
    elseif sum(controlCol == 1) < 1
        isBadFile = 1;
    end

    if isBadFile
        timeCol_datenum = [];
        timeCol_datestr = {};
        lightCol = [];
        return;
    else
        % delete all rows where control flag is not 1
        timeCol(controlCol ~= 1) = [];
        lightCol(controlCol ~= 1) = [];
        
        % convert light date column into double
        lightCol = str2double(lightCol);
        
        % delete any remaining rows with no intensity data
        timeCol(isnan(lightCol)) = [];
        lightCol(isnan(lightCol)) = [];

        % convert date column strings into dates
        timeCol_datenum = datenum(timeCol,'yyyy-mm-dd HH:MM:SS'); % save timestamp in datenum form for comparison purposes
        timeCol_datestr = cellstr(datestr(timeCol_datenum,'yyyy-mm-dd HH:MM:SS.FFF')); % convert datenum to datestr (millisecond precision)

        % eliminate data rows with duplicate datenums
        [sortedCol, I, J] = unique(timeCol_datenum);
        timeCol_datenum = sortedCol;
        timeCol_datestr = timeCol_datestr(I);
        lightCol = lightCol(I);
    end
end
