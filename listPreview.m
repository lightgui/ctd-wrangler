function listPreview(listNames, listArray)
% Displays the contents of one or more file lists in a series of
% listboxes that can be navigated via a horizontal scrollbar.

% Default listbox size is 200x250 pixels
lbox_width = 200;
lbox_height = 250;

% Figure out how many lists need to be previewed
numLists = length(listNames);

% Size main window based on number of lists
if numLists <= 3
    window_width = lbox_width*numLists;
    slider_height = 0;
else
    window_width = lbox_width*3;
    slider_height = 20;
end
window_height = lbox_height + 50;

% Initialize main window
mainFig = figure('Visible','off','numbertitle','off','MenuBar','none',...
    'resize', 'off', 'units', 'pixels', 'Position',[100,100,window_width,window_height]);

% viewPanel is viewable area of the window
viewPanel = uipanel('Parent', mainFig, 'units', 'pixels','Position',[0 slider_height window_width+10 window_height+10-slider_height]);

% listsPanel can be much wider than viewPanel and contains the listboxes
listsPanel = uipanel('Parent', viewPanel, 'units', 'pixels','Position', [0 0 lbox_width*numLists window_height+10-slider_height]);

% scrollbar to move listsPanel within viewPanel for more than 3 lists
if numLists > 3
    slider = uicontrol('Style','Slider','Parent',mainFig,...
        'backgroundcolor', 'w', 'Units','pixels','Position',[0 0 window_width+10 slider_height],...
        'Value',0,'Min', 0, 'Max', lbox_width*numLists-window_width);
    
    % updates listsPanel position constantly, without requiring mousebutton
    % release
    addlistener(slider,'ActionEvent',@(hObject, event) scroll_listsPanel(hObject, event,listsPanel));
end

% create text descriptions of list(s)
for i=1:numLists
    uicontrol('Style','text','Parent',listsPanel,...
        'Position',[0+(i-1)*lbox_width 260 190 20],...
        'String',listNames{i});
end

% create listboxes for list(s)
for i=1:numLists
    uicontrol('Style','listbox','Parent',listsPanel,...
        'Position',[0+(i-1)*lbox_width 0 200 250],...
        'String',listArray{i},...
        'BackgroundColor','w',...
        'Max',2,...
        'Value',[]);
end

% Assign the GUI a name to appear in the window title.
set(mainFig,'Name','File List(s) Preview')

% Move the GUI to the center of the screen and make it visible
movegui(mainFig,'center')
set(mainFig, 'Visible', 'on');

    % Scrolls the listsPanel inside the viewPanel
    function scroll_listsPanel(src, eventdata, arg1)
        val = get(src,'Value');
        set(arg1,'Position',[-val 0 10 1])
    end

end