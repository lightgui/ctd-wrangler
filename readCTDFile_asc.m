function [ CTDHeader CTDData ] = readCTDFile_asc(filePath)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

% read the whole file to a temporary cell array
fid = fopen(filePath,'rt');
tmp = textscan(fid,'%s','Delimiter','\n'); % tmp is one cell of cells
fclose(fid);

tmp = tmp{1}; % access the cells of strings

% split the header by whitespace
header = tmp(1); % header is cell in first row
result = textscan(header{1},'%s','Delimiter'); % default delimiter is whitespace

CTDHeader = result{:}'; % transpose so all headers on one row

% remove POST1900/POST1990 header because its data is always empty
tf = strncmpi(CTDHeader,'post19',6);
if sum(tf)>0
    CTDHeader(tf)=[];
end

numCols = size(CTDHeader,2); % note the number of columns in file

% read the whole file again
% this time, we know how many columns of values to read and can ignore
% header line
fid = fopen(filePath,'rt');
tmp = textscan(fid,repmat('%f',1,numCols), 'headerlines', 1, 'CollectOutput', 1); % tmp is one cell of doubles
fclose(fid);
data = tmp{1}; % access cell of doubles

CTDData = data;

% delete temporary arrays
clear tmp
clear data
clear header

end

