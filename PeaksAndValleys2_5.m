function [maxtab, mintab, IndRangeTabDown, jd, IndRangeTabUp, ju, NotNanInd]=PeaksAndValleys2(v, deltaperc, x)


              %RunDownInd(jd(kk)):IndRangeTabDown(kk,2)
              %IndRangeTabUp(kk,1):RunUpInd( ju(kk))
             
%PeaksAndValies  -- detects peaks in a vector of measurements (given by x  and v)
%        [MAXTAB, MINTAB,  IndRangeTabDown, jd, IndRangeTabUp, ju] = PEAKSANDVALLEYS(X, V, DELTAPERC) 
%        finds the local maxima and minima  in the function given by the discrete vectors X and V.
%        MAXTAB and MINTAB consists of two columns. 
%        Column 1 contains indices in V (and X) , and column 2 the found values in V.
%      
%  No      With [MAXTAB, MINTAB] = PEAKSANDVALLEYS(V, DELTAPERC, X) the indices
%  No      in MAXTAB and MINTAB are replaced with the corresponding
%  No      X-values.
% 
%        A point is considered a maximum peak if it has a locally maximal
%        value, and was preceded (to the left) by a value lower by
%        DELTAPERC*(max(V)-min(V)). I.e. DELTAPERC percantage of the value range 
%        and is treated as noise and is ignored in the linear the extrema search 
%        IndRangeTabDown, IndRangeTabUp are index ranges leading bracketing a  valley (minimum)
%        jd and ju are vectors od length of IndRangeTabDown and IndRangeTabUp,
%        indexing IndRangeTabDown and IndRangeTabUp and giving a
%        restricted bracket around the minima based on smoothed slope information


%
%        Called by CTD Data (Depth vs. Ke, Depth vs. TRANS)
%        use: deltaperc ~ 6;




v = v(:); % Just in case this wasn't a proper vector
x = x(:);
if length(v)~= length(x)
   error('Input vectors v and x must have same length');
end

%if nargin < 3
%  x = (1:length(v))';
%else 
%  x = x(:);
%end
  
%deltaperc=2;


if (length(deltaperc(:)))>1
  error('Input argument DELTAPERC must be a scalar');
end

if deltaperc <= 0 | deltaperc >= 100
  error('Input argument DELTAPERC must be positive and less then and 100');
end

% 
% cd C:\myfiles\pisti\cuhel\LightcodeXie
% 
% FileRef1='010723FPCTD.csv';  
% FileRef2='060808FPCTD.csv' ;
% FileRef3='100608FPCTD.csv' ;
% FileRef4='020718FPCTD.csv'  ;
% FileRef5='060901FBCTD.csv'  ;
% FileRef6='110728FPCTD.csv'  ;
% FileRef7='050803FPCTD.csv'  ;
% FileRef8='060905FPCTD.csv' ;
% FileRef9='120605FPCTD.csv'  ;
% FileRef10='051018FPCTD.csv'  ;
% FileRef11='070627FPCTD.csv'  ;
% FileRef12='120711FPCTD.csv'  ;
% FileRef13='060711FPCTD.csv'  ;
% FileRef14='070722FPCTD.csv'  ;
% FileRef15='060801FPCTD.csv'  ;
% FileRef16='070806FSCTD.csv'  ;
% 
% %begin iterate on files
% for FN=1:16
%     eval(['FileRef=FileRef' num2str(FN) ';']);
%     %ReadCTDData=csvread('010723FPCTD.csv');
%     ReadCTDData=csvread(FileRef);
% 
%                 %Column 1 = depth       %CTDQTRM Col2
%                 %Column 2 = temperature       %CTDQTRM Col3
%                 %Column 3 = fluorescence       %CTDQTRM Col4
%                 %Column 4 = transmission       %CTDQTRM Col4
%                 %Column 5 = PAR       %CTDQTRM Col8
%                 %Column 6 = lnPAR       %CTDQTRM Col12
%                 %Column 7 = Ke
%                 %Column 8 = Interpolated Ke
%                 %Column 9 = 1% Depth (Piecewise)
%                 %Column 10 = 1% Depth (Linear)
%                 %Column 11 = Interpolated PAR
%                 %Column 12 = Smoothed PAR
%                 %Column 13 = Smoothed lnPAR
%                 %Column 14 = Smoothed Ke
%                 %Column 15 = Extrapolated Smoothed Ke
%                 %Column 16 = Smoothed 1% Depth (Piecewise)
%                 %Column 17 = Smoothed 1% Depth (Piecewise)
% 
%             %Depth lnPAR fluorescence  transmission temperature DO2 ExSmKe Ke IntKe
% CTDData=ReadCTDData(:,[1 13 3 4 2 2 15 7 8]);
% CTDData(:,6)=0*CTDData(:,6); %DO2 is not in here
% CTDData(:,[7 8 9])=-CTDData(:,[7 8 9]); %DO2 is not in here
%             
%             
% %plot(CTDData(:,1),CTDData(:,4))            
% 
% x=CTDData(:,1);
% v=CTDData(:,4);
% 
% v = v(:); % Just in case this wasn't a proper vector

NotNanInd=find( ~(isnan(v)|isnan(x)) );

v=v(NotNanInd); %ignore NaNs
x=x(NotNanInd);

for ll=1: length(v)
   if v(ll)>v(ll+1), lookformax=0; mnSearchStartInd=ll; break; end
   if v(ll)<v(ll+1), lookformax=1; mxSearchStartInd=ll; break; end
end
% 
% %for ll=1: length(v)
%    if mean(v(20)-v(1))<0, 
%        lookformax=0; 
%    else
%        lookformax=1;
%    end
% %end


delta=(max(v)-min(v))*.06; % delta 6%
delta=(max(v)-min(v))*deltaperc/100; % delta 6%

v1=(-v(1:end-1)+v(2:end))./(-x(1:end-1)+x(2:end)); %slope of v (not central diff)
%v1=[v1(1);v1];
v1=[v1;v1(end)];
Slope=v1;
smSlope=(v1(1:end-2)+v1(2:end-1)+v1(3:end))/3; %smoothed slope
smSlope=[smSlope(1);smSlope;smSlope(end)];     %smoothed slope
smSlope=Slope; %smoothed slope not used!
maxSlope=max(abs(smSlope(ceil(1+length(smSlope)/15):end)));

deltar=1;

maxtab = [];
mintab = [];
IndRangeTabUp=[];
IndRangeTabDown=[];
SlopeTabUp=[];
SlopeTabDown=[];
RunDownInd=[];
RunUpInd=[];
jd=[];ju=[];
IndRangeTabDown=[];             
IndRangeTabUp=[];

mn = Inf; mx = -Inf;
mnpos = NaN; mxpos = NaN;


%lookformax = 1; %searching for max (min) set above

lookformaxSlopeRangeBounds = [0 maxSlope/2]; %([min for all values, min for mean value]) set;
mnInd=1;
mxInd=1;

for i=1:length(v) %scan the vector left->right
  this = v(i); % fn value
  
  if this > mx, %track rise
      mx = this; mxpos = x(i);  mxInd=i;
  end
  if this < mn, %track drop
      mn = this; mnpos = x(i);  mnInd=i;
  end

  if lookformax
    if this < mx-delta %max found
         %record peak/rise info
         maxtab = [maxtab ; mxpos mx]; %record max position, value
         if mnInd<mxInd,  premxInd=mnInd; 
         else
            premxInd=mxSearchStartInd;
         end
         IndRangeTabUp=[IndRangeTabUp; [premxInd mxInd]]; %record range of rise
         %record Least slope, Ave.slope on rise:
         SlopeTabUp=[SlopeTabUp; [min(smSlope(premxInd:mxInd)) mean(smSlope(premxInd:mxInd))]]; 
         
         mn = this; mnpos = x(i);%switch to minsearch
         mnInd=i;
         mnSearchStartInd=mxInd;
         lookformax = 0;
    end  
  elseif lookformax==0
    if this > mn+delta %min found
         mintab = [mintab ; mnpos mn]; %record min position, value
         if mxInd<mnInd,  premnInd=mxInd; 
         else
            premnInd=mnSearchStartInd;
         end
         IndRangeTabDown=[IndRangeTabDown; [premnInd mnInd]]; %record range of drop
         %record Least slope, Ave slope on rise:
         SlopeTabDown=[SlopeTabDown; [max(smSlope(premnInd:mnInd)) mean(smSlope(premnInd:mnInd))]]; 
 
         mx = this; mxpos = x(i); %switch to maxsearch
         mxInd=i;
         mxSearchStartInd=mnInd;
         lookformax = 1;
    end
  end
end

%--
  %last check
  if lookformax
         %record peak/rise info
         maxtab = [maxtab ; mxpos mx]; %record max position, value
         if mnInd<mxInd,  premxInd=mnInd; 
         else
            premxInd=mxSearchStartInd;
         end
         IndRangeTabUp=[IndRangeTabUp; [premxInd mxInd]]; %record range of rise
         %record Least slope, Ave.slope on rise:
         SlopeTabUp=[SlopeTabUp; [min(smSlope(premxInd:mxInd)) mean(smSlope(premxInd:mxInd))]]; 
         
         mn = this; mnpos = x(i);%switch to minsearch
         if mnpos>mxpos
             mintab = [mintab ; mnpos mn]; %record min position, value
             IndRangeTabDown=[IndRangeTabDown; [mxInd i]]; %record range of drop
         end
         %mnInd=i;
         %mnSearchStartInd=mxInd;
         %lookformax = 0;
  elseif lookformax==0 %lookformin
         mintab = [mintab ; mnpos mn]; %record min position, value
         if mxInd<mnInd,  premnInd=mxInd; 
         else
            premnInd=mnSearchStartInd;
         end
         IndRangeTabDown=[IndRangeTabDown; [premnInd mnInd]]; %record range of drop
         %record Least slope, Ave slope on rise:
         SlopeTabDown=[SlopeTabDown; [max(smSlope(premnInd:mnInd)) mean(smSlope(premnInd:mnInd))]]; 
 
         mx = this; mxpos = x(i); %switch to maxsearch
         if mxpos>mnpos
             maxtab = [maxtab ; mxpos mx]; %record max position, value
             IndRangeTabUp=[IndRangeTabUp; [mnInd i]]; %record range of drop
         end
         %mxInd=i;
         %mxSearchStartInd=mnInd;
         %lookformax = 1;
  end



% figure(1)
% plot(x,v,'b')            
% hold on; 
% plot(mintab(:,1), mintab(:,2), 'g*');
% title(FileRef)
% for kk=1:size(mintab,1)
%     plotRangeDown=IndRangeTabDown(kk,1):IndRangeTabDown(kk,2);
%     plot(x(plotRangeDown),v(plotRangeDown),'m:')
% end
% plot(maxtab(:,1), maxtab(:,2), 'r*');
% for kk=1:size(maxtab,1)
%     plotRangeUp=IndRangeTabUp(kk,1):IndRangeTabUp(kk,2);
%     plot(x(plotRangeUp),v(plotRangeUp),'c:')
% end





%-------------------------
%depthFactor=exp(0.01*(x-min(x)/max(x)-min(x)));% not in this one yet!
depthFactor=Slope*0+1;
FactorSlope=Slope.*depthFactor;


 if prod([size(mintab) size(maxtab)])   %if normal mintab maxtab case
    if mintab(1,1)<maxtab(1,1) kkAdj=0; else kkAdj=1; end  % only for valeys
    if kkAdj==1 & mintab(end,1)>maxtab(end,1), skipLast=1; else skipLast=0; end 
    %starts with max and ends with min (skipLast=1)
    %starts with max and ends with max (skipLast=0)
    if kkAdj==0 & mintab(end,1)>maxtab(end,1), skipLast=1; else skipLast=0; end, 
    %starts with min and ends with min (skipLast=1)
    %starts with min and ends with max (skipLast=0)

        MinCenteredSlopeRunPAve=v1*0;   
        %for kk=1:min(size(mintab,1),size(maxtab,1))

        for kk=1:(size(mintab,1)-skipLast)

            if mintab(kk,1)==x(end), break; end
            RunUpInd=IndRangeTabUp(kk+kkAdj,1):IndRangeTabUp(kk+kkAdj,2);
            pu=3;
            %pu=ceil(length(RunUpInd)/3)+1; % pu: max slope averaging range (up) - range dependent
            for kkI=1:length(RunUpInd)
             if kkI<=pu  MinCenteredSlopeRunPAve(RunUpInd(kkI))=mean(FactorSlope(RunUpInd(1:kkI)));     end
             if kkI>pu   MinCenteredSlopeRunPAve(RunUpInd(kkI))=mean(FactorSlope(RunUpInd(kkI-(pu-1):kkI))); end

            end
            %jupre=find( FactorSlope(RunUpInd)>.7*max(FactorSlope(RunUpInd)) ); %find peak slope up
            jupre=find( MinCenteredSlopeRunPAve(RunUpInd)<-.1 ); 
            if prod(size(jupre))>0
               ju(kk)=jupre(1)-1; fullju(kk)=0; %index for peak slope up first occurance (closest to valey) - indexed relative to RunUpInd
            else
               ju(kk)=length(RunUpInd); fullju(kk)=1; %index for peak slope down first occurance (closest to valey) - indexed relative to RunDownInd
            end    

            RunDownInd=IndRangeTabDown(kk,2)-1:-1:IndRangeTabDown(kk,1);
            pd=3;
            %pd=ceil(length(RunDownInd)/3)+1; % pu: max slope averaging range (down) - range dependent

            for kkI=1:length(RunDownInd)
             if kkI<=pd  MinCenteredSlopeRunPAve(RunDownInd(kkI))=mean(FactorSlope(RunDownInd(1:kkI)));     end
             if kkI>pd   MinCenteredSlopeRunPAve(RunDownInd(kkI))=mean(FactorSlope(RunDownInd(kkI-(pd-1):kkI))); end
            end
            %jdpre=find( FactorSlope(RunDownInd)<0.7*min(FactorSlope(RunDownInd)) ); %find peak slope down
            jdpre=find( MinCenteredSlopeRunPAve(RunDownInd)>0.1);
            if prod(size(jdpre))>0
               jd(kk)=jdpre(1)-1; fulljd(kk)=0;%index for peak slope down first occurance (closest to valey) - indexed relative to RunDownInd
            else
               jd(kk)=length(RunDownInd); fulljd(kk)=1;%index for peak slope down first occurance (closest to valey) - indexed relative to RunDownInd
            end    


        %    figure(1)
        %    %plot(x,v,'k')            
        %    hold on; 
        %    %plot(mintab(:,1), mintab(:,2), 'g*');
        %    plot( x(fliplr(RunDownInd(jd(kk))):RunUpInd( ju(kk)) ) ,...
        %          v(fliplr(RunDownInd(jd(kk))):RunUpInd( ju(kk)) ) ,'k-','LineWidth',2)



                  %RunDownInd(jd(kk)):IndRangeTabDown(kk,2)
                  %IndRangeTabUp(kk,1):RunUpInd( ju(kk))
    % 
    %               XX1=[x(RunDownInd(jd(kk))) x(RunDownInd(jd(kk))) x(IndRangeTabDown(kk,2)) x(IndRangeTabDown(kk,2))];
    %               YL=ylim;
    %               YY1=[YL(2) YL(1) YL(1) YL(2) ];
    %               cdata1(1,1,1:3)=[1 1 1];
    %               cdata1(1,2,1:3)=[1 1 1];
    %               cdata1(1,3,1:3)=[0 0 1];
    %               cdata1(1,4,1:3)=[0 0 1];
    %               h1=patch(XX1,YY1,cdata1),
    %               set(h1, 'EdgeColor','none','facealpha',.4)
    %               
    %               XX2=[x(IndRangeTabUp(kk,1)) x(IndRangeTabUp(kk,1)) x(RunUpInd( ju(kk))) x(RunUpInd( ju(kk)))];
    %               YL=ylim;
    %               YY2=[YL(2) YL(1) YL(1) YL(2) ];
    %               cdata2(1,1,1:3)=[0 0 1];
    %               cdata2(1,2,1:3)=[0 0 1];
    %               cdata2(1,3,1:3)=[1 1 1];
    %               cdata2(1,4,1:3)=[1 1 1];
    %               h2=patch(XX2,YY2,cdata2),
    %               set(h2, 'EdgeColor','none','facealpha',.4)

         %      if  IndRangeTabDown(kk,2)> IndRangeTabUp(kk,1), jj=kk+1  
         %      else jj=kk 
         %      end
         %         mu=1;
         %         XX1=[x(max(1,IndRangeTabDown(kk,2)-4)) x(max(1,IndRangeTabDown(kk,2)-4)) x(IndRangeTabDown(kk,2)) x(IndRangeTabDown(kk,2))];
         %         YL=ylim;
         %         YY1=[YL(2) YL(1) YL(1) YL(2) ];
         %         cdata1(1,1,1:3)=[1 1 1];
         %         cdata1(1,2,1:3)=[1 1 1];
         %         cdata1(1,3,1:3)=[0 0 1*mu];
         %         cdata1(1,4,1:3)=[0 0 1*mu];
         %         h1=patch(XX1,YY1,cdata1),
         %         set(h1, 'EdgeColor','none','facealpha',.2)
         %        xlen=length(x);
         %         XX2=[x(IndRangeTabUp(jj,1)) x(IndRangeTabUp(jj,1)) x(min(xlen,IndRangeTabUp(jj,1)+4)) x(min(xlen,IndRangeTabUp(jj,1)+4))];
         %         YL=ylim;
         %         YY2=[YL(2) YL(1) YL(1) YL(2) ];
         %         cdata2(1,1,1:3)=[0 0 1*mu];
         %         cdata2(1,2,1:3)=[0 0 1*mu];
         %         cdata2(1,3,1:3)=[1 1 1];
         %         cdata2(1,4,1:3)=[1 1 1];
         %         h2=patch(XX2,YY2,cdata2),
         %         set(h2, 'EdgeColor','none','facealpha',.2)


          %  hold off



        end

 else maxtab=[]; mintab=[]; IndRangeTabDown=[]; jd=[]; IndRangeTabUp=[]; ju=[]; NotNanInd=[];  %if not normal mintab maxtab case
 end
 
    %end         

    %end iterate on files
    %end

%#########################
return
    
