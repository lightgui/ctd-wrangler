function [ CTDHeader, CTDData] = readCTDFile_cnv(filePath)
% Reads a CNV file containing CTD Data and returns a numeric array
% containing all numeric data in CTDData and a cell array containing all
% data headers in CTDData

CTDHeader = {};
CTDData = [];

% read the whole file to a temporary cell array
try
fid = fopen(filePath,'rt');
tmp = textscan(fid,'%s','Delimiter','\n');
fclose(fid);
tmp = tmp{1};

% # name 0 = prdM: Pressure, Strain Gauge [db]
% # name 1 = depFM: Depth [fresh water, m]
% # name 2 = t090C: Temperature [ITS-90, deg C]
% # name 3 = v4: Voltage 4
% # name 4 = CStarTr0: Beam Transmission, WET Labs C-Star [%]
% # name 5 = c0uS/cm: Conductivity [uS/cm]
% # name 6 = specc: Specific Conductance [uS/cm]
% # name 7 = par: PAR/Irradiance, Biospherical/Licor
% # name 8 = oxsolMg/L: Oxygen Saturation, Garcia & Gordon [mg/l]
% # name 9 = ph: pH
% # name 10 = orp: Oxidation Reduction Potential [mV]
% # name 11 = wl0: RS-232 WET Labs raw counts 0
% # name 12 = wl1: RS-232 WET Labs raw counts 1
% # name 13 = wl2: RS-232 WET Labs raw counts 2
% # name 14 = timeS: Time, Elapsed [seconds]
% # name 15 = flag:  0.000e+00

% read the header and parse out column names 
idx = cellfun(@(x) strncmp(x,'# name', 6), tmp);
[I, ~] = find(idx == 1);
tmpheader = tmp(I);
header = regexp(tmpheader, '\S*:', 'match');
header = cat(2,header{:});
for i=1:size(header,2)
    header{i}=header{i}(1:end-1);
end
CTDHeader = header;

numCols = size(CTDHeader,2); % note number of columns of data

% find number of total header lines from the index of *END* line
idx = cellfun(@(x) strcmpi(x,'*end*'), tmp);
numHeaderLines = find(idx == 1);

% read the whole file again
% this time, we know how many columns of values to read and can ignore
% header lines
fid = fopen(filePath,'rt');
tmp = textscan(fid,repmat('%f',1,numCols), 'headerlines', numHeaderLines, 'CollectOutput', 1); % tmp is one cell of doubles
fclose(fid);
data = tmp{1}; % access cell of doubles

CTDData = data;

% delete temporary array
clear tmp

catch errorObj
    errordlg(getReport(errorObj,'extended','hyperlinks','off'),'Error','modal');
end

