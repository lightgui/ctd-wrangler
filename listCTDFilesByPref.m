function [ fileList ] = listCTDFilesByPref( startDir, chosenStations, startDate, endDate, fileFormat, dataFormat )
% LISTSTATIONCTDFILES List all files found in a directory that match the
% given station names, date range, file format, and data format

%     OUTPUT: nx4 matrix
%     # name 1 = filename: name of file
%     # name 2 = filedir: containing folder path of file (combine with
%     column 1 to create full path)
%     # name 3 = stn: Station name (parse from file name and match to
%                full station name, use sets and ismember)
%     # name 4 = date: String start time (use start date field. If out
%                of range, use file name date)

% list all files in directory recursively
[fileDirs fileNames] = listAllFiles_dir_name(startDir);
fileList = [fileNames fileDirs];
if strcmpi(dataFormat, 'depth') == 1
    dataFormat = 'QTRZ';
elseif strcmpi(dataFormat, 'pressure') == 1
    dataFormat = 'QTRDBN2';
end

i = 1;
while i <= size(fileList, 1)
    % eliminate files that do not match fileFormat
    % for non-XLS files, eliminate files that do not match dataFormat
    
    if strcmpi(fileList{i,1}(end-3:end),fileFormat) ~= 1 ||... % does not match file format
            (strcmpi(fileFormat, '.ASC') == 1 &&...
            isempty(strfind(lower(fileList{i,1}),lower(dataFormat)))) ||...% does not match asc data format
            (strcmpi(fileFormat, '.XLS') == 1 && isempty(strfind(lower(fileList{i,1}), 'ctdmod'))) % does not match xls data format
        fileList(i,:) = [];
    else
        i = i+1;
    end
end

% Don't want to run identify station twice so have to pare down list
% as much as possible, then run identify station for all files and save directly to
% second column
if ~isempty(fileList)
    stationColumn = cell(size(fileList,1),1); % pre-allocate memory for stations column
    for i = 1:size(fileList,1)
        % identify station for each file
        % have to call identifyCTDStation on full path
        stationColumn{i,1} = identifyCTDStation(fullfile(fileList{i,2},fileList{i,1}));
    end
    fileList = [fileList stationColumn];
    if sum(strcmpi(chosenStations, 'all stations')) < 1 % if all stations chosen, do not have to eliminate any files
        % eliminate files that do not match station name preferences
        i = 1;
        while i <= size(fileList,1)
            if sum(strcmpi(chosenStations, fileList{i,3})) < 1 % station is in 3rd column
                fileList(i,:) = [];
            else
                i = i+1;
            end
        end
    end
end

if ~isempty(fileList)
    % pre-allocate datestr and datenum columns
    dateColumn = cell(size(fileList,1),2);
    i = 1;
    
    % determine datestr and datenum for each file
    while i <= size(fileList, 1)
        % have to call identifyCTDDate on full path
        [dateColumn{i,1} dateColumn{i,2}] = identifyCTDDate(fullfile(fileList{i,2},fileList{i,1}));
        i = i+1;
    end
    
    % append station and date columns (datestr and datenum) to file list and sort list by date
    fileList = [fileList dateColumn];
    fileList = sortrows(fileList, 5); % sort by date
    
    % eliminate all files that are not within date range
    % eliminate all files that are before start date
    while ~isempty(fileList) && fileList{1,5} < datenum(startDate, 'mm-dd-yyyy')
        fileList(1,:) = [];
    end
    
    if ~isempty(fileList)
        % eliminate all files that are after end date
        while ~isempty(fileList) && fileList{end,5} > datenum(endDate, 'mm-dd-yyyy')
            fileList(end,:) = [];
        end
        
        if ~isempty(fileList)
            % delete unnecessary datenum column
            fileList = fileList(:,1:4);
            fileList
        end
    end
end
end
