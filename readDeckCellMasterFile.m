%% Reads in a deck cell master text file and returns its control data, datenum data, datestr data, and light intensity data
function[timeCol_datenum timeCol_datestr lightCol] = readDeckCellMasterFile(filePath)
    fid = fopen(filePath,'r');
    cellText = textscan(fid,'%s %s','Delimiter','\t');
    fclose(fid);

    timeCol = cellText{1};
    lightCol = cellText{2};

    % check if data looks like deck cell data
    isBadFile = 0;
    if isempty(timeCol) || isempty(lightCol)
        isBadFile = 1;
    elseif ~strcmpi(timeCol{1},'Date(yyyy-mm-dd HH:MM:SS.FFF)') || ~strcmpi(lightCol{1},'Intensity')
        isBadFile = 1;
    end

    if isBadFile
        timeCol_datenum = [];
        timeCol_datestr = {};
        lightCol = [];
        return;
    else
        % delete header row
        timeCol(1) = [];
        lightCol(1) = [];

        % convert date column strings into dates
        timeCol_datenum = datenum(timeCol,'yyyy-mm-dd HH:MM:SS'); % save timestamp in datenum form for comparison purposes
        timeCol_datestr = cellstr(datestr(timeCol_datenum,'yyyy-mm-dd HH:MM:SS.FFF')); % convert datenum to datestr (millisecond precision)

        % convert light date column into double
        lightCol = str2double(lightCol);
    end
end
