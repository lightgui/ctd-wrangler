startDir = 'C:\Users\yangxie\Documents\LMPAR2.0\CTDDATAorig';
% startDir = 'C:\Users\yangxie\Documents\LMPAR2.0';
startDate = '01-01-1994';
endDate = '04-01-1995';
fileFormat = '.ASC';
dataFormat = 'Depth';
chosenStations = {'Fox Point';
    'Linnwood 10';
    'Linnwood 20';
    'Linnwood 30';
    'Linnwood 40';
    'Linnwood 50'};
[fileList] = listCTDFilesByPref(startDir, chosenStations, startDate, endDate, fileFormat, dataFormat)