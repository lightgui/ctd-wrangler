function  DataMatrix = LightCodeImport_xls(FileDirectory,FileName)
        %FileDirectory,
        %FileName
        Dir=pwd;
        %cd FileDirectory;

        %ismac=0;
        %FileNameMatrix = ls
        %%Read file
        %for l = 1:size(FileNameMatrix,1)
        %FileName = strtrim(FileNameMatrix(l,:))
            
            %if length(FileName)>4
            %if strcmp(FileName(end-3:end), '.xls') | strcmp(FileName(end-3:end), '.XLS')
                %% Generate Data Matrix
                %Temp = xlsread([FileDirectory FileName] , 'CTDQTRM');
                
%                 xlsfinfo Determine if file contains Microsoft Excel spreadsheet.
%                 STATUS = xlsfinfo(FILENAME) returns the string 'Microsoft Excel
%                 Spreadsheet' if the specified file is in a format that XLSREAD can
%                 read.  Otherwise, STATUS is an empty string, ''.
% 
%                 [STATUS,SHEETS] = xlsfinfo(FILENAME) returns a cell array of strings
%                 containing the names of each spreadsheet in the file. If XLSREAD cannot
%                 read a particular worksheet, the corresponding cell contains an error
%                 message. If xlsfinfo cannot read the file, SHEETS is a string
%                 containing an error message.
% 
%                 [STATUS,SHEETS,FORMAT] = xlsfinfo(FILENAME) returns the format
%                 description that Excel returns for the file.  On systems without Excel
%                 for Windows, FORMAT is an empty string, ''.
% 
%                 Specific Excel formats include, but are not limited to:
%                    'xlWorkbookNormal', 'xlHtml', 'xlXMLSpreadsheet', 'xlCSV'

                
                
           [xlSTATUS,xlSHEETS,xlFORMAT] = xlsfinfo([FileDirectory FileName]);
           sheetValid = any(strcmp(xlSHEETS, 'CTDQTRM'));
                
           if  isempty(xlFORMAT), errordlg('Excel for Windows is not available on this system','Could not read file');
           elseif ~sheetValid,  errordlg('Selected file does not have a CTDQTRM Tab','File Error - Read unsuccessful');
                
           elseif sheetValid,
                [TempNum,TempTxt,TempRaw] = xlsread([FileDirectory FileName], 'CTDQTRM');
                DataMatrix = [TempNum(11-1:size(TempNum,1),1:4)...
                              TempNum(11-1:size(TempNum,1),7)...
                              TempNum(11-1:size(TempNum,1),11)];
                DataMatrix(:,end)=log(DataMatrix(:,end-1));
                %Column 1 = depth       
                %Column 2 = temperature  
                %Column 3 = fluorescence 
                %Column 4 = transmission 
                %Column 5 = PAR   
                %Column 6 = lnPAR  ( if exists)               
 
                %return
                %pause
                
                %Put relevant data from excel file into DataMatrix
                %Column 1 = depth       %CTDQTRM Col2
                %Column 2 = temperature       %CTDQTRM Col3
                %Column 3 = fluorescence       %CTDQTRM Col4
                %Column 4 = transmission       %CTDQTRM Col4
                %Column 5 = PAR       %CTDQTRM Col8
                %Column 6 = lnPAR       %CTDQTRM Col12
                %Column 7 = Ke
                %Column 8 = Interpolated Ke
                %Column 9 = 1% Depth (Piecewise)
                %Column 10 = 1% Depth (Linear)
                %Column 11 = Interpolated PAR
                %Column 12 = Smoothed PAR
                %Column 13 = Smoothed lnPAR
                %Column 14 = Smoothed Ke
                %Column 15 = Extrapolated Smoothed Ke
                %Column 16 = Smoothed 1% Depth (Piecewise)
                %Column 17 = Smoothed 1% Depth (Piecewise)
                
         %DataMatrix = [Temp(11:size(Temp,1),2:5) Temp(11:size(Temp,1),8) Temp(11:size(Temp,1),12)];
                %if size(Temp,1) < 289
                %    DataMatrix = [Temp(11:size(Temp,1),2:5) Temp(11:size(Temp,1),8) Temp(11:size(Temp,1),12)];
                %else
                %    DataMatrix = [Temp(11:289,2:5) Temp(11:289,8) Temp(11:289,12)];
                %end
                
                
                
                %% Calculate Ke values
                DataMatrix(isnan(DataMatrix)) = 0;
                KeMatrix = zeros(size(DataMatrix,1),1);
                
                %Calculate three point moving average slopes of lnPAR data
                for i = 2:size(DataMatrix,1)-1
                    if DataMatrix(i-1,6) ~= 0 && DataMatrix(i,6) ~= 0 && DataMatrix(i+1,6) ~= 0
                        Ke1 = (DataMatrix(i,6) - DataMatrix(i-1,6))/(DataMatrix(i,1) - DataMatrix(i-1,1));
                        Ke2 = (DataMatrix(i+1,6) - DataMatrix(i,6))/(DataMatrix(i+1,1) - DataMatrix(i,1));
                        Ke = (Ke1 + Ke2)/2;
                        if abs(Ke) < 0.0000001
                            Ke = 0;
                        end
                        KeMatrix(i,1) = Ke;
                    end
                end
                
                %KeMatrix;
                
                KeMatrix(KeMatrix == 0) = NaN;
                
                DataMatrix(DataMatrix == 0) = NaN;
                
                %Column 7 = Ke
                DataMatrix = [DataMatrix(:,:) KeMatrix(:,:)];
                
                %% Interpolate Ke column
                %Replace all NaN Ke values in data matrix with interpolated values
                %For first few rows, replace NaN with first actual number
                for i = 1:size(KeMatrix,1)
                    if ~isnan(KeMatrix(i,1))
                        if i>1
                            KeMatrix(1:i-1,:) = KeMatrix(i,:);
                            break;
                        else
                            break;
                        end
                    end
                end
                
                %If last row/s is/are NaN, replace with last actual number
                decIndex = size(KeMatrix,1);
                while decIndex >= 1
                    if ~isnan(KeMatrix(decIndex,1))
                        if decIndex < size(KeMatrix,1)
                            KeMatrix(decIndex+1:size(KeMatrix,1),:) = KeMatrix(decIndex);
                            break;
                        else
                            break;
                        end
                    end
                    decIndex = decIndex-1;
                end
                
                %KeMatrix
                               
                %For middle rows, replace NaN with interpolation
                %between bookends
                
                % I always need to know which elements are NaN,
                % and what size the array is for any method
                keSize = size(KeMatrix, 1);
                indNAN = isnan(KeMatrix(:));

                % list the nodes which are known, and which will
                % be interpolated
                nanList = find(indNAN);
                knownList = find(~indNAN);

                workList = unique([nanList;nanList - 1;nanList + 1]);
                workList(workList <= 1) = [];
                workList(workList >= keSize) = [];
                
                flagMatrix = [workList ismember(workList, nanList)];
                
                flagSize = size(flagMatrix,1);
                
                %Create zero matrix to hold bookends in two columns
                %Column 1 is starting point
                %Column 2 is ending point
                bookendMatrix = zeros(size(nanList,1),3);
                bookIndex = 1;
                
                for i=1:flagSize-1
                    if flagMatrix(i,2) == 0 && flagMatrix(i+1,2) == 1
                        startpt = flagMatrix(i,1);
                        bookendMatrix(bookIndex,1) = startpt;
                        i = i + 3;
                    elseif flagMatrix(i,2) == 1 && flagMatrix(i+1,2) == 0
                            endpt = flagMatrix(i+1,1);
                            bookendMatrix(bookIndex,2) = endpt;
                            bookIndex = bookIndex + 1;
                    end
                end
                
                bookendMatrix;
                lastRow = size(bookendMatrix,1);
                
                for i=1:size(bookendMatrix,1)
                    if bookendMatrix(i,1) == 0
                        lastRow = i-1;
                        break;
                    end
                end
                
                for i=1:lastRow            
                    ind1 = bookendMatrix(i,1);
                    ind2 = bookendMatrix(i,2);
                    slope = (KeMatrix(ind2) - KeMatrix(ind1))/(DataMatrix(ind2,1)-DataMatrix(ind1,1));
                    bookendMatrix(i,3) = slope;
                    for j=ind1+1:ind2-1
                        ke = slope*(DataMatrix(j,1)-DataMatrix(ind1,1))+KeMatrix(ind1);
                        KeMatrix(j,1) = ke;
                    end                        
                end
                
                %bookendMatrix;
                %KeMatrix;

                %Column 8 = Interpolated Ke
                DataMatrix = [DataMatrix(:,:) KeMatrix(:,:)];
                
                
                %% Calculate 1% Depth
 
                %Generate a triangular 0/1 matrix %upper triangular
                
                TriangleMatrix = zeros(size(KeMatrix,1),size(KeMatrix,1));
                for i = 1:size(KeMatrix,1)
                    for j = 1:i
                        TriangleMatrix(j,i) = 1;
                    end
                end
                
                %TriangleMatrix;
                
                
                
                %Calculate 1% depth from extinction coefficients
                %(piecewise)
                temp1 = (KeMatrix * 0.25)';
                
                logMatrix1 = temp1 * TriangleMatrix;
                
                DiffMatrix = logMatrix1 - ones(1,size(KeMatrix,1)) * log(0.01);
                
                index1 = max(find(DiffMatrix > 0));
                index2 = min(find(DiffMatrix < 0));
                
                
                if DiffMatrix(1,size(DiffMatrix,2)) > 0
                    depth1p = nan;
                    depth2p = nan;
                else
                    depth1p = DataMatrix(index1,1);
                    depth2p = DataMatrix(index2,1);
                end
                
                
                
                %Calculate 1% depth (linear)
                %Calculate the first rectangular area between 0m and 0.25m
                firstArea = KeMatrix(1,1) * 0.25;
                
                %Calculate subsequent trapezoidal areas 
                subMatrix = (KeMatrix(1:size(KeMatrix,1)-1,1)...
                    + KeMatrix(2:size(KeMatrix,1),1))' * 0.125;
                
                %First area and trapezoidal areas combined
                temp2 = [firstArea subMatrix(1,:)];
                
                %Matrix of accumulated summed areas
                logMatrix2 = temp2 * TriangleMatrix;
                
                DiffMatrix = logMatrix2 - ones(1,size(KeMatrix,1)) * log(0.01);
                
                index1 = max(find(DiffMatrix > 0));
                index2 = min(find(DiffMatrix < 0));
                
                if DiffMatrix(1,size(DiffMatrix,2)) > 0
                    depth1l = nan;
                    depth2l = nan;
                else
                    depth1l = DataMatrix(index1,1);
                    depth2l = DataMatrix(index2,1);
                end
                
                depthMatrix = zeros(size(DataMatrix,1),2);
                depthMatrix(1,1) = depth1p;
                depthMatrix(2,1) = depth2p;
                depthMatrix(1,2) = depth1l;
                depthMatrix(2,2) = depth2l;
                
                %Column 9 = 1% Depth (Piecewise) %Riemann sum integration approximation
                %Column 10 = 1% Depth (Linear) %trapezoidal approximation
                DataMatrix = [DataMatrix(:,:) depthMatrix(:,:)];
                
                %plot(DataMatrix(:,1),temp1(:,:),'b',DataMatrix(:,1),temp2(:,:),'r')
                
                %% Interpolate PAR column
                % Place values into column #11 of DataMatrix               
                
                parMatrix = DataMatrix(:,5);
                
                startRow = min(find(~isnan(parMatrix)));
                
                %If last row/s is/are NaN, replace with last actual number
                decIndex = size(parMatrix,1);
                while decIndex >= 1
                    if ~isnan(parMatrix(decIndex,1))
                        if decIndex < size(parMatrix,1)
                            parMatrix(decIndex+1:size(parMatrix,1),:) = parMatrix(decIndex);
                            break;
                        else
                            break;
                        end
                    end
                    decIndex = decIndex-1;
                end
                
                
                %Replace NaN with extrapolation between bookends
                
                parSize = size(parMatrix,1);
                indNAN = isnan(parMatrix)   ;                            

                % list the nodes which are known, and which will
                % be interpolated
                nanList = find(indNAN);
                knownList = find(~indNAN);

                workList = unique([nanList;nanList - 1;nanList + 1]);
                workList(workList < 1) = [];
                workList(workList > parSize) = [];
                
                flagMatrix = [workList ismember(workList, nanList)];
                
                flagSize = size(flagMatrix,1);
                
                %Create zero matrix to hold bookends in two columns
                %Column 1 is starting point
                %Column 2 is ending point
                bookendMatrix = zeros(size(nanList,1),3);
                bookIndex = 1;
                
                for i=1:flagSize-1
                    if flagMatrix(i,2) == 0 && flagMatrix(i+1,2) == 1
                        startpt = flagMatrix(i,1);
                        bookendMatrix(bookIndex,1) = startpt;
                        i = i + 3;
                    elseif flagMatrix(i,2) == 1 && flagMatrix(i+1,2) == 0
                            endpt = flagMatrix(i+1,1);
                            bookendMatrix(bookIndex,2) = endpt;
                            bookIndex = bookIndex + 1;
                    end
                end
                
                %bookendMatrix;
                lastRow = size(bookendMatrix,1);
                
                for i=2:size(bookendMatrix,1)
                    if bookendMatrix(i,1) == 0
                        lastRow = i-1;
                        break;
                    end
                end
                
                for i=2:lastRow            
                    ind1 = bookendMatrix(i,1);
                    ind2 = bookendMatrix(i,2);
                    slope = (parMatrix(ind2) - parMatrix(ind1))/(DataMatrix(ind2,1)-DataMatrix(ind1,1));
                    bookendMatrix(i,3) = slope;
                    for j=ind1+1:ind2-1
                        par = slope*(DataMatrix(j,1)-DataMatrix(ind1,1))+parMatrix(ind1);
                        parMatrix(j,1) = par;
                    end                        
                end
                
                %bookendMatrix;
                %parMatrix;
                
                %Column 11 = 1% Interpolated PAR
                DataMatrix = [DataMatrix(:,:) parMatrix(:,:)];                
                
                
                %% Calculate five point moving averages of PAR
                % Place new values into column #12 of DataMatrix
                
                smparMatrix = parMatrix(:,1);
                
                for i = startRow+2:size(DataMatrix,1)-2
                    if parMatrix(i-2,1) ~= 0 && parMatrix(i-1,1) ~= 0 && parMatrix(i,1) ~= 0 &&...
                        parMatrix(i+1,1) ~= 0 && parMatrix(i+2,1) ~= 0
                        average = (parMatrix(i-2,1)+parMatrix(i-1,1)+parMatrix(i,1)+...
                            parMatrix(i+1,1)+parMatrix(i+2,1))/5;
                        smparMatrix(i,1) = average;
                    end
                end
                
                %smparMatrix;
                                
                %Column 12 = 1% Smoothed PAR
                DataMatrix = [DataMatrix(:,:) smparMatrix(:,:)];
                
                %plot(DataMatrix(:,1),DataMatrix(:,11),DataMatrix(:,1),DataMatrix(:,12))
                
                
                %% Calculate natural log of smoothed PAR values
                % Insert values into column #13 of DataMatrix
                
                lnparMatrix = log(smparMatrix(:,1));
                
                DataMatrix = [DataMatrix(:,:) lnparMatrix(:,:)];
                
                
                %% Calculate smoothed Ke values
                % Insert values into column #14 of DataMatrix
                
                smKeMatrix = zeros(size(DataMatrix,1),1);
                
                %Calculate three point moving average slopes of smoothed lnPAR data
                for i = 2:size(DataMatrix,1)-1
                    if DataMatrix(i-1,13) ~= 0 && DataMatrix(i,13) ~= 0 && DataMatrix(i+1,13) ~= 0
                        Ke1 = (DataMatrix(i,13) - DataMatrix(i-1,13))/(DataMatrix(i,1) - DataMatrix(i-1,1));
                        Ke2 = (DataMatrix(i+1,13) - DataMatrix(i,13))/(DataMatrix(i+1,1) - DataMatrix(i,1));
                        Ke = (Ke1 + Ke2)/2;
                        if abs(Ke) < 0.0000001
                            Ke = 0;
                        end
                        smKeMatrix(i,1) = Ke;
                    end
                end
                
                %smKeMatrix
                
                smKeMatrix(smKeMatrix == 0) = NaN;
                
                %Column 14 % Smoothed Ke
                DataMatrix = [DataMatrix(:,:) smKeMatrix(:,:)];
                
                
                %% Extrapolate unfilled smoothed Ke values
                % Insert values into column #15 of DataMatrix
                
                %For first few rows, replace NaN with first actual number
                for i = 1:size(smKeMatrix,1)
                    if ~isnan(smKeMatrix(i,1))
                        if i > 1
                            smKeMatrix(1:i-1,1) = smKeMatrix(i,1);
                            break;
                        else
                            break;
                        end
                    end
                end
                
                %If last row/s is/are NaN, replace with last actual number
                decIndex = size(smKeMatrix,1);
                while decIndex >= 1
                    if ~isnan(smKeMatrix(decIndex,1))
                        if decIndex < size(smKeMatrix,1)
                            smKeMatrix(decIndex+1:size(smKeMatrix,1),:) = smKeMatrix(decIndex);
                            break;
                        else
                            break;
                        end
                    end
                    decIndex = decIndex-1;
                end
                
                %smKeMatrix;
                
                %Column 15 %    interpolate unfilled smoothed Ke values
                DataMatrix = [DataMatrix(:,:) smKeMatrix(:,:)];

                return
                
                %% Calculate 1% Depth from smoothed Ke
                 
                %Calculate 1% depth (piecewise)
                %Calculate rectangular areas for each Ke
                temp1 = (smKeMatrix * 0.25)';
                
                %Matrix of accumulated summed areas
                areaMatrix1 = temp1 * TriangleMatrix;
                
                diffMatrix1 = areaMatrix1 - ones(1,size(smKeMatrix,1)) * log(0.01);
                
                index1 = max(find(diffMatrix1 > 0));
                index2 = min(find(diffMatrix1 < 0));
                
                
                if diffMatrix1(1,size(diffMatrix1,2)) > 0
                    depth1p = nan;
                    depth2p = nan;
                else
                    depth1p = DataMatrix(index1,1);
                    depth2p = DataMatrix(index2,1);
                end               
                
                
                %Calculate 1% depth (linear)
                %Calculate the first rectangular area between 0m and 0.25m
                firstArea = smKeMatrix(1,1) * 0.25;
                
                %Calculate subsequent trapezoidal areas 
                subMatrix = (smKeMatrix(1:size(smKeMatrix,1)-1,1)...
                    + smKeMatrix(2:size(smKeMatrix,1),1))' * 0.125;
                
                %First area and trapezoidal areas combined
                temp2 = [firstArea subMatrix(1,:)];
                
                %Matrix of accumulated summed areas
                areaMatrix2 = temp2 * TriangleMatrix;
                
                diffMatrix2 = areaMatrix2 - ones(1,size(smKeMatrix,1)) * log(0.01);
                
                index1 = max(find(diffMatrix2 > 0));
                index2 = min(find(diffMatrix2 < 0));
                
                if diffMatrix2(1,size(diffMatrix2,2)) > 0
                    depth1l = nan;
                    depth2l = nan;
                else
                    depth1l = DataMatrix(index1,1);
                    depth2l = DataMatrix(index2,1);
                end
                
                depthMatrix2 = zeros(size(DataMatrix,1),2);
                depthMatrix2(1,1) = depth1p;
                depthMatrix2(2,1) = depth2p;
                depthMatrix2(1,2) = depth1l;
                depthMatrix2(2,2) = depth2l;
                
                %Column 16 17 %    interpolate unfilled smoothed Ke values
                DataMatrix = [DataMatrix(:,:) depthMatrix2(:,:)];
                
                
                  %% Write to CSV
                                
                  %Enter correct csv folder
                  %eval(['cd ' csvDir]);
                
                   %Export relevant data to csv file in csv folder
          %      if ~2==exist([FileName(1:end-4), '.csv'],'file'),
          %        csvwrite([FileName(1:end-4), '.csv'], DataMatrix);
          %      end
                   %Go back to directory for xls files
                   %eval(['cd ' currentDir]);
                
                
           
           
           end
        %    end 
        %end
        %eval(['cd ' Dir]);
        