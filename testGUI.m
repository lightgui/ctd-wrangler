figure(1)
main_axes = axes('units','normalized','position',[0.06 0.09 0.7 0.7], 'Tag', 'mainAxes');
[ax0,h0L,h0R] = plotyy(main_axes, 0,0,0,0);
set(ax0(1),'Xcolor','w','Ycolor','w','Ytick',[],'Xtick',[],'YAxisLocation','Left','visible','on','Tag','ax0L')
set(ax0(2),'Xcolor','w','Ycolor','w','Ytick',[],'Xtick',[],'YAxisLocation','Right','visible','on','Tag','ax0R')
set(h0L,'Color','w');
set(h0R,'Color','w');

[ax1,h1L,h1R] = plotyy(main_axes,1:3,[1 3 1],1:3,[3 2 1]);
set(ax1, 'color','none')
view(ax1(1),[45 91]);

% [ax2,h2L,h2R] = plotyy(main_axes,1:3,rand(3,1),1:3,rand(3,1));
% set(ax2, 'color','none')