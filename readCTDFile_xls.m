function [ CTDHeader1, CTDData1, CTDHeader2, CTDData2 ] = readCTDFile_xls(filePath)
% Read and store CTD Data and Column Headers from the CTDQTRM and CTDN2 tabs
% in a CTD XLS file. Store in 3-dimensional cell and numeric arrays, where
% the third dimension is the tab.

CTDHeader1 = {};
CTDHeader2 = {};
CTDData1 = [];
CTDData2 = [];
[CTDHeader1, CTDData1] = readCTDxls(filePath, 'ctdqtrm');
[CTDHeader2, CTDData2] = readCTDxls(filePath, 'ctdn2');
    
end

    function [headers data] = readCTDxls(filePath, tab)
        headers = {};
        data = [];
        % Read in ALL columns
        % TempNum is array of doubles
        % TempTxt is cell array containing text cells
        % TempRaw is cell array containing all unprocessed numeric and text data
        try
            [TempNum,TempTxt,TempRaw] = xlsread(filePath, tab);
            % Assume POST1900 is always a header in both QTRM and N2 tabs
            % Search for POST1900 as an indicator for the row containing the headers
            headerRow = 0;
            for row = 1:size(TempRaw,1)
                if strcmpi(TempRaw{row,1},'post1900')
                    headerRow = row;
                    break;
                end
            end
            if headerRow == 0 % don't continue if headerRow never found
                return;
            end
            
            headers = TempRaw(headerRow,:); % save cell array row containing headers
            
            % Assume that the row after the headers row is the beginning of data
            % First line of data is usually empty, so skip to next line
            data = TempRaw(headerRow+1+1:end,:); % save cell array row containing data
            
            % Delete any NaN or empty values from header cell, and the corresponding column
            % in data array
            col = 1;
            while col <= length(headers)
                if sum(isnan(headers{col})) || isempty(headers{col})
                    headers(col) = [];
                    data(:,col) = [];
                else
                    col = col + 1;
                end
            end
            
            % Some XLS files have non-numeric (i.e. #NUM!) in data cells, which causes an error in
            % cell2mat method
            % replace these cells with NaN values
            for col = 1:size(data,2)
                for row = 1:size(data,1)
                    if ~isnumeric(data{row,col})
                        data{row,col} = NaN;
                    end
                end
            end
            
            % Delete any extra rows of NaN or non-numeric values from data
            lastRow = size(data,1);
            for row = 1:size(data,1)
                if isnan(data{row,2}) % use depth values as indicators for nan cut-off
                    lastRow = row-1;
                    break;
                end
            end
            data = data(1:lastRow,:);

            data = cell2mat(data); % save numeric array containing data
        catch errorObj
            errordlg(getReport(errorObj,'extended','hyperlinks','off'),'XLS Read Error','modal');
        end
        
        % Sample Header:
        %'DEPTHm'    'T090C'    'FLUOR'    'XMISS'    'C0uS/cm'    'SPCOND'     'PAR'    'DO2mg'    'pH'    'ORP'    'lnPAR'
        
        % # name 0 = depFM: Depth [fresh water, m]
        % # name 1 = t090C: Temperature [ITS-90, deg C]
        % # name 2 = v4: Voltage 4
        % # name 3 = xmiss: Beam Transmission, Chelsea/Seatech/Wetlab CStar [%]
        % # name 4 = c0uS/cm: Conductivity [uS/cm]
        % # name 5 = specc: Specific Conductance [uS/cm]
        % # name 6 = par: PAR/Irradiance, Biospherical/Licor
        % # name 7 = oxsatMg/L: Oxygen Saturation [mg/l]
        % # name 8 = ph: pH
        % # name 9 = orp: Oxidation Reduction Potential [mV]
        % # name 10 = nbin: number of scans per bin
        % # name 11 = flag: flag
        
    end
