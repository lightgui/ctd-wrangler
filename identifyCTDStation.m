function [ stationNameResult ] = identifyCTDStation( fileFullPath, presetStation )
%identifyCTDStation(fileFullPath, presetStation) Identifies the station name of the CTD file from the file name
%   Compares the station abbreviation in file name to a list of station
%   name aliases

AllStations_alias = {'Fox Point', 'FP', 'X', 'X';...
    'Main Gap', 'MGAP', 'X', 'X';...
    'Linnwood 10', 'LW10', 'X', 'X';...
    'Linnwood 20', 'LW20', 'LW', 'X';...
    'Linnwood 30', 'LW30', 'X', 'X';...
    'Linnwood 40', 'LW40', 'X', 'X';...
    'Linnwood 50', 'LW50', 'X', 'X';...
    'Harbor N', 'ON', 'X', 'X';...
    'Harbor S', 'OS', 'X', 'X';...
    'Junction', 'JC', 'JT', 'X';...
    'Prins Willem', 'PW', 'X', 'X';...
    'Green Can 6m', 'GC06', 'X', 'X';...
    'Green Can S', 'GCS', 'GC10', 'GS';...
    'Green Can Deep', 'GCD', 'GC20', 'GD';...
    'Green Can 30', 'GC30', 'X', 'X';...
    'Green Can 40', 'GC40', 'X', 'X';...
    'Green Can 50', 'GC50', 'X', 'X';...
    'East Reef Peak', 'ER', 'X', 'X';...
    'Sheb Reef Peak', 'SR', 'X', 'X';...
    'NE Reef Peak', 'NE', 'MR', 'X'};

% determine alias list used from the presetStation option
if nargin == 1
    stationAlias = AllStations_alias(:,:);
elseif nargin == 2
    % locate preset station in station alias list
    findStationName = strcmp(presetStation, AllStations_alias);
    [I,J] = find(findStationName == 1);
    % restrict station alias list to only one station
    stationAlias = AllStations_alias(I,:);
end

% Parse out file name from full path
bsIndices = strfind(fileFullPath,'\');
fileName = fileFullPath(bsIndices(end)+1:end);

% Station abbreviation parsed from file name
% If XLS file, name ends with CTDmod
if strcmpi(fileName(end-2:end),'xls')
    Station_endIndex = strfind(lower(fileName),lower('CTDmod'))-1;
else
    Station_endIndex = strfind(lower(fileName),lower('QT'))-1;
end
if strcmpi(fileName(1),'P') == 1
    Station_abbr = fileName(8:Station_endIndex);
else
    Station_abbr = fileName(7:Station_endIndex);
end

% Match station abbreviation to known station aliases
StationIDResult = strcmpi(Station_abbr, stationAlias);
[I,J] = find(StationIDResult == 1);
if ~isempty(I)
    stationNameResult = stationAlias{I,1};
elseif size(Station_abbr,2) > 2
    % if initial search fails, try comparing successively truncated
    % versions of station abbreviation
    length = size(Station_abbr,2)-1;
    while length >= 2
        StationIDResult = strcmpi(Station_abbr(1:length), stationAlias);
        [I,J] = find(StationIDResult == 1);
        % if search still fails, make result as 'Unknown'
        if isempty(I)
            stationNameResult = 'Unknown';
            length = length -1;
        else
            stationNameResult = stationAlias{I,1};
            break;
        end
    end
else
    stationNameResult = 'Unknown';
end

end
