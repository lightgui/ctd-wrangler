h = figure();
position = [10,100,90,20];
hContainer = h;  % can be any uipanel or figure handle
options = {'a','b','c'};
model = javax.swing.DefaultComboBoxModel(options);
[jCombo, mCombo] = javacomponent('javax.swing.JComboBox', position, hContainer);
set(mCombo,'Units','normalized','Position',[0.5 0.5 0.4 0.2]);
jCombo.setModel(model);
jCombo.setEditable(true);
set(jCombo, 'ActionPerformedCallback', @editForcedXMin_cb)