function [] = lightgui49()
% Authors:  Istvan Lauko, Yang Xie

enabledH = [];

%% Declare color variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Color pallette for variables, axes, and lines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Kenneth Kelly's maximally distinct 21 colors
% Must be displayed in order to be maximally distinct
black =     [0.0000    0.0000   0.0000]; %Black
vYellow =   [1.0000    0.7020   0.0000]; %Vivid Yellow
strPurple = [0.5020    0.2431   0.4588]; %Strong Purple
vOrange =   [1.0000    0.4078   0.0000]; %Vivid Orange
vlBlue =    [0.6510    0.7412   0.8431]; %Very Light Blue
vRed =      [0.7569    0.0000   0.1255]; %Vivid Red
grYellow =  [0.8078    0.6353   0.3843]; %Grayish Yellow
mGray =     [0.5059    0.4392   0.4000]; %Medium Gray
vGreen =    [0.0000    0.4902   0.2039]; %Vivid Green
sPPink =    [0.9647    0.4627   0.5569]; %Strong Purplish Pink
strBlue =   [0.0000    0.3255   0.5412]; %Strong Blue
strYPink =  [1.0000    0.4784   0.3608]; %Strong Yellowish Pink
strViolet = [0.3255    0.2157   0.4784]; %Strong Violet
vOYellow =  [1.0000    0.5569   0.0000]; %Vivid Orange Yellow
strPRed =   [0.7020    0.1569   0.3176]; %Strong Purplish Red
vGYellow =  [0.9569    0.7843   0.0000]; %Vivid Greenish Yellow
strRBrown = [0.4980    0.0941   0.0510]; %Strong Reddish Brown
vYGreen =   [0.5765    0.6667   0.0000]; %Vivid Yellowish Green
dYBrown =   [0.3490    0.2000   0.0824]; %Deep Yellowish Brown
vROrange =  [0.9451    0.2275   0.0745]; %Vivid Reddish Orange
dOGreen =   [0.1373    0.1725   0.0863]; %Dark Olive Green

%% Initialize default variable properties

% Store variables in a hierarchical structure
% Struct for individual variables contain helpful fields for plotting,
% identification, and data storage
Post1900 = struct(...
    'dispName', 'Post1900',...
    'exportName', 'Post1900',...
    'aliases', {{'post1900', 'post1990'}},...
    'unit', 'days',...
    'data', [],...
    'canPlot', 0,...
    'varType', 'X',...
    'color', [],...
    'lineStyle', 'none',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11d');

Depth = struct(...
    'dispName', 'Depth',...
    'exportName', 'DepthFM',...
    'aliases', {{'depfm', 'depthfm', 'depthm'}},...
    'unit', 'm',...
    'data', [],...
    'canPlot', 1,...
    'varType', 'X',...
    'color', [],...
    'lineStyle', 'none',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

PressureD = struct(...
    'dispName', 'PressureD',...
    'exportName', 'PrDM',...
    'aliases', {{'prdm'}},...
    'unit', 'db',...
    'data', [],...
    'canPlot', 1,...
    'varType', 'X',...
    'color', [],...
    'lineStyle', 'none',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

PressureS = struct(...
    'dispName', 'PressureS',...
    'exportName', 'PrSM',...
    'aliases', {{'prsm', 'press'}},...
    'unit', 'db',...
    'data', [],...
    'canPlot', 1,...
    'varType', 'X',...
    'color', [],...
    'lineStyle', 'none',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

Temp = struct(...
    'dispName', 'Temp',...
    'exportName', 'TempC',...
    'aliases', {{'t090c', 'tempc'}},...
    'unit', 'deg C',...
    'data', [],...
    'canPlot', 1,...
    'varType','Y',...
    'color', black,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.4f');

Fluor = struct(...
    'dispName', 'Fluor',...
    'exportName', 'Fluor',...
    'aliases', {{'v4', 'fluor', 'fls', 'fleco-afl', 'fluorv'}},...
    'unit', '??',...
    'data', [],...
    'canPlot', 1,...
    'varType','Y',...
    'color', strPurple,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.4f');

WL0 = struct(...
    'dispName', 'WL0',...
    'exportName', 'WL0',...
    'aliases', {{'wl0'}},...
    'unit', '??',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strPurple,...
    'lineStyle', 'none',...
    'marker', '+',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3e');

WL1 = struct(...
    'dispName', 'WL1',...
    'exportName', 'WL1',...
    'aliases', {{'wl1'}},...
    'unit', '??',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strPurple,...
    'lineStyle', 'none',...
    'marker', 'o',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3e');

WL2 = struct(...
    'dispName', 'WL2',...
    'exportName', 'WL2',...
    'aliases', {{'wl2'}},...
    'unit', '??',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strPurple,...
    'lineStyle', 'none',...
    'marker', '*',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3e');

Xmiss = struct(...
    'dispName', 'Xmiss',...
    'exportName', 'XMiss',...
    'aliases', {{'xmiss' 'cstartr0'}},...
    'unit', '%',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vOrange,...
    'lineStyle', 'none',...
    'marker', '.',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.4f');

Cond = struct(...
    'dispName', 'Cond',...
    'exportName', 'Cond',...
    'aliases', {{'cond', 'c0us/cm'}},...
    'unit', 'uS/cm',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vlBlue,...
    'lineStyle', '-',...
    'marker', '.',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.6f');

Specc = struct(...
    'dispName', 'Specc',...
    'exportName', 'Specc',...
    'aliases', {{'specc', 'spcond'}},...
    'unit', 'uS/cm',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', mGray,...
    'lineStyle', '-',...
    'marker', '.',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

PAR = struct(...
    'dispName', 'PAR',...
    'exportName', 'PAR',...
    'aliases', {{'par'}},...
    'unit', 'uE/m^2/sec',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vRed,...
    'lineStyle', 'none',...
    'marker', '+',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.4e');

PAR_adj = struct(...
    'dispName', 'PAR_adj',...
    'exportName', 'PAR_adj',...
    'aliases', {{'par_adj', 'paradj'}},...
    'unit', 'uE/m^2/sec',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vRed,...
    'lineStyle', 'none',...
    'marker', 'o',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.4e');

lnPAR = struct(...
    'dispName', 'lnPAR',...
    'exportName', 'lnPAR',...
    'aliases', {{'lnpar'}},...
    'unit', 'unitless',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vRed,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.6f');

lnPAR_adj = struct(...
    'dispName', 'lnPAR_adj',...
    'exportName', 'lnPAR_adj',...
    'aliases', {{'lnpar_adj', 'lnparadj'}},...
    'unit', 'unitless',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vRed,...
    'lineStyle', '--',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.6f');

Ke = struct(...
    'dispName', 'Ke',...
    'exportName', 'Ke',...
    'aliases', {{'ke'}},...
    'unit', 'unitless',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vGreen,...
    'lineStyle', 'none',...
    'marker', '+',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.6f');

Ke_adj = struct(...
    'dispName', 'Ke_adj',...
    'exportName', 'Ke_adj',...
    'aliases', {{'ke_adj', 'keadj'}},...
    'unit', 'unitless',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vGreen,...
    'lineStyle', 'none',...
    'marker', 'o',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.6f');

Oxsat = struct(...
    'dispName', 'Oxsat',...
    'exportName', 'OxSat',...
    'aliases', {{'oxsat', 'oxsatmg/l', 'do2mg', 'oxsolmg/l'}},...
    'unit', 'mg/L',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', sPPink,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.5f');

pH = struct(...
    'dispName', 'pH',...
    'exportName', 'pH',...
    'aliases', {{'ph'}},...
    'unit', 'unitless',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strBlue,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

ORP = struct(...
    'dispName', 'ORP',...
    'exportName', 'ORP',...
    'aliases', {{'orp'}},...
    'unit', 'mV',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strYPink,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.4f');

Nbin = struct(...
    'dispName', 'Nbin',...
    'exportName', 'NBin',...
    'aliases', {{'nbin'}},...
    'unit', 'bins',...
    'data', [],...
    'canPlot',0,...
    'varType', 'Y',...
    'color', [],...
    'lineStyle', 'none',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11d');

TimeS = struct(...
    'dispName', 'TimeS',...
    'exportName', 'TimeS',...
    'aliases', {{'times'}},...
    'unit', 'seconds',...
    'data', [],...
    'canPlot',1,...
    'color', [],...
    'varType', 'X',...
    'lineStyle', 'none',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

Flag = struct(...
    'dispName', 'Flag',...
    'exportName', 'Flag',...
    'aliases', {{'flag'}},...
    'unit', 'unitless',...
    'data', [],...
    'canPlot',0,...
    'varType', 'Y',...
    'color', [],...
    'lineStyle', 'none',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3e');

E = struct(...
    'dispName', 'E',...
    'exportName', 'E',...
    'aliases', {{'e'}},...
    'unit', '??',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strViolet,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3e');

E108 = struct(...
    'dispName', 'E108',...
    'exportName', 'E10^-8',...
    'aliases', {{'e10^-8'}},...
    'unit', '??',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vOYellow,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.1f');

N2 = struct(...
    'dispName', 'N2',...
    'exportName', 'N^2',...
    'aliases', {{'n^2'}},...
    'unit', '??',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strPRed,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3e');

N = struct(...
    'dispName', 'N',...
    'exportName', 'N',...
    'aliases', {{'n'}},...
    'unit', '??',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', vGYellow,...
    'lineStyle', '-',...
    'marker', 'none',...
    'lineWidth', 1.5,...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.2f');

DeckLight = struct(...
    'dispName', 'DeckLight',...
    'exportName', 'DeckLight',...
    'aliases', {{'decklight'}},...
    'unit', 'uE/m^2/sec',...
    'data', [],...
    'canPlot',1,...
    'varType','Y',...
    'color', strRBrown,...
    'lineStyle', 'none',...
    'marker', '.',...
    'lineWidth', [],...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.2f');

OPDep = struct(...
    'dispName', 'OPDep',...
    'exportName', 'OPDep',...
    'aliases', {{'opdep'}},...
    'unit', 'm',...
    'data', [],...
    'canPlot',1,...
    'varType','1%X',...
    'color', black,...
    'lineStyle', ':',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

OPDep_adj = struct(...
    'dispName', 'OPDep_adj',...
    'exportName', 'OPDep_adj',...
    'aliases', {{'opdep_adj','opdepadj'}},...
    'unit', 'm',...
    'data', [],...
    'canPlot',1,...
    'varType','1%X',...
    'color', black,...
    'lineStyle', '--',...
    'lineWidth', [],...
    'marker', 'none',...
    'markerSize', [],...
    'markerFaceColor',[],...
    'markerEdgeColor',[],...
    'exportFormat', '%11.3f');

% Cell array of copies of above variables, separated in Z and N2 groups
masterCTDVars.Z = struct('Depth',Depth, 'PressureD',PressureD, 'Temp',Temp, 'Fluor',Fluor, 'WL0',WL0,...
    'WL1',WL1, 'WL2',WL2, 'Xmiss',Xmiss, 'Cond',Cond,...
    'Specc',Specc, 'PAR',PAR, 'PAR_adj',PAR_adj, 'lnPAR',lnPAR,...
    'lnPAR_adj',lnPAR_adj, 'Ke',Ke, 'Ke_adj',Ke_adj, 'Oxsat',Oxsat,...
    'pH', pH, 'ORP',ORP, 'Post1900',Post1900, 'Nbin',Nbin, 'TimeS',TimeS,...
    'Flag',Flag, 'DeckLight',DeckLight, 'OPDep',OPDep, 'OPDep_adj',OPDep_adj);

masterCTDVars.N2 = struct('PressureS',PressureS, 'Depth',Depth, 'Temp',Temp, 'Fluor',Fluor,...
    'Xmiss',Xmiss, 'Specc',Specc, 'E',E, 'E108',E108,...
    'N2',N2, 'N',N, 'Post1900',Post1900, 'Flag',Flag);

% check for existence of MAT containing saved master var structures
% if it doesn't exist (first time running program), create the MAT file and
% save system default vars to it
SystemDefault = masterCTDVars;
if ~exist('masterCTDVarsGroups.mat','file')
    % save system default master vars to MAT
    masterCTDVarsGroups.SystemDefault = SystemDefault;
    save('masterCTDVarsGroups.mat','masterCTDVarsGroups');
    % add standard group (clone of system defaults) to MAT
    Standard = SystemDefault;
    masterCTDVarsGroups.Standard = Standard;
    save('masterCTDVarsGroups.mat','masterCTDVarsGroups');
    clear Standard
    clear SystemDefault
end
% defaultMasterCTDVars = masterCTDVars;

% check prefs for default master variable group
% if no prefs yet (first time running program), set default to 'Standard'
if ispref('lightgui','defaultMasterCTDVarsGroup')
    defaultMasterCTDVarsGroup = getpref('lightgui','defaultMasterCTDVarsGroup');
else
    defaultMasterCTDVarsGroup = 'Standard';
    setpref('lightgui','defaultMasterCTDVarsGroup',defaultMasterCTDVarsGroup);
end

% load all master variable groups from MAT
loadedStruct = load('masterCTDVarsGroups.mat');
% set master vars to default group
masterCTDVars = loadedStruct.masterCTDVarsGroups.(defaultMasterCTDVarsGroup);
clear loadedStruct

%% Other variables

% default variables displayed in variable selection panel when program is
% first initialized (max 20)
defaultCTDDataType = 'Z';

defaultCTDVarsZY = {'Temp', 'Fluor', 'WL0', 'WL1', 'WL2', 'Xmiss', 'Cond',...
    'Specc', 'PAR', 'PAR_adj', 'lnPAR', 'lnPAR_adj', 'Ke', 'Ke_adj', 'Oxsat',...
    'pH', 'ORP', 'DeckLight'};
defaultCTDVarsZX = {'Depth'};

defaultCTDVarsN2Y = {'Temp', 'Fluor', 'Xmiss', 'Specc', 'E', 'E108', 'N2', 'N'};
defaultCTDVarsN2X = {'PressureS'};

currCTDDataType = defaultCTDDataType;

% preferred variables are set by user in variable preferences
% these are persistent across multiple files, until the preferences are
% changed
% *changed by "set variables" button only

if ispref('lightgui','preferredCTDVarsZY')
    preferredCTDVarsZY = getpref('lightgui','preferredCTDVarsZY');
else
    preferredCTDVarsZY = defaultCTDVarsZY;
    setpref('lightgui','preferredCTDVarsZY',preferredCTDVarsZY);
end

if ispref('lightgui','preferredCTDVarsZX')
    preferredCTDVarsZX = getpref('lightgui','preferredCTDVarsZX');
else
    preferredCTDVarsZX = defaultCTDVarsZX;
    setpref('lightgui','preferredCTDVarsZX',preferredCTDVarsZX);
end

if ispref('lightgui','preferredCTDVarsN2Y')
    preferredCTDVarsN2Y = getpref('lightgui','preferredCTDVarsN2Y');
else
    preferredCTDVarsN2Y = defaultCTDVarsN2Y;
    setpref('lightgui','preferredCTDVarsN2Y',preferredCTDVarsN2Y);
end

if ispref('lightgui','preferredCTDVarsN2X')
    preferredCTDVarsN2X = getpref('lightgui','preferredCTDVarsN2X');
else
    preferredCTDVarsN2X = defaultCTDVarsN2X;
    setpref('lightgui','preferredCTDVarsN2X',preferredCTDVarsN2X);
end

% make sure all preferredCTDVars are found in the current master variable group
% delete inconsistencies by intersecting the two sets
preferredCTDVarsZY = intersect(preferredCTDVarsZY, fieldnames(masterCTDVars.Z));
preferredCTDVarsZX = intersect(preferredCTDVarsZX, fieldnames(masterCTDVars.Z));
preferredCTDVarsN2Y = intersect(preferredCTDVarsN2Y, fieldnames(masterCTDVars.N2));
preferredCTDVarsN2X = intersect(preferredCTDVarsN2X, fieldnames(masterCTDVars.N2));

% variables that are displayed in varSelectionPanel
% *always follows the corresponding preferredCTDVars, changes when the data
% type is changed only
currCTDVarsY = preferredCTDVarsZY;
currCTDVarsX = preferredCTDVarsZX;

% variables that are automatically plotted when the plot is initialized or
% refreshed
% *changed when the user toggles variables on/off (manually only, not when
% disabled) and when the currCTDVarsY are modified
% always a subset of preferredCTDVarsY
if ispref('lightgui','plottedCTDVarsZY')
    plottedCTDVarsZY = getpref('lightgui','plottedCTDVarsZY');
else
    plottedCTDVarsZY = preferredCTDVarsZY;
    setpref('lightgui','plottedCTDVarsZY',plottedCTDVarsZY);
end

if ispref('lightgui','plottedCTDVarsN2Y')
    plottedCTDVarsN2Y = getpref('lightgui','plottedCTDVarsN2Y');
else
    plottedCTDVarsN2Y = preferredCTDVarsN2Y;
    setpref('lightgui','plottedCTDVarsN2Y',plottedCTDVarsN2Y);
end

% axes that are visible when the plot if initialized or refreshed
% *changed when the user manually axes visibility via radio buttons
if ispref('lightgui','axLVarZ')
    axLVarZ = getpref('lightgui','axLVarZ');
else
    axLVarZ = plottedCTDVarsZY{1};
    setpref('lightgui','axLVarZ',axLVarZ);
end

if ispref('lightgui','axRVarZ')
    axRVarZ = getpref('lightgui','axRVarZ');
else
    axRVarZ = plottedCTDVarsZY{2};
    setpref('lightgui','axRVarZ',axRVarZ);
end

if ispref('lightgui','axLVarN2')
    axLVarN2 = getpref('lightgui','axLVarN2');
else
    axLVarN2 = plottedCTDVarsN2Y{1};
    setpref('lightgui','axLVarN2',axLVarN2);
end

if ispref('lightgui','axRVarN2')
    axRVarN2 = getpref('lightgui','axRVarN2');
else
    axRVarN2 = plottedCTDVarsN2Y{2};
    setpref('lightgui','axRVarN2',axRVarN2);
end

% create structure for forcedYLims with empty min and max
% values
% Get the plottable Y variables of the two data types
vnames = fieldnames(masterCTDVars.Z);
vnames_yz = cell(1,length(vnames));
for vnum = 1:length(vnames)
    vprops = masterCTDVars.Z.(vnames{vnum});
    if vprops.canPlot && strcmpi(vprops.varType, 'y')
        vnames_yz{vnum} = vnames{vnum};
    end
end
vnames_yz(cellfun('isempty',vnames_yz)) = [];
vnames = fieldnames(masterCTDVars.N2);
vnames_yn2 = cell(1,length(vnames));
for vnum = 1:length(vnames)
    vprops = masterCTDVars.N2.(vnames{vnum});
    if vprops.canPlot && strcmpi(vprops.varType, 'y')
        vnames_yn2{vnum} = vnames{vnum};
    end
end
vnames_yn2(cellfun('isempty',vnames_yn2)) = [];

for vnum = 1:length(vnames_yz)
    forcedYLims.Z.(vnames_yz{vnum}) = struct('FYMin',[],'FYMax',[]);
end
for vnum = 1:length(vnames_yn2)
    forcedYLims.N2.(vnames_yn2{vnum}) = struct('FYMin',[],'FYMax',[]);
end

%% Initialize main GUI figure properties

set(0,'Units','characters');
screenSize = get(0,'screensize');

% want main figure to start maximized
figHeight = screenSize(4) - 8; % 8 characters account for size of taskbar
figWidth = screenSize(3);

mainFig = figure ( 'windowstyle', 'normal', 'resize', 'on', ...
    'name','CTDWrangler', 'MenuBar','none', 'Toolbar','none', 'NumberTitle', 'off',...
    'Tag', 'mainFigure', 'units','characters', 'Position',[0 0 figWidth figHeight],...
    'KeyPressFcn',{@restoreFig}, 'CloseRequestFcn',@mainFigClose, 'visible','on');

waterIcon = 'water.png';
if exist(waterIcon,'file')
    javaFrame = get(mainFig,'JavaFrame');
    javaFrame.setFigureIcon(javax.swing.ImageIcon(which(waterIcon)));
end

% Set the color of the GUI to the default color of the OS (makes gui
% components blend in with window)
defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
set(mainFig,'Color',defaultBackgroundColor);


%% Initialize GUI components

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Major Panel Components in mainFig
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% eastern panel containing control components
controlPanel = uipanel('Parent',mainFig, 'Units','normalized', 'Position',[0.75 0 0.25 1],...
    'Visible','off', 'Tag','controlPanel');

controlPHidePanel = uipanel('Parent',mainFig, 'Units','normalized', 'Position',[0.738 0.08 0.012 0.82],...
    'BorderType','line', 'Visible','on', 'Tag','controlPHidePanel');

% western panel containing y or xlim components
westPanel = uipanel('Parent',mainFig, 'units','normalized', 'position',[0.0 0.08 0.1 0.82],...
    'Visible','off', 'Tag','westPanel');

westPHidePanel = uipanel('Parent',mainFig, 'Units','normalized', 'Position',[0.1 0.08 0.012 0.82],...
    'BorderType','line', 'Visible','off', 'Tag','controlPHidePanel');

% northern panel containing slider, timeline, file list switch buttons
sliderPanel = uipanel('Parent',mainFig, 'units','normalized',...
    'position',[0.1 0.9 0.65 0.1],...
    'Visible','off', 'Tag','sliderPanel');

sliderPHidePanel = uipanel('Parent',mainFig, 'Units','normalized', 'Position',[0.1 0.88 0.65 0.02],...
    'BorderType','line', 'Visible','off', 'Tag','sliderPHidePanel');

% southern panel containing y or xlim components
southPanel = uipanel('Parent',mainFig, 'units','normalized', 'position',[0.1 0.0 0.65 0.08],...
    'Visible','off', 'Tag','southPanel');

southPHidePanel = uipanel('Parent',mainFig, 'Units','normalized', 'Position',[0.1 0.08 0.65 0.02],...
    'BorderType','line', 'Visible','off', 'Tag','sliderPHidePanel');

% panel containing main plotting axes
axesPanel = uipanel('Parent', mainFig, 'units','normalized', 'position',[0.12 0.1 0.61 0.78],...
    'BorderType','none', 'Visible','off', 'Tag','axesPanel');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in axesPanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize empty main axes for plot
main_axes = axes('Parent',axesPanel,'units','normalized','position',[0.07 0.08 0.86 0.87], 'Tag', 'mainAxes');
[ax,h0_L,h0_R] = plotyy(main_axes, 0,0,0,0);
set(ax(1), 'Xcolor','k', 'Ycolor','k', 'Ytick',[], 'Xtick',[], 'YAxisLocation','Left',...
    'visible','on', 'Tag','ax0L');
set(ax(2), 'Xcolor','k', 'Ycolor','k', 'Ytick',[], 'Xtick',[], 'YAxisLocation','Right',...
    'visible','on', 'Tag','ax0R');
set(h0_L,'Color','w','Tag','h0_L');
set(h0_R,'Color','w','Tag','h0_R');

% Create an axis and line that can be overlayed with other axes
    function [] = createCTDVarAxis(yAxisLocation, axisTag, lineTag)
        % Color is 'none' so the axis is transparent and can be overlayed
        % Ycolor comes from pre-defined palette
        axisHandle = axes('Parent',axesPanel,'units','normalized','Position',get(ax(1),'Position'),...
            'XAxisLocation','bottom','YAxisLocation',yAxisLocation,'Color','none','XColor','w',...
            'XGrid','off','YGrid','off', 'Box','off',...
            'Ycolor','w','Visible','off',...
            'Tag', axisTag);
        hold(axisHandle,'on')
        set(get(axisHandle,'Ylabel'),'String','','FontWeight', 'Bold')
        set(get(axisHandle,'Xlabel'),'String','','FontWeight', 'Bold')
        hold(axisHandle,'off')
        line(0,0, 'Color', 'w', 'LineStyle', '-', 'Marker', '.', 'Parent',axisHandle,....
            'Tag',lineTag,'Visible','off');
    end

MAX_YVARS = 20;
for j = 1:MAX_YVARS
    num = num2str(j);
    axTagL = strcat('ax',num,'L');
    axTagR = strcat('ax',num,'R');
    lineTagL = strcat('h',num,'_L');
    lineTagR = strcat('h',num,'_R');
    createCTDVarAxis('left', axTagL, lineTagL);
    createCTDVarAxis('right', axTagR, lineTagR);
end

% create axes and stems for plotting vertical lines of 1% depth
axOPD = axes('Parent',axesPanel, 'units','normalized', 'Position',get(ax(1),'Position'),...
    'Tag','axOPD');
stemOPD = stem([0 0], [0 0;0 0], 'Parent',axOPD, 'Color','black', 'LineStyle',':', 'Marker','none',....
    'Visible','off');
baseline = get(stemOPD(1),'BaseLine');
set(baseline, 'Visible','off');
baseline = get(stemOPD(2),'BaseLine');
set(baseline, 'Visible','off');
parentAx = get(stemOPD(1),'Parent');
set(parentAx, 'Visible','off', 'Tag','axOPD', 'YLim',[0 1], 'Color','none');
set(stemOPD(1), 'LineStyle',OPDep.lineStyle, 'Tag','stemOPD');
set(stemOPD(2), 'LineStyle',OPDep_adj.lineStyle, 'Tag','stemOPD_adj');

set(axesPanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in sliderPanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Slider/Timeline Component
%--------------------------------------------------------------------------
axPos = [0.12 0.4 0.84 0.2];
sliderPos = [0.1 0.6 0.88 0.2];

% default empty timeline
sliderAxDefault = axes('Parent',sliderPanel, 'units','normalized', 'Position',axPos,...
    'fontunits','normalized', 'fontsize',1.9,...
    'Color','none','YTickLabel',[], 'YTick',[],...
    'Visible','on', 'Tag','sliderAxDefault');

line(1:6, 0*(1:6), 'Parent',sliderAxDefault, 'LineStyle','none', 'Tag','sliderLineDefault');

set(sliderAxDefault,'XTick',1:6, 'XTickLabel',[]); % set linearly spaced xticks (unlabeled)

% axis displaying chronological ordering of files
% make vertical size really small so it looks like a line and not a box
sliderAxChrono = axes('Parent',sliderPanel, 'units','normalized', 'Position',axPos,...
    'Ycolor',[0.94 0.94 0.94], 'fontunits','normalized', 'fontsize',0.8,...
    'Color','none','YTickLabel',[], 'YTick',[],...
    'Visible','off', 'Tag','sliderAxChrono');

line(0,0,'Parent',sliderAxChrono, 'LineStyle','none',...
    'Marker','.', 'Color','r', 'Visible','off', 'Tag','sliderLineChrono_red');

line(0,0,'Parent',sliderAxChrono, 'LineStyle','none',...
    'Marker','.', 'Color','b', 'Visible','off', 'Tag','sliderLineChrono_blue');

% Slider for navigating chronological file list
uicontrol('BackgroundColor' , [0.991569 0.993725 0.9912],'Parent',sliderPanel,...
    'Style','slider', 'Value',1, 'Min',1,'units', 'normalized',...
    'Max',6, 'SliderStep',[1 2], ...
    'Position',sliderPos, 'Visible','off', 'Tag','sliderChrono',...
    'Callback',{@slider_callback});

% -----------------------------------------------
% axis displaying sequential ordering of files
sliderAxSequen = axes('Parent',sliderPanel, 'units','normalized', 'Position',axPos,...
    'Ycolor',[0.94 0.94 0.94], 'fontunits','normalized', 'fontsize',0.8,...
    'Color','none','YTickLabel',[], 'YTick',[],...
    'Visible','off', 'Tag','sliderAxSequen');

line(0,0,'Parent',sliderAxSequen, 'LineStyle','none',...
    'Marker','.', 'Color','r', 'Visible','off', 'Tag','sliderLineSequen_red');

line(0,0,'Parent',sliderAxSequen, 'LineStyle','none',...
    'Marker','.', 'Color','b', 'Visible','off', 'Tag','sliderLineSequen_blue');

% Slider for navigating sequential file list
uicontrol('BackgroundColor' , [0.991569 0.993725 0.9912],'Parent',sliderPanel,...
    'Style','slider', 'Value',1, 'Min',1,'units', 'normalized',...
    'Max',1, 'SliderStep',[1 2], ...
    'Position',sliderPos, 'Visible','off', 'Tag','sliderSequen',...
    'Callback',@slider_callback);

% Timeline/File List Toggle Buttons
% -------------------------------------------------------------------------

% Toggle button group to switch between chronological and ordered list view of
% file list
flistViewMode = uibuttongroup('Parent',sliderPanel, 'Units','normalized', 'Position',[0.01 0.1 0.08 0.9],...
    'Title','List Mode', 'TitlePosition','centertop', 'BorderType','etchedin', 'Tag','flistViewMode',...
    'SelectionChangeFcn',{@switchFlistViewMode}, 'Visible','off');

% colormap for clock image
TimeChoice = [ ...
    3 3 3 3 3 3 3 3 3 3 3 3 3 3 3;
    3 3 3 3 3 1 1 1 1 1 3 3 3 3 3;
    3 3 3 1 1 1 2 2 2 1 1 1 3 3 3;
    3 3 1 1 2 2 2 2 2 2 2 1 1 3 3;
    3 3 1 2 2 2 2 2 2 1 2 2 1 3 3;
    3 1 1 2 2 2 2 2 1 2 2 2 1 1 3;
    3 1 2 2 2 2 2 1 2 2 2 2 2 1 3;
    3 1 2 2 2 2 2 1 2 2 2 2 2 1 3;
    3 1 1 2 2 2 2 1 2 2 2 2 1 1 3;
    3 3 1 2 2 2 2 1 2 2 2 2 1 3 3;
    3 3 1 1 2 2 2 1 2 2 2 1 1 3 3;
    3 3 3 1 1 1 2 2 2 1 1 1 3 3 3;
    3 3 3 3 3 1 1 1 1 1 3 3 3 3 3;
    3 3 3 3 3 3 3 3 3 3 3 3 3 3 3];

cmap = [0 0 0;255 240 160;255 255 255]/255;
TimeChoice_im = ind2rgb(double(TimeChoice),cmap);

% Button to display slider axis as chronological sequence
uicontrol('Style','togglebutton',...
    'units', 'normalized',...
    'position', [0.0 0.0 0.5 1.0],  ...
    'fontunits', 'normalized', ...
    'parent',flistViewMode,  ...
    'CData',TimeChoice_im,...
    'ToolTip','View current file list chronologically',...
    'Tag','chronoMode');

% color map for list image
ListChoice = [ ...
    3 3 1 1 1 1 1 1 1 1 1 3;
    3 1 1 2 2 2 2 2 2 2 1 3;
    3 1 2 1 1 1 1 1 1 2 1 3;
    3 1 2 2 2 2 2 2 2 2 1 3;
    3 1 2 1 1 1 1 1 1 2 1 3;
    3 1 2 2 2 2 2 2 2 2 1 3;
    3 1 2 1 1 1 1 1 1 2 1 3;
    3 1 2 2 2 2 2 2 2 2 1 3;
    3 1 2 1 1 1 1 1 1 2 1 3;
    3 1 2 2 2 2 2 2 2 2 1 3;
    3 1 2 1 1 1 1 1 1 2 1 3;
    3 1 2 2 2 2 2 2 2 1 1 3;
    3 1 1 1 1 1 1 1 1 1 3 3];

cmap = [0 0 0;255 240 160;255 255 255]/255;
ListChoice_im = ind2rgb(double(ListChoice),cmap);

% Button to display slider axis as sequential sequence
uicontrol('Style','togglebutton',...
    'units', 'normalized',...
    'position', [0.5 0.0 0.5 1.0],  ...
    'fontunits', 'normalized', ...
    'CData',ListChoice_im,...
    'parent',flistViewMode,  ...
    'ToolTip','View current file list sequentially',...
    'Tag', 'sequenMode');

set(sliderPanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in sliderPHidePanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% button to minimize/restore slider panel
resizeSP = uicontrol ('parent',sliderPHidePanel, 'style','pushbutton', 'units','normalized',...
    'position',[0.3 0.0 0.4 1.0], 'FontUnits','normalized', 'FontSize',0.5,...
    'String','<HTML>&#9650;</HTML>', 'Visible','on', 'Enable','on',...
    'TooltipString','Hide slider panel', 'CallBack',{@resizeSliderPanel_cb}, 'Tag','resizeSliderPanel');
set(sliderPHidePanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in southPanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% X Modification Components
xMinPanel = uipanel('Parent',southPanel, 'units','normalized', 'position',[0.05 0.1 0.07 0.9],...
    'Title','X Min', 'TitlePosition','centertop', 'Visible','off', 'Tag','xMinPanel');
uicontrol('Parent',xMinPanel, 'Style','edit', 'units','normalized',...
    'position',[0 0 1 1], 'String','', 'Enable','inactive', 'Tag','xMinDisp');
set(xMinPanel,'Visible','on');

posXMin = get(xMinPanel,'Position');
ts = sprintf('%s\n%s','Type any number to persistently set the minimum of the X axis.',...
    'Type ''Auto'' (no quotes) to allow the program to automatically set the minimum.');
forcedXMinPanel = uipanel('Parent',southPanel, 'units','normalized', 'position',[posXMin(1)+posXMin(3) posXMin(2) 0.1 posXMin(4)],...
    'Title','Forced X Min', 'TitlePosition','centertop', 'Visible','off', 'Tag','forcedXMinPanel');
uicontrol('Parent',forcedXMinPanel, 'Style','edit', 'units','normalized', 'position',[0 0 1 1],...
    'String','Auto', 'Enable','off', 'BackgroundColor','w', 'Callback',{@editForcedXMin_cb}, 'Tag','xMinEdit',...
    'TooltipString',ts);
set(forcedXMinPanel,'Visible','on');

xMaxPanel = uipanel('Parent',southPanel, 'units','normalized', 'position',[1-0.05-0.07 0.1 0.07 0.9],...
    'Title','X Max', 'TitlePosition','centertop', 'Visible','off', 'Tag','xMaxPanel');
uicontrol('Parent',xMaxPanel, 'Style','edit', 'units','normalized',...
    'position',[0 0 1 1], 'String','', 'Enable','inactive', 'Tag','xMaxDisp');
set(xMaxPanel,'Visible','on');

posXMax = get(xMaxPanel,'Position');
ts = sprintf('%s\n%s','Type any number to persistently set the maximum of the X axis.',...
    'Type ''Auto'' (no quotes) to allow the program to automatically set the maximum.');
forcedXMaxPanel = uipanel('Parent',southPanel, 'units','normalized', 'position',[posXMax(1)-0.1 posXMax(2) 0.1 posXMax(4)],...
    'Title','Forced X Max', 'TitlePosition','centertop', 'Visible','off', 'Tag','forcedXMaxPanel');
uicontrol('Parent',forcedXMaxPanel, 'Style','edit', 'units','normalized', 'position',[0 0 1 1],...
    'String','Auto', 'Enable','off', 'BackgroundColor','w', 'Callback',{@editForcedXMax_cb}, 'Tag','xMaxEdit',...
    'TooltipString',ts);
set(forcedXMaxPanel,'Visible','on');

mouseXPanel = uipanel('Parent',southPanel, 'Units','normalized', 'Position',[0.5-0.1/2 0.1 0.1 0.9],...
    'Title','X Value', 'TitlePosition','centertop', 'BorderType','etchedin', 'Visible','off',...
    'Tag','mouseXPanel');
% displays the X Value corresponding to mouse position on plot
uicontrol('Style','edit',...
    'parent',mouseXPanel,...
    'units','normalized',...
    'position',[0 0 1 1],  ...
    'fontunits','normalized',...
    'fontsize',0.5,...
    'horizontalAlignment','center',...
    'string','',...
    'Enable','inactive',...
    'Tag','pointerXValText');
set(mouseXPanel,'Visible','on');

set(southPanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in southPHidePanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% button to minimize/restore plot modification panel
resizeSP = uicontrol ('parent',southPHidePanel, 'style','pushbutton', 'units','normalized',...
    'position',[0.3 0.0 0.4 1.0], 'FontUnits','normalized', 'FontSize',0.5,...
    'String','<HTML>&#9660</HTML>', 'Visible','on', 'Enable','on',...
    'TooltipString','Hide X Lim panel', 'CallBack',{@resizeSouthPanel_cb}, 'Tag','resizeSouthPanel');
set(southPHidePanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in westPanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Y Modification Components
yAxMenuPanel = uipanel('Parent',westPanel, 'units','normalized', 'position',[0.05 0.95-3*0.07 0.9 0.07],...
    'Title','Y Axis Variable', 'TitlePosition','centertop', 'Visible','on', 'Tag','yAxMenuPanel');
yAxMenu = uicontrol('Parent',yAxMenuPanel, 'Style','popupmenu', 'units','normalized',...
    'position',[0 0 1 1], 'String',{'None'}, 'BackgroundColor','w', 'Enable','off',...
    'Callback',{@yAxMenu_cb}, 'Tag','yAxMenu');
tts = sprintf(['Choose Y variable from menu to view and edit its Y axis limits.\n',...
    'Asterix next to variable name indicates line is not visible, so Y limits are not available.']);
set(yAxMenu,'TooltipString',tts);

% Auto all variable Y lims
icon = 'home.png';
s = 'Restore all forced Y limits to ''Auto''.';
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05 0.95-4*0.07-0.01 0.9/4 0.07], 'CData',cdata, 'TooltipString',s,...
        'Enable','off', 'Callback',{@autoYLim_cb}, 'Tag','autoYLim');
else
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05 0.95-4*0.07-0.01 0.9/4 0.07], 'String','Au', 'TooltipString',s,...
        'Enable','off', 'Callback',{@autoYLim_cb}, 'Tag','autoYLim');
end

% Scan file picks for Y limits
icon = 'search.png';
s = sprintf('Scan all file picks, determine\npersistent Y limits that normalizes all file data,\nand force these limits.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05+0.9/4 0.95-4*0.07-0.01 0.9/4 0.07], 'CData',cdata, 'TooltipString',s,...
        'Enable','off', 'Callback',{@scanYLim_cb}, 'Tag','scanYLim');
else
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05+0.9/4 0.95-4*0.07-0.01 0.9/4 0.07], 'String','Sc', 'TooltipString',s,...
        'Enable','off', 'Callback',{@scanYLim_cb}, 'Tag','scanYLim');
end

% Save Y limits
icon = 'save_bw.png';
s = sprintf('Save Y limits structure to MAT file.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05+2*0.9/4 0.95-4*0.07-0.01 0.9/4 0.07], 'CData',cdata, 'TooltipString',s,...
        'Enable','off', 'Callback',{@saveYLim_cb}, 'Tag','saveYLim');
else
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05+2*0.9/4 0.95-4*0.07-0.01 0.9/4 0.07], 'String','Sa', 'TooltipString',s,...
        'Enable','off', 'Callback',{@saveYLim_cb}, 'Tag','saveYLim');
end

% Load Y limits
icon = 'open_bw.png';
s = sprintf('Load Y limits structure from MAT file.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05+3*0.9/4 0.95-4*0.07-0.01 0.9/4 0.07], 'CData',cdata, 'TooltipString',s,...
        'Enable','off', 'Callback',{@loadYLim_cb}, 'Tag','loadYLim');
else
    uicontrol('Parent',westPanel, 'Style','pushbutton', 'units','normalized',...
        'position',[0.05+3*0.9/4 0.95-4*0.07-0.01 0.9/4 0.07], 'String','Ld', 'TooltipString',s,...
        'Enable','off', 'Callback',{@loadYLim_cb}, 'Tag','loadYLim');
end

% Y min
yMinPanel = uipanel('Parent',westPanel, 'units','normalized', 'position',[0.15 0.05 0.7 0.07],...
    'Title','Y Min', 'TitlePosition','centertop', 'Visible','off', 'Tag','yMinPanel');
uicontrol('Parent',yMinPanel, 'Style','edit', 'units','normalized',...
    'position',[0 0 1 1], 'String','', 'Enable','inactive', 'Tag','yMinDisp');
set(yMinPanel,'Visible','on');

ts = sprintf('%s\n%s','Type any number to persistently set the minimum of the Y axis.',...
    'Type ''Auto'' (no quotes) to allow the program to automatically set the minimum.');
forcedYMinPanel = uipanel('Parent',westPanel, 'units','normalized', 'position',[0.05 0.05+0.07 0.9 0.07],...
    'Title','Forced Y Min', 'TitlePosition','centertop', 'Visible','off', 'Tag','forcedYMinPanel');
uicontrol('Parent',forcedYMinPanel, 'Style','edit', 'units','normalized', 'position',[0 0 1 1],...
    'String','Auto', 'BackgroundColor','w', 'Enable','off', 'Callback',{@editForcedYMin_cb}, 'Tag','yMinEdit',...
    'TooltipString',ts);
set(forcedYMinPanel,'Visible','on');

% Y max
yMaxPanel = uipanel('Parent',westPanel, 'units','normalized', 'position',[0.15 0.95-0.07 0.7 0.07],...
    'Title','Y Max', 'TitlePosition','centertop', 'Visible','off', 'Tag','yMaxPanel');
uicontrol('Parent',yMaxPanel, 'Style','edit', 'units','normalized',...
    'position',[0 0 1 1], 'String','', 'Enable','inactive', 'Tag','yMaxDisp');
set(yMaxPanel,'Visible','on');

ts = sprintf('%s\n%s','Type any number to persistently set the maximum of the Y axis.',...
    'Type ''Auto'' (no quotes) to allow the program to automatically set the maximum.');
forcedYMaxPanel = uipanel('Parent',westPanel, 'units','normalized', 'position',[0.05 0.95-2*0.07 0.9 0.07],...
    'Title','Forced Y Max', 'TitlePosition','centertop', 'Visible','off', 'Tag','forcedYMaxPanel');
uicontrol('Parent',forcedYMaxPanel, 'Style','edit', 'units','normalized', 'position',[0 0 1 1],...
    'String','Auto', 'BackgroundColor','w', 'Enable','off', 'Callback',{@editForcedYMax_cb}, 'Tag','yMaxEdit',...
    'TooltipString',ts);
set(forcedYMaxPanel,'Visible','on');

set(westPanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in westPHidePanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% button to minimize/restore plot modification panel
resizeWP = uicontrol ('parent',westPHidePanel, 'style','pushbutton', 'units','normalized',...
    'position',[0.0 0.47 1.0 0.06], 'FontUnits','normalized', 'FontSize',0.17,...
    'String','<HTML>&#9668</HTML>', 'Visible','on', 'Enable','on',...
    'TooltipString','Hide Y Lim panel', 'CallBack',{@resizeWestPanel_cb}, 'Tag','resizeWestPanel');
set(westPHidePanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in controlPHidePanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% button to hide controlPanel
resizeCP = uicontrol ('parent',controlPHidePanel, ...
    'style','pushbutton', 'units','normalized', 'position',[0.0 0.47 1.0 0.06],...
    'FontUnits','normalized', 'FontSize',0.17, 'String','<HTML>&#9658</HTML>', 'ToolTipString','Hide control panel',...
    'Visible','on', 'Enable','on', 'CallBack',{@resizeControlPanel_cb}, 'Tag','resizeControlPanel');
set(controlPHidePanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sub-panels in controlPanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Panel inside controlPanel that contains current file info
fileInfoPanel = uipanel('Parent',controlPanel, 'Units','normalized', 'Position',[0 0.85 1 0.15],...
    'Visible','off', 'Tag','fileInfoPanel');

% Panel inside controlPanel that contains variable selection components
varSelectionPanel = uipanel('Parent',controlPanel, 'Units','normalized', 'Position',[0 0.15 1 0.7],...
    'Visible','off', 'Tag','varSelectionPanel');

% Panel inside controlPanel that contains extra functionality components
consolePanel = uipanel('Parent',controlPanel, 'Units','normalized', 'Position',[0 0 1 0.15],...
    'Visible','off', 'Tag','consolePanel');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in fileInfoPanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

currCTDFileInfo = {'File Name: N/A' 'Station: N/A' 'Date: N/A' 'File 0 of 0'};

uicontrol('Parent',fileInfoPanel, 'style','listbox',...
    'units','normalized', 'position',[0 0 1 1],...
    'horizontalalignment','left',...
    'ForegroundColor',[0 0 205/255],...
    'FontUnits','normalized',...
    'FontSize',0.15,...
    'string', currCTDFileInfo,...
    'Min',0, 'Max',2,...
    'Enable','inactive', 'Value',[], 'Tag','fileInfoBox');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in varSelectionPanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Panel components
%--------------------------------------------------------------------------

dTypePanel = uipanel('Parent',varSelectionPanel, 'Units','normalized',...
    'Position',[0.1 0.9 0.35 0.1], 'Title','Data Type', 'TitlePosition','centertop',...
    'Tag','dataTypePanel');

uicontrol('Parent',dTypePanel, 'Style','popupmenu', 'Units','normalized', 'Position',[0 0 1 1],...
    'FontWeight','bold', 'FontSize',9, 'String', currCTDDataType, 'Enable','off',...
    'BackgroundColor','white', 'Callback',{@switchXLSPlot}, 'Tag','dataTypeMenu');

xVarPanel = uipanel('Parent',varSelectionPanel, 'Units','normalized',...
    'Position',[0.55 0.9 0.35 0.1], 'Title', 'X Variable', 'TitlePosition','centertop',...
    'Tag','xVarPanel');

uicontrol('Parent',xVarPanel, 'Style','popupmenu', 'Units','normalized', 'Position',[0 0 1 1],...
    'FontWeight','bold', 'FontSize',9,...
    'String', currCTDVarsX{1},...
    'Enable','off',...
    'BackgroundColor','white', 'Callback',{@xVarMenu_cb}, 'Tag','xVarMenu');

% Y variables
varVisPanel1 = uipanel('Parent',varSelectionPanel, 'Units','normalized',...
    'Position',[0.05 0.1 0.3 0.8], 'Title','Y Variables', 'TitlePosition','centertop',...
    'Tag','varVisPanel1');

axSelPanel1 = uipanel('Parent',varSelectionPanel, 'Units','normalized',...
    'Position',[0.35 0.1 0.15 0.8], 'Title','L/R', 'TitlePosition','centertop',...
    'Tag','axSelPanel1');

varVisPanel2 = uipanel('Parent',varSelectionPanel, 'Units','normalized',...
    'Position',[0.5 0.1 0.3 0.8], 'Title','Y Variables', 'TitlePosition','centertop',...
    'Tag','varVisPanel2');

axSelPanel2 = uipanel('Parent',varSelectionPanel, 'Units','normalized',...
    'Position',[0.8 0.1 0.15 0.8], 'Title','L/R', 'TitlePosition','centertop',...
    'Tag','axSelPanel2');

% Button group containing radio buttons for left axis selection
varChoiceGroup1AxL = uibuttongroup('Parent',axSelPanel1, 'Units','Normalized',...
    'Position',[0.0 0.0 0.5 1.00],...
    'BorderType','line', 'Tag','varChoiceGroup1AxL', 'SelectionChangeFcn',{@selchbk_callAxL},...
    'SelectedObject',[]);

% Button group containing radio buttons for right axis selection
varChoiceGroup1AxR = uibuttongroup('Parent',axSelPanel1, 'Units','Normalized',...
    'Position',[0.5 0.0 0.5 1.00],...
    'BorderType','line', 'Tag','varChoiceGroup1AxR', 'SelectionChangeFcn',{@selchbk_callAxR},...
    'SelectedObject',[]);

% Button group containing radio buttons for left axis selection
varChoiceGroup2AxL = uibuttongroup('Parent',axSelPanel2, 'Units','Normalized',...
    'Position',[0.0 0.0 0.5 1.00],...
    'BorderType','line', 'Tag','varChoiceGroup2AxL', 'SelectionChangeFcn',{@selchbk_callAxL},...
    'SelectedObject',[]);

% Button group containing radio buttons for right axis selection
varChoiceGroup2AxR = uibuttongroup('Parent',axSelPanel2, 'Units','Normalized',...
    'Position',[0.5 0.0 0.5 1.00],...
    'BorderType','line', 'Tag','varChoiceGroup2AxR', 'SelectionChangeFcn',{@selchbk_callAxR},...
    'SelectedObject',[]);

% Edit Variables Buttons
% -------------------------------------------------------------------------

varGroupPanel = uipanel('Parent',varSelectionPanel', 'Units','normalized', 'Position',[0.05 0.02 0.4 0.07],...
    'Title','Variable Group', 'TitlePosition','centertop');
defGroup = getpref('lightgui','defaultMasterCTDVarsGroup');
uicontrol('Style','edit', 'Parent',varGroupPanel, 'Units','normalized',...
    'Position',[0 0 1 1], 'String',defGroup, 'Enable','inactive', 'Tag','currVarGroup');

icon = 'checkbox.png';
ts = sprintf('%s\n%s','Set Variables:', 'Choose what variables to display in the selection array above.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'ToolTipString',ts,...
        'Units','normalized', 'Position',[0.45 0.02 0.5/4 0.07], 'Tag','setVariablesBtn',...
        'CData',cdata, 'Callback',{@setVariables_cb});
else
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'ToolTipString',ts,...
        'Units','normalized', 'Position',[0.45 0.02 0.5/4 0.07], 'Tag','setVariablesBtn',...
        'Callback',{@setVariables_cb});
end

icon = 'configure.png';
ts = sprintf('%s\n%s','Configure Variables:', 'Customize the properties of each variable in the current variable group.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'ToolTipString',ts,...
        'Units','normalized', 'Position',[0.45+0.5/4 0.02 0.5/4 0.07], 'Tag','configVarPropsBtn',...
        'CData',cdata, 'Callback',{@configVarProps_cb});
else
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'ToolTipString',ts,...
        'Units','normalized', 'Position',[0.45+0.5/4 0.02 0.5/4 0.07], 'Tag','configVarPropsBtn',...
        'Callback',{@configVarProps_cb});
end

icon = 'add.png';
ts = sprintf('%s\n%s','Add New Variable:', 'Add a new variable to the current variable group and customize the variable''s properties.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'ToolTipString',ts,...
        'Units','normalized', 'Position',[0.45+2*0.5/4 0.02 0.5/4 0.07], 'Tag','addNewVar',...
        'CData',cdata, 'Callback',{@addNewVar_cb});
else
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'ToolTipString',ts,...
        'Units','normalized', 'Position',[0.45+2*0.5/4 0.02 0.5/4 0.07], 'Tag','addNewVar',...
        'Callback',{@addNewVar_cb});
end

icon = 'open_bw2.png';
ts = sprintf('%s\n%s','Load/Manage Variable Group:', 'Load a previously saved variable group, and/or set it as the program default.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'TooltipString',ts,...
        'Units','normalized', 'Position',[0.45+3*0.5/4 0.02 0.5/4 0.07], 'Tag','openVarGroup',...
        'CData',cdata, 'Callback',{@openVarGroup_cb});
else
    uicontrol('Style','pushbutton', 'Parent',varSelectionPanel, 'TooltipString',ts,...
        'Units','normalized', 'Position',[0.45+3*0.5/4 0.02 0.5/4 0.07], 'Tag','openVarGroup',...
        'Callback',{@openVarGroup_cb});
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in consolePanel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Display output messages in a stack
consoleBox = uicontrol('Parent',consolePanel,...
    'Units','normalized',...
    'Position',[0,0,1.0,0.85],...
    'Style','edit',...
    'Max',100,...
    'Enable','inactive',...
    'HorizontalAlignment','left',...
    'String','',...
    'Tag','consoleBox');

% button to resize console box
uicontrol ('parent',consolePanel, ...
    'style','pushbutton', 'units','normalized', 'position',[0 0.85 0.9 0.15],...
    'FontUnits','normalized', 'FontSize',0.5, 'String','<HTML>&#9650</HTML>', 'ToolTipString','Expand log',...
    'Visible','on', 'Enable','on', 'CallBack',{@resizeConsole_cb}, 'Tag','resizeConsoleBox');

% button to save console box log to html
uicontrol ('parent',consolePanel, ...
    'style','pushbutton', 'units','normalized', 'position',[0.9 0.85 0.1 0.15],...
    'FontUnits','normalized', 'FontSize',1.0, 'String','<HTML>&#9998</HTML>', 'ToolTipString','Export log',...
    'Visible','on', 'Enable','on', 'CallBack',{@exportLog_cb}, 'Tag','exportLog');

% Invisible button for focusing on when we don't want selection highlight
% borders on a component
uicontrol ('parent',consolePanel, ...
    'style','pushbutton', 'units','normalized', 'position',[0 0 0.01 0.01],...
    'Visible','on', 'Enable','off', 'Tag','ghostButton');

set(consolePanel,'Visible','on');
set(varSelectionPanel,'Visible','on');
set(fileInfoPanel,'Visible','on');
set(controlPanel,'Visible','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Components in toolbar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mainToolbar = uitoolbar(mainFig,'Visible','off');

% Method to read in an icon file and output the properly formatted cdata
% for use in a uitool
    function [cdata] = readIcon(iconFileName)
        [cdata,map,alpha] = imread(iconFileName);
        if ~isempty(map) % indexed image
            % Convert white pixels into a transparent background
            map((map(:,1)+map(:,2)+map(:,3)==3)) = NaN;
            % Convert into 3D RGB-space
            cdata = ind2rgb(cdata,map);
        else % non-indexed image
            info = imfinfo(iconFileName);
            bitDepth = info.BitDepth;
            if bitDepth == 48
                cdata = double(cdata)/(2^16-1);
            elseif bitDepth == 24
                cdata = double(cdata)/(2^8-1);
            end
            if ~isempty(alpha)
                cdata(alpha < 1) = NaN;
            end
        end
    end

% Load files icon
icon = 'file_open.png';
if exist(icon,'file')
    cdata = readIcon(icon);
    uipushtool(mainToolbar, 'CData',cdata, 'TooltipString','Load CTD file(s)',...
    'Separator','on', 'ClickedCallback',{@FList_pb_call});
else
    uipushtool(mainToolbar, 'TooltipString','Load CTD file(s)',...
    'Separator','on', 'ClickedCallback',{@FList_pb_call});
end

% Export to txt icon
icon = 'file_save.png';
if exist(icon,'file')
    cdata = readIcon(icon);
    uipushtool(mainToolbar, 'CData',cdata, 'TooltipString','Save CTD data to TXT',...
    'Separator','on', 'ClickedCallback',{@saveDataToText_cb});
else
    uipushtool(mainToolbar, 'TooltipString','Save CTD data to TXT',...
    'Separator','on', 'ClickedCallback',{@saveDataToText_cb});
end

% Autosave icon
icon = 'autosave.gif';
if exist(icon,'file')
    cdata = readIcon(icon);
    s = sprintf('Enable autosave of CTD data to TXT');
    uitoggletool(mainToolbar, 'CData',cdata, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@enableAutosave_cb}, 'OffCallback',{@disableAutosave_cb});
else
    s = sprintf('Enable autosave of CTD data to TXT');
    uitoggletool(mainToolbar, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@enableAutosave_cb}, 'OffCallback',{@disableAutosave_cb});
end

% Batch process icon
icon = 'calculator.png';
if exist(icon,'file')
    cdata = readIcon(icon);
    uipushtool(mainToolbar, 'CData',cdata, 'TooltipString','Batch process all file picks and autosave to TXT',...
        'Separator','on', 'ClickedCallback',{@batchProcess_cb});
else
    uipushtool(mainToolbar, 'TooltipString','Batch process all file picks and autosave to TXT',...
        'Separator','on', 'ClickedCallback',{@batchProcess_cb});
end

% Save plot image icon
icon = 'snapshot.png';
if exist(icon,'file')
    cdata = readIcon(icon);
    uipushtool(mainToolbar, 'CData',cdata, 'TooltipString','Save image of plot',...
        'Separator','on', 'ClickedCallback',{@snapshot_cb});
else
    uipushtool(mainToolbar, 'TooltipString','Save image of plot',...
        'Separator','on', 'ClickedCallback',{@snapshot_cb});
end

% Display table icon
icon = 'table.gif';
if exist(icon,'file')
    cdata = readIcon(icon);
    uipushtool(mainToolbar, 'CData',cdata, 'TooltipString','Display table of CTD data',...
        'Separator','on', 'ClickedCallback',{@displayDataTable_tb});
else
    uipushtool(mainToolbar, 'TooltipString','Display table of CTD data',...
        'Separator','on', 'ClickedCallback',{@displayDataTable_tb});
end

% Display 1% depth icon
icon = 'wave.gif';
if exist(icon,'file')
    cdata = readIcon(icon);
    s = sprintf('Display one percent depth line(s) on plot.\n Vertical dashed line = Ke derived.\n Vertical dotted line = Ke_adj derived.');
    uitoggletool(mainToolbar, 'CData',cdata, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@showOnePercentDepth_cb}, 'OffCallback',{@hideOnePercentDepth_cb});
else
    s = sprintf('Display one percent depth line(s) on plot.\n Vertical dashed line = Ke derived.\n Vertical dotted line = Ke_adj derived.');
    uitoggletool(mainToolbar, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@showOnePercentDepth_cb}, 'OffCallback',{@hideOnePercentDepth_cb});
end

% Check depth icon
icon = 'marker.gif';
if exist(icon,'file')
    cdata = readIcon(icon);
    s = sprintf(['Left click mouse to check X Value on plot (result displayed in box below plot).\n',...
        'Right click to deactivate crosshair.']);
    uitoggletool(mainToolbar, 'CData',cdata, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@enableChkDepth_cb});
else
    s = sprintf(['Use mouse pointer to check X Value on plot (result displayed in box below plot).\n',...
        'Right click to deactivate crosshair.']);
    uitoggletool(mainToolbar, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@enableChkDepth_cb});
end

% Rotate plot icon
icon = 'rotate_clockwise.png';
if exist(icon,'file')
    cdata = readIcon(icon);
    s = sprintf('Rotate plot clockwise 90 degrees so that x values are vertical.');
    uitoggletool(mainToolbar, 'CData',cdata, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@rotatePlot_cb}, 'OffCallback',{@unrotatePlot_cb},...
        'Tag','mouseXBtn');
else
    s = sprintf('Rotate plot clockwise 90 degrees so that x values are vertical.');
    uitoggletool(mainToolbar, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@rotatePlot_cb}, 'OffCallback',{@unrotatePlot_cb},...
        'Tag','mouseXBtn');
end

% Show/hide grid icon
icon = 'grid.gif';
s = sprintf('Show X and Y axis grid lines.');
if exist(icon,'file')
    cdata = readIcon(icon);
    uitoggletool(mainToolbar, 'CData',cdata, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@showGrid_cb}, 'OffCallback',{@hideGrid_cb});
else
    uitoggletool(mainToolbar, 'TooltipString',s,...
        'Separator','on', 'OnCallback',{@showGrid_cb}, 'OffCallback',{@hideGrid_cb});
end

% Deck cell manager icon
icon = 'demoicon.gif';
if exist(icon,'file')
    cdata = readIcon(icon);
    uipushtool(mainToolbar, 'CData',cdata, 'TooltipString','Open deck cell manager',...
        'Separator','on', 'ClickedCallback',{@DeckCellMng_pb_call});
else
    uipushtool(mainToolbar, 'TooltipString','Open deck cell manager',...
        'Separator','on', 'ClickedCallback',{@DeckCellMng_pb_call});
end

set(mainToolbar,'Visible','on');

%% Save GUI data and make GUI visible

% save all GUI handles to local variable and to guidata for mainFig
mainFigHandles = guihandles(mainFig);
mainFigData = struct('handles',mainFigHandles, 'data',[]);

mainFigData.data.masterCTDVars = masterCTDVars;
mainFigData.data.currCTDDataType = currCTDDataType;
mainFigData.data.currCTDVarsY = currCTDVarsY;
mainFigData.data.currCTDVarsX = currCTDVarsX;
mainFigData.data.plottedCTDVarsZY = plottedCTDVarsZY;
mainFigData.data.plottedCTDVarsN2Y = plottedCTDVarsN2Y;
mainFigData.data.preferredCTDVarsZY = preferredCTDVarsZY;
mainFigData.data.preferredCTDVarsZX = preferredCTDVarsZX;
mainFigData.data.preferredCTDVarsN2Y = preferredCTDVarsN2Y;
mainFigData.data.preferredCTDVarsN2X = preferredCTDVarsN2X;

mainFigData.data.axLVarZ = axLVarZ;
mainFigData.data.axRVarZ = axRVarZ;
mainFigData.data.axLVarN2 = axLVarN2;
mainFigData.data.axRVarN2 = axRVarN2;

mainFigData.data.CTDFullFilePicks = {};
mainFigData.data.currCTDFileIndex = -1;
mainFigData.data.currCTDFileFullPath = '';

mainFigData.data.flistViewMode = 'chronoMode';
mainFigData.data.chronoHash = [];
mainFigData.data.chronoValues = [];
mainFigData.data.sequenHash = [];
mainFigData.data.sequenValues = [];

mainFigData.data.forcedYLims = forcedYLims;
mainFigData.data.forcedXMin = [];
mainFigData.data.forcedXMax = [];

mainFigData.data.autosaveOn = 0;
mainFigData.data.procCTDPath = '';

mainFigData.data.currMasterVarsGroup = defaultMasterCTDVarsGroup;

mainLogText = '';
deckMngLogText = '';

guidata(mainFig, mainFigData); % save all of the above in mainFig's guidata
% access handles via:
% mainFigData = guidata(mainFig);
% mainFigData.handles.<component tag>

initVarSelPanel();
updateVarSelPanel();

movegui(mainFig,'north');
set(mainFig, 'visible','on');

mainLogText = appendLogText(mainLogText,'Program initialized.','success');
mainLogText = updateLog(mainLogText,consoleBox);

%% Major helper functions

% resize all mainFig panels based on what panels are hidden
    function[] = resizeMainFigPanels()
        % ap = axes panel
        % np = north panel (slider panel)
        % wp = west panel (y/xlim panel)
        % sp = south panel (y/xlim panel)
        % ep = east panel (control panel)
        % whp = west hide button panel
        % nhp = north hide button panel
        % ehp = east hide button panel
        % shp = south hide button panel
        ap = axesPanel; 
        np = sliderPanel; wp = westPanel; ep = controlPanel; sp = southPanel;
        nhp = sliderPHidePanel; whp = westPHidePanel;
        ehp = controlPHidePanel; shp = southPHidePanel;
        ap_pos = get(ap,'Position');
        np_pos = get(np,'Position'); wp_pos = get(wp,'Position');
        sp_pos = get(sp,'Position');
        nhp_pos = get(nhp,'Position'); whp_pos = get(whp,'Position');
        ehp_pos = get(ehp,'Position'); shp_pos = get(shp,'Position');
        
        % enforce horizontal contiguousness of west and east
        % hide buttons panels with axes panel
        ap_pos(1) = whp_pos(1) + whp_pos(3);
        ap_pos(3) = ehp_pos(1) - (whp_pos(1) + whp_pos(3));
        
        % enforce vertical contiguousness of south and north
        % hide buttons panels with axes panel
        ap_pos(2) = shp_pos(2) + shp_pos(4);
        ap_pos(4) = nhp_pos(2) - (shp_pos(2) + shp_pos(4));
        
        set(ap,'Position',ap_pos);
        
        % south and north hide panels and panels must have same left position and width as
        % axes panel
        sp_pos(1) = ap_pos(1); sp_pos(3) = ap_pos(3); set(sp,'Position',sp_pos);
        shp_pos(1) = ap_pos(1); shp_pos(3) = ap_pos(3); set(shp,'Position',shp_pos);
        np_pos(1) = ap_pos(1); np_pos(3) = ap_pos(3); set(np,'Position',np_pos);
        nhp_pos(1) = ap_pos(1); nhp_pos(3) = ap_pos(3); set(nhp,'Position',nhp_pos);
        
        % west panels and east hide panel must have same bottom position
        % and height as axes panel
        wp_pos(2) = ap_pos(2); wp_pos(4) = ap_pos(4); set(wp,'Position',wp_pos);
        whp_pos(2) = ap_pos(2); whp_pos(4) = ap_pos(4); set(whp,'Position',whp_pos);
        ehp_pos(2) = ap_pos(2); ehp_pos(4) = ap_pos(4); set(ehp,'Position',ehp_pos);
    end

% update all text fields in fileInfoPanel with info about the current
% CTD file
    function [] = updateFileInfoPanel(fileFullPath)
        myData = guidata(mainFig); % access all handles in GUI
        [~, fileName fileExt] = fileparts(fileFullPath);
        currFileInfo = {'File Name: N/A' 'Station: N/A' 'Date: N/A' 'File 0 of 0'};
        currFileInfo{1} = ['File Name: ', [fileName fileExt]];
        % get file info and update text fields in fileInfoPanel
        [stationName sampleDateNum] = getCTDFileInfo(fileFullPath);
        currFileInfo{2} = ['Station: ', stationName];
        if ~isempty(sampleDateNum)
            currFileInfo{3} = ['Date: ', datestr(sampleDateNum)];
        end
        currFileInfo{4} = ['File ',num2str(myData.data.currCTDFileIndex),...
            ' of ',num2str(length(myData.data.CTDFullFilePicks))];
        set(myData.handles.fileInfoBox,'String',currFileInfo);
        clear myData;
    end

% create the maximum number of sub-panels, toggle buttons, and radiobuttons
% for variable selection
    function [] = initVarSelPanel()
        myData = guidata(mainFig);
        set(myData.handles.varSelectionPanel,'Visible','off');
        
        % need 2 varVisPanels and axSelPanels for max 20 Y variables
        numPanelChildren = ceil(MAX_YVARS/2);
        set(myData.handles.varVisPanel1,'Position',[0.05 0.1 0.3 0.8]);
        set(myData.handles.axSelPanel1,'Position',[0.35 0.1 0.15 0.8]);
        set(myData.handles.varVisPanel2,'Position',[0.5 0.1 0.3 0.8]);
        set(myData.handles.axSelPanel2,'Position',[0.8 0.1 0.15 0.8]);
        set(myData.handles.varVisPanel2,'Visible','on');
        set(myData.handles.axSelPanel2,'Visible','on');
        
        % figure out nicely spaced panel heights based on number of variables
        panelHeight = floor((1/numPanelChildren)*100)/100;
        varPanelPos1 = [0.0 1.0-panelHeight 1.0 panelHeight];
        varPanelPos2 = [0.0 1.0-panelHeight 1.0 panelHeight];
        radioButtonPos1 = [0.0 1.0-panelHeight 1.0 panelHeight];
        radioButtonPos2 = [0.0 1.0-panelHeight 1.0 panelHeight];
        mainFigHandles = guihandles(mainFig); % get structure of all graphics handles so far
        
        % update the y-variable panels and radiobutton panels
        % Creates a uipanel to hold color togglebutton and variable text
        function[] = createCTDVarPanel(parent, pos, tag)
            uipanel('Parent',parent, 'Units','normalized', 'Position',pos,...
                'BorderType','line', 'Tag',tag);
        end
        
        % create a radiobutton inside a buttongroup
        function[] = createAxRadioButton(parent, pos, tag)
            uicontrol('Style','radiobutton', 'Parent',parent,...
                'units', 'normalized','Position',pos,...
                'Tag',tag, 'Enable','off', 'HandleVisibility','off');
        end
        
        % Iteratively create uipanels with successive positions and tags
        for panelNum = 1:MAX_YVARS
            varPanelTag = strcat('var',num2str(panelNum),'Panel');
            varAXLChoiceTag = strcat('var',num2str(panelNum),'AxLChoice');
            varAXRChoiceTag = strcat('var',num2str(panelNum),'AxRChoice');
            if panelNum <= numPanelChildren
                createCTDVarPanel(myData.handles.varVisPanel1, varPanelPos1, varPanelTag);
                varPanelPos1 = varPanelPos1 - [0 panelHeight 0 0];
                createAxRadioButton(myData.handles.varChoiceGroup1AxL, radioButtonPos1, varAXLChoiceTag);
                createAxRadioButton(myData.handles.varChoiceGroup1AxR, radioButtonPos1, varAXRChoiceTag);
                radioButtonPos1 = radioButtonPos1 - [0 panelHeight 0 0];
            else
                createCTDVarPanel(myData.handles.varVisPanel2, varPanelPos2, varPanelTag);
                varPanelPos2 = varPanelPos2 - [0 panelHeight 0 0];
                createAxRadioButton(myData.handles.varChoiceGroup2AxL, radioButtonPos2, varAXLChoiceTag);
                createAxRadioButton(myData.handles.varChoiceGroup2AxR, radioButtonPos2, varAXRChoiceTag);
                radioButtonPos2 = radioButtonPos2 - [0 panelHeight 0 0];
            end
        end
        
        % Creates a togglebutton that indicates the color of the
        % variable and whether it is plotted
        function[] = createVarChoiceButton(parent, color, tag)
            uicontrol('Parent',parent, 'Style','togglebutton', 'Units','normalized',...
                'Position',[0.05 0.1 0.2 0.8], 'BackgroundColor',color,...
                'Enable','off', 'Value', 0, 'Tag',tag,...
                'Callback',{@VarChoice_check_call});
        end
        
        % Creates a text field for a variable
        function[] = createVarText(parent, string, tag)
            uicontrol('Parent',parent, 'Style','text', 'Units','normalized',...
                'fontsize',8, 'fontweight','bold', 'Position',[0.3 0.2 0.65 0.6],...
                'String',string, 'horizontalalignment','left',...
                'Tag',tag);
        end
        
        mainFigHandles = guihandles(mainFig); % get structure of all graphics handles so far
        defaultColor = get(0, 'defaultUicontrolBackgroundColor'); % get default gray color
        
        % iteratively create toggle buttons for each variable
        for varNumber = 1:MAX_YVARS
            varNumStr = num2str(varNumber);
            varChoiceTag = strcat('var',varNumStr,'Choice');
            varPanelTag = strcat('var',varNumStr,'Panel');
            varTextTag = strcat('var',varNumStr,'Text');
            createVarChoiceButton(mainFigHandles.(varPanelTag), defaultColor, varChoiceTag);
            createVarText(mainFigHandles.(varPanelTag), '', varTextTag);
        end
        
        mainFigHandles = guihandles(mainFig); % update mainFigHandles
        myData.handles = mainFigHandles; % update myData
        guidata(mainFig,myData);
    end

% update all components of variable selection panel in main figure
% so it reflects changes in current variables and the current data
    function [] = updateVarSelPanel()
        myData = guidata(mainFig);
        set(myData.handles.varSelectionPanel,'Visible','off');
        
        currVarsX = myData.data.currCTDVarsX;
        currVarsY = myData.data.currCTDVarsY;
        currDataType = myData.data.currCTDDataType;
        
        numVarsY = length(currVarsY);
        
        % update the name in data type panel
        % if xls file, set menu elements and choice
        if myData.data.currCTDFileIndex > 0
            fileFullPath = myData.data.currCTDFileFullPath;
            if strcmpi(fileFullPath(end-2:end),'xls')
                set(myData.handles.dataTypeMenu,'String',{'Z' 'N2'},'Enable','on');
                if strcmpi(currDataType,'Z')
                    set(myData.handles.dataTypeMenu,'Value',1);
                elseif strcmpi(currDataType,'N2')
                    set(myData.handles.dataTypeMenu,'Value',2);
                end
            else
                set(myData.handles.dataTypeMenu,'String',currDataType,'Enable','off');
                enabledH(enabledH == myData.handles.dataTypeMenu) = []; % remove handle from re-enable list
            end
        end
        
        % populate x variable menu if data was loaded
        % Get the X variables of the current data type that have data
        varNames = fieldnames(myData.data.masterCTDVars.(currDataType));
        xVarNames = cell(1,length(varNames)); % pre-allocate
        for varNum = 1:length(varNames)
            varProps = myData.data.masterCTDVars.(currDataType).(varNames{varNum});
            if varProps.canPlot && strcmpi(varProps.varType,'x') && ~isempty(varProps.data)
                xData = varProps.data;
                if sum(isnan(xData)) < length(xData)
                    xVarNames{varNum} = varNames{varNum};
                end
            end
        end
        xVarNames(cellfun('isempty',xVarNames)) = []; % trim pre-allocated cell array
        
        if isempty(xVarNames)
            set(myData.handles.xVarMenu,'String','None','Enable','off');
            enabledH(enabledH == myData.handles.xVarMenu) = []; % remove handle from re-enable list
        else
            set(myData.handles.xVarMenu,'String',xVarNames);
            if length(xVarNames) > 1
                matchInd = find(strcmp(currVarsX{1},xVarNames));
                if ~isempty(matchInd)
                    set(myData.handles.xVarMenu,'Value',matchInd);
                else
                    set(myData.handles.xVarMenu,'Value',1);
                    myData.data.currCTDVarsX = xVarNames(1);
                end
                set(myData.handles.xVarMenu,'Enable','on');
            else
                set(myData.handles.xVarMenu,'Value',1);
                myData.data.currCTDVarsX = xVarNames(1);
                set(myData.handles.xVarMenu,'Enable','off');
                enabledH(enabledH == myData.handles.xVarMenu) = []; % remove handle from re-enable list
            end
        end
        
        % set position of variable visibility and ax selection panels
        if numVarsY > MAX_YVARS/2 % need 2 varVisPanels and axSelPanels
            numChildren = ceil(numVarsY/2);
            set(myData.handles.varVisPanel1,'Position',[0.05 0.1 0.3 0.8]);
            set(myData.handles.axSelPanel1,'Position',[0.35 0.1 0.15 0.8]);
            set(myData.handles.varVisPanel2,'Position',[0.5 0.1 0.3 0.8]);
            set(myData.handles.axSelPanel2,'Position',[0.8 0.1 0.15 0.8]);
            set(myData.handles.varVisPanel2,'Visible','on');
            set(myData.handles.axSelPanel2,'Visible','on');
        else
            % need 1 varVisPanel and axSelPanel
            numChildren = numVarsY;
            set(myData.handles.varVisPanel1,'Position',[0.05 0.1 0.75 0.8]);
            set(myData.handles.axSelPanel1,'Position',[0.8 0.1 0.15 0.8]);
            set(myData.handles.varVisPanel2,'Visible','off');
            set(myData.handles.axSelPanel2,'Visible','off');
        end
        
        % figure out nicely spaced panel heights based on number of variables
        panelHeight = floor((1/numChildren)*100)/100;
        varPanelPos1 = [0.0 1.0-panelHeight 1.0 panelHeight];
        varPanelPos2 = [0.0 1.0-panelHeight 1.0 panelHeight];
        radioButtonPos1 = [0.0 1.0-panelHeight 1.0 panelHeight];
        radioButtonPos2 = [0.0 1.0-panelHeight 1.0 panelHeight];
        
        % Iteratively change the visibility and position of single variable
        % panels and radiobuttons
        for panelNum = 1:MAX_YVARS
            varPanelTag = strcat('var',num2str(panelNum),'Panel');
            varAxLChoiceTag = strcat('var',num2str(panelNum),'AxLChoice');
            varAxRChoiceTag = strcat('var',num2str(panelNum),'AxRChoice');
            if panelNum <= numVarsY
                if panelNum <= numChildren
                    set(myData.handles.(varPanelTag), 'Parent',myData.handles.varVisPanel1,...
                        'Position',varPanelPos1, 'Visible','on');
                    varPanelPos1 = varPanelPos1 - [0 panelHeight 0 0];
                    set(myData.handles.(varAxLChoiceTag), 'Parent',myData.handles.varChoiceGroup1AxL,...
                        'Position',radioButtonPos1, 'Visible','on');
                    set(myData.handles.(varAxRChoiceTag), 'Parent',myData.handles.varChoiceGroup1AxR,...
                        'Position',radioButtonPos1, 'Visible','on');
                    radioButtonPos1 = radioButtonPos1 - [0 panelHeight 0 0];
                else
                    set(myData.handles.(varPanelTag), 'Parent',myData.handles.varVisPanel2,...
                        'Position',varPanelPos2, 'Visible','on');
                    varPanelPos2 = varPanelPos2 - [0 panelHeight 0 0];
                    set(myData.handles.(varAxLChoiceTag), 'Parent',myData.handles.varChoiceGroup2AxL,...
                        'Position',radioButtonPos2, 'Visible','on');
                    set(myData.handles.(varAxRChoiceTag), 'Parent',myData.handles.varChoiceGroup2AxR,...
                        'Position',radioButtonPos2, 'Visible','on');
                    radioButtonPos2 = radioButtonPos2 - [0 panelHeight 0 0];
                end
            else
                set(myData.handles.(varPanelTag),'Visible','off');
                set(myData.handles.(varAxLChoiceTag),'Visible','off');
                set(myData.handles.(varAxRChoiceTag),'Visible','off');
            end
        end
        
        % iteratively set the visibility of text and toggle buttons for each single variable
        for varNumber = 1:MAX_YVARS
            varNumStr = num2str(varNumber);
            varChoiceTag = strcat('var',varNumStr,'Choice');
            varPanelTag = strcat('var',varNumStr,'Panel');
            varTextTag = strcat('var',varNumStr,'Text');
            if varNumber <= numVarsY
                set(myData.handles.(varChoiceTag), 'Parent',myData.handles.(varPanelTag),...
                    'Visible','on');
                set(myData.handles.(varTextTag), 'Parent',myData.handles.(varPanelTag),...
                    'Visible','on');
            else
                set(myData.handles.(varChoiceTag), 'Visible','off');
                set(myData.handles.(varTextTag), 'Visible','off');
            end
        end
        
        % initially clear the axis selection radiobuttons
        set(myData.handles.varChoiceGroup1AxL,'SelectedObject',[]);
        set(myData.handles.varChoiceGroup1AxR,'SelectedObject',[]);
        set(myData.handles.varChoiceGroup2AxL,'SelectedObject',[]);
        set(myData.handles.varChoiceGroup2AxR,'SelectedObject',[]);
        
        % determine the currently selected variables for left and right
        % axes
        if strcmpi(currDataType,'Z')
            axLVar = myData.data.axLVarZ;
            axRVar = myData.data.axRVarZ;
        elseif strcmpi(currDataType,'N2')
            axLVar = myData.data.axLVarN2;
            axRVar = myData.data.axRVarN2;
        end
        
        % update the toggle button colors and text in var panels to reflect
        % current Y vars, update the axis selection radiobuttons to reflect
        % selected axes
        for i = 1:length(currVarsY)
            toggleTag = ['var' int2str(i) 'Choice'];
            textTag = ['var' int2str(i) 'Text'];
            yVarProps = myData.data.masterCTDVars.(currDataType).(currVarsY{i});
            % set the toggle button color to match variable color
            set(myData.handles.(toggleTag),'BackgroundColor',...
                yVarProps.color);
            
            % set the toggle button tooltip to describe variable visual properties
            if isempty(yVarProps.markerFaceColor)
                mfc = [NaN NaN NaN];
            else
                mfc = [yVarProps.markerFaceColor(1),yVarProps.markerFaceColor(2),yVarProps.markerFaceColor(3)];
            end
            if isempty(yVarProps.markerEdgeColor)
                mec = [NaN NaN NaN];
            else
                mec = [yVarProps.markerEdgeColor(1),yVarProps.markerEdgeColor(2),yVarProps.markerEdgeColor(3)];
            end
            tt = sprintf(['Color: [%5.4f %5.4f %5.4f]\n',...
                'Line Style: %s\n',...
                'Line Width: %2.1f\n',...
                'Marker Style: %s\n',...
                'Marker Size: %2.1f\n',...
                'Marker Face Color: [%5.4f %5.4f %5.4f]\n',...
                'Marker Edge Color: [%5.4f %5.4f %5.4f]'],...
                yVarProps.color(1),yVarProps.color(2),yVarProps.color(3),...
                yVarProps.lineStyle,...
                yVarProps.lineWidth,...
                yVarProps.marker,...
                yVarProps.markerSize,...
                mfc(1),mfc(2),mfc(3),...
                mec(2),mec(2),mec(3));
            set(myData.handles.(toggleTag),'TooltipString',tt);
            
            % set the toggle button value to match plotted variable
            % preferences
            plottedVars = {};
            if strcmpi(currDataType,'Z')
                plottedVars = myData.data.plottedCTDVarsZY;
            elseif strcmpi(currDataType,'N2')
                plottedVars = myData.data.plottedCTDVarsN2Y;
            end
            if sum(strcmp(plottedVars,currVarsY{i})) > 0
                set(myData.handles.(toggleTag),'Value',1)
            else
                set(myData.handles.(toggleTag),'Value',0)
            end
            
            % revert back to default ui color if button was not pressed
            if get(myData.handles.(toggleTag),'Value') < 1
                disabledColor = get(0,'defaultUicontrolBackgroundColor');
                set(myData.handles.(toggleTag),'BackgroundColor', disabledColor);
            end
            
            % set the variable panel text to match variable name
            set(myData.handles.(textTag),'String',currVarsY{i});
            
            % if only one column of variable panels, enlarge the font
            if numVarsY <= MAX_YVARS/2
                set(myData.handles.(textTag),'fontsize',10);
            else
                set(myData.handles.(textTag),'fontsize',8);
            end
            
            varAxLChoiceTag = strcat('var',num2str(i),'AxLChoice');
            varAxRChoiceTag = strcat('var',num2str(i),'AxRChoice');
            % activate the corresponding radio buttons for matching variables
            if strcmpi(currVarsY{i},axLVar)
                if i <= numChildren
                    set(myData.handles.varChoiceGroup1AxL,'SelectedObject',myData.handles.(varAxLChoiceTag));
                else
                    set(myData.handles.varChoiceGroup2AxL,'SelectedObject',myData.handles.(varAxLChoiceTag));
                end
            end
            if strcmpi(currVarsY{i},axRVar)
                if i <= numChildren
                    set(myData.handles.varChoiceGroup1AxR,'SelectedObject',myData.handles.(varAxRChoiceTag));
                else
                    set(myData.handles.varChoiceGroup2AxR,'SelectedObject',myData.handles.(varAxRChoiceTag));
                end
            end
        end
        
        % if data is empty or all nan, disable corresponding graphics
        % elements
        for i = 1:length(currVarsY)
            toggleTag = ['var' int2str(i) 'Choice'];
            radioLTag = ['var' int2str(i) 'AxLChoice'];
            radioRTag = ['var' int2str(i) 'AxRChoice'];
            textTag = ['var' int2str(i) 'Text'];
            varData = myData.data.masterCTDVars.(currDataType).(currVarsY{i}).data;
            if isempty(varData) || sum(isnan(varData))==length(varData) || isempty(xVarNames)
                % disable toggle button for unavailable variables
                disabledColor = get(0,'defaultUicontrolBackgroundColor');
                set(myData.handles.(toggleTag),'BackgroundColor',disabledColor);
                set(myData.handles.(toggleTag),'Enable','off');
                enabledH(enabledH == myData.handles.(toggleTag)) = []; % remove handle from re-enable list
                set(myData.handles.(toggleTag),'Value',0);
                % set variable text to disabled color
                set(myData.handles.(textTag),'Enable','off');
                enabledH(enabledH == myData.handles.(textTag)) = []; % remove handle from re-enable list
            else
                set(myData.handles.(toggleTag),'Enable','on');
                set(myData.handles.(textTag),'Enable','on');
            end
            clear varData
            if get(myData.handles.(toggleTag),'Value') < 1
                % disable radio buttons whenever toggle button is not
                % pressed
                set(myData.handles.(radioLTag),'Enable','off');
                enabledH(enabledH == myData.handles.(radioLTag)) = []; % remove handle from re-enable list
                set(myData.handles.(radioRTag),'Enable','off');
                enabledH(enabledH == myData.handles.(radioRTag)) = []; % remove handle from re-enable list
            else
                set(myData.handles.(radioLTag),'Enable','on');
                set(myData.handles.(radioRTag),'Enable','on');
            end
        end
        
        % If the selected radiobutton is now disabled, clear selection from
        % button group
        selRadio1L = get(myData.handles.varChoiceGroup1AxL,'SelectedObject');
        selRadio1R = get(myData.handles.varChoiceGroup1AxR,'SelectedObject');
        selRadio2L = get(myData.handles.varChoiceGroup2AxL,'SelectedObject');
        selRadio2R = get(myData.handles.varChoiceGroup2AxR,'SelectedObject');
        if strcmpi(get(selRadio1L,'Enable'),'off') && sum(enabledH == selRadio1L) == 0
            set(myData.handles.varChoiceGroup1AxL,'SelectedObject',[]);
        end
        if strcmpi(get(selRadio1R,'Enable'),'off') && sum(enabledH == selRadio1R) == 0
            set(myData.handles.varChoiceGroup1AxR,'SelectedObject',[]);
        end
        if strcmpi(get(selRadio2L,'Enable'),'off') && sum(enabledH == selRadio2L) == 0
            set(myData.handles.varChoiceGroup2AxL,'SelectedObject',[]);
        end
        if strcmpi(get(selRadio2R,'Enable'),'off') && sum(enabledH == selRadio2R) == 0
            set(myData.handles.varChoiceGroup2AxR,'SelectedObject',[]);
        end
        
        set(myData.handles.varSelectionPanel,'Visible','on');

        guidata(mainFig,myData);
        
        clear myData
        clear currVarsX
        clear currVarsY
        clear currDataType
    end

% preprocess a single CTD file for plotting
    function [ppSuccess] = preprocessCTDFile(fileFullPath)
        ppSuccess = 0;
        
        myData = guidata(mainFig);
        myData.data.currCTDFileFullPath = fileFullPath; % used for updating file info panel and initializing sliders
        guidata(mainFig,myData);
        
        % read in data from CTD file
        [CTDHeader1 CTDData1 CTDHeader2 CTDData2] = readCTDFile_allFormats(fileFullPath);
        
        % check if read fails
        if isempty(CTDHeader1) && isempty(CTDData1) && isempty(CTDHeader2) &&...
                isempty(CTDData2)
            msg = ['File read for ' fileFullPath ' failed!'];
            errordlg(msg, 'File Read Error', 'modal');
            mainLogText = appendLogText(mainLogText, msg,'error');
            return; % quit early if read fails
        end
        
        clear myData
        clear myHandles
        
        clearAllVarData(); % make sure we start with freshly empty data for all CTD variables
        
        myData = guidata(mainFig); % reloads master variables that were cleared
        myHandles = myData.handles;
        
        % if there is a table open, delete it
        dataTableH = findobj('Tag','dataTableFig');
        if ~isempty(dataTableH)
            delete(dataTableH);
        end
        
        % reset/disable axis modification controls
        set(myHandles.pointerXValText,'String','');
        set(myHandles.xMinEdit,'Enable','off');
        set(myHandles.xMaxEdit,'Enable','off');
        
        fileExtension = fileFullPath(end-2:end);
        
        % check if file is an XLS file with two sets of data
        if strcmpi(fileExtension,'xls')
            % insert data into matching Z master variables
            % (Header1 and Data1 will always be Z for XLS files)
            if ~isempty(CTDHeader1) && ~isempty(CTDData1)
                matchHeaderAndInsertData('Z', CTDHeader1, CTDData1);
                % insert lnPAR and Ke into master variable data structure if
                % PAR/PAR_adj is available
                insertKe();
                insertOnePercentDepth();
            end
            
            % insert data into matching N2 master variables
            % (Header2 and Data2 will always be N2 for XLS files)
            if ~isempty(CTDHeader2) && ~isempty(CTDData2)
                matchHeaderAndInsertData('N2', CTDHeader2, CTDData2);
            end
            
            % if non-XLS file
        else
            % check dataType of file (Z or N2) by checking for the presence of
            % "N" in file headers
            if sum(strcmpi(CTDHeader1,'n')) >= 1
                % if file headers contain N, set dataType to N2
                dataType = 'N2';
            else
                % otherwise set to Z
                dataType = 'Z';
            end
            
            % if data type changed
            if ~strcmp(myData.data.currCTDDataType, dataType)
                if strcmp(dataType,'N2')
                    myData.data.currCTDVarsX = myData.data.preferredCTDVarsN2X;
                    myData.data.currCTDVarsY = myData.data.preferredCTDVarsN2Y;
                elseif strcmp(dataType,'Z')
                    myData.data.currCTDVarsX = myData.data.preferredCTDVarsZX;
                    myData.data.currCTDVarsY = myData.data.preferredCTDVarsZY;
                end
            end
            myData.data.currCTDDataType = dataType;
            guidata(mainFig, myData);
            clear myData
            
            if ~isempty(CTDHeader1) && ~isempty(CTDData1)
                % insert data into matching Z or N2 master variable
                matchHeaderAndInsertData(dataType, CTDHeader1, CTDData1);
                
                % insert lnPAR and Ke into master variable data structure if
                % PAR/PAR_adj is available
                insertKe();
                insertOnePercentDepth();
            end
        end
        
        ppSuccess = 1;
        
        % enable axis modification controls
        set(myHandles.xMinEdit,'Enable','on');
        set(myHandles.xMaxEdit,'Enable','on');
        
        % Nested function: match each variable in CTDHeader to a master list based on
        % dataType, and insert the corresponding CTDData column into the
        % master variable structure's "data" field
        function[] = matchHeaderAndInsertData(dataType, CTDHeader, CTDData)
            myData = guidata(mainFig); % load guidata
            
            % iterate through all file headers
            for headerNum=1:length(CTDHeader)
                header = CTDHeader{1,headerNum};
                fieldNames = fieldnames(myData.data.masterCTDVars.(dataType)); % all master variables
                isHeaderMatched = 0; % reset flag to stop searching
                for fieldNum = 1:length(fieldNames) % iterate through all master variables
                    headerAliases = myData.data.masterCTDVars.(dataType).(fieldNames{fieldNum}).aliases; % aliases for master variable
                    for aliasNum = 1:length(headerAliases) % iterate through all aliases
                        if strcmpi(header,headerAliases{aliasNum}) % match found
                            % pre-allocate data value to equal the number
                            % of independent (X) values, like Depth and
                            % Pressure
                            myData.data.masterCTDVars.(dataType).(fieldNames{fieldNum}).data = nan(length(CTDData(:,1)),1);
                            % insert CTD data column into master variable's
                            % data field
                            myData.data.masterCTDVars.(dataType).(fieldNames{fieldNum}).data = CTDData(:,headerNum);
                            isHeaderMatched = 1; % flag to stop searching through master variables
                            break;
                        end
                    end
                    if isHeaderMatched
                        break;
                    end
                end
                if ~isHeaderMatched
                    msg = ['Could not identify the header ',header,' in master list!'];
                    errordlg(msg, 'Header Alias Error', 'modal');
                    mainLogText = appendLogText(mainLogText,msg,'error');
                    % TODO: any special action?
                end
            end
            guidata(mainFig,myData); % save guidata
            clear myData
            clear varsType
        end
    end

% set the data of the current CTD file to the appropriate axes/lines, according to variable
% preferences set on the variable selection panel
    function [] = updatePlot()
        myData = guidata(mainFig);
        myHandles = myData.handles;
        dataType = myData.data.currCTDDataType;
        currVarsX = myData.data.currCTDVarsX;
        currVarsY = myData.data.currCTDVarsY;
        forcedXMin = myData.data.forcedXMin;
        forcedXMax = myData.data.forcedXMax;
        
        % do nothing if there is no current file picks
        if isempty(myData.data.CTDFullFilePicks)
            clear myData
            return;
        end
        
        % show warning message if x-axis data is empty or all NaN
        xData = myData.data.masterCTDVars.(dataType).(currVarsX{1}).data;
        if isempty(myData.data.masterCTDVars.(dataType).(currVarsX{1}).data) ||...
                sum(isnan(xData))==length(xData)
            [~, ~, fileExt] = fileparts(myData.data.CTDFullFilePicks{myData.data.currCTDFileIndex});
            if strcmpi(fileExt,'.xls')
                msg = ['The x-axis variable ' currVarsX{1} ' has no valid values! Try switching the data type.'];
                warndlg(msg, 'No X Values');
                mainLogText = appendLogText(mainLogText,msg,'warning');
                mainLogText = updateLog(mainLogText,consoleBox);
            end
        end
        clear xData
        
        % iterate through all plot-related handles and change line
        % visibility according to variable selection panel state
        fxminFailed = 0;
        fxmaxFailed = 0;
        i = 1;
        while i <= MAX_YVARS
            % get handle for left and right axes
            axLTag = ['ax' num2str(i) 'L'];
            axRTag = ['ax' num2str(i) 'R'];
            axLHandle = myHandles.(axLTag);
            axRHandle = myHandles.(axRTag);
            
            % get handle for left and right lines
            lineLTag = ['h' num2str(i) '_L'];
            lineRTag = ['h' num2str(i) '_R'];
            lineLHandle = myHandles.(lineLTag);
            lineRHandle = myHandles.(lineRTag);
            
            % initially clear all lines and axes (so that any older elements
            % that do not relate to the current variables are cleared)
            set(lineLHandle,'Visible','off');
            set(lineRHandle,'Visible','off');
            set(axLHandle,'Visible','off');
            set(axRHandle,'Visible','off');
            
            % reset the Xlim and Ylim mode to "auto" for all axes
            set(axLHandle,'XLimMode','Auto');
            set(axRHandle,'XLimMode','Auto');
            set(axLHandle,'YLimMode','Auto');
            set(axRHandle,'YLimMode','Auto');
            
            if i <= length(currVarsY)
                % get variable's name
                varTextTag = ['var' num2str(i) 'Text'];
                varTextHandle = myHandles.(varTextTag);
                varText = get(varTextHandle,'String');
                
                yVarProps = myData.data.masterCTDVars.(dataType).(varText);
                xVarProps = myData.data.masterCTDVars.(dataType).(currVarsX{1});
                
                if ~isempty(xVarProps.data) && ~isempty(yVarProps.data)...
                        && sum(isnan(yVarProps.data)) < length(yVarProps.data)
                    % set axes and lines to variable properties
                    set(axLHandle,'YColor', yVarProps.color);
                    set(axLHandle,'XColor', 'k');
                    set(get(axLHandle,'YLabel'), 'String', strcat(currVarsY{i},...
                        ' (', yVarProps.unit, ')'));
                    set(get(axLHandle,'XLabel'), 'String', strcat(currVarsX{1},...
                        ' (', xVarProps.unit, ')'));
                    
                    set(axRHandle,'YColor', yVarProps.color);
                    set(axRHandle,'XColor', 'k');
                    set(get(axRHandle,'YLabel'), 'String', strcat(currVarsY{i},...
                        ' (', yVarProps.unit, ')'));
                    set(get(axRHandle,'XLabel'), 'String', strcat(currVarsX{1},...
                        ' (', xVarProps.unit, ')'));
                    
                    set(lineLHandle,'Color', yVarProps.color);
                    set(lineLHandle,'LineStyle', yVarProps.lineStyle);
                    if ~isempty(yVarProps.lineWidth)
                        set(lineLHandle,'LineWidth', yVarProps.lineWidth);
                    end
                    set(lineLHandle,'Marker', yVarProps.marker);
                    set(lineLHandle,'YData', yVarProps.data);
                    set(lineLHandle,'XData', xVarProps.data);
                    
                    set(lineRHandle,'Color', yVarProps.color);
                    set(lineRHandle,'LineStyle', yVarProps.lineStyle);
                    if ~isempty(yVarProps.lineWidth)
                        set(lineLHandle,'LineWidth', yVarProps.lineWidth);
                    end
                    set(lineRHandle,'Marker', yVarProps.marker);
                    set(lineRHandle,'YData', yVarProps.data);
                    set(lineRHandle,'XData', xVarProps.data);
                    
                    % check state of variable toggle button
                    varChoiceTag = ['var' num2str(i) 'Choice'];
                    varChoiceHandle = myHandles.(varChoiceTag);
                    isLineActive = get(varChoiceHandle,'value');
                    
                    % set visibility of line
                    if isLineActive
                        % set visible on
                        set(lineLHandle,'Visible','on');
                        set(lineRHandle,'Visible','on');
                        
                        % set automatic max of x axis to be 101% of max x data
                        xlimL = get(axLHandle,'Xlim');
                        xlimR = get(axRHandle,'Xlim');
                        xlimL(2) = 1.01*max(xVarProps.data);
                        xlimR(2) = 1.01*max(xVarProps.data);
                        
                        % TODO: force xlim if specified
                        % if forced xlim is not valid, revert to default
                        if ~isempty(myData.data.forcedXMin)
                            % xmin must < max of data
                            if forcedXMin < max(xVarProps.data)
                                xlimL(1) = forcedXMin;
                                xlimR(1) = forcedXMin;
                            else
                                fxminFailed = 1;
                            end
                        end
                        if ~isempty(myData.data.forcedXMax)
                            % xmax must > min of data
                            if forcedXMax > min(xVarProps.data)
                                xlimL(2) = forcedXMax;
                                xlimR(2) = forcedXMax;
                            else
                                fxmaxFailed = 1;
                            end
                        end
                        
                        % set final xlim
                        set(axLHandle,'Xlim', [xlimL(1) xlimL(2)]);
                        set(axRHandle,'Xlim', [xlimR(1) xlimR(2)]);
                        
                        % set forced ylim, if specified
                        if ~isempty(myData.data.forcedYLims)
                            fymin = myData.data.forcedYLims.(dataType).(varText).FYMin;
                            fymax = myData.data.forcedYLims.(dataType).(varText).FYMax;
                            ylim = get(axLHandle,'Ylim');
                            if ~isempty(fymin) && ~isempty(fymax)
                                set(axLHandle,'Ylim', [fymin fymax]);
                            elseif ~isempty(fymin)
                                set(axLHandle,'Ylim', [fymin ylim(2)]);
                            elseif ~isempty(fymax)
                                set(axLHandle,'Ylim', [ylim(1) fymax]);
                            end
                            ylim = get(axLHandle,'Ylim');
                            set(axRHandle,'Ylim',[ylim(1) ylim(2)]); % right axis always identical to left axis
                        end
                    end
                end
            end
            i = i + 1;
        end
        
        % display warning dlg if forced xlim could not be enforced
        if fxminFailed
            msg = 'The forced X min is greater than the maximum of the data. Default x min will be displayed.';
            mainLogText = appendLogText(mainLogText,msg,'warning');
        end
        if fxmaxFailed
            msg = 'The forced X max is less than the minimum of the data. Default x max will be displayed.';
            mainLogText = appendLogText(mainLogText,msg,'warning');
        end
        
        if ~isempty(currVarsY)
            if exist('xlimL','var')
                set(myHandles.xMinDisp,'String',num2str(round(xlimL(1)*100)/100));
                set(myHandles.xMaxDisp,'String',num2str(round(xlimL(2)*100)/100));
            end
        end
        
        % set visibility of axes by checking radio button selection
        function[] = setAxisVis(axSelPanel)
            % either one column or two columns of axis selection panel(s)
            % will be visible
            if strcmp(get(axSelPanel,'Visible'),'on')
                % check children of axis selection panel, which will be the
                % axis selection button groups for left and right axes
                axSelBtnGroups = get(axSelPanel,'Children');
                % find the selected object/axis in button group and make the
                % corresponding axis visible
                tag1 = get(axSelBtnGroups(1),'Tag'); % check children for L or R axis tag
                if strcmpi(tag1(end),'L')
                    selAxL = get(axSelBtnGroups(1), 'SelectedObject');
                    selAxR = get(axSelBtnGroups(2), 'SelectedObject');
                else
                    selAxL = get(axSelBtnGroups(2), 'SelectedObject');
                    selAxR = get(axSelBtnGroups(1), 'SelectedObject');
                end
                
                % set visibility of axis
                if ~isempty(selAxL)
                    varNum = getTagVarNum(selAxL);
                    axLTag = ['ax' num2str(varNum) 'L'];
                    axLHandle = myHandles.(axLTag);
                    set(axLHandle,'Visible','on');
                end
                
                if ~isempty(selAxR)
                    varNum = getTagVarNum(selAxR);
                    axRTag = ['ax' num2str(varNum) 'R'];
                    axRHandle = myHandles.(axRTag);
                    set(axRHandle,'Visible','on');
                end
            end
        end
        
        setAxisVis(myHandles.axSelPanel1);
        setAxisVis(myHandles.axSelPanel2);
        
        % update stem plot(s) of 1% depth
        opdepth = myData.data.masterCTDVars.Z.OPDep.data;
        opdepth_adj = myData.data.masterCTDVars.Z.OPDep_adj.data;
        
        % find an active line and set the stem plot's xlim to that line's
        % axes xlim
        currXLim = [0 1]; % if no active lines, 1% depth should not be visible
        for i = 1:length(currVarsY)
            varChoiceTag = ['var' num2str(i) 'Choice'];
            varChoiceHandle = myHandles.(varChoiceTag);
            isLineActive = get(varChoiceHandle,'value');
            if isLineActive
                axH = ['ax',num2str(i),'L'];
                currXLim = get(myData.handles.(axH),'XLim');
                break;
            end
        end
        set(myData.handles.axOPD, 'XLim',currXLim);
        
        if ~isempty(opdepth)
            set(myData.handles.stemOPD, 'XData',[opdepth(1) 0], 'YData',[1 0],...
                'LineWidth',2, 'LineStyle','--');
        else
            set(myData.handles.stemOPD, 'XData',[0 0], 'YData',[0 0]);
        end
        if ~isempty(opdepth_adj)
            set(myData.handles.stemOPD_adj, 'XData',[opdepth_adj(1) 0], 'YData',[1 0],...
                'LineWidth',2, 'LineStyle',':');
        else
            set(myData.handles.stemOPD_adj, 'XData',[0 0], 'YData',[0 0]);
        end
        guidata(mainFig,myData);
        clear myData
        clear myHandles
    end

% initialize parameters of the chronological and sequential sliders
% based on list of picked files
    function[] = initSlider()
        myData = guidata(mainFig);
        fullFilePicks = myData.data.CTDFullFilePicks;
        myHandles = myData.handles;
        
        % if only one file loaded, no need for slider
        if length(fullFilePicks) > 1
            DataTime = datenum(0*(1:length(fullFilePicks))); % pre-allocate
            
            % check file type and get datenum for each file
            for fileNum = 1:length(fullFilePicks)
                fileFullPath = fullFilePicks{fileNum};
                [~, fileDateNum] = identifyCTDDate(fileFullPath);
                DataTime(fileNum) = fileDateNum;
            end
            
            % chronoValues stores chronologically sorted array of datenums
            % chronoHash stores the fullfilepick indices in file chronological order
            [chronoValues chronoHash] = sort( DataTime );
            myData.data.chronoValues = chronoValues;
            myData.data.chronoHash = chronoHash;
            
            % sequenValues stores sequentially ordered file indices
            % sequenHash stores fullfilepicks indices in file sequential order
            sequenValues = 1:length(fullFilePicks);
            sequenHash = sequenValues;
            myData.data.sequenValues = sequenValues;
            myData.data.sequenHash = sequenHash;
            
            % ------------------------------------------------------------
            % set up chronological axis and slider
            sliderAxChrono = myData.handles.sliderAxChrono;
            sliderLineChrono_red = myData.handles.sliderLineChrono_red;
            sliderChrono = myData.handles.sliderChrono;
            
            %             if chronoValues(1) ~= chronoValues(end)
            %                 % max of 6 ticks
            %                 if length(chronoValues) <= 6
            %                     numTicks = length(chronoValues);
            %                 else
            %                     numTicks = 6;
            %                 end
            %                 set(sliderAxChrono,'XTick',linspace(chronoValues(1),chronoValues(end),numTicks));
            %                 datetick(sliderAxChrono,'x','mmm-dd-yy','keepticks');
            %
            %                 % plot the chronological station samples as dots on axis
            %                 set(sliderLineChrono_red, 'XData',chronoValues, 'YData',0*chronoValues);
            %
            %                 % set up slider
            %                 set(sliderChrono, 'min', chronoValues(1));
            %                 set(sliderChrono, 'max', chronoValues(end));
            %                 timelineRange = chronoValues(end) - chronoValues(1) + 1;
            %                 set(sliderChrono, 'sliderstep', [1/timelineRange 1/timelineRange]);
            %             end
            
            % set up axis
            dateValues = chronoValues;
            chronoValues = 1:length(fullFilePicks);
            myData.data.chronoValues = chronoValues;
            % max of 6 ticks
            if length(chronoValues) <= 6
                numTicks = length(chronoValues);
            else
                numTicks = 6;
            end
            chronoTicks = linspace(chronoValues(1),chronoValues(end),numTicks);
            set(sliderAxChrono,'XTick',chronoTicks);
            xlabels_datenum = zeros(1, numTicks);
            % set custom x tick labels by averaging file dates closest to
            % the tick position
            for i = 1:numTicks
                xlabels_datenum(i) = (dateValues(floor(chronoTicks(i))) + dateValues(ceil(chronoTicks(i))))/2;
            end
            xlabels = datestr(xlabels_datenum,'mmm-dd-yy');
            set(sliderAxChrono,'XTickLabel',xlabels);
            set(sliderAxChrono,'Xlim',[chronoValues(1) chronoValues(end)]);
            set(sliderLineChrono_red, 'XData',chronoValues, 'YData',0*chronoValues);
            
            % set up slider
            set(sliderChrono, 'min', chronoValues(1));
            set(sliderChrono, 'max', chronoValues(end));
            timelineRange = chronoValues(end) - chronoValues(1) + 1;
            set(sliderChrono, 'sliderstep', [1/timelineRange 1/timelineRange]);
            %                 msg = ['Multiple picked files at both ends of chronological',...
            %                     ' list are from the same date. This causes the slider and/or timeline to malfunction.',...
            %                     ' The timeline will retain its chronological ordering, but',...
            %                     ' file intervals will now be evenly spaced.'];
            %                 mainLogText = appendLogText(mainLogText,msg,'warning');

            % ------------------------------------------------------------
            % set up sequential axis and slider
            sliderAxSequen = myData.handles.sliderAxSequen;
            sliderLineSequen_red = myData.handles.sliderLineSequen_red;
            sliderSequen = myData.handles.sliderSequen;
            
            numTicks = length(fullFilePicks);
            
            % set up axis
            set(sliderAxSequen,'XTickLabelMode','auto')
            % limit to max of 40 XTicks
            set(sliderAxSequen,'XTick',1:ceil(numTicks/40):numTicks,'Xlim',[1 numTicks]);
            
            % plot the sequential station samples as dots on axis
            set(sliderLineSequen_red, 'XData',sequenValues, 'YData',0*sequenValues);
            
            % set up slider
            set(sliderSequen, 'min', sequenValues(1));
            set(sliderSequen, 'max', sequenValues(end));
            sequenceRange = sequenValues(end) - sequenValues(1) + 1;
            set(sliderSequen, 'sliderstep', [1/sequenceRange 1/sequenceRange]);
            
            % make slider mode toggle buttons visible
            set(myHandles.flistViewMode,'Visible','on');
        end
        
        guidata(mainFig, myData);
        clear fullFilePicks
        clear myData
    end

% refreshes the currently active slider type's components and
% parameters based on index in file list
    function[] = updateSlider()
        myData = guidata(mainFig);
        fullFilePicks = myData.data.CTDFullFilePicks;
        if length(fullFilePicks) > 1
            sliderMode = myData.data.flistViewMode;
            currFileIndex = myData.data.currCTDFileIndex;
            
            if strcmp(sliderMode,'chronoMode')
                sliderLineChrono_blue = myData.handles.sliderLineChrono_blue;
                sliderChrono = myData.handles.sliderChrono;
                chronoValues = myData.data.chronoValues;
                
                % set position of blue dot on axis
                set(sliderLineChrono_blue, 'XData', chronoValues(currFileIndex),...
                    'YData', 0);
                
                % sets position of slider bar to correspond to current file
                set(sliderChrono,'value',chronoValues(currFileIndex));
                myData.data.oldSliderValue = get(sliderChrono, 'value'); % need this for slider callback calculation
                guidata(mainFig,myData);
                clear myData
                
                toggleSliderVis('chrono'); % toggle visibility of chrono slider only
                
            elseif strcmp(sliderMode,'sequenMode')
                sliderLineSequen_blue = myData.handles.sliderLineSequen_blue;
                sliderSequen = myData.handles.sliderSequen;
                sequenValues = myData.data.sequenValues;
                
                % set position of blue dot on axis
                set(sliderLineSequen_blue, 'XData', sequenValues(currFileIndex),...
                    'YData', 0);
                
                % sets position of slider bar to correspond to current file
                set(sliderSequen,'value',sequenValues(currFileIndex));
                myData.data.oldSliderValue = get(sliderSequen, 'value'); % need this for slider callback calculation
                guidata(mainFig,myData);
                clear myData
                
                toggleSliderVis('sequen'); % toggle visibility of sequen slider only
            end
        else
            toggleSliderVis('default'); % toggle visibility of default slider only
        end
    end

% detects presence of PAR data, calculates lnPAR and Ke
% automatically, and inserts into master variable data structure
    function[] = insertKe()
        myData = guidata(mainFig);
        par = myData.data.masterCTDVars.Z.PAR.data;
        par_adj = myData.data.masterCTDVars.Z.PAR_adj.data;
        
        ke = myData.data.masterCTDVars.Z.Ke.data;
        ke_adj = myData.data.masterCTDVars.Z.Ke_adj.data;
        
        depth = myData.data.masterCTDVars.Z.Depth.data;
        
        if isempty(ke)
            % check if at least 10 good data points are present
            if sum(isfinite(par)) > 9
                % calculate lnPAR
                lnPAR_temp = log(par);
                % interpolate lnPAR data
                lnPAR_intp = interpolateVector(lnPAR_temp);
                % extrapolate lnPAR data
                lnPAR_extp = extrapolateVector(depth, lnPAR_intp);
                % smooth lnPAR data
                lnPAR_smooth = fivePtSmooth(lnPAR_extp);
                % calculate ke
                ke = calcKe(depth, lnPAR_smooth);
                % extrapolate ke
                ke_extp = extrapolateVector(depth, ke);
                % insert lnPAR and Ke into master data structure
                myData.data.masterCTDVars.Z.lnPAR.data = lnPAR_smooth;
                myData.data.masterCTDVars.Z.Ke.data = ke_extp;
                msg = 'lnPAR and Ke was calculated and added to available variables.';
                mainLogText = appendLogText(mainLogText,msg,'success');
            else
                msg = 'Not enough PAR values to calculate Ke.';
                mainLogText = appendLogText(mainLogText,msg,'warning');
            end
        end
        
        if isempty(ke_adj)
            % check if at least 10 good data points are present
            if sum(isfinite(par_adj)) > 9
                % calculate lnPAR
                lnPAR_temp = log(par_adj);
                % interpolate lnPAR data
                lnPAR_intp = interpolateVector(lnPAR_temp);
                % extrapolate lnPAR data
                lnPAR_extp = extrapolateVector(depth, lnPAR_intp);
                % smooth lnPAR data
                lnPAR_smooth = fivePtSmooth(lnPAR_extp);
                % calculate ke
                ke = calcKe(depth, lnPAR_smooth);
                % extrapolate ke
                ke_extp = extrapolateVector(depth, ke);
                % insert lnPAR and Ke into master data structure
                myData.data.masterCTDVars.Z.lnPAR_adj.data = lnPAR_smooth;
                myData.data.masterCTDVars.Z.Ke_adj.data = ke_extp;
                msg = 'lnPAR_adj and Ke_adj was calculated and added to available variables.';
                mainLogText = appendLogText(mainLogText,msg,'success');
            else
                msg = 'Not enough PAR values to calculate Ke_adj.';
                mainLogText = appendLogText(mainLogText,msg,'warning');
            end
        end
        
        guidata(mainFig,myData);
        clear myData
        
        % calculates Ke from depth and PAR data
        function[KeData] = calcKe(depthData, lnPARData)
            KeData = nan(length(lnPARData),1); % pre-allocate
            % Use three point moving average slopes to approximate Ke
            for i = 2:length(lnPARData)-1
                Ke1 = (lnPARData(i,1) - lnPARData(i-1,1))/(depthData(i,1) - depthData(i-1,1));
                Ke2 = (lnPARData(i+1,1) - lnPARData(i,1))/(depthData(i+1,1) - depthData(i,1));
                KeVal = (Ke1 + Ke2)/2;
                KeData(i,1) = KeVal;
            end
        end
        
        function[data_intp] = interpolateVector(data)
            % For a vector with non-finite values, fill in middle non-finite values with interpolated
            % values
            % Non-finite means NaN, -Inf, +Inf
            
            % Default result if no change required
            data_intp = data;
            
            % Only need to interpolate if not all finite or all non-finite
            if ~((sum(isfinite(data)) == length(data)) || (sum(isfinite(data)) == 0))
                isNum = isfinite(data); % construct vector of boolean values indicating value is finite
                headInd = find(isNum, 1, 'first'); % boundaries of values to be interpolated
                tailInd = find(isNum, 1, 'last');
                
                % only look at middle section of matrix bookended by numerical values
                data_mid = data(headInd:tailInd);
                
                if ~isempty(data_mid)
                    bd = ~isfinite(data_mid);
                    
                    gd = find(~bd);
                    
                    % For middle rows, replace non-finite values with interpolation
                    % between bookends
                    data_mid(bd) = interp1(gd, data_mid(gd), find(bd)); % use 1D interpolation
                    data(headInd:tailInd) = data_mid;
                    data_intp = data;
                end
            end
        end
        
        function[yData_extp] = extrapolateVector(xData, yData)
            % Fit polynomial to data and use polynomial to fill in missing
            % extremes of data
            isNum = isfinite(yData);
            % range of good values
            headInd = find(isNum, 1, 'first');
            tailInd = find(isNum, 1, 'last');
            badIndexes = find(~isNum);
            yData_extp = yData;
            if sum(badIndexes) > 0
                % that best fit good data at top and bottom depths
                % worst case: 10 good data values at middle
                % preferred: 20 good data values (5 meters) from top and from bottom
                % TODO: how should we try to linearly regress in the cases of
                % shade?
                minRange = 10;
                prefRange = 20;
                range = prefRange;
                if (headInd + prefRange - 1) > tailInd
                    range = minRange;
                end
                % use centering and scaling algorithm for polyfit (output
                % [p s mu]) to improve polynomial and fitting algorithm
                [pTop, ~, muTop] = polyfit(xData(headInd:headInd+range-1),yData(headInd:headInd+range-1), 3);
                [pBot, ~, muBot] = polyfit(xData(tailInd-range+1:tailInd),yData(tailInd-range+1:tailInd), 3);
                % fill in top missing values by extrapolating from fitted polynomial
                yData_extp(badIndexes(badIndexes < headInd)) = polyval(pTop,xData(badIndexes(badIndexes < headInd)),[],muTop);
                % fill in bottom missing values by extrapolating from fitted polynomial
                yData_extp(badIndexes(badIndexes > tailInd)) = polyval(pBot,xData(badIndexes(badIndexes > tailInd)),[],muBot);
            end
        end
        
        % smooth data via five point moving average
        function[yData_smooth] = fivePtSmooth(yData)
            isNum = isfinite(yData);
            % find range of good values
            headInd = find(isNum, 1, 'first');
            tailInd = find(isNum, 1, 'last');
            yData_smooth = yData;
            % replace middle values with 5 point moving averages
            for i = (headInd+2):(tailInd-2)
                average = (yData(i-2)+yData(i-1)+yData(i)+yData(i+1)+yData(i+2))/5;
                yData_smooth(i) = average;
            end
        end
    end

% Calculate 1% depth
    function[] = insertOnePercentDepth()
        myData = guidata(mainFig);
        depth = myData.data.masterCTDVars.Z.Depth.data;
        ke = myData.data.masterCTDVars.Z.Ke.data;
        ke_adj = myData.data.masterCTDVars.Z.Ke_adj.data;
        opdep = myData.data.masterCTDVars.Z.OPDep.data;
        opdep_adj = myData.data.masterCTDVars.Z.OPDep_adj.data;
        
        if size(depth,1) > size(depth,2)
            depth = depth';
        end
        
        if isempty(opdep)
            if isempty(ke)
                msg = 'No 1% depth calculated: no Ke data available.';
                mainLogText = appendLogText(mainLogText,msg,'warning');
            else
                if size(ke,1) > size(ke,2)
                    ke = ke';
                    onePercentDepth = calc1PD(depth, ke);
                    if isempty(onePercentDepth)
                        myData.data.masterCTDVars.Z.OPDep.data = 9999;
                        msg = 'No 1% depth calculated from Ke: value appears to be beyond maximum depth of data.';
                        mainLogText = appendLogText(mainLogText,msg,'warning');
                    else
                        msg = '1% depth was calculated from Ke and added to available variables.';
                        mainLogText = appendLogText(mainLogText,msg,'success');
                        myData.data.masterCTDVars.Z.OPDep.data = onePercentDepth;
                    end
                end
            end
        end
        
        if isempty(opdep_adj)
            if isempty(ke_adj)
                msg = 'No 1% depth calculated: no Ke_adj data available.';
                mainLogText = appendLogText(mainLogText,msg,'warning');
            else
                if size(ke_adj,1) > size(ke_adj,2)
                    ke_adj = ke_adj';
                    onePercentDepth = calc1PD(depth, ke_adj);
                    if isempty(onePercentDepth)
                        myData.data.masterCTDVars.Z.OPDep_adj.data = 9999;
                        msg = 'No 1% depth calculated from Ke_adj: value appears to be beyond maximum depth of data.';
                        mainLogText = appendLogText(mainLogText,msg,'warning');
                    else
                        msg = '1% depth was calculated from Ke_adj and added to available variables.';
                        mainLogText = appendLogText(mainLogText,msg,'success');
                        myData.data.masterCTDVars.Z.OPDep_adj.data = onePercentDepth;
                    end
                end
            end
        end
        
        guidata(mainFig, myData);
        clear myData
        
        function[onePercentDepth] = calc1PD(depth, ke)
            %Calculate the first rectangular area between 0m and 0.25m
            firstArea = ke(1) * depth(1);
            
            keSums = ke(1:end-1) + ke(2:end);
            depthIntervals = depth(2:end) - depth(1:end-1);
            
            %Calculate subsequent trapezoidal areas
            keAreas = (keSums .* depthIntervals)/2;
            %First area and trapezoidal areas combined
            keAreas = [firstArea keAreas];
            
            % Binary search for cumulative sum of areas that is closest to
            % ln(1%)
            startInd = 1;
            endInd = length(keAreas);
            currInd = ceil(endInd/2);
            zeroInd = [];
            leftZeroInd = [];
            rightZeroInd = [];
            while currInd <= length(keAreas) && currInd >= 1
                cumSumArea = keAreas(1:currInd) * ones(currInd,1);
                diff = cumSumArea - log(0.01);
                if diff == 0
                    zeroInd = currInd;
                    break;
                elseif diff > 0
                    startInd = currInd;
                    currInd = ceil((currInd + endInd)/2);
                else % diff < 0
                    endInd = currInd;
                    currInd = floor((startInd + currInd)/2);
                end
                if startInd == endInd % zero is beyond scope of data
                    zeroInd = [];
                    break;
                end
                if currInd == startInd % zero is between data points
                    cumSumArea = keAreas(1:endInd) * ones(endInd,1);
                    diff = cumSumArea - log(0.01);
                    if diff < 0
                        leftZeroInd = startInd;
                        rightZeroInd = endInd;
                        break;
                    else
                        continue;
                    end
                end
                if currInd == endInd % zero is between data points
                    cumSumArea = keAreas(1:startInd) * ones(startInd,1);
                    diff = cumSumArea - log(0.01);
                    if diff > 0
                        leftZeroInd = startInd;
                        rightZeroInd = endInd;
                        break;
                    else
                        continue;
                    end
                end
            end
            
            if ~isempty(zeroInd)
                onePercentDepth = depth(zeroInd);
            elseif ~isempty(leftZeroInd) && ~isempty(rightZeroInd)
                onePercentDepth = (depth(leftZeroInd) + depth(rightZeroInd))/2;
            else
                onePercentDepth = [];
            end
        end
    end

% updates a console output box by pushing a message to the top of the
% message stack
    function[logText] = appendLogText(logText, text,type)
        % Parse the message type and prepare the HTML message segment
        if nargin<3,  type='s';  end
        if strcmpi(type(1:2),'p_')
            % "p_" type indicates new paragraph
            % p_ctd = star symbols around file name
            % p_deck = sun symbols around file name
            % p_master = white sun symbols around file name
            switch lower(type(3))
                case 'c'
                    icon = '&#9733;';
                case 'd'
                    icon = '&#9728';
                case 'm'
                    icon = '&#9788';
                otherwise
                    icon = '';
            end
            newText = ['<p>',icon,' ',text,' ',icon,'<br>'];
        else
            % error = error img before line
            % warning = warning img before line
            % success = success img before line
            switch lower(type(1))
                case 'e',  icon = 'error.png';      color='8B0000';
                case 'w',  icon = 'warning.png';    color='CC7000';
                otherwise, icon = 'success.png';    color='006400';
            end
            if exist(icon,'file')
                iconTxt =['<img src="file:///',which(icon),'" height=16 width=16>'];
                msgTxt = ['&nbsp;<font color=',color,'>',text,'</font>'];
                newText = [iconTxt,msgTxt,'<br>'];
            else
                msgTxt = ['&nbsp;<font color=',color,'>',text,'</font>'];
                newText = [msgTxt,'<br>'];
            end
        end
        
        % Append text segment to bottom of accumulated text and save
        logText = [logText newText];
    end

% displays the messages accumulated so far to the given output editbox
    function[logText] = updateLog(logText, consoleH)
        jScrollPane = findjobj(consoleH); % findjobj is the time bottleneck, takes tenths of a second
        jViewPort = jScrollPane.getViewport;
        jEditbox = jViewPort.getComponent(0);
        % Ensure we have an HTML-ready editbox
        HTMLclassname = 'javax.swing.text.html.HTMLEditorKit';
        if ~isa(jEditbox.getEditorKit,HTMLclassname)
            jEditbox.setContentType('text/html');
        end
        % Place accumulated log text at bottom of the editbox
        currentHTML = char(jEditbox.getText);
        currentHTML = strrep(currentHTML,'</body>',logText);
        jEditbox.setText(currentHTML); % missing ending tags are automatically added i.e. </p>, </body>
        endPosition = jEditbox.getDocument.getLength;
        jEditbox.setCaretPosition(endPosition);
        
        % reset accumulated log text
        logText = '';
    end

% update components of y axis modification panel
    function[] = updateYModPanel()
        myData = guidata(mainFig);
        currVarsY = myData.data.currCTDVarsY;
        % populate ylim menu with names corresponding to pushed toggle buttons
        % append asterix if not pushed/disabled
        if isempty(currVarsY)
            set(yAxMenu,'String','None','Enable','off');
            set(myData.handles.yMinDisp,'String','');
            set(myData.handles.yMaxDisp,'String','');
            set(myData.handles.yMinEdit,'String','','Enable','off');
            set(myData.handles.yMaxEdit,'String','','Enable','off');
            set(myData.handles.scanYLim,'Enable','off');
            set(myData.handles.autoYLim,'Enable','off');
            set(myData.handles.saveYLim,'Enable','off');
            set(myData.handles.loadYLim,'Enable','off');
        else
            % set yaxmenu list
            ylimList = cell(1,length(currVarsY)); % pre-allocate
            for i = 1:length(currVarsY)
                toggleTag = ['var' int2str(i) 'Choice'];
                textTag = ['var' int2str(i) 'Text'];
                varName = get(myData.handles.(textTag),'String');
                if get(myData.handles.(toggleTag),'Value') == 1
                    ylimList{i} = ['Ax ',int2str(i),' - ',varName];
                else
                    ylimList{i} = ['Ax ',int2str(i),' - ',varName,'*'];
                end
            end
            mval = get(yAxMenu,'Value');
            if mval > length(ylimList)
                set(yAxMenu,'Value',1);
                mval = 1;
            end
            set(yAxMenu,'String',ylimList,'Enable','on');
            % set ylim display
            mlist = get(yAxMenu,'String');
            mstr = mlist{mval};
            axNumCell = regexp(mstr, '\d+', 'match'); % regexp match returns cell array
            axNum = str2double(axNumCell{1,1}); % convert str to number
            axH = ['ax' int2str(axNum) 'L'];
            axYlim = get(myData.handles.(axH),'Ylim');
            if  strcmp(mstr(end),'*')
                set(myData.handles.yMinDisp,'String','');
                set(myData.handles.yMaxDisp,'String','');
            else
                set(myData.handles.yMinDisp,'String',num2str(axYlim(1)));
                set(myData.handles.yMaxDisp,'String',num2str(axYlim(2)));
            end
            % set forced ylim display
            currDataType = myData.data.currCTDDataType;
            textTag = ['var' int2str(axNum) 'Text'];
            varName = get(myData.handles.(textTag),'String');
            fymin = myData.data.forcedYLims.(currDataType).(varName).FYMin;
            fymax = myData.data.forcedYLims.(currDataType).(varName).FYMax;
            set(myData.handles.yMinEdit,'Enable','on');
            set(myData.handles.yMaxEdit,'Enable','on');
            if isempty(fymin)
                set(myData.handles.yMinEdit,'String','Auto');
            else
                set(myData.handles.yMinEdit,'String',num2str(fymin));
            end
            if isempty(fymax)
                set(myData.handles.yMaxEdit,'String','Auto');
            else
                set(myData.handles.yMaxEdit,'String',num2str(fymax));
            end
            set(myData.handles.scanYLim,'Enable','on');
            set(myData.handles.autoYLim,'Enable','on');
            set(myData.handles.saveYLim,'Enable','on');
            set(myData.handles.loadYLim,'Enable','on');
        end
        clear myData
    end

% format imported data and header so they are ready to be written to file
    function[data, header, formatSpec] = prepDataForWrite()
        data = [];
        header = {};
        formatSpec = '';
        
        myData = guidata(mainFig);
        currDataType = myData.data.currCTDDataType;
        currVarsX = myData.data.currCTDVarsX;
        
        masterVars = myData.data.masterCTDVars.(currDataType);
        xData = masterVars.(currVarsX{1}).data;
        
        if ~isempty(xData)
            % pre-allocate
            varNames = fieldnames(masterVars);
            header = cell(length(varNames),1);
            data = NaN(length(xData), length(varNames));
            formatSpecCell = cell(1,length(varNames)+1);
            for varNum = 1:length(varNames)
                varProps = masterVars.(varNames{varNum});
                varData = varProps.data;
                if ~isempty(varData) % only write non-empty data fields
                    if size(varData,1) > size(varData,2) % make sure vardata is in a column
                        data(:,varNum) = varData;
                    else
                        data(:,varNum) = varData';
                    end
                    header{varNum, 1} = varProps.exportName;
                    formatSpecCell{varNum} = [varProps.exportFormat ' '];
                end
            end
            % Append newline character to formatSpec
            formatSpecCell{end} = '\n';
            
            % trim empty cells from header and formatSpecCell and nan
            % columns from data
            header(cellfun('isempty',header)) = [];
            formatSpecCell(cellfun('isempty',formatSpecCell)) = [];
            data(:,isnan(data(1,:))) = [];
            
            % remove extra whitespace from last formatSpec
            formatSpecCell{end-1} = strrep(formatSpecCell{end-1},' ','');
            
            % convert formatSpec into string
            formatSpec = cell2mat(formatSpecCell);
            
            data = num2cell(data);
            data = data';
        end
        
        clear myData
        clear masterVars
    end

% save row wise data with header row to a specified txt file
    function[success] = writeDataToText(data, header, formatSpec, destFullPath)
        success = 0;
        error(nargchk(4, 4, nargin, 'struct'))
        if isempty(data)
            msg = 'Data save error: no data available';
            mainLogText = appendLogText(mainLogText,msg,'error');
        else
            fid = fopen(destFullPath,'w+');
            % printing header
            fprintf(fid,'%11s ',header{1:end-1});
            fprintf(fid,'%11s\n',header{end});
            % printing the data
            fprintf(fid,formatSpec,data{:});
            fclose(fid);
            success = 1;
        end
    end


%% Minor helper functions

% clears the data field of all master CTD variable structures
% called when pre-processing a new file
    function [] = clearAllVarData()
        myData = guidata(mainFig);
        fieldNames = fieldnames(myData.data.masterCTDVars.Z);
        % iteratively set each Z variable's "data" field to empty array
        for fieldNum = 1:length(fieldNames)
            myData.data.masterCTDVars.Z.(fieldNames{fieldNum}).data = [];
        end
        fieldNames = fieldnames(myData.data.masterCTDVars.N2);
        % iteratively set each N2 variable's "data" field to empty array
        for fieldNum = 1:length(fieldNames)
            myData.data.masterCTDVars.N2.(fieldNames{fieldNum}).data = [];
        end
        guidata(mainFig, myData); % save everything in guidata
        clear myData; % clear temp variable
    end

% identify the file name, station name, and sample datenum of a CTD file,
% given the full path of the file
    function [stationName sampleDateNum] = getCTDFileInfo(fileFullPath)
        stationName = 'N/A';
        sampleDateNum = [];
        try
            stationName = identifyCTDStation(fileFullPath);
            [~, sampleDateNum] = identifyCTDDate(fileFullPath);
        catch errorObj
            errordlg(getReport(errorObj,'extended','hyperlinks','off'),'Error', 'modal');
            mainLogText = appendLogText(mainLogText,'Could not identify CTD station and/or date info.','error');
        end
    end

% get the variable number in a variable-related uicontrol tag
    function [varNum] = getTagVarNum(objHandle)
        if isempty(objHandle)
            varNum = [];
        else
            objTag = get(objHandle,'Tag');
            varNumCell = regexp(objTag, '\d+', 'match'); % regexp match returns cell array
            varNum = str2double(varNumCell{1,1}); % convert cell to number
        end
    end

% toggle between visibility of the default, sequential, and
% chronological slider components
    function [] = toggleSliderVis(sliderType)
        myData = guidata(mainFig);
        myHandles = myData.handles;
        if strcmp(sliderType,'default')
            % set toggle buttons invisible
            set(myHandles.flistViewMode,'Visible','off');
            % set chrono slider components invisible
            setSliderChronoVis(0);
            % set sequen slider components invisible
            setSliderSequenVis(0);
            % set default slider components visible
            setSliderDefaultVis(1);
        elseif strcmp(sliderType,'chrono')
            % set toggle buttons visible
            set(myHandles.flistViewMode,'Visible','on');
            % set chrono slider components invisible
            setSliderChronoVis(1);
            % set sequen slider components invisible
            setSliderSequenVis(0);
            % set default slider components visible
            setSliderDefaultVis(0);
        elseif strcmp(sliderType,'sequen')
            % set toggle buttons visibility
            if myData.data.chronoValues(1) ~= myData.data.chronoValues(end)
                set(myHandles.flistViewMode,'Visible','on');
            else
                set(myHandles.flistViewMode,'Visible','off');
            end
            % set chrono slider components invisible
            setSliderChronoVis(0);
            % set sequen slider components invisible
            setSliderSequenVis(1);
            % set default slider components visible
            setSliderDefaultVis(0);
        end
        % set visibility of all chrono slider components
        function[] = setSliderChronoVis(isVisible)
            if isVisible
                set(myHandles.sliderAxChrono,'Visible','on');
                set(myHandles.sliderLineChrono_red,'Visible','on');
                set(myHandles.sliderLineChrono_blue,'Visible','on');
                set(myHandles.sliderChrono,'Visible','on');
            else
                set(myHandles.sliderAxChrono,'Visible','off');
                set(myHandles.sliderLineChrono_red,'Visible','off');
                set(myHandles.sliderLineChrono_blue,'Visible','off');
                set(myHandles.sliderChrono,'Visible','off');
            end
        end
        % set visibility of all sequen slider components
        function[] = setSliderSequenVis(isVisible)
            if isVisible
                set(myHandles.sliderAxSequen,'Visible','on');
                set(myHandles.sliderLineSequen_red,'Visible','on');
                set(myHandles.sliderLineSequen_blue,'Visible','on');
                set(myHandles.sliderSequen,'Visible','on');
            else
                set(myHandles.sliderAxSequen,'Visible','off');
                set(myHandles.sliderLineSequen_red,'Visible','off');
                set(myHandles.sliderLineSequen_blue,'Visible','off');
                set(myHandles.sliderSequen,'Visible','off');
            end
        end
        % set visibility of all default slider components
        function[] = setSliderDefaultVis(isVisible)
            if isVisible
                set(myHandles.sliderAxDefault,'Visible','on');
                set(myHandles.sliderLineDefault,'Visible','on');
                
            else
                set(myHandles.sliderAxDefault,'Visible','off');
                set(myHandles.sliderLineDefault,'Visible','off');
                
            end
        end
        clear myData
        clear myHandles
    end

    function[] = resetForcedYLims()
        myData = guidata(mainFig);
        fylims = myData.data.forcedYLims;
        vnm = fieldnames(fylims.Z);
        for i = 1:length(vnm)
            fylims.Z.(vnm{i}).FYMin = [];
            fylims.Z.(vnm{i}).FYMax = [];
        end
        vnm = fieldnames(fylims.N2);
        for i = 1:length(vnm)
            fylims.N2.(vnm{i}).FYMin = [];
            fylims.N2.(vnm{i}).FYMax = [];
        end
        myData.data.forcedYLims = fylims;
        guidata(mainFig,myData);
        clear myData
    end

%% Callback functions

% Callback for the "Load File(s)" toolbar pushbutton.
    function [] = FList_pb_call(varargin)
        set(0, 'PointerLocation', [screenSize(3)/2, screenSize(4)/2]);
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        fullFilePicks = light_uipickfiles11();
        
        % remove files that are not a CTD extension or have a parseable
        % sample date
        numRemoved = 0;
        fileNum = 1;
        while fileNum <= length(fullFilePicks)
            errorFlag = 0;
            fileFullPath = fullFilePicks{fileNum};
            [~, fileName, fileExt] = fileparts(fileFullPath);
            if ~strcmpi(fileExt,'.asc') && ~strcmpi(fileExt,'.cnv') && ~strcmpi(fileExt,'.xls')...
                    && ~strcmpi(fileExt,'.txt')
                errorFlag = 1;
            else
                [~, fileDateNum] = identifyCTDDate(fileFullPath);
                if isempty(fileDateNum)
                    errorFlag = 1;
                end
            end
            if errorFlag
                msg = ['The file ', [fileName fileExt],...
                    ' does not have file extension asc/txt/cnv/xls, and/or',...
                    ' does not contain a parseable sample date.',...
                    ' It was thus removed from the file picks list.'];
                mainLogText = appendLogText(mainLogText,msg,'warning');
                fullFilePicks(fileNum) = [];
                numRemoved = numRemoved + 1;
            else
                fileNum = fileNum + 1;
            end
        end
        
        % display warning dialogue for removed files
        if numRemoved > 0
            warnStr = [int2str(numRemoved), ' incompatible files were removed from the file picks list.',...
                ' See console output for removed file names.'];
            warndlg(warnStr,'File Picks Warning')
        end
        
        if ~isempty(fullFilePicks)
            myData = guidata(mainFig);
            myData.data.CTDFullFilePicks = fullFilePicks;
            myData.data.currCTDFileIndex = 1;
            currFileIndex = myData.data.currCTDFileIndex;
            guidata(mainFig,myData);
            clear myData
            
            if length(fullFilePicks) == 1
                fullFileName = fullFilePicks{1};
                updateSlider();
            elseif length(fullFilePicks) > 1
                initSlider();
                updateSlider();
                myData = guidata(mainFig);
                fullFilePicks = myData.data.CTDFullFilePicks;
                % use hash map to make sure the correct first file is
                % pre-processed (chronological or sequential)
                chronoHash = myData.data.chronoHash;
                sequenHash = myData.data.sequenHash;
                fullFileName = '';
                if strcmp(myData.data.flistViewMode,'chronoMode')
                    fullFileName = fullFilePicks{chronoHash(currFileIndex)};
                elseif strcmp(myData.data.flistViewMode,'sequenMode')
                    fullFileName = fullFilePicks{sequenHash(currFileIndex)};
                end
                clear myData
            end
            
            [~,fileName,fileExt] = fileparts(fullFileName);
            msg = [fileName, fileExt];
            mainLogText = appendLogText(mainLogText,msg,'p_ctd');
            % preprocess file
            ppSuccess = preprocessCTDFile(fullFileName);
            if ppSuccess
                updateVarSelPanel();
                % update text fields in fileInfoPanel
                updateFileInfoPanel(fullFileName);
                updatePlot();
                resetForcedYLims();
                updateYModPanel();
                
                myData = guidata(mainFig);
                % autosave if enabled
                if myData.data.autosaveOn
                    [data, header, formatSpec] = prepDataForWrite();
                    currFullFile = myData.data.CTDFullFilePicks{myData.data.currCTDFileIndex};
                    [ctdPath, ctdName, ctdExt] = fileparts(currFullFile);
                    saveFileName = [ctdName '.txt'];
                    if ~isempty(myData.data.procCTDPath)
                        destFullPath = fullfile(myData.data.procCTDPath, saveFileName);
                    else
                        destFullPath = fullfile(ctdPath, saveFileName);
                    end
                    writeOK = writeDataToText(data, header, formatSpec, destFullPath);
                    if writeOK
                        msg = [ctdName, ctdExt, ' was autosaved to ', destFullPath];
                        mainLogText = appendLogText(mainLogText,msg,'success');
                    end
                end
                clear myData
            end
        end
        clear fullFilePicks
        mainLogText = updateLog(mainLogText,consoleBox);
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end


    function [] = VarChoice_check_call(hObject, ~)
        set(mainFig,'Pointer','watch');
        pause(0.01)
        
        % if button is now enabled, change to variable color
        % if button is now disabled, change to panel's background color
        myData = guidata(mainFig);
        currDataType = myData.data.currCTDDataType;
        currVarsY = myData.data.currCTDVarsY;
        numVarsY = length(currVarsY);
        buttonDown = get(hObject,'value');
        % get the number of the variable from button tag
        varNum = getTagVarNum(hObject);
        varName = currVarsY{varNum};
        % get plotted variable preferences and left/right axis preferences
        if strcmpi(currDataType,'Z')
            plottedVarsY = myData.data.plottedCTDVarsZY;
            axLVar = myData.data.axLVarZ;
            axRVar = myData.data.axRVarZ;
        elseif strcmpi(currDataType,'N2')
            plottedVarsY = myData.data.plottedCTDVarsN2Y;
            axLVar = myData.data.axLVarN2;
            axRVar = myData.data.axRVarN2;
        end
        % radio button tags associated with variable
        radioLTag = ['var' int2str(varNum) 'AxLChoice'];
        radioRTag = ['var' int2str(varNum) 'AxRChoice'];
        % search for the current variable in plotted variables
        matchIndex = find(strcmp(varName,plottedVarsY));
        
        % get handle for left and right axes
        axLTag = ['ax' num2str(varNum) 'L'];
        axRTag = ['ax' num2str(varNum) 'R'];
        axLHandle = myData.handles.(axLTag);
        axRHandle = myData.handles.(axRTag);
        % get handle for left and right lines
        lineLTag = ['h' num2str(varNum) '_L'];
        lineRTag = ['h' num2str(varNum) '_R'];
        lineLHandle = myData.handles.(lineLTag);
        lineRHandle = myData.handles.(lineRTag);
            
        if buttonDown
            % set the color of the button based on the associated variable
            set(hObject,'BackgroundColor',myData.data.masterCTDVars.(currDataType).(varName).color);
            % add the activated variable to plotted variable preferences if
            % not already in it
            if isempty(matchIndex)
                plottedVarsY{length(plottedVarsY)+1} = varName;
            end
            
            % set corresponding line on plot to visible
            set(lineLHandle,'Visible','on');
            set(lineRHandle,'Visible','on');
            
            % enable radio buttons whenever toggle button is
            % pressed
            set(myData.handles.(radioLTag),'Enable','on');
            set(myData.handles.(radioRTag),'Enable','on');
            
            % if radio button was previous selected before being disabled,
            % re-enable it, and make the corresponding axes visible
            if numVarsY > MAX_YVARS/2
                numChildren = ceil(numVarsY/2);
            else
                numChildren = numVarsY;
            end
            if strcmpi(varName,axLVar)
                if varNum <= numChildren
                    set(myData.handles.varChoiceGroup1AxL,'SelectedObject',myData.handles.(radioLTag));
                else
                    set(myData.handles.varChoiceGroup2AxL,'SelectedObject',myData.handles.(radioLTag));
                end
                set(axLHandle,'Visible','on');
            end
            if strcmpi(varName,axRVar)
                if varNum <= numChildren
                    set(myData.handles.varChoiceGroup1AxR,'SelectedObject',myData.handles.(radioRTag));
                else
                    set(myData.handles.varChoiceGroup2AxR,'SelectedObject',myData.handles.(radioRTag));
                end
                set(axRHandle,'Visible','on');
            end
        else
            % get the default ui color
            disabledColor = get(0,'defaultUicontrolBackgroundColor');
            set(hObject,'BackgroundColor', disabledColor);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
            % remove the deactivated variable from plotted variable
            % preferences
            plottedVarsY(matchIndex) = [];
            % make corresponding line on plot invisible
            set(lineLHandle,'Visible','off');
            set(lineRHandle,'Visible','off');
            % disable radio buttons whenever toggle button is not
            % pressed
            set(myData.handles.(radioLTag),'Enable','off');
            set(myData.handles.(radioRTag),'Enable','off');
            % make corresponding axes invisible
            set(axLHandle,'Visible','off');
            set(axRHandle,'Visible','off');
            % If the selected radiobutton is now disabled, clear selection from
            % button group
            selRadio1L = get(myData.handles.varChoiceGroup1AxL,'SelectedObject');
            selRadio1R = get(myData.handles.varChoiceGroup1AxR,'SelectedObject');
            selRadio2L = get(myData.handles.varChoiceGroup2AxL,'SelectedObject');
            selRadio2R = get(myData.handles.varChoiceGroup2AxR,'SelectedObject');
            if strcmpi(get(selRadio1L,'Enable'),'off')
                set(myData.handles.varChoiceGroup1AxL,'SelectedObject',[]);
            end
            if strcmpi(get(selRadio1R,'Enable'),'off')
                set(myData.handles.varChoiceGroup1AxR,'SelectedObject',[]);
            end
            if strcmpi(get(selRadio2L,'Enable'),'off')
                set(myData.handles.varChoiceGroup2AxL,'SelectedObject',[]);
            end
            if strcmpi(get(selRadio2R,'Enable'),'off')
                set(myData.handles.varChoiceGroup2AxR,'SelectedObject',[]);
            end
        end
        
        % get handle for left and right lines
        lineLTag = ['h' num2str(varNum) '_L'];
        lineRTag = ['h' num2str(varNum) '_R'];
        lineLHandle = myData.handles.(lineLTag);
        lineRHandle = myData.handles.(lineRTag);
        % set visibility of corresponding line on plot
        if buttonDown
            set(lineLHandle,'Visible','on');
            set(lineRHandle,'Visible','on');
        else
            set(lineLHandle,'Visible','off');
            set(lineRHandle,'Visible','off');
        end
        
        % save preferences
        if strcmpi(currDataType,'Z')
            myData.data.plottedCTDVarsZY = plottedVarsY;
            setpref('lightgui','plottedCTDVarsZY',myData.data.plottedCTDVarsZY);
        elseif strcmpi(currDataType,'N2')
            myData.data.plottedCTDVarsN2Y = plottedVarsY;
            setpref('lightgui','plottedCTDVarsN2Y',myData.data.plottedCTDVarsN2Y);
        end
        
        guidata(mainFig,myData);
        clear myData;
        clear currVarsY;
        updateYModPanel();
        set(mainFig,'Pointer','arrow');
    end


    function [] = selchbk_callAxL(~,eventdata)
        set(mainFig,'Pointer','watch');
        pause(0.01)
        
        % get variable number corresponding to selected object
        newTagNum = getTagVarNum(eventdata.NewValue);
        
        myData = guidata(mainFig);
        myHandles = myData.handles;
        numVarsY = length(myData.data.currCTDVarsY);
        
        % initially clear the left axis selection radiobuttons
        set(myHandles.varChoiceGroup1AxL,'SelectedObject',[]);
        set(myHandles.varChoiceGroup2AxL,'SelectedObject',[]);
        % clear left axis visibility for all axes
        for i = 1:MAX_YVARS
            axLTag = ['ax' num2str(i) 'L'];
            axLHandle = myHandles.(axLTag);
            set(axLHandle,'Visible','off');
        end
        if ~isempty(newTagNum)
            % radio button tag associated with variable
            radioLTag = ['var' int2str(newTagNum) 'AxLChoice'];
            % set the left axis selection radiobutton again
            if numVarsY > MAX_YVARS/2
                numChildren = ceil(numVarsY/2);
            else
                numChildren = numVarsY;
            end
            if newTagNum <= numChildren
                set(myData.handles.varChoiceGroup1AxL,'SelectedObject',myData.handles.(radioLTag));
            else
                set(myData.handles.varChoiceGroup2AxL,'SelectedObject',myData.handles.(radioLTag));
            end
            % set corresponding axes visibility
            newAxLTag = ['ax' num2str(newTagNum) 'L'];
            newAxLHandle = myHandles.(newAxLTag);
            set(newAxLHandle,'Visible','on');
            % save axis selection preferences
            varTextTag = strcat('var',int2str(newTagNum),'Text');
            varTextStr = get(myHandles.(varTextTag),'String');
            if strcmp(myData.data.currCTDDataType,'Z')
                myData.data.axLVarZ = varTextStr;
                setpref('lightgui','axLVarZ',myData.data.axLVarZ);
            elseif strcmp(myData.data.currCTDDataType,'N2')
                myData.data.axLVarN2 = varTextStr;
                setpref('lightgui','axLVarN2',myData.data.axLVarN2);
            end
            guidata(mainFig,myData);
        end
        
        clear myHandles;
        clear myData;
        
        set(mainFig,'Pointer','arrow');
    end


    function [] = selchbk_callAxR(~,eventdata)
        set(mainFig,'Pointer','watch');
        pause(0.01)
        
        % get variable number corresponding to selected object
        newTagNum = getTagVarNum(eventdata.NewValue);
        
        myData = guidata(mainFig);
        myHandles = myData.handles;
        numVarsY = length(myData.data.currCTDVarsY);
        
        % initially clear the right axis selection radiobuttons
        set(myHandles.varChoiceGroup1AxR,'SelectedObject',[]);
        set(myHandles.varChoiceGroup2AxR,'SelectedObject',[]);
        % clear right axis visibility for all axes
        for i = 1:MAX_YVARS
            axRTag = ['ax' num2str(i) 'R'];
            axRHandle = myHandles.(axRTag);
            set(axRHandle,'Visible','off');
        end
        if ~isempty(newTagNum)
            % radio button tag associated with variable
            radioRTag = ['var' int2str(newTagNum) 'AxRChoice'];
            % set the right axis selection radiobutton again
            if numVarsY > MAX_YVARS/2
                numChildren = ceil(numVarsY/2);
            else
                numChildren = numVarsY;
            end
            if newTagNum <= numChildren
                set(myData.handles.varChoiceGroup1AxR,'SelectedObject',myData.handles.(radioRTag));
            else
                set(myData.handles.varChoiceGroup2AxR,'SelectedObject',myData.handles.(radioRTag));
            end
            % set corresponding axes visibility
            newAxRTag = ['ax' num2str(newTagNum) 'R'];
            newAxRHandle = myHandles.(newAxRTag);
            set(newAxRHandle,'Visible','on');
            % save axis selection preferences
            varTextTag = strcat('var',int2str(newTagNum),'Text');
            varTextStr = get(myHandles.(varTextTag),'String');
            if strcmp(myData.data.currCTDDataType,'Z')
                myData.data.axRVarZ = varTextStr;
                setpref('lightgui','axRVarZ',myData.data.axRVarZ);
            elseif strcmp(myData.data.currCTDDataType,'N2')
                myData.data.axRVarN2 = varTextStr;
                setpref('lightgui','axRVarN2',myData.data.axRVarN2);
            end
            guidata(mainFig,myData);
        end
        
        clear myHandles;
        clear myData;
        
        set(mainFig,'Pointer','arrow');
    end


    function [] = enableChkDepth_cb(hObject,~)
        set(mainFig,'Pointer','crosshair');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        set(hObject,'Enable','on');
        
        chkDepthOn = 1;
        while chkDepthOn
            [xVal, ~, button]=ginputc(1,'LineWidth',1.5);
            if button == 3
                set(hObject,'State','off');
                break;
            end
            if isempty(get(gca,'Tag'))
                xVal = -1;
                delete(gca);
            end
            myData = guidata(mainFig);
            myHandles = myData.handles;
            currVarsX = myData.data.currCTDVarsX;
            xVar = currVarsX{1};
            currDataType = myData.data.currCTDDataType;
            xData = myData.data.masterCTDVars.(currDataType).(xVar).data;
            xUnit = myData.data.masterCTDVars.(currDataType).(xVar).unit;
            if ~isempty(xData) && (xVal >= 0) && (xVal < (floor(max(xData))+1))
                xValStr=num2str(round(xVal*100)/100);
                set(myHandles.pointerXValText,'String',[xValStr ' ' xUnit]);
                set(myHandles.pointerXValText,'BackgroundColor','yellow');
                pause(0.3)
                set(myHandles.pointerXValText,'BackgroundColor',defaultBackgroundColor);
                pause(0.3)
                set(myHandles.pointerXValText,'BackgroundColor','yellow');
                pause(0.3)
                set(myHandles.pointerXValText,'BackgroundColor',defaultBackgroundColor);
            else
                set(myHandles.pointerXValText,'String','');
            end
        end
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

% Callback for "Save Plot" pushbutton
    function [] = snapshot_cb(varargin)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        myData = guidata(mainFig);
        if myData.data.currCTDFileIndex < 1
            msg = 'CTD Plot export error: No data loaded.';
            mainLogText = appendLogText(mainLogText,msg,'error');
        else
            frame = getframe(mainFig);
            imagen = frame.cdata;
            
            %setup save
            %write as [CTDfilename].png
            fileFullPath = myData.data.CTDFullFilePicks{myData.data.currCTDFileIndex};
            [ctdPath, ctdName, ~] = fileparts(fileFullPath);
            imName = ctdName;
            
            oldPath = pwd;
            cd(ctdPath);
            
            % Save entire frame
            imNamepng=[ imName '.png'];
            % Save the image to disk
            [SaveimFilename, SaveimPathname, filterindex] = uiputfile( ...
                {...
                '*.png',  'PNG-files  (*Plot.png)'; ...
                '*.jpg',  'JPEG-files (*Plot.jpg)'; ...
                '*.gif',  'GIF files  (*Plot.gif)'; ...
                '*.*',    'All Files  (*Plot.*)'...
                }, ...
                'Save window shot as',imNamepng...
                );
            if filterindex ~= 0
                imwrite(imagen,  [SaveimPathname  SaveimFilename ]);
                msg = ['Snapshot saved successfully to ', SaveimPathname, SaveimFilename];
                mainLogText = appendLogText(mainLogText,msg,'success');
                mainLogText = updateLog(mainLogText,consoleBox);
            end
            
            cd(oldPath);
            
            clear imagen
            
        end
        
        clear myData
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function [] = slider_callback(hObject,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        myData = guidata(mainFig);
        newSliderValue = get(hObject, 'value');
        oldSliderValue = myData.data.oldSliderValue;
        fullFilePicks = myData.data.CTDFullFilePicks;
        currFileIndex = myData.data.currCTDFileIndex;
        % NOTE: 1 is used as the value difference comparison because it is
        % the minimum interval between datenums
        if (newSliderValue - oldSliderValue) > 0 && round(newSliderValue - oldSliderValue) <= 1
            % go to next indexed file
            if currFileIndex > 0 && currFileIndex < length(fullFilePicks)
                currFileIndex = currFileIndex + 1;
            end
        elseif  (newSliderValue - oldSliderValue) < 0 && round(newSliderValue - oldSliderValue) >= -1
            % go to previous indexed file
            if currFileIndex > 1
                currFileIndex = currFileIndex - 1;
            end
        else
            % go to nearest file
            if strcmp(myData.data.flistViewMode,'chronoMode')
                [~, currFileIndex] = min(abs(myData.data.chronoValues - newSliderValue));
            elseif strcmp(myData.data.flistViewMode,'sequenMode')
                [~, currFileIndex] = min(abs(myData.data.sequenValues - newSliderValue));
            end
        end
        
        % save currFileIndex to guidata
        myData.data.currCTDFileIndex = currFileIndex;
        guidata(mainFig,myData);
        
        % update slider based on mode
        updateSlider();
        
        % preprocess file
        chronoHash = myData.data.chronoHash;
        sequenHash = myData.data.sequenHash;
        fullFileName = '';
        if strcmp(myData.data.flistViewMode,'chronoMode')
            fullFileName = fullFilePicks{chronoHash(currFileIndex)};
        elseif strcmp(myData.data.flistViewMode,'sequenMode')
            fullFileName = fullFilePicks{sequenHash(currFileIndex)};
        end
        
        [~,fileName,fileExt] = fileparts(fullFileName);
        msg = [fileName, fileExt];
        mainLogText = appendLogText(mainLogText,msg,'p_ctd');
        ppSuccess = preprocessCTDFile(fullFileName);
        if ppSuccess
            updateVarSelPanel();
            % update text fields in fileInfoPanel
            updateFileInfoPanel(fullFileName);
            updatePlot();
            updateYModPanel();
            
            % autosave if enabled
            myData = guidata(mainFig);
            if myData.data.autosaveOn
                [data, header, formatSpec] = prepDataForWrite();
                currFullFile = myData.data.CTDFullFilePicks{myData.data.currCTDFileIndex};
                [ctdPath, ctdName, ctdExt] = fileparts(currFullFile);
                saveFileName = [ctdName '.txt'];
                if ~isempty(myData.data.procCTDPath)
                    destFullPath = fullfile(myData.data.procCTDPath, saveFileName);
                else
                    destFullPath = fullfile(ctdPath, saveFileName);
                end
                writeOK = writeDataToText(data, header, formatSpec, destFullPath);
                if writeOK
                    msg = [ctdName, ctdExt, ' was autosaved to ', destFullPath];
                    mainLogText = appendLogText(mainLogText,msg,'success');
                end
            end
            clear myData
        end
        
        clear myData
        mainLogText = updateLog(mainLogText,consoleBox);
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
        uicontrol(hObject);
    end

% changes the current data type so that data from another XLS tab is displayed
    function [] = switchXLSPlot(hObject,~)
         set(mainFig,'Pointer','watch');
         % disable all enabled components while processing
         enabledH = findobj(mainFig,'Enable','on');
         set(enabledH,'Enable','off');
         pause(0.01)
 
         % change data type to match selected object
         myData = guidata(mainFig);
         objValue = get(hObject,'Value');
         objString = get(hObject,'String');
         selDataType = objString{objValue};
         myData.data.currCTDDataType = selDataType;
         if strcmpi(selDataType,'Z')
             myData.data.currCTDVarsX = myData.data.preferredCTDVarsZX;
             myData.data.currCTDVarsY = myData.data.preferredCTDVarsZY;
         elseif strcmpi(selDataType,'N2')
             myData.data.currCTDVarsX = myData.data.preferredCTDVarsN2X;
             myData.data.currCTDVarsY = myData.data.preferredCTDVarsN2Y;
         end
         
         guidata(mainFig,myData);
         % update var selection panel
         updateVarSelPanel();
         % update plot
         updatePlot();
         updateYModPanel();
         clear myData
 
         set(mainFig,'Pointer','arrow');
         set(enabledH,'Enable','on');
         enabledH = [];
     end

% changes the current x variable and updates the plot
    function [] = xVarMenu_cb(hObject,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        % change the current x variable
        myData = guidata(mainFig);
        currDataType = myData.data.currCTDDataType;
        varNames = get(hObject,'String');
        newValue = get(hObject,'Value');
        newVar = varNames{newValue};
        if strcmpi(currDataType,'Z')
            myData.data.preferredCTDVarsZX = {newVar};
            setpref('lightgui','preferredCTDVarsZX',myData.data.preferredCTDVarsZX);
            myData.data.currCTDVarsX = myData.data.preferredCTDVarsZX;
        elseif strcmpi(currDataType,'N2')
            myData.data.preferredCTDVarsN2X = {newVar};
            setpref('lightgui','preferredCTDVarsN2X',myData.data.preferredCTDVarsN2X);
            myData.data.currCTDVarsX = myData.data.preferredCTDVarsN2X;
        end
        guidata(mainFig,myData);
        clear myData;
        updatePlot();
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

% Callback function for "Set Variables" button
    function [] = setVariables_cb(~,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        % Call nested function that displays pop-up window
        getVariablePrefs();
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
        
        function [] = getVariablePrefs()
            % Allows user to set what variables and how many variables to display
            % in the lightgui variable selection panel
            
            figWidth = screenSize(3)/3;
            figHeight = screenSize(4)-6;
            cancelFlag = 0;
            
            myData = guidata(mainFig);
            currDataType = myData.data.currCTDDataType;
            currVarsY = myData.data.currCTDVarsY;
            
            varsFig = figure ( 'windowstyle', 'modal', 'resize', 'on', ...
                'name','Set CTD Variables', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                'Tag', 'varsFigure', 'units','characters', 'Position',[100 100 figWidth figHeight],...
                'CreateFcn', {@movegui,'north'});
            
            set(varsFig,'Color',defaultBackgroundColor);
            
            % Done button
            uicontrol('Style','PushButton','Units','Normalized','Position',[.25 .05 .15 .05],...
                'FontSize', 10,'String','Done','CallBack','uiresume(gcbf)');
            
            % Cancel button
            uicontrol('Style','PushButton','Units','Normalized','Position',[.6 .05 .15 .05],...
                'FontSize', 10,'String','Cancel','CallBack',{@callback_btn_cancel});
            
            % Data type panel and text
            dataTypePanel = uipanel('Parent',varsFig, 'units','normalized',...
                'Title','Data Type', 'TitlePosition','centertop', 'Position',[0.4,0.9,0.2,0.06],...
                'Visible','off');
            
            uicontrol('Style','text', 'Parent',dataTypePanel,...
                'String',currDataType, 'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                'FontSize',10, 'FontWeight','bold');
            
            set(dataTypePanel,'Visible','on');
            
            % Outer panels for two columns of Y variables
            columnPanel1 = uipanel('Parent',varsFig, 'Units','normalized',...
                'Position',[0.1 0.1 0.4 0.8], 'Title','Y Variables', 'TitlePosition','centertop');
            
            columnPanel2 = uipanel('Parent',varsFig, 'Units','normalized',...
                'Position',[0.5 0.1 0.4 0.8], 'Title','Y Variables', 'TitlePosition','centertop');
            
            % figure out nicely spaced panel heights based on number of variables
            numPanelChildren = ceil(MAX_YVARS/2);
            panelHeight = floor((1/numPanelChildren)*100)/100;
            varPanelPos1 = [0.0 1.0-panelHeight 1.0 panelHeight];
            varPanelPos2 = [0.0 1.0-panelHeight 1.0 panelHeight];
            
            % iteratively create panels containing variable checkbox and drop-down menu
            for i = 1:MAX_YVARS
                svPanelTag = strcat('svPanel',int2str(i));
                menuTag = strcat('yMenu', int2str(i));
                chbTag = strcat('chkBox', int2str(i));
                if i <= numPanelChildren
                    svPanelHandle = uipanel('Parent',columnPanel1, 'Units','normalized',...
                        'Position',varPanelPos1, 'Tag',svPanelTag);
                    varPanelPos1 = varPanelPos1 - [0 panelHeight 0 0];
                else
                    svPanelHandle = uipanel('Parent',columnPanel2, 'Units','normalized',...
                        'Position',varPanelPos2, 'Tag',svPanelTag);
                    varPanelPos2 = varPanelPos2 - [0 panelHeight 0 0];
                end
                uicontrol('Style','checkbox', 'Parent',svPanelHandle,...
                    'Units','normalized', 'Position', [0.05 0 0.1 1], 'Max',1, 'Min', 0,...
                    'Callback',{@callback_checkbox}, 'Tag',chbTag);
                uicontrol('Style','popupmenu', 'Parent',svPanelHandle,...
                    'String',{'test1','test2'}, 'Value',1,...
                    'Units','normalized', 'Position',[0.15 0 0.8 0.9],...
                    'FontSize',12,...
                    'FontWeight','bold', 'Callback',{@callback_menu},...
                    'Tag',menuTag);
            end
            
            % Get the plottable Y variables of the current data type
            varNames = fieldnames(myData.data.masterCTDVars.(currDataType));
            yVarNames = cell(1,length(varNames));
            for varNum = 1:length(varNames)
                yVarProps = myData.data.masterCTDVars.(currDataType).(varNames{varNum});
                if yVarProps.canPlot && strcmpi(yVarProps.varType, 'y')
                    yVarNames{varNum} = varNames{varNum};
                end
            end
            yVarNames(cellfun('isempty',yVarNames)) = []; % trim pre-allocated cell array
            
            myHandles = guihandles(gcf);
            % Set the variable options of the y variable menus
            availableVars = yVarNames;
            for tagNum = 1:MAX_YVARS
                menuTag = strcat('yMenu',int2str(tagNum));
                menuHandle = myHandles.(menuTag);
                chkBoxTag = strcat('chkBox',int2str(tagNum));
                chkBoxHandle = myHandles.(chkBoxTag);
                if tagNum <= length(yVarNames)
                    set(menuHandle,'String',yVarNames);
                    if tagNum <= length(currVarsY)
                        % Set the menu selections to the currently selected variables
                        value = find(strcmp(currVarsY{tagNum}, yVarNames));
                        % remove variable from available variable list
                        availableVars(strcmp(currVarsY{tagNum}, availableVars)) = [];
                        set(menuHandle,'Value',value);
                        % Set the correct text color
                        set(menuHandle,'ForegroundColor',myData.data.masterCTDVars.(currDataType).(currVarsY{tagNum}).color);
                        set(chkBoxHandle,'Value',1);
                    else % y variables are available but not in current vars
                        value = find(strcmp(availableVars{1}, yVarNames));
                        availableVars(1) = [];
                        set(menuHandle,'Value',value);
                        set(chkBoxHandle,'Value',0);
                        set(menuHandle,'Enable','off');
                    end
                else % no more available y variables
                    set(chkBoxHandle,'Value',0);
                    set(chkBoxHandle,'Enable','off');
                    set(menuHandle,'String','N/A');
                    set(menuHandle,'Enable','off');
                end
            end
            
            set(varsFig,'Visible','on');
            uiwait(varsFig);
            
            if ~cancelFlag
                % remove duplicates
                if length(currVarsY) > 1
                    numDupes = 0;
                    for a = 1:length(currVarsY)-1
                        if ~isempty(currVarsY{a})
                            for b = a+1:length(currVarsY)
                                if ~isempty(currVarsY{b})
                                    if strcmp(currVarsY{a},currVarsY{b})
                                        currVarsY{b} = [];
                                        numDupes = numDupes + 1;
                                    end
                                end
                            end
                        end
                    end
                    if numDupes > 0
                        msg = [int2str(numDupes), ' duplicate variables were detected',...
                            ' and removed from selected variables.'];
                        mainLogText = appendLogText(mainLogText,msg,'warning');
                        mainLogText = updateLog(mainLogText,consoleBox);
                    end
                end
                
                i = 1;
                % delete any disabled/removed variables from current variable list
                while i <= length(currVarsY)
                    if isempty(currVarsY{i})
                        currVarsY(i) = [];
                    else
                        i = i+1;
                    end
                end
                
                % save selected variables to preferred variable list
                if strcmpi(currDataType,'Z')
                    myData.data.preferredCTDVarsZY = currVarsY;
                    setpref('lightgui','preferredCTDVarsZY',myData.data.preferredCTDVarsZY);
                    % save selected variables to guidata's currCTDVarsY
                    myData.data.currCTDVarsY = myData.data.preferredCTDVarsZY;
                    % update plotted ctd vars by intersecting with
                    % preferred vars
                    myData.data.plottedCTDVarsZY = intersect(myData.data.plottedCTDVarsZY, myData.data.preferredCTDVarsZY);
                    setpref('lightgui','plottedCTDVarsZY',myData.data.plottedCTDVarsZY);
                elseif strcmpi(currDataType,'N2')
                    myData.data.preferredCTDVarsN2Y = currVarsY;
                    setpref('lightgui','preferredCTDVarsN2Y',myData.data.preferredCTDVarsN2Y);
                    myData.data.currCTDVarsY = myData.data.preferredCTDVarsN2Y;
                    myData.data.plottedCTDVarsN2Y = intersect(myData.data.plottedCTDVarsN2Y, myData.data.preferredCTDVarsN2Y);
                    setpref('lightgui','plottedCTDVarsN2Y',myData.data.plottedCTDVarsN2Y);
                end
                
                guidata(mainFig, myData);
                
                % update variable selection panel
                updateVarSelPanel();
                % update plot
                updatePlot();
                updateYModPanel();
            end
            
            clear myData
            try
                delete(varsFig);
            catch
            end
            
            function [] = callback_menu(hObject,~)
                tagVarNum = getTagVarNum(hObject);
                newValue = get(hObject,'Value');
                menuVars = get(hObject,'String');
                newVar = menuVars{newValue};
                oldVar = currVarsY{tagVarNum};
                if ~strcmpi(oldVar,newVar) % menu selection changed
                    currVarsY{tagVarNum} = newVar;
                    % Set the correct text color
                    set(hObject,'ForegroundColor',myData.data.masterCTDVars.(currDataType).(newVar).color);
                end
            end
            
            function [] = callback_checkbox(hObject,~)
                tagVarNum = getTagVarNum(hObject);
                yMenuTag = strcat('yMenu',num2str(tagVarNum));
                yMenuHandle = findobj(gcf,'Tag',yMenuTag);
                vars = get(yMenuHandle,'String');
                val = get(yMenuHandle,'Value');
                currVar = vars{val};
                % if box is checked, enable yMenu and add variable to
                % currVarsY
                if (get(hObject,'Value') == get(hObject,'Max'))
                    set(yMenuHandle,'Enable','on');
                    set(yMenuHandle,'ForegroundColor',myData.data.masterCTDVars.(currDataType).(currVar).color);
                    currVarsY{tagVarNum} = currVar;
                else
                    % if box is unchecked, disable yMenu and add empty variable to
                    % currVarsY
                    set(yMenuHandle,'Enable','off');
                    currVarsY{tagVarNum} = [];
                end
            end
            
            function [] = callback_btn_cancel(~,~)
                cancelFlag = 1;
                uiresume(gcbf);
            end
        end
    end

    function [] = configVarProps_cb(~,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        configVarProps();
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
        
        function configVarProps()
            myData = guidata(mainFig);
            masterVars = myData.data.masterCTDVars;
            currDataType = myData.data.currCTDDataType;
            
            dispName = '';
            exportName = '';
            aliases = {};
            unit = '';
            exportFormat = '';
            flag = '';
            fwidth = [];
            precision = [];
            convChar = '';
            canPlot = [];
            varType = '';
            color = [];
            lineWidth = [];
            lineStyle = '';
            marker = '';
            markerEdgeColor = [];
            markerFaceColor = [];
            markerSize = [];
            
            plotYH = [];
            propsChanged = 0;
            
            figWidth = screenSize(3);
            figHeight = screenSize(4)-6;
            
            varPropFig = figure ( 'windowstyle', 'normal', 'resize', 'on', ...
                'name','Configure Variable Properties', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                'Tag', 'varPropFig', 'units','characters', 'Position',[100 100 figWidth figHeight],...
                'CreateFcn', {@movegui,'north'});
            
            set(varPropFig,'windowstyle','modal');
            set(varPropFig,'Color',defaultBackgroundColor);
            
            dataTypePanel = uipanel('Parent',varPropFig, 'Units','normalized', 'Position',[0.1 0.9 0.2 0.08],...
                'FontWeight','bold', 'Title','Data Type', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            dataTypeMenu = uicontrol('Parent',dataTypePanel, 'Style','popupmenu', 'Units','normalized',...
                'Position',[0 0 1 1], 'FontSize',12, 'FontWeight','bold', 'String',{'Z','N2'},...
                'BackgroundColor','w', 'Visible','on', 'Callback',{@switchDataType_cb});
            if strcmpi(currDataType,'z')
                set(dataTypeMenu,'Value',1);
                chosenDataType = 'Z';
            else
                set(dataTypeMenu,'Value',2);
                chosenDataType = 'N2';
            end
            function[] = switchDataType_cb(hObject,~)
                val = get(hObject,'Value');
                switch val
                    case 1
                        set(varChoiceMenu,'String',varNamesZ);
                        newDataType = 'Z';
                    case 2
                        set(varChoiceMenu,'String',varNamesN2);
                        newDataType = 'N2';
                end
                
                % if value changed, ask user if they want to save current
                % variable props
                if ~strcmpi(newDataType,chosenDataType)
                    if propsChanged
                        choice = questdlg(['Would you like to save changes to the properties of variable ',...
                            chosenVar,'?'],...
                            'Switch variable', ...
                            'Yes','No','Yes');
                        % Handle response
                        switch choice
                            case 'Yes'
                                saveVarProps();
                            case 'No'
                        end
                    end
                    % change chosen data type and chosen var
                    % repopulate var panels
                    chosenDataType = newDataType;
                    varStrs = get(varChoiceMenu,'String');
                    varNum = get(varChoiceMenu,'Value');
                    chosenVar = varStrs{varNum};
                    refreshVarProps();
                    propsChanged = 0;
                end
            end
            
            varChoicePanel = uipanel('Parent',varPropFig, 'Units','normalized', 'Position',[0.4 0.9 0.5 0.08],...
                'FontWeight','bold', 'Title','Variables', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            varChoiceMenu = uicontrol('Parent',varChoicePanel, 'Style','popupmenu', 'Units','normalized',...
                'Position',[0 0 1 1], 'FontSize',12, 'FontWeight','bold', 'BackgroundColor','w',...
                'Visible','on', 'Callback',{@switchVarChoice_cb});
            function[] = switchVarChoice_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                newVar = strs{val};
                if ~strcmpi(newVar,chosenVar)
                    if propsChanged
                        % if value changed, ask user if they want to save current
                        % variable props
                        choice = questdlg(['Would you like to save changes to the properties of variable ',...
                            chosenVar,'?'],...
                            'Switch variable', ...
                            'Yes','No','Yes');
                        % Handle response
                        switch choice
                            case 'Yes'
                                saveVarProps();
                            case 'No'
                        end
                    end
                    % change chosen var to repopulate var panels
                    chosenVar = newVar;
                    refreshVarProps();
                    propsChanged = 0;
                end
            end
            
            uicontrol('Parent', varPropFig, 'Style','PushButton','Units','Normalized','Position',[0.39 0.03 0.1 0.05],...
                'FontSize',11, 'FontWeight','bold', 'String','Done', 'CallBack',{@done_cb});
            function[] = done_cb(~,~)
                choice = questdlg(['This will save changes to all variable properties ',...
                    'and close this window. Would you like to proceed?'],...
                    'Save and Close', ...
                    'Yes','No','No');
                % Handle response
                switch choice
                    case 'Yes'
                        currMGroup = myData.data.currMasterVarsGroup;
                        saveMasterGroup = inputdlg(['Enter a group name for the saved variables ',...
                            '(group name must be a valid Matlab variable name): '],...
                            'Save Variable Group', 1, {currMGroup});
                        if isempty(saveMasterGroup)
                            return;
                        else
                            while ~isvarname(saveMasterGroup{1})
                                errordlg([saveMasterGroup{1}, ' is not a valid group name! The group',...
                                    ' name must be a valid Matlab variable name. A valid variable',...
                                    ' name is a character string of letters, digits and underscores.',...
                                    ' The first character must be a letter, and the name cannot be a keyword.'],...
                                    'Group Name Error','modal');
                                saveMasterGroup = inputdlg(['Enter a group name for the saved variables ',...
                                    '(group name must be a valid Matlab variable name): '],...
                                    'Save Variable Group', 1, {currMGroup});
                                if isempty(saveMasterGroup)
                                    return;
                                end
                            end
                        end
                        
                        saveVarProps();
                        
                        % clear all data from var props before saving to
                        % file
                        fieldNames = fieldnames(masterVars.Z);
                        % iteratively set each Z variable's "data" field to empty array
                        for fieldNum = 1:length(fieldNames)
                            masterVars.Z.(fieldNames{fieldNum}).data = [];
                        end
                        fieldNames = fieldnames(masterVars.N2);
                        % iteratively set each N2 variable's "data" field to empty array
                        for fieldNum = 1:length(fieldNames)
                            masterVars.N2.(fieldNames{fieldNum}).data = [];
                        end
                        myData.data.masterCTDVars = masterVars;
                        % make sure all preferredCTDVars are found in the current master variable group
                        % delete inconsistencies by intersecting the two sets
                        myData.data.preferredCTDVarsZY = intersect(myData.data.preferredCTDVarsZY, fieldnames(masterVars.Z));
                        myData.data.preferredCTDVarsZX = intersect(myData.data.preferredCTDVarsZX, fieldnames(masterVars.Z));
                        myData.data.preferredCTDVarsN2Y = intersect(myData.data.preferredCTDVarsN2Y, fieldnames(masterVars.N2));
                        myData.data.preferredCTDVarsN2X = intersect(myData.data.preferredCTDVarsN2X, fieldnames(masterVars.N2));
                        myData.data.plottedCTDVarsZY = intersect(myData.data.plottedCTDVarsZY, myData.data.preferredCTDVarsZY);
                        myData.data.plottedCTDVarsN2Y = intersect(myData.data.plottedCTDVarsN2Y, myData.data.preferredCTDVarsN2Y);
                        if strcmpi(myData.data.currCTDDataType,'z')
                            myData.data.currCTDVarsY = myData.data.preferredCTDVarsZY;
                            myData.data.currCTDVarsX = myData.data.preferredCTDVarsZX;
                        else
                            myData.data.currCTDVarsY = myData.data.preferredCTDVarsN2Y;
                            myData.data.currCTDVarsX = myData.data.preferredCTDVarsN2X;
                        end
                        guidata(mainFig,myData);
                        
                        % save var group to MAT file
                        ls = load('masterCTDVarsGroups.mat','masterCTDVarsGroups');
                        masterCTDVarsGroups = ls.masterCTDVarsGroups;
                        masterCTDVarsGroups.(saveMasterGroup{1}) = masterVars;
                        save('masterCTDVarsGroups.mat','masterCTDVarsGroups');
                        myData.data.currMasterVarsGroup = saveMasterGroup{1};
                        guidata(mainFig,myData);
                        set(myData.handles.currVarGroup,'String',saveMasterGroup{1});
                        
                        delete(varPropFig);
                        clear myData
                        
                        myData = guidata(mainFig);
                        % re-preprocess current CTD file, if any
                        fullFilePicks = myData.data.CTDFullFilePicks;
                        clear myData
                        if ~isempty(fullFilePicks)
                            myData = guidata(mainFig);
                            currFileIndex = myData.data.currCTDFileIndex;
                            % use hash map to make sure the correct first file is
                            % pre-processed (chronological or sequential)
                            chronoHash = myData.data.chronoHash;
                            sequenHash = myData.data.sequenHash;
                            fullFileName = '';
                            if strcmp(myData.data.flistViewMode,'chronoMode')
                                fullFileName = fullFilePicks{chronoHash(currFileIndex)};
                            elseif strcmp(myData.data.flistViewMode,'sequenMode')
                                fullFileName = fullFilePicks{sequenHash(currFileIndex)};
                            end
                            clear myData
                            
                            [~,fileName,fileExt] = fileparts(fullFileName);
                            msg = [fileName, fileExt];
                            mainLogText = appendLogText(mainLogText,msg,'p_ctd');
                            % preprocess file
                            ppSuccess = preprocessCTDFile(fullFileName);
                            if ppSuccess
                                updateVarSelPanel();
                                % update text fields in fileInfoPanel
                                updateFileInfoPanel(fullFileName);
                                updatePlot();
                                resetForcedYLims();
                                updateYModPanel();
                            end
                        end
                        msg = ['One or more variables'' properties were modified in current variable group ',...
                            saveMasterGroup{1},'.'];
                        mainLogText = appendLogText(mainLogText,msg,'success');
                        mainLogText = updateLog(mainLogText,consoleBox);
                    case 'No'
                end
            end
            
            uicontrol('Parent', varPropFig, 'Style','PushButton','Units','Normalized','Position',[0.51 0.03 0.1 0.05],...
                'FontSize',11, 'FontWeight','bold', 'String','Cancel', 'CallBack',{@cancel_cb});
            function[] = cancel_cb(~,~)
                clear myData
                clear masterVars
                delete(varPropFig);
            end
            
            uicontrol('Parent',varPropFig, 'Units','normalized', 'Position',[0.75 0.03 0.2 0.05],...
                'FontSize',11, 'FontWeight','bold', 'ForegroundColor',[200/255 0 0],...
                'String','Restore System Defaults', 'Callback',{@restoreSysDefProps_cb});
            function[] = restoreSysDefProps_cb(~,~)
                choice = questdlg(['This will replace all current variables with system default variables and properties. ',...
                    'Would you like to proceed?'],...
                    'Switch variable', ...
                    'Yes','No','No');
                % Handle response
                switch choice
                    case 'Yes'
                        ls = load('masterCTDVarsGroups.mat','masterCTDVarsGroups');
                        masterCTDVarsGroups = ls.masterCTDVarsGroups;
                        masterVars = masterCTDVarsGroups.SystemDefault;
                        varNamesZ = fieldnames(masterVars.Z);
                        varNamesN2 = fieldnames(masterVars.N2);
                        if strcmpi(chosenDataType,'z')
                            set(varChoiceMenu,'String',varNamesZ);
                        else
                            set(varChoiceMenu,'String',varNamesN2);
                        end
                        varStrs = get(varChoiceMenu,'String');
                        set(varChoiceMenu,'Value',1);
                        varNum = 1;
                        chosenVar = varStrs{varNum};
                        clear masterCTDVarsGroups
                        clear ls
                        refreshVarProps();
                    case 'No'
                end
            end
            
            varPropsPanel = uipanel('Parent',varPropFig, 'Units','normalized', 'Position',[0.05 0.1 0.9 0.78],...
                'FontWeight','bold', 'Title','Variable Properties', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            
            blr = 0.01;
            bt = 0.01;
            bb = 0.1;
            wl = (1 - 2*blr)/3;
            ws = (1 - 2*blr)/4;
            h = (1 - bt - bb)/4;
            
            dispNamePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-h ws h],...
                'Title','Display Name', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['The display name is used as the Matlab variable name for a CTD variable,\n',...
                ' and is also the variable''s displayed name in the variable selection panel.\n',...
                'It must be a valid Matlab variable name.\n',...
                'A valid variable name is a character string of letters, digits and underscores.\n',...
                'The first character must be a letter, and the name cannot be a keyword.']);
            dispNameEdit = uicontrol('Parent',dispNamePanel, 'Style','edit', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String','', 'Callback',{@editDispName_cb},...
                'Tooltipstring',ts);
            % validate display name entered in field and save to local
            % variable
            function[] = editDispName_cb(hObject,~)
                str = get(hObject,'String');
                % validate entered display name must be proper matlab variable
                if ~isvarname(str)
                    errordlg([str, ' is not a valid display name! The display',...
                        ' name must be a valid Matlab variable name. A valid variable',...
                        ' name is a character string of letters, digits and underscores.',...
                        ' The first character must be a letter, and the name cannot be a keyword.'],...
                        'Display Name Error','modal');
                    set(hObject,'String',dispName);
                else
                    dispName = str;
                end
                propsChanged = 1;
            end
            
            exportNamePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+ws 1-bt-h ws h],...
                'Title','Export Name', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['The export name is used as a column header name when CTD data is saved to text file.\n',...
                'It can be any sequence of letters, digits, and other symbols.']);
            exportNameEdit = uicontrol('Parent',exportNamePanel, 'Style','edit', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String','', 'Callback',{@editExportName_cb},...
                'Tooltipstring',ts);
            function[] = editExportName_cb(hObject,~)
                str = get(hObject,'String');
                if ~isvarname(str)
                    errordlg('The export name cannot be an empty string!',...
                        'Export Name Error','modal');
                    set(hObject,'String',exportName);
                else
                    exportName = str;
                end
                propsChanged = 1;
            end
            
            aliasPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-h ws h],...
                'Title','Aliases', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['Aliases are alternate column header names for a CTD variable.\n',...
                'They are used to parse column headers when reading input data files.\n',...
                'An alias can be any sequence of letters, digits, and other symbols.']);
            chooseAlias = uicontrol('Parent',aliasPanel, 'Style','listbox', 'Units','normalized',...
                'Position',[0.05 0.3 0.9 0.65], 'Min',1, 'Max',1, 'Value',1,...
                'BackgroundColor','white', 'FontSize',10, 'String','',...
                'TooltipString',ts);
            uicontrol('Parent',aliasPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.05 0.1 0.4 0.2], 'String','Remove', 'Callback',{@removeAlias_cb},...
                'TooltipString','Remove the selected alias from list.');
            function[] = removeAlias_cb(~,~)
                val = get(chooseAlias,'Value');
                if ~isempty(aliases)
                    aliases(val) = [];
                    set(chooseAlias,'String',aliases, 'Value',1);
                end
                propsChanged = 1;
            end
            uicontrol('Parent',aliasPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.55 0.1 0.4 0.2], 'String','Add', 'Callback',{@addAlias_cb},...
                'TooltipString','Add a new alias to the list.');
            function[] = addAlias_cb(~,~)
                newAlias = inputdlg('Enter a new alias:','Add Alias');
                aliases = [aliases newAlias];
                set(chooseAlias,'String',aliases);
                set(chooseAlias,'Value',1);
                propsChanged = 1;
            end
            
            unitPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+3*ws 1-bt-h ws h],...
                'Title','Unit Name', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['The unit name for the CTD variable is displayed on the Y axis of the plot.\n',...
                'A unit name can be any sequence of letters, digits, and other symbols.']);
            unitEdit = uicontrol('Parent',unitPanel, 'Style','edit', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String','', 'Callback',{@editUnit_cb},...
                'TooltipString',ts);
            function[] = editUnit_cb(hObject,~)
                str = get(hObject,'String');
                unit = str;
                propsChanged = 1;
            end
            
            exportFormatPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-2*h 2*ws h],...
                'Title','Export Format', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            inputPanel = uipanel('Parent',exportFormatPanel, 'Units','normalized',...
                'position',[0.05 0.55 0.45 0.45], 'Title','Test Input', 'TitlePosition','lefttop');
            testIn = '9.3';
            ts = 'Enter any character sequence to test the output of the export format.';
            inputEdit = uicontrol('Parent',inputPanel, 'Style','edit',...
                'units','normalized', 'position',[0 0 1 1], 'String',testIn,...
                'callback',{@editInput}, 'TooltipString',ts, 'horizontalAlignment','left',...
                'BackgroundColor','w');
            function[] = editInput(~,~)
                updateExportFormat()
            end
            outputPanel = uipanel('Parent',exportFormatPanel, 'Units','normalized',...
                'position',[0.5 0.55 0.45 0.45], 'Title','Test Output', 'TitlePosition','lefttop');
            ts = 'Displays the test input in the currently set export format.';
            outputDisp = uicontrol('Parent',outputPanel, 'Style','edit',...
                'units','normalized', 'position',[0 0 1 1], 'String','',...
                'Enable','inactive', 'TooltipString',ts, 'horizontalAlignment','left',...
                'BackgroundColor','w');
            
            ts = 'Special flag applies special formatting to exported data based on the flag character chosen.';
            flagPanel = uipanel('Parent',exportFormatPanel, 'Units','normalized', 'Position',[0.05 0.05 0.9/4 0.5],...
                'Title','Special Flag', 'TitlePosition','lefttop');
            flagMenu = uicontrol('Parent',flagPanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0 0 1 1], 'BackgroundColor','white',...
                'String',{'none','''-'' - Left-justify','''+'' - Print sign character',...
                ''' '' - Insert space before value','''0'' - Pad with zeros'},...
                'Value',1, 'TooltipString',ts, 'Callback',{@chooseFlag_cb});
            
            function[] = chooseFlag_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                if strcmpi(str,'none')
                    str = '';
                else
                    str = str(2);
                end
                flag = str;
                updateExportFormat();
                propsChanged = 1;
            end
            
            fwidthPanel = uipanel('Parent',exportFormatPanel', 'Units','normalized', 'Position',[0.05+0.9/4 0.05 0.9/4 0.5],...
                'Title','Field Width', 'TitlePosition','lefttop');
            ts = sprintf('%s\n%s\n%s','Field width indicates how many characters exported data should occupy.',...
                'If the number of characters printed is less than field width, the characters will be space-padded.',...
                'Field width can be empty.');
            fwidthEdit = uicontrol('Parent',fwidthPanel, 'Style','edit',...
                'Units','normalized', 'Position',[0 0.25 1 0.75], 'BackgroundColor','white',...
                'String','', 'Callback',{@editFWidth_cb}, 'TooltipString',ts);
            function[] = editFWidth_cb(hObject,~)
                % must be an integer greater than or equal to one
                str = get(hObject,'String');
                d = str2double(str);
                if isnan(d) || d < 1
                    errordlg([str, ' is not a valid field width value! It must be an integer',...
                        ' greater than or equal to one.'],...
                        'Field Width Error','modal');
                    set(hObject,'String',fwidth);
                else
                    fwidth = int2str(uint8(d));
                    set(hObject,'String',fwidth);
                    updateExportFormat();
                end
                propsChanged = 1;
            end
            
            precisionPanel = uipanel('Parent',exportFormatPanel', 'Units','normalized', 'Position',[0.05+2*0.9/4 0.05 0.9/4 0.5],...
                'Title','Precision', 'TitlePosition','lefttop');
            ts = sprintf('%s\n%s\n%s\n%s','Precision indicates how many digits to print after the decimal point.',...
                'If the number of decimal digits is less than the precision number, the decimal will be zero-padded.',...
                'Precision is not limited by field width.','Precision can be empty.');
            precisionEdit = uicontrol('Parent',precisionPanel, 'Style','edit',...
                'Units','normalized', 'Position',[0 0.25 1 0.75], 'BackgroundColor','white',...
                'String','', 'Callback',{@editPrecision_cb}, 'TooltipString',ts);
            function[] = editPrecision_cb(hObject,~)
                % must be an integer greater than or equal to zero, or
                % empty
                str = get(hObject,'String');
                if isempty(str)
                    precision = str;
                    updateExportFormat();
                else
                    d = str2double(str);
                    if isnan(d) || d < 0
                        errordlg([str, ' is not a valid precision! It must be empty, or an integer',...
                            ' greater than or equal to zero.'],...
                            'Precision Error','modal');
                        set(hObject,'String',precision);
                    else
                        precision = int2str(uint8(d));
                        set(hObject,'String',precision);
                        updateExportFormat();
                    end
                end
                propsChanged = 1;
            end
            
            convPanel = uipanel('Parent',exportFormatPanel', 'Units','normalized', 'Position',[0.05+3*0.9/4 0.05 0.9/4 0.5],...
                'Title','Conv Char', 'TitlePosition','lefttop');
            ts = sprintf('%s\n%s','Conversion character indicates the type of character sequence to be printed.',...
                'Choose the type that best represents the data.');
            convMenu = uicontrol('Parent',convPanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0 0 1 1], 'BackgroundColor','white',...
                'String',{'''d'' - signed integer','''f'' - fixed-point floating number',...
                '''e'' - exponential notation','''s'' - string of characters','''c'' - single character'},...
                'Value',1, 'Callback',{@chooseConvChar_cb}, 'Tooltipstring',ts);
            
            function[] = chooseConvChar_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                convChar = str(2);
                updateExportFormat();
                propsChanged = 1;
            end
            
            function[] = updateExportFormat()
                str = get(inputEdit,'String');
                d = str2double(str);
                if isnan(d)
                    testIn = str;
                else
                    testIn = d;
                end
                if isempty(precision)
                    exportFormat = ['%',flag,fwidth,convChar];
                else
                    exportFormat = ['%',flag,fwidth,'.',precision,convChar];
                end
                set(outputDisp,'String',sprintf(exportFormat,testIn));
            end
            
            canPlotPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-2*h ws h],...
                'Title','Should the variable be plotted?', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s\n%s','Choose ''Yes'' if the variable should be plotted as an X or Y variable on main plot.',...
                'Line customization options (below) are only enabled if ''Yes'' is chosen,',...
                'and the variable type is ''Y''.');
            canPlotMenu = uicontrol('Parent',canPlotPanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String',{'Yes','No'}, 'Callback',{@chooseCanPlot_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseCanPlot_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                if strcmpi(str,'yes')
                    canPlot = 1;
                else
                    canPlot = 0;
                end
                if canPlot
                    set(varTypeMenu,'Enable','on');
                else
                    set(varTypeMenu,'Enable','off');
                end
                if strcmpi(varType,'y') && canPlot
                    set(plotYH,'Enable','on');
                else
                    set(plotYH,'Enable','off');
                end
                propsChanged = 1;
            end
            
            varTypePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+3*ws 1-bt-2*h ws h],...
                'Title','Variable Type', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s\n%s','Choose whether the plotted variable is an X or Y variable.',...
                'Line customization options (below) are only enabled if the variable is plottable,',...
                'and the variable type is ''Y''.');
            varTypeMenu = uicontrol('Parent',varTypePanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String',{'Y','X'}, 'Callback',{@chooseVarType_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseVarType_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                varType = strs{val};
                if strcmpi(varType,'y') && canPlot
                    set(plotYH,'Enable','on');
                else
                    set(plotYH,'Enable','off');
                end
                propsChanged = 1;
            end
            
            samplePlotPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-3*h ws h],...
                'Title','Sample Plot', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            axes('Parent',samplePlotPanel, 'Units','normalized',...
                'Position',[0 0.05 1 0.95]);
            sampleLine = line(1:9,1:9);

            function[] = updateSamplePlot()
                if ~isempty(lineStyle)
                    set(sampleLine,'lineStyle',lineStyle);
                end
                if ~isempty(color)
                    set(sampleLine,'color',color);
                end
                if ~isempty(lineWidth)
                    set(sampleLine,'lineWidth',lineWidth);
                end
                if ~isempty(marker)
                    set(sampleLine,'marker',marker);
                end
                if ~isempty(markerEdgeColor)
                    set(sampleLine,'markerEdgeColor',markerEdgeColor);
                end
                if ~isempty(markerFaceColor)
                    set(sampleLine,'markerFaceColor',markerFaceColor);
                end
                if ~isempty(markerSize)
                    set(sampleLine,'markerSize',markerSize);
                end
            end
            
            lineStylePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+ws 1-bt-3*h ws h],...
                'Title','Line Style', 'TitlePosition','lefttop', 'bordertype','etchedin',...
                'visible','on');
            ts = sprintf('%s\n%s\n%s','Choose the style of the line connecting Y data points.',...
                'Choose ''none'' when you want to place a marker at each point,',...
                'but do not want the points connected with a line');
            lineStyleMenu = uicontrol('parent',lineStylePanel, 'style','popupmenu',...
                'units','normalized', 'position',[0.1 0.25 0.8 0.5], 'fontsize',10,...
                'BackgroundColor','white',...
                'string',{'''-'' - Solid line','''--'' - Dashed line',...
                ''':'' - Dotted line','''-.'' - Dash-dot line','''none'' - No line'},...
                'callback',{@chooseLineStyle_cb}, 'Value',1, 'TooltipString',ts);
            
            function[] = chooseLineStyle_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                apoInd = strfind(str,'''');
                lineStyle = str((apoInd(1)+1):(apoInd(2)-1));
                updateSamplePlot()
                propsChanged = 1;
            end
            
            colorPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-3*h ws h],...
                'Title','Line Color', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            colorBox = uicontrol('Parent',colorPanel, 'Style','pushbutton',...
                'Units','normalized', 'Position',[0.1 0.5 0.8 0.4],'Enable','off');
            
            ts = sprintf('%s','Specify the color of the line connecting Y data points.');
            colorBtn = uicontrol('Parent',colorPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.1 0.1 0.8 0.3], 'String','Change Color', 'Callback',{@chooseColor_cb},...
                'TooltipString',ts);
            function[] = chooseColor_cb(~,~)
                color = uisetcolor(colorBox);
                set(colorBox,'BackgroundColor',color);
                updateSamplePlot()
                propsChanged = 1;
            end
            
            lineWidthPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+3*ws 1-bt-3*h ws h],...
                'Title','Line Width', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s','Choose the width of the line connecting Y data points.',...
                'Default line width is 0.5 points.');
            lineWidthMenu = uicontrol('Parent',lineWidthPanel, 'Style','popupmenu', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'String',{'0.5','1.0','1.5','2.0','2.5','3.0','3.5','4.0'}, 'Callback',{@chooseLineWidth_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseLineWidth_cb(src,~)
                % must be greater than zero and a multiple of 0.5
                strs = get(src,'String');
                val = get(src,'Value');
                str = strs{val};
                d = str2double(str);
                lineWidth = d;
                updateSamplePlot()
                propsChanged = 1;
            end
            
            markerPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-4*h ws h],...
                'Title','Marker Style', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s','Choose the marker to be displayed on top of Y data points.',...
                'Marker property is independent of line style property.');
            markerMenu = uicontrol('parent',markerPanel, 'style','popupmenu',...
                'units','normalized', 'position',[0.1 0.25 0.8 0.5], 'fontsize',10,...
                'BackgroundColor','white',...
                'string',{'''+'' - Plus sign','''o'' - Circle','''*'' - Asterisk',...
                '''.'' - Point','''x'' - Cross','''s'' - Square','''d'' - Diamond',...
                '''^'' - Upward-pointing triangle','''v'' - Downward-pointing triangle',...
                '''<'' - Left-pointing triangle','''>'' - Right-pointing triangle',...
                '''p'' - Pentagram','''h'' - Hexagram','''none'' - No marker'},...
                'Value',1, 'callback',{@chooseMarker_cb}, 'tooltipstring',ts);
            
            function[] = chooseMarker_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                marker = str(2);
                updateSamplePlot()
                propsChanged = 1;
            end
            
            markerEdgeColorPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+ws 1-bt-4*h ws h],...
                'Title','Marker Edge Color', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            markerEdgeColorBox = uicontrol('Parent',markerEdgeColorPanel, 'Style','pushbutton',...
                'Units','normalized', 'Position',[0.1 0.5 0.8 0.4],'Enable','off');
            
            ts = sprintf('%s\n%s','Set the color of the marker, or the edge color for filled markers.',...
                'Filled markers are circle, square, diamond, pentagram, hexagram, and the four triangles.');
            markerEdgeColorBtn = uicontrol('Parent',markerEdgeColorPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.1 0.1 0.8 0.3], 'String','Change Color', 'Callback',{@chooseMarkerEdgeColor_cb},...
                'TooltipString',ts);
            function[] = chooseMarkerEdgeColor_cb(~,~)
                markerEdgeColor = uisetcolor(markerEdgeColorBox);
                set(markerEdgeColorBox,'BackgroundColor',markerEdgeColor);
                updateSamplePlot()
                propsChanged = 1;
            end
            
            markerFaceColorPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-4*h ws h],...
                'Title','Marker Face Color', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            markerFaceColorBox = uicontrol('Parent',markerFaceColorPanel, 'Style','pushbutton',...
                'Units','normalized', 'Position',[0.1 0.5 0.8 0.4],'Enable','off');
            
            ts = sprintf('%s\n%s','Set the fill color for markers that are closed shapes:',...
                'Circle, square, diamond, pentagram, hexagram, and the four triangles.');
            markerFaceColorBtn = uicontrol('Parent',markerFaceColorPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.1 0.1 0.8 0.3], 'String','Change Color', 'Callback',{@chooseMarkerFaceColor_cb},...
                'TooltipString',ts);
            function[] = chooseMarkerFaceColor_cb(~,~)
                markerFaceColor = uisetcolor(markerFaceColorBox);
                set(markerFaceColorBox,'BackgroundColor',markerFaceColor);
                updateSamplePlot()
                propsChanged = 1;
            end
            
            markerSizePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+3*ws 1-bt-4*h ws h],...
                'Title','Marker Size', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s','Choose the size of the marker in points.',...
                'The default value is 6.');
            markerSizeMenu = uicontrol('Parent',markerSizePanel, 'Style','popupmenu', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'String',{'6' '7' '8' '9' '10' '11' '12'}, 'Callback',{@chooseMarkerSize_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseMarkerSize_cb(hObject,~)
                % must be greater than zero
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                d = str2double(str);
                markerSize = d;
                updateSamplePlot()
                propsChanged = 1;
            end
            
            uicontrol('Parent',varPropsPanel, 'Style','pushbutton', 'Units','normalized', 'Position',[0.44 0.03 0.12 0.05],...
                'String','Save', 'FontWeight','bold', 'FontSize',11, 'Callback',{@saveVarProps_cb});
            function[] = saveVarProps_cb(~,~)
                choice = questdlg(['Are you sure you want to save changes to the properties of variable ''',...
                    chosenVar,'''?'],...
                    'Save variable properties', ...
                    'Yes','No','No');
                % Handle response
                switch choice
                    case 'Yes'
                        saveVarProps();
                    case 'No'
                end
            end
            
            function[] = saveVarProps()
                masterVars.(chosenDataType).(chosenVar).dispName = dispName;
                masterVars.(chosenDataType).(chosenVar).exportName = exportName;
                masterVars.(chosenDataType).(chosenVar).aliases = aliases;
                masterVars.(chosenDataType).(chosenVar).unit = unit;
                masterVars.(chosenDataType).(chosenVar).exportFormat = exportFormat;
                masterVars.(chosenDataType).(chosenVar).canPlot = canPlot;
                masterVars.(chosenDataType).(chosenVar).varType = varType;
                masterVars.(chosenDataType).(chosenVar).color = color;
                masterVars.(chosenDataType).(chosenVar).lineWidth = lineWidth;
                masterVars.(chosenDataType).(chosenVar).lineStyle = lineStyle;
                masterVars.(chosenDataType).(chosenVar).marker = marker;
                masterVars.(chosenDataType).(chosenVar).markerEdgeColor = markerEdgeColor;
                masterVars.(chosenDataType).(chosenVar).markerFaceColor = markerFaceColor;
                masterVars.(chosenDataType).(chosenVar).markerSize = markerSize;
            end
            
            % Save the components that should be disabled if the chosen variable is
            % not a plottable Y variable
            plotYH = [colorBtn, lineStyleMenu, lineWidthMenu, markerMenu,...
                markerEdgeColorBtn, markerFaceColorBtn, markerSizeMenu];
            
            % Initial variable's properties
            varNamesZ = fieldnames(masterVars.Z);
            varNamesN2 = fieldnames(masterVars.N2);
            if strcmpi(chosenDataType,'z')
                set(varChoiceMenu,'String',varNamesZ);
            else
                set(varChoiceMenu,'String',varNamesN2);
            end
            varStrs = get(varChoiceMenu,'String');
            varNum = get(varChoiceMenu,'Value');
            chosenVar = varStrs{varNum};            
            refreshVarProps();
            
            % repopulate the var prop panels with the values of the chosen
            % variable
            function[] = refreshVarProps()
                chosenVarProps = masterVars.(chosenDataType).(chosenVar);
                dispName = chosenVarProps.dispName;
                exportName = chosenVarProps.exportName;
                aliases = chosenVarProps.aliases;
                unit = chosenVarProps.unit;
                exportFormat = chosenVarProps.exportFormat;
                canPlot = chosenVarProps.canPlot;
                varType = chosenVarProps.varType;
                color = chosenVarProps.color;
                lineWidth = chosenVarProps.lineWidth;
                lineStyle = chosenVarProps.lineStyle;
                marker = chosenVarProps.marker;
                markerEdgeColor = chosenVarProps.markerEdgeColor;
                markerFaceColor = chosenVarProps.markerFaceColor;
                markerSize = chosenVarProps.markerSize;
                
                set(dispNameEdit, 'String',dispName);
                set(exportNameEdit, 'String',exportName);
                set(chooseAlias,'String',aliases);
                set(unitEdit,'String',unit);
                
                % %<flag><field width>.<precision><conversion character>
                function[flag,fwidth,precision,convChar] = parseExportFormat(format)
                    flagInd = regexp(format,'[0\-\+ #]');
                    if isempty(flagInd)
                        flag = '';
                    else
                        flag = format(flagInd);
                    end
                    [fwidthStart fwidthEnd] = regexp(format,'[1-9]+\d*\D','once');
                    fwidth = format(fwidthStart:fwidthEnd-1);
                    [precisionStart precisionEnd] = regexp(format,'\.\d+');
                    if isempty(precisionStart)
                        precision = '';
                    else
                        precision = format(precisionStart+1:precisionEnd);
                    end
                    convChar = format(end);
                end
                
                [flag,fwidth,precision,convChar] = parseExportFormat(exportFormat);
                switch flag
                    case '-'
                        set(flagMenu,'Value',2);
                    case '+'
                        set(flagMenu,'Value',3);
                    case ' '
                        set(flagMenu,'Value',4);
                    case '0'
                        set(flagMenu,'Value',5);
                    otherwise
                        set(flagMenu,'Value',1);
                end
                
                set(fwidthEdit,'String',fwidth);
                set(precisionEdit,'String',precision);
                switch convChar
                    case 'd'
                        set(convMenu,'Value',1);
                    case 'f'
                        set(convMenu,'Value',2);
                    case 'e'
                        set(convMenu,'Value',3);
                    case 's'
                        set(convMenu,'Value',4);
                    case 'c'
                        set(convMenu,'Value',5);
                    otherwise
                        set(convMenu,'Value',1);
                end
                
                updateExportFormat();
                
                if ~isempty(canPlot)
                    if canPlot == 1
                        set(canPlotMenu,'Value',1);
                    else
                        set(canPlotMenu,'Value',2);
                    end
                end
                if canPlot
                    set(varTypeMenu,'Enable','on');
                else
                    set(varTypeMenu,'Enable','off');
                end
                if ~isempty(varType)
                    varTypeInd = find(strcmpi(varType,get(varTypeMenu,'String')),1);
                    set(varTypeMenu,'Value',varTypeInd);
                end
                switch lineStyle
                    case '-'
                        set(lineStyleMenu,'Value',1);
                    case '--'
                        set(lineStyleMenu,'Value',2);
                    case ':'
                        set(lineStyleMenu,'Value',3);
                    case '-.'
                        set(lineStyleMenu,'Value',4);
                    case 'none'
                        set(lineStyleMenu,'Value',5);
                    otherwise
                        set(lineStyleMenu,'Value',5);
                end
                if ~isempty(color)
                    set(colorBox,'BackgroundColor',color);
                end
                widths = get(lineWidthMenu,'String');
                if ~isempty(lineWidth)
                    set(lineWidthMenu,'Value',find(strcmpi(widths,num2str(lineWidth)),1));
                else
                    set(lineWidthMenu,'Value',1);
                end
                switch marker
                    case '+'
                        set(markerMenu,'Value',1);
                    case 'o'
                        set(markerMenu,'Value',2);
                    case '*'
                        set(markerMenu,'Value',3);
                    case '.'
                        set(markerMenu,'Value',4);
                    case 'x'
                        set(markerMenu,'Value',5);
                    case 's'
                        set(markerMenu,'Value',6);
                    case 'd'
                        set(markerMenu,'Value',7);
                    case '^'
                        set(markerMenu,'Value',8);
                    case 'v'
                        set(markerMenu,'Value',9);
                    case '<'
                        set(markerMenu,'Value',10);
                    case '>'
                        set(markerMenu,'Value',11);
                    case 'p'
                        set(markerMenu,'Value',12);
                    case 'h'
                        set(markerMenu,'Value',13);
                    case 'none'
                        set(markerMenu,'Value',14);
                    otherwise
                        set(markerMenu,'Value',14);
                end
                if ~isempty(markerEdgeColor)
                    set(markerEdgeColorBox,'BackgroundColor',markerEdgeColor);
                end
                if ~isempty(markerFaceColor)
                    set(markerFaceColorBox,'BackgroundColor',markerFaceColor);
                end
                msizes = get(markerSizeMenu,'String');
                if ~isempty(markerSize)
                    set(markerSizeMenu,'Value',find(strcmpi(msizes,num2str(markerSize)),1));
                else
                    set(markerSizeMenu,'Value',1);
                end
                
                updateSamplePlot();
                
                if strcmpi(varType,'y') && canPlot
                    set(plotYH,'Enable','on');
                else
                    set(plotYH,'Enable','off');
                end
            end
            
            set(varPropFig,'Visible','on');            
        end
    end

    function[] = addNewVar_cb(~,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        addNewVar();
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
        
        function addNewVar()
            myData = guidata(mainFig);
            masterVars = myData.data.masterCTDVars;
            
            dispName = '';
            exportName = '';
            aliases = {};
            unit = '';
            exportFormat = '';
            flag = '';
            fwidth = [];
            precision = [];
            convChar = '';
            dataType = '';
            canPlot = [];
            varType = '';
            color = [];
            lineWidth = [];
            lineStyle = '';
            marker = '';
            markerEdgeColor = [];
            markerFaceColor = [];
            markerSize = [];
            
            plotYH = [];
            
            figWidth = screenSize(3);
            figHeight = screenSize(4)-6;
            
            newVarFig = figure ( 'windowstyle', 'normal', 'resize', 'on', ...
                'name','Add New Variable', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                'Tag', 'newVarFig', 'units','characters', 'Position',[100 100 figWidth figHeight],...
                'CreateFcn', {@movegui,'north'});
            
            set(newVarFig,'windowstyle','modal');
            set(newVarFig,'Color',defaultBackgroundColor);
            
            uicontrol('Parent', newVarFig, 'Style','PushButton','Units','Normalized','Position',[0.33 0.03 0.1 0.05],...
                'FontSize',10, 'FontWeight','bold', 'String','Done', 'CallBack',{@done_cb});
            function[] = done_cb(~,~)
                % TODO: check for validity of dispName, exportName
                
                choice = questdlg(['This will save the properties of new variable ''',...
                    dispName, ''', add the new variable to the current variable group, ',...
                    'and close this window. Would you like to proceed?'],...
                    'Save and Close', ...
                    'Yes','No','No');
                % Handle response
                switch choice
                    case 'Yes'
                        % check if dispName is redundant
                        fn = fieldnames(masterVars.(dataType));
                        if sum(strcmpi(fn,dispName)) > 0
                            errordlg(['The variable ', dispName, ' is already present in the current variable group!',...
                                ' Please give the new variable a unique display name.'],...
                                'Redundant Display Name','modal');
                        else
                            currMGroup = myData.data.currMasterVarsGroup;
                            saveMasterGroup = inputdlg(['Enter a group name for the newly appended variable group ',...
                                '(group name must be a valid Matlab variable name): '],...
                                'Save Variable Group', 1, {currMGroup});
                            if isempty(saveMasterGroup)
                                return;
                            else
                                while ~isvarname(saveMasterGroup{1})
                                    errordlg([saveMasterGroup{1}, ' is not a valid group name! The group',...
                                        ' name must be a valid Matlab variable name. A valid variable',...
                                        ' name is a character string of letters, digits and underscores.',...
                                        ' The first character must be a letter, and the name cannot be a keyword.'],...
                                        'Group Name Error','modal');
                                    saveMasterGroup = inputdlg(['Enter a group name for the saved variables ',...
                                        '(group name must be a valid Matlab variable name): '],...
                                        'Save Variable Group', 1, {currMGroup});
                                    if isempty(saveMasterGroup)
                                        return;
                                    end
                                end
                            end
                            
                            saveNewVarProps();
                            
                            % clear all data from var props before saving to
                            % file
                            fieldNames = fieldnames(masterVars.Z);
                            % iteratively set each Z variable's "data" field to empty array
                            for fieldNum = 1:length(fieldNames)
                                masterVars.Z.(fieldNames{fieldNum}).data = [];
                            end
                            fieldNames = fieldnames(masterVars.N2);
                            % iteratively set each N2 variable's "data" field to empty array
                            for fieldNum = 1:length(fieldNames)
                                masterVars.N2.(fieldNames{fieldNum}).data = [];
                            end
                            myData.data.masterCTDVars = masterVars;
                            guidata(mainFig,myData);
                            
                            % save var group to MAT file
                            ls = load('masterCTDVarsGroups.mat','masterCTDVarsGroups');
                            masterCTDVarsGroups = ls.masterCTDVarsGroups;
                            masterCTDVarsGroups.(saveMasterGroup{1}) = masterVars;
                            save('masterCTDVarsGroups.mat','masterCTDVarsGroups');
                            myData.data.currMasterVarsGroup = saveMasterGroup{1};
                            guidata(mainFig,myData);
                            set(myData.handles.currVarGroup,'String',saveMasterGroup{1});
                            
                            delete(newVarFig);
                            clear myData
                            
                            myData = guidata(mainFig);
                            % re-preprocess current CTD file, if any
                            fullFilePicks = myData.data.CTDFullFilePicks;
                            clear myData
                            if ~isempty(fullFilePicks)
                                myData = guidata(mainFig);
                                currFileIndex = myData.data.currCTDFileIndex;
                                % use hash map to make sure the correct first file is
                                % pre-processed (chronological or sequential)
                                chronoHash = myData.data.chronoHash;
                                sequenHash = myData.data.sequenHash;
                                fullFileName = '';
                                if strcmp(myData.data.flistViewMode,'chronoMode')
                                    fullFileName = fullFilePicks{chronoHash(currFileIndex)};
                                elseif strcmp(myData.data.flistViewMode,'sequenMode')
                                    fullFileName = fullFilePicks{sequenHash(currFileIndex)};
                                end
                                clear myData
                                
                                [~,fileName,fileExt] = fileparts(fullFileName);
                                msg = [fileName, fileExt];
                                mainLogText = appendLogText(mainLogText,msg,'p_ctd');
                                % preprocess file
                                ppSuccess = preprocessCTDFile(fullFileName);
                                if ppSuccess
                                    updateVarSelPanel();
                                    % update text fields in fileInfoPanel
                                    updateFileInfoPanel(fullFileName);
                                    updatePlot();
                                    resetForcedYLims();
                                    updateYModPanel();
                                end
                            end
                            msg = ['One or more new variables were added to current variable group ',...
                                saveMasterGroup{1},'. Use the ''Set Variables'' button to enable newly added variables.'];
                            mainLogText = appendLogText(mainLogText,msg,'success');
                            mainLogText = updateLog(mainLogText,consoleBox);
                        end
                    case 'No'
                end
            end
            
            uicontrol('Parent', newVarFig, 'Style','PushButton','Units','Normalized','Position',[0.57 0.03 0.1 0.05],...
                'FontSize',10, 'FontWeight','bold', 'String','Cancel', 'CallBack',{@cancel_cb});
            function[] = cancel_cb(~,~)
                clear myData
                clear masterVars
                delete(newVarFig);
            end
            
            uicontrol('Parent',newVarFig, 'Style','pushbutton', 'units','normalized',...
                'Position',[0.45 0.03 0.1 0.05], 'FontSize',10, 'FontWeight','bold',...
                'String','Add Another', 'CallBack',{@addAnother_cb});
            function[] = addAnother_cb(~,~)
                % TODO: check for validity of dispName, exportName
                
                choice = questdlg(['This will save the properties of new variable ''',...
                    dispName, ''', add the new variable to the current variable group, ',...
                    'and reset this window. Would you like to proceed?'],...
                    'Save and Reset', ...
                    'Yes','No','No');
                % Handle response
                switch choice
                    case 'Yes'
                        % check if dispName is redundant
                        fn = fieldnames(masterVars.(dataType));
                        if sum(strcmpi(fn,dispName)) > 0
                            errordlg(['The variable ', dispName, ' is already present in the current variable group!',...
                                ' Please give the new variable a unique display name.'],...
                                'Redundant Display Name','modal');
                        else
                            saveNewVarProps();
                            resetVarProps();
                        end
                    case 'No'
                end
            end
                        
            function[] = saveNewVarProps()
                masterVars.(dataType).(dispName).dispName = dispName;
                masterVars.(dataType).(dispName).exportName = exportName;
                masterVars.(dataType).(dispName).aliases = aliases;
                masterVars.(dataType).(dispName).unit = unit;
                masterVars.(dataType).(dispName).exportFormat = exportFormat;
                masterVars.(dataType).(dispName).canPlot = canPlot;
                masterVars.(dataType).(dispName).varType = varType;
                masterVars.(dataType).(dispName).color = color;
                masterVars.(dataType).(dispName).lineWidth = lineWidth;
                masterVars.(dataType).(dispName).lineStyle = lineStyle;
                masterVars.(dataType).(dispName).marker = marker;
                masterVars.(dataType).(dispName).markerEdgeColor = markerEdgeColor;
                masterVars.(dataType).(dispName).markerFaceColor = markerFaceColor;
                masterVars.(dataType).(dispName).markerSize = markerSize;
                masterVars.(dataType).(dispName).data = [];
            end
            
            varPropsPanel = uipanel('Parent',newVarFig, 'Units','normalized', 'Position',[0.05 0.1 0.9 0.9],...
                'FontWeight','bold', 'Title','Variable Properties', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            
            blr = 0.01;
            bt = 0.01;
            bb = 0.01;
            wl = (1 - 2*blr)/3;
            ws = (1 - 2*blr)/4;
            h = (1 - bt - bb)/4;
            
            dispNamePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-h ws h],...
                'Title','Display Name', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['The display name is used as the Matlab variable name for a CTD variable,\n',...
                ' and is also the variable''s displayed name in the variable selection panel.\n',...
                'It must be a valid Matlab variable name.\n',...
                'A valid variable name is a character string of letters, digits and underscores.\n',...
                'The first character must be a letter, and the name cannot be a keyword.']);
            dispNameEdit = uicontrol('Parent',dispNamePanel, 'Style','edit', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String','', 'Callback',{@editDispName_cb},...
                'Tooltipstring',ts);
            % validate display name entered in field and save to local
            % variable
            function[] = editDispName_cb(hObject,~)
                str = get(hObject,'String');
                % validate entered display name must be proper matlab variable
                if ~isvarname(str)
                    errordlg([str, ' is not a valid display name! The display',...
                        ' name must be a valid Matlab variable name. A valid variable',...
                        ' name is a character string of letters, digits and underscores.',...
                        ' The first character must be a letter, and the name cannot be a keyword.'],...
                        'Display Name Error','modal');
                    set(hObject,'String',dispName);
                else
                    dispName = str;
                end
            end
            
            exportNamePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+ws 1-bt-h ws h],...
                'Title','Export Name', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['The export name is used as a column header name when CTD data is saved to text file.\n',...
                'It can be any sequence of letters, digits, and other symbols.']);
            exportNameEdit = uicontrol('Parent',exportNamePanel, 'Style','edit', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String','', 'Callback',{@editExportName_cb},...
                'Tooltipstring',ts);
            function[] = editExportName_cb(hObject,~)
                str = get(hObject,'String');
                if ~isvarname(str)
                    errordlg('The export name cannot be an empty string!',...
                        'Export Name Error','modal');
                    set(hObject,'String',exportName);
                else
                    exportName = str;
                end
            end
            
            aliasPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-h ws h],...
                'Title','Aliases', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['Aliases are alternate column header names for a CTD variable.\n',...
                'They are used to parse column headers when reading input data files.\n',...
                'An alias can be any sequence of letters, digits, and other symbols.']);
            chooseAlias = uicontrol('Parent',aliasPanel, 'Style','listbox', 'Units','normalized',...
                'Position',[0.05 0.3 0.9 0.65], 'Min',1, 'Max',1, 'Value',1,...
                'BackgroundColor','white', 'FontSize',10, 'String','',...
                'TooltipString',ts);
            uicontrol('Parent',aliasPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.05 0.1 0.4 0.2], 'String','Remove', 'Callback',{@removeAlias_cb},...
                'TooltipString','Remove the selected alias from list.');
            function[] = removeAlias_cb(~,~)
                val = get(chooseAlias,'Value');
                if ~isempty(aliases)
                    aliases(val) = [];
                    set(chooseAlias,'String',aliases, 'Value',1);
                end
            end
            uicontrol('Parent',aliasPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.55 0.1 0.4 0.2], 'String','Add', 'Callback',{@addAlias_cb},...
                'TooltipString','Add a new alias to the list.');
            function[] = addAlias_cb(~,~)
                newAlias = inputdlg('Enter a new alias:','Add Alias');
                aliases = [aliases newAlias];
                set(chooseAlias,'String',aliases);
                set(chooseAlias,'Value',1);
            end
            
            unitPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+3*ws 1-bt-h ws h],...
                'Title','Unit Name', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf(['The unit name for the CTD variable is displayed on the Y axis of the plot.\n',...
                'A unit name can be any sequence of letters, digits, and other symbols.']);
            unitEdit = uicontrol('Parent',unitPanel, 'Style','edit', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String','', 'Callback',{@editUnit_cb},...
                'TooltipString',ts);
            function[] = editUnit_cb(hObject,~)
                str = get(hObject,'String');
                unit = str;
            end
            
            exportFormatPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-2*h 2*ws h],...
                'Title','Export Format', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            inputPanel = uipanel('Parent',exportFormatPanel, 'Units','normalized',...
                'position',[0.05 0.55 0.45 0.45], 'Title','Test Input', 'TitlePosition','lefttop');
            testIn = '9.3';
            ts = 'Enter any character sequence to test the output of the export format.';
            inputEdit = uicontrol('Parent',inputPanel, 'Style','edit',...
                'units','normalized', 'position',[0 0 1 1], 'String',testIn,...
                'callback',{@editInput}, 'TooltipString',ts, 'horizontalAlignment','left',...
                'BackgroundColor','w');
            function[] = editInput(~,~)
                updateExportFormat()
            end
            outputPanel = uipanel('Parent',exportFormatPanel, 'Units','normalized',...
                'position',[0.5 0.55 0.45 0.45], 'Title','Test Output', 'TitlePosition','lefttop');
            ts = 'Displays the test input in the currently set export format.';
            outputDisp = uicontrol('Parent',outputPanel, 'Style','edit',...
                'units','normalized', 'position',[0 0 1 1], 'String','',...
                'Enable','inactive', 'TooltipString',ts, 'horizontalAlignment','left',...
                'BackgroundColor','w');
            
            ts = 'Special flag applies special formatting to exported data based on the flag character chosen.';
            flagPanel = uipanel('Parent',exportFormatPanel, 'Units','normalized', 'Position',[0.05 0.05 0.9/4 0.5],...
                'Title','Special Flag', 'TitlePosition','lefttop');
            flagMenu = uicontrol('Parent',flagPanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0 0 1 1], 'BackgroundColor','white',...
                'String',{'none','''-'' - Left-justify','''+'' - Print sign character',...
                ''' '' - Insert space before value','''0'' - Pad with zeros'},...
                'Value',1, 'TooltipString',ts, 'Callback',{@chooseFlag_cb});
            
            function[] = chooseFlag_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                if strcmpi(str,'none')
                    str = '';
                else
                    str = str(2);
                end
                flag = str;
                updateExportFormat();
            end
            
            fwidthPanel = uipanel('Parent',exportFormatPanel', 'Units','normalized', 'Position',[0.05+0.9/4 0.05 0.9/4 0.5],...
                'Title','Field Width', 'TitlePosition','lefttop');
            ts = sprintf('%s\n%s\n%s','Field width indicates how many characters exported data should occupy.',...
                'If the number of characters printed is less than field width, the characters will be space-padded.',...
                'Field width can be empty.');
            fwidthEdit = uicontrol('Parent',fwidthPanel, 'Style','edit',...
                'Units','normalized', 'Position',[0 0.25 1 0.75], 'BackgroundColor','white',...
                'String','', 'Callback',{@editFWidth_cb}, 'TooltipString',ts);
            function[] = editFWidth_cb(hObject,~)
                % must be an integer greater than or equal to one
                str = get(hObject,'String');
                d = str2double(str);
                if isnan(d) || d < 1
                    errordlg([str, ' is not a valid field width value! It must be an integer',...
                        ' greater than or equal to one.'],...
                        'Field Width Error','modal');
                    set(hObject,'String',fwidth);
                else
                    fwidth = int2str(uint8(d));
                    set(hObject,'String',fwidth);
                    updateExportFormat();
                end
            end
            
            precisionPanel = uipanel('Parent',exportFormatPanel', 'Units','normalized', 'Position',[0.05+2*0.9/4 0.05 0.9/4 0.5],...
                'Title','Precision', 'TitlePosition','lefttop');
            ts = sprintf('%s\n%s\n%s\n%s','Precision indicates how many digits to print after the decimal point.',...
                'If the number of decimal digits is less than the precision number, the decimal will be zero-padded.',...
                'Precision is not limited by field width.','Precision can be empty.');
            precisionEdit = uicontrol('Parent',precisionPanel, 'Style','edit',...
                'Units','normalized', 'Position',[0 0.25 1 0.75], 'BackgroundColor','white',...
                'String','', 'Callback',{@editPrecision_cb}, 'TooltipString',ts);
            function[] = editPrecision_cb(hObject,~)
                % must be an integer greater than or equal to zero, or
                % empty
                str = get(hObject,'String');
                if isempty(str)
                    precision = str;
                    updateExportFormat();
                else
                    d = str2double(str);
                    if isnan(d) || d < 0
                        errordlg([str, ' is not a valid precision! It must be empty, or an integer',...
                            ' greater than or equal to zero.'],...
                            'Precision Error','modal');
                        set(hObject,'String',precision);
                    else
                        precision = int2str(uint8(d));
                        set(hObject,'String',precision);
                        updateExportFormat();
                    end
                end
            end
            
            convPanel = uipanel('Parent',exportFormatPanel', 'Units','normalized', 'Position',[0.05+3*0.9/4 0.05 0.9/4 0.5],...
                'Title','Conv Char', 'TitlePosition','lefttop');
            ts = sprintf('%s\n%s','Conversion character indicates the type of character sequence to be printed.',...
                'Choose the type that best represents the data.');
            convMenu = uicontrol('Parent',convPanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0 0 1 1], 'BackgroundColor','white',...
                'String',{'''d'' - signed integer','''f'' - fixed-point floating number',...
                '''e'' - exponential notation','''s'' - string of characters','''c'' - single character'},...
                'Value',1, 'Callback',{@chooseConvChar_cb}, 'Tooltipstring',ts);
            
            function[] = chooseConvChar_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                convChar = str(2);
                updateExportFormat();
            end
            
            function[] = updateExportFormat()
                str = get(inputEdit,'String');
                d = str2double(str);
                if isnan(d)
                    testIn = str;
                else
                    testIn = d;
                end
                if isempty(precision)
                    exportFormat = ['%',flag,fwidth,convChar];
                else
                    exportFormat = ['%',flag,fwidth,'.',precision,convChar];
                end
                set(outputDisp,'String',sprintf(exportFormat,testIn));
            end
            
            dataTypePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-2*h ws*2/3 h],...
                'Title','Data Type', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s','Choose whether the plotted variable is a Z or N2 dependent variable.',...
                'The data type is used to differentiate between duplicate variables.');
            dataTypeMenu = uicontrol('Parent',dataTypePanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String',{'Z','N2'}, 'Callback',{@chooseDataType_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseDataType_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                if strcmpi(str,'z')
                    dataType = 'Z';
                else
                    dataType = 'N2';
                end
            end
            
            canPlotPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws+ws*2/3 1-bt-2*h ws*2/3 h],...
                'Title','Should the variable be plotted?', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s\n%s','Choose ''Yes'' if the variable should be plotted as an X or Y variable on main plot.',...
                'Line customization options (below) are only enabled if ''Yes'' is chosen,',...
                'and the variable type is ''Y''.');
            canPlotMenu = uicontrol('Parent',canPlotPanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String',{'Yes','No'}, 'Callback',{@chooseCanPlot_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseCanPlot_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                if strcmpi(str,'yes')
                    canPlot = 1;
                else
                    canPlot = 0;
                end
                if canPlot
                    set(varTypeMenu,'Enable','on');
                else
                    set(varTypeMenu,'Enable','off');
                end
                if strcmpi(varType,'y') && canPlot
                    set(plotYH,'Enable','on');
                else
                    set(plotYH,'Enable','off');
                end
            end
            
            varTypePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws+ws*4/3 1-bt-2*h ws*2/3 h],...
                'Title','Variable Type', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s\n%s','Choose whether the plotted variable is an X or Y variable.',...
                'Line customization options (below) are only enabled if the variable is plottable,',...
                'and the variable type is ''Y''.');
            varTypeMenu = uicontrol('Parent',varTypePanel, 'Style','popupmenu',...
                'Units','normalized', 'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'FontSize',10, 'String',{'Y','X'}, 'Callback',{@chooseVarType_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseVarType_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                varType = strs{val};
                if strcmpi(varType,'y') && canPlot
                    set(plotYH,'Enable','on');
                else
                    set(plotYH,'Enable','off');
                end
            end
            
            samplePlotPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-3*h ws h],...
                'Title','Sample Plot', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            axes('Parent',samplePlotPanel, 'Units','normalized',...
                'Position',[0 0.05 1 0.95]);
            sampleLine = line(1:9,1:9);

            function[] = updateSamplePlot()
                if ~isempty(lineStyle)
                    set(sampleLine,'lineStyle',lineStyle);
                end
                if ~isempty(color)
                    set(sampleLine,'color',color);
                end
                if ~isempty(lineWidth)
                    set(sampleLine,'lineWidth',lineWidth);
                end
                if ~isempty(marker)
                    set(sampleLine,'marker',marker);
                end
                if ~isempty(markerEdgeColor)
                    set(sampleLine,'markerEdgeColor',markerEdgeColor);
                end
                if ~isempty(markerFaceColor)
                    set(sampleLine,'markerFaceColor',markerFaceColor);
                end
                if ~isempty(markerSize)
                    set(sampleLine,'markerSize',markerSize);
                end
            end
            
            lineStylePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+ws 1-bt-3*h ws h],...
                'Title','Line Style', 'TitlePosition','lefttop', 'bordertype','etchedin',...
                'visible','on');
            ts = sprintf('%s\n%s\n%s','Choose the style of the line connecting Y data points.',...
                'Choose ''none'' when you want to place a marker at each point,',...
                'but do not want the points connected with a line');
            lineStyleMenu = uicontrol('parent',lineStylePanel, 'style','popupmenu',...
                'units','normalized', 'position',[0.1 0.25 0.8 0.5], 'fontsize',10,...
                'BackgroundColor','white',...
                'string',{'''-'' - Solid line','''--'' - Dashed line',...
                ''':'' - Dotted line','''-.'' - Dash-dot line','''none'' - No line'},...
                'callback',{@chooseLineStyle_cb}, 'Value',1, 'TooltipString',ts);
            
            function[] = chooseLineStyle_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                apoInd = strfind(str,'''');
                lineStyle = str((apoInd(1)+1):(apoInd(2)-1));
                updateSamplePlot()
            end
            
            colorPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-3*h ws h],...
                'Title','Line Color', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            colorBox = uicontrol('Parent',colorPanel, 'Style','pushbutton',...
                'Units','normalized', 'Position',[0.1 0.5 0.8 0.4],'Enable','off');
            
            ts = sprintf('%s','Specify the color of the line connecting Y data points.');
            colorBtn = uicontrol('Parent',colorPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.1 0.1 0.8 0.3], 'String','Change Color', 'Callback',{@chooseColor_cb},...
                'TooltipString',ts);
            function[] = chooseColor_cb(~,~)
                color = uisetcolor(colorBox);
                set(colorBox,'BackgroundColor',color);
                updateSamplePlot()
            end
            
            lineWidthPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+3*ws 1-bt-3*h ws h],...
                'Title','Line Width', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s','Choose the width of the line connecting Y data points.',...
                'Default line width is 0.5 points.');
            lineWidthMenu = uicontrol('Parent',lineWidthPanel, 'Style','popupmenu', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'String',{'0.5','1.0','1.5','2.0','2.5','3.0','3.5','4.0'}, 'Callback',{@chooseLineWidth_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseLineWidth_cb(src,~)
                % must be greater than zero and a multiple of 0.5
                strs = get(src,'String');
                val = get(src,'Value');
                str = strs{val};
                d = str2double(str);
                lineWidth = d;
                updateSamplePlot()
            end
            
            markerPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr 1-bt-4*h ws h],...
                'Title','Marker Style', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s','Choose the marker to be displayed on top of Y data points.',...
                'Marker property is independent of line style property.');
            markerMenu = uicontrol('parent',markerPanel, 'style','popupmenu',...
                'units','normalized', 'position',[0.1 0.25 0.8 0.5], 'fontsize',10,...
                'BackgroundColor','white',...
                'string',{'''+'' - Plus sign','''o'' - Circle','''*'' - Asterisk',...
                '''.'' - Point','''x'' - Cross','''s'' - Square','''d'' - Diamond',...
                '''^'' - Upward-pointing triangle','''v'' - Downward-pointing triangle',...
                '''<'' - Left-pointing triangle','''>'' - Right-pointing triangle',...
                '''p'' - Pentagram','''h'' - Hexagram','''none'' - No marker'},...
                'Value',1, 'callback',{@chooseMarker_cb}, 'tooltipstring',ts);
            
            function[] = chooseMarker_cb(hObject,~)
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                marker = str(2);
                updateSamplePlot();
            end
            
            markerEdgeColorPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+ws 1-bt-4*h ws h],...
                'Title','Marker Edge Color', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            markerEdgeColorBox = uicontrol('Parent',markerEdgeColorPanel, 'Style','pushbutton',...
                'Units','normalized', 'Position',[0.1 0.5 0.8 0.4],'Enable','off');
            
            ts = sprintf('%s\n%s','Set the color of the marker, or the edge color for filled markers.',...
                'Filled markers are circle, square, diamond, pentagram, hexagram, and the four triangles.');
            markerEdgeColorBtn = uicontrol('Parent',markerEdgeColorPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.1 0.1 0.8 0.3], 'String','Change Color', 'Callback',{@chooseMarkerEdgeColor_cb},...
                'TooltipString',ts);
            function[] = chooseMarkerEdgeColor_cb(~,~)
                markerEdgeColor = uisetcolor(markerEdgeColorBox);
                set(markerEdgeColorBox,'BackgroundColor',markerEdgeColor);
                updateSamplePlot()
            end
            
            markerFaceColorPanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+2*ws 1-bt-4*h ws h],...
                'Title','Marker Face Color', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            markerFaceColorBox = uicontrol('Parent',markerFaceColorPanel, 'Style','pushbutton',...
                'Units','normalized', 'Position',[0.1 0.5 0.8 0.4],'Enable','off');
            
            ts = sprintf('%s\n%s','Set the fill color for markers that are closed shapes:',...
                'Circle, square, diamond, pentagram, hexagram, and the four triangles.');
            markerFaceColorBtn = uicontrol('Parent',markerFaceColorPanel, 'Style','pushbutton', 'Units','normalized',...
                'Position',[0.1 0.1 0.8 0.3], 'String','Change Color', 'Callback',{@chooseMarkerFaceColor_cb},...
                'TooltipString',ts);
            function[] = chooseMarkerFaceColor_cb(~,~)
                markerFaceColor = uisetcolor(markerFaceColorBox);
                set(markerFaceColorBox,'BackgroundColor',markerFaceColor);
                updateSamplePlot()
            end
            
            markerSizePanel = uipanel('Parent',varPropsPanel, 'Units','normalized', 'Position',[blr+3*ws 1-bt-4*h ws h],...
                'Title','Marker Size', 'TitlePosition','lefttop', 'BorderType','etchedin',...
                'Visible','on');
            ts = sprintf('%s\n%s','Choose the size of the marker in points.',...
                'The default value is 6.');
            markerSizeMenu = uicontrol('Parent',markerSizePanel, 'Style','popupmenu', 'Units','normalized',...
                'Position',[0.05 0.25 0.9 0.5], 'BackgroundColor','white',...
                'String',{'6' '7' '8' '9' '10' '11' '12'}, 'Callback',{@chooseMarkerSize_cb},...
                'Value',1, 'TooltipString',ts);
            
            function[] = chooseMarkerSize_cb(hObject,~)
                % must be greater than zero
                strs = get(hObject,'String');
                val = get(hObject,'Value');
                str = strs{val};
                d = str2double(str);
                markerSize = d;
                updateSamplePlot()
            end
            
            % Save the components that should be disabled if the chosen variable is
            % not a plottable Y variable
            plotYH = [colorBtn, lineStyleMenu, lineWidthMenu, markerMenu,...
                markerEdgeColorBtn, markerFaceColorBtn, markerSizeMenu];
                       
            resetVarProps();
            
            % repopulate the var prop panels with default values
            function[] = resetVarProps()
                dispName = 'newVar';
                exportName = 'newVar';
                aliases = {};
                unit = '';
                exportFormat = '11d';
                flag = '';
                fwidth = [];
                precision = [];
                convChar = '';
                dataType = 'Z';
                canPlot = 1;
                varType = 'Y';
                color = [0 0 0];
                lineWidth = 1.5;
                lineStyle = '-';
                marker = '';
                markerEdgeColor = [];
                markerFaceColor = [];
                markerSize = [];
                
                set(dispNameEdit, 'String',dispName);
                set(exportNameEdit, 'String',exportName);
                set(chooseAlias,'String',aliases);
                set(unitEdit,'String',unit);
                
                % %<flag><field width>.<precision><conversion character>
                function[flag,fwidth,precision,convChar] = parseExportFormat(format)
                    flagInd = regexp(format,'[0\-\+ #]');
                    if isempty(flagInd)
                        flag = '';
                    else
                        flag = format(flagInd);
                    end
                    [fwidthStart fwidthEnd] = regexp(format,'[1-9]+\d*\D','once');
                    fwidth = format(fwidthStart:fwidthEnd-1);
                    [precisionStart precisionEnd] = regexp(format,'\.\d+');
                    if isempty(precisionStart)
                        precision = '';
                    else
                        precision = format(precisionStart+1:precisionEnd);
                    end
                    convChar = format(end);
                end
                
                [flag,fwidth,precision,convChar] = parseExportFormat(exportFormat);
                switch flag
                    case '-'
                        set(flagMenu,'Value',2);
                    case '+'
                        set(flagMenu,'Value',3);
                    case ' '
                        set(flagMenu,'Value',4);
                    case '0'
                        set(flagMenu,'Value',5);
                    otherwise
                        set(flagMenu,'Value',1);
                end
                
                set(fwidthEdit,'String',fwidth);
                set(precisionEdit,'String',precision);
                switch convChar
                    case 'd'
                        set(convMenu,'Value',1);
                    case 'f'
                        set(convMenu,'Value',2);
                    case 'e'
                        set(convMenu,'Value',3);
                    case 's'
                        set(convMenu,'Value',4);
                    case 'c'
                        set(convMenu,'Value',5);
                    otherwise
                        set(convMenu,'Value',1);
                end
                
                updateExportFormat();
                if ~isempty(dataType)
                    if strcmpi(dataType,'z')
                        set(dataTypeMenu,'Value',1);
                    elseif strcmpi(dataType,'n2')
                        set(dataTypeMenu,'Value',2);
                    end
                end
                if ~isempty(canPlot)
                    if canPlot == 1
                        set(canPlotMenu,'Value',1);
                    else
                        set(canPlotMenu,'Value',2);
                    end
                end
                if canPlot
                    set(varTypeMenu,'Enable','on');
                else
                    set(varTypeMenu,'Enable','off');
                end
                if ~isempty(varType)
                    varTypeInd = find(strcmpi(varType,get(varTypeMenu,'String')),1);
                    set(varTypeMenu,'Value',varTypeInd);
                end
                switch lineStyle
                    case '-'
                        set(lineStyleMenu,'Value',1);
                    case '--'
                        set(lineStyleMenu,'Value',2);
                    case ':'
                        set(lineStyleMenu,'Value',3);
                    case '-.'
                        set(lineStyleMenu,'Value',4);
                    case 'none'
                        set(lineStyleMenu,'Value',5);
                    otherwise
                        set(lineStyleMenu,'Value',5);
                end
                if ~isempty(color)
                    set(colorBox,'BackgroundColor',color);
                end
                widths = get(lineWidthMenu,'String');
                if ~isempty(lineWidth)
                    set(lineWidthMenu,'Value',find(strcmpi(widths,num2str(lineWidth)),1));
                else
                    set(lineWidthMenu,'Value',1);
                end
                switch marker
                    case '+'
                        set(markerMenu,'Value',1);
                    case 'o'
                        set(markerMenu,'Value',2);
                    case '*'
                        set(markerMenu,'Value',3);
                    case '.'
                        set(markerMenu,'Value',4);
                    case 'x'
                        set(markerMenu,'Value',5);
                    case 's'
                        set(markerMenu,'Value',6);
                    case 'd'
                        set(markerMenu,'Value',7);
                    case '^'
                        set(markerMenu,'Value',8);
                    case 'v'
                        set(markerMenu,'Value',9);
                    case '<'
                        set(markerMenu,'Value',10);
                    case '>'
                        set(markerMenu,'Value',11);
                    case 'p'
                        set(markerMenu,'Value',12);
                    case 'h'
                        set(markerMenu,'Value',13);
                    case 'none'
                        set(markerMenu,'Value',14);
                    otherwise
                        set(markerMenu,'Value',14);
                end
                if ~isempty(markerEdgeColor)
                    set(markerEdgeColorBox,'BackgroundColor',markerEdgeColor);
                end
                if ~isempty(markerFaceColor)
                    set(markerFaceColorBox,'BackgroundColor',markerFaceColor);
                end
                msizes = get(markerSizeMenu,'String');
                if ~isempty(markerSize)
                    set(markerSizeMenu,'Value',find(strcmpi(msizes,num2str(markerSize)),1));
                else
                    set(markerSizeMenu,'Value',1);
                end
                
                updateSamplePlot();
                
                if strcmpi(varType,'y') && canPlot
                    set(plotYH,'Enable','on');
                else
                    set(plotYH,'Enable','off');
                end
            end
            set(newVarFig,'Visible','on');
        end
    end

% load or delete a saved variable property group and/or set default group
    function[] = openVarGroup_cb(~,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        openVarGroupFig();
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
        
        function[] = openVarGroupFig()
            figWidth = screenSize(3)/4;
            figHeight = screenSize(4)/3;
            varGroupFig = figure ( 'windowstyle', 'normal', 'resize', 'off', ...
                'name','Load/Manage Variable Property Group', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                'Tag', 'varGroupFig', 'units','characters', 'Position',[10 10 figWidth figHeight],...
                'CreateFcn', {@movegui,'center'}, 'Color',defaultBackgroundColor);
            
            % display default var group
            defGroupPanel = uipanel('Parent',varGroupFig, 'units','normalized',...
                'position',[0.1 0.8 0.8 0.15], 'title','Default Variable Property Group');
            defaultGroup = getpref('lightgui','defaultMasterCTDVarsGroup');
            defGroupDisp = uicontrol('Parent',defGroupPanel, 'style','edit', 'units','normalized',...
                'position',[0 0 1 1], 'String',defaultGroup, 'Enable','inactive');
            
            % list saved var groups
            listPanel = uipanel('Parent',varGroupFig, 'units','normalized',...
                'position',[0.1 0.2 0.8 0.55], 'title','Saved Variable Property Groups');
            varGroupList = uicontrol('Parent',listPanel, 'style','listbox', 'units','normalized',...
                'position',[0 0 1 1], 'BackgroundColor','w', 'Callback',{@selectGroup});
            % get saved var groups from MAT file and hide system default
            % group
            ls = load('masterCTDVarsGroups.mat');
            mgroups = ls.masterCTDVarsGroups;
            mgroupNames = fieldnames(mgroups);
            mgroupNames(strcmpi(mgroupNames,'systemdefault')) = [];
            set(varGroupList,'String',mgroupNames);
            myData = guidata(mainFig);
            currGroup = myData.data.currMasterVarsGroup;
            set(varGroupList,'Value',find(strcmpi(mgroupNames,currGroup),1));
            
            function[] = selectGroup(src,~)
                val = get(src,'Value');
                list = get(src,'String');
                currGroup = list{val};
            end
            
            % load var group button
            ts = 'Load selected variable group into program';
            uicontrol('Parent',varGroupFig, 'units','normalized', 'position',[0.1 0.05 0.2 0.1],...
                'String','Load', 'TooltipString', ts, 'Callback',{@loadGroup});
            function[] = loadGroup(~,~)
                myData.data.currMasterVarsGroup = currGroup;
                guidata(mainFig,myData);
                uiresume(gcbf);
            end
            % set as default button
            ts = sprintf(['Set the selected variable group as the default.',...
                '\n','The default group will be loaded automatically when the program is initialized.']);
            uicontrol('Parent',varGroupFig, 'units','normalized', 'position',[0.35 0.05 0.3 0.1],...
                'String','Set as Default', 'TooltipString', ts, 'Callback',{@setDefGroup});
            function[] = setDefGroup(~,~)
                defaultGroup = currGroup;
                setpref('lightgui','defaultMasterCTDVarsGroup',defaultGroup);
                set(defGroupDisp,'String',defaultGroup);
            end
            % cancel button
            uicontrol('Parent',varGroupFig, 'units','normalized', 'position',[0.7 0.05 0.2 0.1],...
                'String','Cancel', 'Callback',{@cancel_cb});
            function[] = cancel_cb(~,~)
                uiresume(gcbf);
                cancelled = 1;
            end
            
            set(varGroupFig,'windowstyle','modal');
            set(varGroupFig,'visible','on');
            cancelled = 0;
            uiwait(gcf);
            if ~cancelled
                set(myData.handles.currVarGroup,'String',currGroup);
                myData.data.masterCTDVars = ls.masterCTDVarsGroups.(currGroup);
                % make sure all preferredCTDVars are found in the current master variable group
                % delete inconsistencies by intersecting the two sets
                myData.data.preferredCTDVarsZY = intersect(myData.data.preferredCTDVarsZY, fieldnames(myData.data.masterCTDVars.Z));
                myData.data.preferredCTDVarsZX = intersect(myData.data.preferredCTDVarsZX, fieldnames(myData.data.masterCTDVars.Z));
                myData.data.preferredCTDVarsN2Y = intersect(myData.data.preferredCTDVarsN2Y, fieldnames(myData.data.masterCTDVars.N2));
                myData.data.preferredCTDVarsN2X = intersect(myData.data.preferredCTDVarsN2X, fieldnames(myData.data.masterCTDVars.N2));
                myData.data.plottedCTDVarsZY = intersect(myData.data.plottedCTDVarsZY, myData.data.preferredCTDVarsZY);
                myData.data.plottedCTDVarsN2Y = intersect(myData.data.plottedCTDVarsN2Y, myData.data.preferredCTDVarsN2Y);
                if strcmpi(myData.data.currCTDDataType,'z')
                    myData.data.currCTDVarsY = myData.data.preferredCTDVarsZY;
                    myData.data.currCTDVarsX = myData.data.preferredCTDVarsZX;
                else
                    myData.data.currCTDVarsY = myData.data.preferredCTDVarsN2Y;
                    myData.data.currCTDVarsX = myData.data.preferredCTDVarsN2X;
                end
                guidata(mainFig,myData);
                % re-preprocess current CTD file, if any
                fullFilePicks = myData.data.CTDFullFilePicks;
                clear myData
                if ~isempty(fullFilePicks)
                    myData = guidata(mainFig);
                    currFileIndex = myData.data.currCTDFileIndex;
                    % use hash map to make sure the correct first file is
                    % pre-processed (chronological or sequential)
                    chronoHash = myData.data.chronoHash;
                    sequenHash = myData.data.sequenHash;
                    fullFileName = '';
                    if strcmp(myData.data.flistViewMode,'chronoMode')
                        fullFileName = fullFilePicks{chronoHash(currFileIndex)};
                    elseif strcmp(myData.data.flistViewMode,'sequenMode')
                        fullFileName = fullFilePicks{sequenHash(currFileIndex)};
                    end
                    clear myData
                    
                    [~,fileName,fileExt] = fileparts(fullFileName);
                    msg = [fileName, fileExt];
                    mainLogText = appendLogText(mainLogText,msg,'p_ctd');
                    % preprocess file
                    ppSuccess = preprocessCTDFile(fullFileName);
                    if ppSuccess
                        updateVarSelPanel();
                        % update text fields in fileInfoPanel
                        updateFileInfoPanel(fullFileName);
                        updatePlot();
                        resetForcedYLims();
                        updateYModPanel();
                    end
                end
                msg = 'A previously saved group of variables was loaded.';
                mainLogText = appendLogText(mainLogText,msg,'success');
                mainLogText = updateLog(mainLogText,consoleBox);
            end
            clear myData
            delete(varGroupFig);
        end
    end

% change selection function for timeline/list toggle buttons that is active
% beside the slider
    function [] = switchFlistViewMode(hObject,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        % change data type to match selected object
        myData = guidata(mainFig);
        selObjHandle = get(hObject,'SelectedObject');
        selObjTag = get(selObjHandle,'Tag');
        
        chronoHash = myData.data.chronoHash;
        sequenHash = myData.data.sequenHash;
        currFileIndex = myData.data.currCTDFileIndex;
        fullFilePicks = myData.data.CTDFullFilePicks;
        % map the current file to its position in the new slider type
        if strcmp(selObjTag, 'chronoMode')
            currFileIndex = find(chronoHash == sequenHash(currFileIndex),1);
        elseif strcmp(selObjTag, 'sequenMode')
            currFileIndex = find(sequenHash == chronoHash(currFileIndex),1);
        end
        myData.data.flistViewMode = selObjTag;
        myData.data.currCTDFileIndex = currFileIndex;
        guidata(mainFig,myData);
        clear myData
        updateSlider();
        if strcmp(selObjTag,'chronoMode')
            updateFileInfoPanel(fullFilePicks{chronoHash(currFileIndex)});
        elseif strcmp(selObjTag,'sequenMode')
            updateFileInfoPanel(fullFilePicks{sequenHash(currFileIndex)});
        end
        clear fullFilePicks
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

%     function [] = configAxesCB(hObject, eventData)
%         figWidth = 500;
%         figHeight = 600;
%         configAxesFig = figure ( 'windowstyle', 'modal', 'resize', 'off', ...
%             'name','Configure Axes Properties', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
%             'Tag', 'configAxesFig', 'units','pixels', 'Position',[100 100 figWidth figHeight],...
%             'CreateFcn', {@movegui,'center'});
%
%         % Done button
%         uicontrol('Parent',configAxesFig, 'Style','PushButton','Units','Normalized','Position',[.25 .03 .15 .05],...
%             'FontSize', 10,'String','Done','CallBack','uiresume(gcbf)');
%
%         % Cancel button
%         uicontrol('Parent',configAxesFig, 'Style','PushButton','Units','Normalized','Position',[.6 .03 .15 .05],...
%             'FontSize', 11,'String','Cancel','CallBack',{@callback_btn_cancel});
%
%         set(configAxesFig,'Visible','on');
%     end


% Callback function for "DeckCellMng" button
    function [] = DeckCellMng_pb_call(~,~)
        % Call nested function that displays pop-up window
        DeckCellMng();
        
        function [] = DeckCellMng()
            deckCellFilePicks = {};
            currDeckCellFilePath = '';
            
            currMasterDeckFilePath = '';
            masterDeckDateNumData = [];
            masterDeckLightData = [];
            % max size of master deck cell array (assuming max 50 days of
            % 24-hour 1 sample per second deck cell recordings)
            MAX_MASTER_SIZE = 4320000;
            
            ctdFilePicks = {};
            currCTDFilePath = '';
            
            figWidth = screenSize(3)-40;
            figHeight = screenSize(4)-6;
            
            DeckCellFig = figure ( 'windowstyle', 'modal', 'resize', 'on', ...
                'name','Manage Deck Cell Data', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                'Tag', 'DeckCellFigure', 'units','characters', 'Position',[100 100 figWidth figHeight],...
                'KeyPressFcn',{@restoreFig}, 'CreateFcn', {@movegui,'north'});
            defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
            set(DeckCellFig,'Color',defaultBackgroundColor);
%             set(DeckCellFig,'windowstyle','normal');
            
            panelWidth = 0.2;
            marginWidth = 0.04;
            midPanelWidth = (1 - 3*panelWidth - 2*marginWidth)/2;
            panelHeight = 0.7;
            midPanelHeight = 0.4;
            
            btnHeight = 0.08;
            numBtns = 3;
            btnSpace = (0.35 - numBtns*0.08)/(numBtns-1);
            
            DeckCellPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','Deck Cell Files', 'TitlePosition','centertop',...
                'Position',[marginWidth,0.25,panelWidth,panelHeight],...
                'Visible','off');
            
            uicontrol('Style','pushbutton', 'Parent',DeckCellPanel, 'units','normalized',...
                'position', [0.1 0.6+2*btnHeight+2*btnSpace 0.8 btnHeight],...
                'String','Load File(s)',...
                'Tooltipstring','Load one or more deck cell file(s)',...
                'Callback',{@loadDeckCell_cb});
            
            s = sprintf('View plot of data from single deck cell file\nselected in the list below');
            uicontrol('Style','pushbutton', 'Parent',DeckCellPanel, 'units','normalized',...
                'position', [0.1 0.6+btnHeight+btnSpace 0.8 btnHeight],...
                'String','View Plot (selected file)',...
                'Tooltipstring',s,...
                'Callback',{@plotDeckCell_cb});
            
            s = sprintf('View table of data from single deck cell file\nselected in list below');
            uicontrol('Style','pushbutton', 'Parent',DeckCellPanel, 'units','normalized',...
                'position', [0.1 0.6 0.8 btnHeight], 'String','View Table (selected file)',...
                'Tooltipstring',s,...
                'Callback',{@tableDeckCell_cb});
            
            uicontrol('Style','text', 'Parent',DeckCellPanel, 'units','normalized',...
                'position', [0.1 0.5 0.8 0.05], 'String', 'Current Deck Cell File(s):');
            
            DeckCellList = uicontrol('Style','listbox', 'Parent',DeckCellPanel, 'units','normalized',...
                'position', [0.1 0.05 0.8 0.45], 'String',{}, 'Max',1, 'Min',1, 'Callback',{@selectDeckCell_cb});
            
            set(DeckCellPanel,'Visible','on');
            
            Deck2MasterPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','Deck Cell to Master', 'TitlePosition','centertop',...
                'Position',[marginWidth+panelWidth,0.25,midPanelWidth,midPanelHeight],...
                'Visible','off');
            
%             s = sprintf(['Iteratively add data from each\nloaded deck cell file in list (left panel)\n',...
%                 'to the loaded master file (right panel).\nOverwrite redundant deck cell data in master.']);
%             uicontrol('Style','pushbutton', 'Parent',Deck2MasterPanel, 'units','normalized',...
%                 'position', [0.1 0.6 0.8 0.2], 'String','Add to Master (overwrite)',...
%                 'Tooltipstring',s,...
%                 'Callback',{@addDeckCellToMasterOW_cb});
            
            s = sprintf(['Iteratively add data from each\nloaded deck cell file in list (left panel)\n',...
                'to the loaded master file (right panel).\nSkip deck cell file if redundant data is detected in master.']);
            uicontrol('Style','pushbutton', 'Parent',Deck2MasterPanel, 'units','normalized',...
                'position', [0.1 0.4 0.8 0.2], 'String','<HTML>Add to Master &#8594</HTML>',...
                'Tooltipstring',s,...
                'Callback',{@addDeckCellToMasterAP_cb});
            
            set(Deck2MasterPanel,'Visible','on');
            
            MasterDeckCellPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','Master Deck Cell File', 'TitlePosition','centertop',...
                'Position',[marginWidth+panelWidth+midPanelWidth,0.25,panelWidth,panelHeight],...
                'Visible','off');
            
            numBtns = 4;
            btnSpace = (0.35 - numBtns*0.08)/(numBtns-1);
            
            uicontrol('Style','pushbutton', 'Parent',MasterDeckCellPanel, 'units','normalized',...
                'position', [0.1 0.6+3*btnHeight+3*btnSpace 0.8 btnHeight],...
                'String','Load File',...
                'Tooltipstring','Load single deck cell master file',...
                'Callback',{@loadMasterFile_cb});
            
            uicontrol('Style','pushbutton', 'Parent',MasterDeckCellPanel, 'units','normalized',...
                'position', [0.1 0.6+2*btnHeight+2*btnSpace 0.8 btnHeight],...
                'String','View Plot',...
                'Tooltipstring','View plots (by date) of deck cell data in loaded master file',...
                'Callback',{@plotMasterFile_cb});
            
            uicontrol('Style','pushbutton', 'Parent',MasterDeckCellPanel, 'units','normalized',...
                'position', [0.1 0.6+btnHeight+btnSpace 0.8 btnHeight],...
                'String','View Table',...
                'Tooltipstring','View table of deck cell data in loaded master file',...
                'Callback',{@tableMasterFile_cb});
            
            uicontrol('Style','pushbutton', 'Parent',MasterDeckCellPanel, 'units','normalized',...
                'position', [0.1 0.6 0.8 btnHeight], 'String','Export to TXT',...
                'Tooltipstring','Export updated master file to tab-delimited text file',...
                'Callback',{@exportMasterFile_cb});
            
            uicontrol('Style','text', 'Parent',MasterDeckCellPanel, 'units','normalized',...
                'position', [0.1 0.5 0.8 0.05], 'String', 'Current Master Deck Cell File:');
            
            MasterDeckText = uicontrol('Style','edit', 'Parent',MasterDeckCellPanel,...
                'units','normalized', 'position', [0.1 0.05 0.8 0.45],...
                'HorizontalAlignment', 'left', 'String','', 'Min',1, 'Max',10,...
                'Enable','inactive');
            
            set(MasterDeckCellPanel,'Visible','on');
            
            Master2CTDPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','Master to CTD', 'TitlePosition','centertop',...
                'Position',[marginWidth+2*panelWidth+midPanelWidth,0.25,midPanelWidth,midPanelHeight],...
                'Visible','off');
            
            s = sprintf(['Use deck cell data from loaded master file (left panel)\n',...
                'to iteratively adjust PAR values\n',...
                'of each loaded CTD file in list (right panel).\n',...
                'PAR values are overwritten by adjusted PAR values in CTD file.\n',...
                'NOTE: Only unaveraged .cnv CTD files may be used.']);
            uicontrol('Style','pushbutton', 'Parent',Master2CTDPanel, 'units','normalized',...
                'position', [0.1 0.6 0.8 0.2], 'String','Adjust + Export (overwrite)',...
                'Tooltipstring',s,...
                'Callback',{@adjustCTD_OW_cb});
            
            s = sprintf(['Use deck cell data from loaded master file (left panel)\n',...
                'to iteratively adjust PAR values\n',...
                'of each loaded CTD file in list (right panel).\n',...
                'Adjusted PAR values and deck cell intensity values are\n',...
                'appended to the CTD file.\n',...
                'NOTE: Only unaveraged .asc CTD files may be used.']);
            uicontrol('Style','pushbutton', 'Parent',Master2CTDPanel, 'units','normalized',...
                'position', [0.1 0.2 0.8 0.2], 'String','Adjust + Export (append)',...
                'Tooltipstring',s,...
                'Callback',{@adjustCTD_AP_cb});
            
            set(Master2CTDPanel,'Visible','on');
            
            CTDPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','CTD Files', 'TitlePosition','centertop',...
                'Position',[marginWidth+2*panelWidth+2*midPanelWidth,0.25,panelWidth,panelHeight],...
                'Visible','off');
            
            uicontrol('Style','pushbutton', 'Parent',CTDPanel, 'units','normalized',...
                'position', [0.1 0.87 0.8 btnHeight], 'String','Load File(s)',...
                'Tooltipstring','Load one or more CTD file(s)',...
                'Callback',{@loadCTD_cb});
            
            s = sprintf('View table of data from\nsingle CTD file\nselected in list below');
            uicontrol('Style','pushbutton', 'Parent',CTDPanel, 'units','normalized',...
                'position', [0.1 0.6 0.8 btnHeight], 'String','View Table (selected file)',...
                'Tooltipstring',s,...
                'Callback',{@tableCTD_cb});
            
            uicontrol('Style','text', 'Parent',CTDPanel, 'units','normalized',...
                'position', [0.1 0.5 0.8 0.05], 'String', 'Current CTD File(s):');
            
            % if CTD files were already picked in the main program, transfer them to deck cell manager
            mainFigData = guidata(mainFig);
            previousCTDFilePicks = mainFigData.data.CTDFullFilePicks;
            clear mainFigData;
            if ~isempty(previousCTDFilePicks)
                ctdFilePicks = previousCTDFilePicks;
                currCTDFilePath = ctdFilePicks{1};
            end
            
            CTDList = uicontrol('Style','listbox', 'Parent',CTDPanel, 'units','normalized',...
                'position', [0.1 0.05 0.8 0.45], 'String',ctdFilePicks, 'Max',1, 'Min',1,...
                'Callback',{@selectCTD_cb});
            
            set(CTDPanel,'Visible','on');
            
                        % Close button
            uicontrol('Parent',DeckCellFig, 'Style','PushButton','Units','Normalized','Position',[.425 .03 .15 .05],...
                'FontSize', 11,'String','Close','CallBack',{@close_cb});
            
            dcmGhostButton = uicontrol('Parent',DeckCellFig, 'Style','PushButton','Units','Normalized',...
                'Position',[0 0 0.001 0.001], 'Enable','off');
            
            StatusPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','Status Msg', 'TitlePosition','lefttop', 'Position',[0.1,0.1,0.8,0.15],...
                'Visible','off', 'Tag','statusPanel');
            
            StatusTypeText = uicontrol('Style','edit', 'Parent',StatusPanel,...
                'Units','normalized', 'Position',[0.0 0.0 1.0 0.85],...
                'ForegroundColor','blue', 'FontSize',8, 'HorizontalAlignment','left',...
                'Max',100, 'Enable','Inactive');
            
            % button to resize status box
            uicontrol ('parent',StatusPanel, ...
                'style','pushbutton', 'units','normalized', 'position',[0 0.85 1 0.15],...
                'FontUnits','normalized', 'FontSize',0.5, 'String','<HTML>&#9650</HTML>', 'ToolTipString','Expand log',...
                'Visible','on', 'Enable','on', 'CallBack',{@resizeStatusBox_cb});
            
            deckMngLogText = appendLogText(deckMngLogText,'Deck Cell Manager initialized','success');
            deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
            
            set(StatusPanel,'Visible','on');
            
            set(DeckCellFig,'Visible','on');

            %DeckCell Mngmnt callback functions
            
            function [] = loadDeckCell_cb(~,~)
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                deckCellFilePicks = light_uipickfiles11();
                if ~isempty(deckCellFilePicks)
                    set(DeckCellList,'String',deckCellFilePicks,'Value',1);
                    currDeckCellFilePath = deckCellFilePicks{1};
                    msg = [num2str(length(deckCellFilePicks)), ' deck cell file(s) loaded.'];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                end
                
                set(DeckCellFig,'Pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
            end
            
            function [] = plotDeckCell_cb(~,~)
                if isempty(currDeckCellFilePath)
                    msg = 'Deck plot table error: No deck cell file is selected.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                else
                    set(DeckCellFig,'pointer','watch');
                    % disable all enabled components while processing
                    enabledH = findobj(DeckCellFig,'Enable','on');
                    set(enabledH,'Enable','off');
                    pause(0.01)
                    
                    listVal = get(DeckCellList,'Value');
                    listStr = get(DeckCellList,'String');
                    currDeckCellFilePath = listStr{listVal};
                    % read in deck cell file
                    [timeCol_datenum, ~, lightCol] = readDeckCellFile(currDeckCellFilePath);
                    [~, fileName fileExt] = fileparts(currDeckCellFilePath);
                    if isempty(timeCol_datenum)
                        msg = ['Deck cell file read error: ', ' No deck cell data found in ',[fileName fileExt]];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                        deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    else
                        [~, fileName, fileExt] = fileparts(currDeckCellFilePath);
                        % plot light intensity
                        figure ('name',[fileName fileExt], 'NumberTitle', 'off');
                        plot(timeCol_datenum, lightCol,'r.')
                        xlabel('Time (hour:minute)')
                        ylabel('Intensity')
                        datetick('x','HH:MM')
                        title(['Deck Light Data: ' datestr(timeCol_datenum(1)) ' to ' datestr(timeCol_datenum(end))])
                    end
                    
                    set(DeckCellFig,'pointer','arrow');
                    set(enabledH,'Enable','on');
                    enabledH = [];
                end
            end
            
            function [] = tableDeckCell_cb(~,~)
                if isempty(currDeckCellFilePath)
                    msg = 'Deck cell table error: No deck cell file is selected.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                listVal = get(DeckCellList,'Value');
                listStr = get(DeckCellList,'String');
                currDeckCellFilePath = listStr{listVal};
                % read in deck cell file
                [timeCol_datenum timeCol_datestr lightCol] = readDeckCellFile(currDeckCellFilePath);
                [~, fileName fileExt] = fileparts(currDeckCellFilePath);
                if isempty(timeCol_datenum)
                    msg = ['Deck cell file read error: ', 'No deck cell data found in ',[fileName fileExt]];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                else
                    % if a table is already open, delete it
                    dataTableH = findobj('Tag','dataTableFig');
                    if ~isempty(dataTableH)
                        delete(dataTableH);
                    end
                    
                    figWidth = screenSize(3)/3;
                    figHeight = screenSize(4)-6;
                    
                    dataTableFig = figure ('resize', 'on', 'name',['Deck Cell Data Table: ' [fileName fileExt]],...
                        'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                        'units','characters', 'Position',[100 100 figWidth figHeight],...
                        'CreateFcn',{@movegui,'northeast'});
                    
                    defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
                    set(dataTableFig,'Color',defaultBackgroundColor);
                    
                    dataTable = uitable('Parent',dataTableFig, 'Units','Normalized',...
                        'Position',[0 0 1 1], 'RowName',[], 'ColumnWidth',{200 'auto'},...
                        'ColumnFormat',{'char' 'char'});
                    
                    tableData = [timeCol_datestr num2cell(lightCol)];
                    colNames = {'Date(yyyy-mm-dd HH:MM:SS.FFF)' 'Intensity'};
                    
                    set(dataTable,'Data',tableData, 'ColumnName',colNames);
                    
                    set(dataTableFig,'Visible','on');
                end
                
                set(DeckCellFig,'pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
                
            end
            
            function [] = selectDeckCell_cb(hObject,~)
                files = get(hObject,'String');
                index_selected = get(hObject,'Value');
                file_selected = files{index_selected};
                currDeckCellFilePath = file_selected;
                [~, fileName fileExt] = fileparts(currDeckCellFilePath);
                msg = ['Deck cell file ', [fileName fileExt], ' was selected.'];
                deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
            end
            
%             function [] = addDeckCellToMasterOW_cb(~,~)
%                 if isempty(deckCellFilePicks)
%                     msg = 'Add deck cell to master error: No deck files are loaded.';
%                     deckMngLogText = appendLogText(deckMngLogText,msg,'error');
%                     deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
%                     return;
%                 end
%                 if isempty(currMasterDeckFilePath)
%                     choice = questdlg('No deck cell master file is currently loaded. Would you like to create a new master file?',...
%                         'Create new master file', ...
%                         'Yes','No','No');
%                     % Handle response
%                     switch choice
%                         case 'Yes'
%                             oldPath = pwd;
%                             if ispref('lightgui','masterPath')
%                                 prefMasterPath = getpref('lightgui','masterPath');
%                                 cd(prefMasterPath);
%                             end
%                             % create new file path
%                             [filename, pathname, filterindex] = uiputfile( ...
%                                 {...
%                                 '*.txt','Master Deck Cell file TXT (*.txt)'; ...
%                                 }, ...
%                                 'Export Deck Cell Master file as'...
%                                 );
%                             cd(oldPath);
%                             if filterindex ~= 0
%                                 currMasterDeckFilePath = fullfile(pathname, filename);
%                                 set(MasterDeckText,'String',{'File Name:'; filename;...
%                                     'File Path:'; pathname});
%                                 setpref('lightgui','masterPath',pathname);
%                                 pause(0.01)
%                             else
%                                 return;
%                             end
%                         case 'No'
%                             % do nothing
%                             return;
%                     end
%                 end
%                 
%                 set(DeckCellFig,'pointer','watch');
%                 % disable all enabled components while processing
%                 enabledH = findobj(DeckCellFig,'Enable','on');
%                 set(enabledH,'Enable','off');
%                 pause(0.01)
%                 
%                 for deckNum = 1:length(deckCellFilePicks)
%                     set(DeckCellList,'Value',deckNum);
%                     fileFullPath = deckCellFilePicks{deckNum};
%                     [~, fileName, fileExt] = fileparts(fileFullPath);
%                     [timeCol_datenum, ~, lightCol] = readDeckCellFile(fileFullPath);
%                     
%                     msg = [fileName, fileExt];
%                     deckMngLogText = appendLogText(deckMngLogText,msg,'p_deck');
%                     
%                     if isempty(timeCol_datenum)
%                         msg = ['Deck cell file read error: ', 'No deck cell data found in ', [fileName fileExt]];
%                         deckMngLogText = appendLogText(deckMngLogText,msg,'error');
%                         continue;
%                     end
%                     if isempty(masterDeckDateNumData)
%                         %insert current data in empty master file
%                         masterDeckDateNumData = timeCol_datenum;
%                         masterDeckLightData = lightCol;
%                     else
%                         % insert current data in correct order in master file
%                         FirstTimeStamp = timeCol_datenum(1);
%                         LastTimeStamp = timeCol_datenum(end);
%                         FirstTimeStampMaster = masterDeckDateNumData(1);
%                         LastTimeStampMaster = masterDeckDateNumData(end);
%                         if FirstTimeStampMaster>LastTimeStamp %insert sample data in front of master data
%                             masterDeckDateNumData = [timeCol_datenum; masterDeckDateNumData];
%                             masterDeckLightData = [lightCol; masterDeckLightData];
%                         elseif FirstTimeStamp>LastTimeStampMaster %insert sample data after master data
%                             masterDeckDateNumData = [masterDeckDateNumData; timeCol_datenum];
%                             masterDeckLightData = [masterDeckLightData; lightCol];
%                         else %insert sample data after master data and resort
%                             masterDeckDateNumData = [masterDeckDateNumData; timeCol_datenum];
%                             masterDeckLightData = [masterDeckLightData; lightCol];
%                             % sort data by datenum and eliminate
%                             % duplicates
%                             [sortedCol, I, ~] = unique(masterDeckDateNumData);
%                             masterDeckDateNumData = sortedCol;
%                             masterDeckLightData = masterDeckLightData(I);
%                         end
%                     end
%                     [~, masterFileName, masterFileExt] = fileparts(currMasterDeckFilePath);
%                     msg = ['Deck cell file ', [fileName fileExt],...
%                         ' was added successfully to master file ',...
%                         [masterFileName masterFileExt]];
%                     deckMngLogText = appendLogText(deckMngLogText,msg,'success');
%                 end
%                 
%                 deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
%                 
%                 % prompt to save
%                 choice = questdlg('Would you like to save the updated master file?',...
%                     'Save Master Deck Cell', ...
%                     'Yes','No','Yes');
%                 % Handle response
%                 switch choice
%                     case 'Yes'
%                         enabledHTemp = enabledH;
%                         exportMasterFile_cb();
%                         enabledH = enabledHTemp;
%                     case 'No'
%                         % do nothing
%                 end
%                 
%                 set(DeckCellFig,'pointer','arrow');
%                 set(enabledH,'Enable','on');
%                 enabledH = [];
%             end
            
            function [] = addDeckCellToMasterAP_cb(~,~)
                if isempty(deckCellFilePicks)
                    msg = 'Add deck cell to master error: No deck files are loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                if isempty(currMasterDeckFilePath)
                    choice = questdlg('No deck cell master file is currently loaded. Would you like to create a new master file?',...
                        'Create new master file', ...
                        'Yes','No','No');
                    % Handle response
                    switch choice
                        case 'Yes'
                            oldPath = pwd;
                            if ispref('lightgui','masterPath')
                                prefMasterPath = getpref('lightgui','masterPath');
                                cd(prefMasterPath);
                            end
                            % create new file path
                            [filename, pathname, filterindex] = uiputfile( ...
                                {...
                                '*.txt','Master Deck Cell file TXT (*.txt)'; ...
                                }, ...
                                'Export Deck Cell Master file as'...
                                );
                            cd(oldPath);
                            if filterindex ~= 0
                                currMasterDeckFilePath = fullfile(pathname, filename);
                                set(MasterDeckText,'String',{'File Name:'; filename;...
                                    'File Path:'; pathname});
                                setpref('lightgui','masterPath',pathname);
                                pause(0.01)
                            else
                                return;
                            end
                        case 'No'
                            msg = 'Add deck cell to master error: No master file is loaded.';
                            deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                            deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                            return;
                    end
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                % pre-allocate temp arrays
                tempDatenum = zeros(MAX_MASTER_SIZE,1);
                tempLight = zeros(MAX_MASTER_SIZE,1);
                dateHead = 0; % pointer to first datenum index
                dateTail = 0; % pointer to last datenum index
                
                % insert loaded master data into temp arrays
                if ~isempty(masterDeckDateNumData)
                    tempDatenum(1:length(masterDeckDateNumData)) = masterDeckDateNumData;
                    dateHead = 1;
                    dateTail = length(masterDeckDateNumData);
                end
                if ~isempty(masterDeckLightData)
                    tempLight(1:length(masterDeckLightData)) = masterDeckLightData;
                end
                
                for deckNum = 1:length(deckCellFilePicks)
                    set(DeckCellList,'Value',deckNum);
                    fileFullPath = deckCellFilePicks{deckNum};
                    [~, fileName, fileExt] = fileparts(fileFullPath);
                    [datenumCol, ~, lightCol] = readDeckCellFile(fileFullPath);
                    
                    msg = [fileName, fileExt];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'p_deck');
                    
                    if isempty(datenumCol) || isempty(lightCol)
                        msg = ['Deck cell file read error: ', 'No deck cell data found in ', [fileName fileExt],...
                            ' File skipped.'];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                        continue;
                    end
                    
                    if length(datenumCol) ~= length(lightCol)
                        msg = ['Deck cell file read error: ', 'Light data and date data have different lengths in ',...
                            [fileName fileExt], ' This could lead to bugs. File skipped.'];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                        continue;
                    end
                    
                    if dateHead == 0 % temp arrays are empty (no master data yet)
                        tempDatenum(1:length(datenumCol)) = datenumCol;
                        tempLight(1:length(datenumCol)) = lightCol;
                        dateHead = 1;
                        dateTail = length(datenumCol);
                    else
                        % temp arrays are not empty (some master data already
                        % present)
                        firstTimeDeck = datenumCol(1);
                        lastTimeDeck = datenumCol(end);
                        firstTimeMaster = tempDatenum(dateHead);
                        lastTimeMaster = tempDatenum(dateTail);
                        % no overlap
                        if firstTimeDeck>lastTimeMaster
                            % insert sample data after master data
                            tempDatenum(dateTail+1:dateTail+length(datenumCol)) = datenumCol;
                            tempLight(dateTail+1:dateTail+length(datenumCol)) = lightCol;
                            dateTail = dateTail + length(datenumCol);
                            % no overlap
                        elseif lastTimeDeck<firstTimeMaster
                            % insert sample data in front of master data
                            % copy existing data to the right in temp
                            % arrays to make way for new data
                            tempDatenum(length(datenumCol)+1:length(datenumCol)+(dateTail-dateHead+1)) = tempDatenum(dateHead:dateTail);
                            tempLight(length(datenumCol)+1:length(datenumCol)+(dateTail-dateHead+1)) = tempLight(dateHead:dateTail);
                            % insert sample data in left gap
                            tempDatenum(1:length(datenumCol)) = datenumCol;
                            tempLight(1:length(datenumCol)) = lightCol;
                            dateTail = dateTail + length(datenumCol);
                            % partial overlap at beginning
                        elseif lastTimeDeck >= firstTimeMaster && firstTimeDeck < firstTimeMaster
                            % search for matching last time in temp
                            % master
                            [ltMatchInd, ~, ltMatchIndR] = findTimeMatch(tempDatenum,dateTail-dateHead+1,lastTimeDeck);
                            if ~isempty(ltMatchInd)
                                % copy data starting after ltMatchInd
                                copyDatenum = tempDatenum(ltMatchInd+1,dateTail);
                                copyLight = tempLight(ltMatchInd+1,dateTail);
                                % insert sample data in beginning
                                tempDatenum(1:length(datenumCol)) = datenumCol;
                                tempLight(1:length(datenumCol)) = lightCol;
                                % re-insert copy of remaining data
                                tempDatenum(length(datenumCol)+1:length(datenumCol)+length(copyDatenum)) = copyDatenum;
                                tempLight(length(datenumCol)+1:length(datenumCol)+length(copyDatenum)) = copyLight;
                                dateTail = length(datenumCol)+length(copyDatenum);
                            else
                                % copy data starting at ltMatchIndR
                                copyDatenum = tempDatenum(ltMatchIndR,dateTail);
                                copyLight = tempLight(ltMatchIndR,dateTail);
                                % insert sample data in beginning
                                tempDatenum(1:length(datenumCol)) = datenumCol;
                                tempLight(1:length(datenumCol)) = lightCol;
                                % re-insert copy of remaining data
                                tempDatenum(length(datenumCol)+1:length(datenumCol)+length(copyDatenum)) = copyDatenum;
                                tempLight(length(datenumCol)+1:length(datenumCol)+length(copyDatenum)) = copyLight;
                                dateTail = length(datenumCol)+length(copyDatenum);
                            end
                            clear copyDatenum
                            clear copyLight
                            % partial overlap at end
                        elseif firstTimeDeck <= lastTimeMaster && lastTimeDeck > lastTimeMaster
                            % search for matching first time in temp
                            % master
                            [ftMatchInd, ~, ftMatchIndR] = findTimeMatch(tempDatenum,dateTail-dateHead+1,firstTimeDeck);
                            if ~isempty(ftMatchInd)
                                % insert sample data starting at ftMatchInd
                                tempDatenum(ftMatchInd:ftMatchInd+length(datenumCol)-1) = datenumCol;
                                tempLight(ftMatchInd:ftMatchInd+length(datenumCol)-1) = lightCol;
                                dateTail = ftMatchInd+length(datenumCol)-1;
                            else
                                % insert sample data starting at
                                % ftMatchIndR
                                tempDatenum(ftMatchIndR:ftMatchIndR+length(datenumCol)-1) = datenumCol;
                                tempLight(ftMatchIndR:ftMatchIndR+length(datenumCol)-1) = lightCol;
                                dateTail = ftMatchIndR+length(datenumCol)-1;
                            end
                            % sample data is somewhere in middle of master data
                        else
                            % search for matching start and end times
                            [ftMatchInd, ftMatchIndL, ftMatchIndR] = findTimeMatch(tempDatenum,dateTail-dateHead+1,firstTimeDeck);
                            [ltMatchInd, ~, ltMatchIndR] = findTimeMatch(tempDatenum,dateTail-dateHead+1,lastTimeDeck);
                            if ~isempty(ftMatchInd) && ~isempty(ltMatchInd)
                                % complete overlap
                                if (ltMatchInd-ftMatchInd+1) == length(datenumCol)
                                    msg = ['Deck cell data from ', [fileName fileExt],...
                                        ' is already in master file. File skipped.'];
                                    deckMngLogText = appendLogText(deckMngLogText,msg,'warning');
                                    continue;
                                    % partial overlap on both sides
                                else
                                    % copy data after ltMatchInd
                                    copyDatenum = tempDatenum(ltMatchInd+1:dateTail);
                                    copyLight = tempLight(ltMatchInd+1:dateTail);
                                    % insert sample data at ftMatchInd
                                    tempDatenum(ftMatchInd:ftMatchInd+length(datenumCol)-1) = datenumCol;
                                    tempLight(ftMatchInd:ftMatchInd+length(datenumCol)-1) = lightCol;
                                    % insert copied data after new data
                                    tempDatenum(ftMatchInd+length(datenumCol):ftMatchInd+length(datenumCol)+length(copyDatenum)-1) = copyDatenum;
                                    tempLight(ftMatchInd+length(datenumCol):ftMatchInd+length(datenumCol)+length(copyDatenum)-1) = copyLight;
                                    dateTail = ftMatchInd+length(datenumCol)+length(copyDatenum)-1;
                                    clear copyDatenum
                                    clear copyLight
                                end
                                % partial overlap on left side
                            elseif ~isempty(ftMatchInd) && isempty(ltMatchInd)
                                % copy data at and after ltMatchIndR
                                copyDatenum = tempDatenum(ltMatchIndR:dateTail);
                                copyLight = tempLight(ltMatchIndR:dateTail);
                                % insert sample data at ftMatchInd
                                tempDatenum(ftMatchInd:ftMatchInd+length(datenumCol)-1) = datenumCol;
                                tempLight(ftMatchInd:ftMatchInd+length(datenumCol)-1) = lightCol;
                                % insert copied data after new data
                                tempDatenum(ftMatchInd+length(datenumCol):ftMatchInd+length(datenumCol)+length(copyDatenum)-1) = copyDatenum;
                                tempLight(ftMatchInd+length(datenumCol):ftMatchInd+length(datenumCol)+length(copyDatenum)-1) = copyLight;
                                dateTail = ftMatchInd+length(datenumCol)+length(copyDatenum)-1;
                                clear copyDatenum
                                clear copyLight
                                % partial overlap on right side
                            elseif isempty(ftMatchInd) && ~isempty(ltMatchInd)
                                % copy data after ltMatchInd
                                copyDatenum = tempDatenum(ltMatchInd+1:dateTail);
                                copyLight = tempLight(ltMatchInd+1:dateTail);
                                % insert sample data at ftMatchIndR
                                tempDatenum(ftMatchIndR:ftMatchIndR+length(datenumCol)-1) = datenumCol;
                                tempLight(ftMatchIndR:ftMatchIndR+length(datenumCol)-1) = lightCol;
                                % insert copied data after new data
                                tempDatenum(ftMatchIndR+length(datenumCol):ftMatchIndR+length(datenumCol)+length(copyDatenum)-1) = copyDatenum;
                                tempLight(ftMatchIndR+length(datenumCol):ftMatchIndR+length(datenumCol)+length(copyDatenum)-1) = copyLight;
                                dateTail = ftMatchIndR+length(datenumCol)+length(copyDatenum)-1;
                                clear copyDatenum
                                clear copyLight
                                % no overlap
                            else % isempty(ftMatchInd && isempty(ltMatchInd)
                                % copy data at and after ftMatchIndR
                                copyDatenum = tempDatenum(ftMatchIndR:dateTail);
                                copyLight = tempLight(ftMatchIndR:dateTail);
                                % insert sample data after ftMatchIndL
                                tempDatenum(ftMatchIndL+1:ftMatchIndL+length(datenumCol)) = datenumCol;
                                tempLight(ftMatchIndL+1:ftMatchIndL+length(datenumCol)) = lightCol;
                                % insert copied data after new data
                                tempDatenum(ftMatchIndL+length(datenumCol)+1:ftMatchIndL+length(datenumCol)+length(copyDatenum)) = copyDatenum;
                                tempLight(ftMatchIndL+length(datenumCol)+1:ftMatchIndL+length(datenumCol)+length(copyDatenum)) = copyLight;
                                dateTail = ftMatchIndL+length(datenumCol)+length(copyDatenum);
                                clear copyDatenum
                                clear copyLight
                            end
                        end
                    end
                    [~, masterFileName, masterFileExt] = fileparts(currMasterDeckFilePath);
                    msg = ['Deck cell file ', [fileName fileExt],...
                        ' was added successfully to master file ',...
                        [masterFileName masterFileExt]];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                end
                
                % remove empty values in temp arrays
                masterDeckDateNumData = tempDatenum(dateHead:dateTail);
                masterDeckLightData = tempLight(dateHead:dateTail);
                clear tempDatenum
                clear tempLight
                
                % prompt to save
                choice = questdlg('Would you like to save the updated master file?',...
                    'Save Master Deck Cell', ...
                    'Yes','No','Yes');
                % Handle response
                switch choice
                    case 'Yes'
                        enabledHTemp = enabledH;
                        exportMasterFile_cb();
                        enabledH = enabledHTemp;
                    case 'No'
                        % do nothing
                end
                
                deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                
                set(DeckCellFig,'pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
                
                % Helper function to binary search for matching
                % time for sample in master array. If no match found,
                % return neighboring left and right indices where match should have been.
                function[matchInd, matchIndL, matchIndR] = findTimeMatch(tempArray, tempLength, targetTime)
                    startInd = 1;
                    endInd = tempLength;
                    currInd = ceil(endInd/2);
                    matchInd = [];
                    matchIndL = [];
                    matchIndR = [];
                    % test for extreme ends first
                    if tempArray(startInd) == targetTime
                        matchInd = startInd;
                        return;
                    end
                    if tempArray(endInd) == targetTime
                        matchInd = endInd;
                        return;
                    end
                    % commence binary search
                    while currInd <= tempLength && currInd >= 1
                        currTime = tempArray(currInd);
                        if currTime == targetTime
                            matchInd = currInd;
                            break;
                        elseif currTime < targetTime
                            startInd = currInd;
                            currInd = ceil((currInd + endInd)/2);
                        else % currTime > targetTime
                            endInd = currInd;
                            currInd = floor((startInd + currInd)/2);
                        end
                        if startInd + 1 == endInd % target is between adjacent times
                            matchIndL = startInd;
                            matchIndR = endInd;
                            break;
                        end
                    end
                end
            end
            
            function [] = loadMasterFile_cb(~,~)
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                currMasterDeckFilePath = '';
                masterDeckDateNumData = [];
                masterDeckLightData = [];
                
                oldPath = pwd;
                
                if ispref('lightgui','masterPath')
                    prefMasterPath = getpref('lightgui','masterPath');
                    cd(prefMasterPath);
                end
                
                [fileName, filePath, filterIndex] = uigetfile( ...
                    {'*.txt','Master Deck Cell file TXT (*.txt)'},...
                    'Pick a Deck Cell Master file'...
                    );
                
                if filterIndex ~= 0
                    currMasterDeckFilePath = fullfile(filePath, fileName);
                    [masterDeckDateNumData, ~, masterDeckLightData] = readDeckCellMasterFile(currMasterDeckFilePath);
                    if ~isempty(masterDeckDateNumData) && ~isempty(masterDeckLightData)
                        set(MasterDeckText,'String',{'File Name:'; fileName;...
                            'File Path:'; filePath});
                        msg = ['Deck cell master file ', fileName, ' was loaded.'];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                        deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                        % set preferred master path
                        setpref('lightgui','masterPath',filePath);
                    else
                        msg = ['Master file read error: No deck cell data was found in ',...
                            fileName];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                        deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                        currMasterDeckFilePath = '';
                        set(MasterDeckText,'String','');
                    end
                end
                cd(oldPath);
                
                set(DeckCellFig,'pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
            end
            
            function [] = tableMasterFile_cb(~,~)
                if isempty(currMasterDeckFilePath)
                    msg = 'Master table error: No deck cell master file is loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                % if a table is already open, delete it
                dataTableH = findobj('Tag','dataTableFig');
                if ~isempty(dataTableH)
                    delete(dataTableH);
                end
                
                figWidth = screenSize(3)/3;
                figHeight = screenSize(4)-6;
                
                [~, fileName, fileExt] = fileparts(currMasterDeckFilePath);
                
                dataTableFig = figure ('resize', 'on', 'name',['Deck Cell Master Data Table: ' [fileName fileExt]],...
                    'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                    'units','characters', 'Position',[100 100 figWidth figHeight],...
                    'CreateFcn',{@movegui,'northeast'});
                
                defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
                set(dataTableFig,'Color',defaultBackgroundColor);
                
                dataTable = uitable('Parent',dataTableFig, 'Units','Normalized',...
                    'Position',[0 0 1 1], 'RowName',[], 'ColumnWidth',{'auto' 'auto'},...
                    'ColumnFormat',{'char' 'char'});
                
                tableData = [cellstr(datestr(masterDeckDateNumData,'yyyy-mm-dd HH:MM:SS.FFF')) num2cell(masterDeckLightData)];
                colNames = {'Date(yyyy-mm-dd HH:MM:SS.FFF)' 'Intensity'};
                
                set(dataTable,'Data',tableData, 'ColumnName',colNames);
                
                set(dataTableFig,'Visible','on');
                
                set(DeckCellFig,'Pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
            end
            
            function [] = plotMasterFile_cb(~,~)
                if isempty(currMasterDeckFilePath)
                    msg = 'Master plot error: No deck cell master file is loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                figWidth = screenSize(3)/4;
                figHeight = screenSize(4)/2;
                
                [~, fileName, fileExt] = fileparts(currMasterDeckFilePath);
                
                timeCovFig = figure ('resize', 'on', 'name',['Deck Cell Master Dates: ' [fileName fileExt]],...
                    'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                    'units','characters', 'Position',[100 100 figWidth figHeight],...
                    'CreateFcn',{@movegui,'northeast'});
                
                defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
                set(timeCovFig,'Color',defaultBackgroundColor);
                
                datePanel = uipanel('Parent',timeCovFig, 'units','normalized',...
                    'Title','Dates Covered (click to view plot)', 'TitlePosition','centertop',...
                    'Position',[0.05 0.05 0.9 0.9], 'Visible','off');
                
                dateList = uicontrol('Style','listbox', 'Parent',datePanel, 'units','normalized',...
                    'position', [0 0 1 1], 'String',{}, 'Max',1, 'Min',1, 'Callback',{@dateList_cb});
                
                datesCovered = unique(cellstr(datestr(masterDeckDateNumData,'yyyy-mm-dd')));
                set(dateList,'String',datesCovered);
                
                set(datePanel,'Visible','on');
                set(timeCovFig,'Visible','on');
                
                set(DeckCellFig,'pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
                
                function[] = dateList_cb(hObject,~)
                    set(timeCovFig,'pointer','watch');
                    % disable all enabled components while processing
                    eh = findobj(timeCovFig,'Enable','on');
                    set(eh, 'Enable','off');
                    pause(0.01)
                    
                    dates = get(hObject,'String');
                    index_selected = get(hObject,'Value');
                    date_selected = dates{index_selected};
                    dateOnlyCell = cellstr(datestr(masterDeckDateNumData,'yyyy-mm-dd'));
                    matchInd = strcmp(dateOnlyCell,date_selected);
                    matchDateNums = masterDeckDateNumData(matchInd);
                    matchLight = masterDeckLightData(matchInd);
                    
                    [~, fileName, fileExt] = fileparts(currMasterDeckFilePath);
                    % plot light intensity
                    figure ('name',fileName, 'NumberTitle', 'off');
                    plot(matchDateNums, matchLight,'r.')
                    xlabel('Time (hour:minute)')
                    ylabel('Intensity')
                    datetick('x','HH:MM')
                    title(['Deck Cell Master Light Data: ' date_selected])
                    
                    set(timeCovFig,'pointer','arrow');
                    set(eh,'Enable','on');
                end
            end
            
            function [] = exportMasterFile_cb(~,~)
                if isempty(currMasterDeckFilePath)
                    msg = 'Master export error: No deck cell master file is loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                [filePath, fileName, fileExt] = fileparts(currMasterDeckFilePath);
                oldPath = pwd;
                cd(filePath);
                [saveFileName, savePath, filterindex] = uiputfile( ...
                    {...
                    '*.txt',  'TXT-files  (*.txt)'; ...
                    }, ...
                    'Export DeckCell Master File as',[fileName fileExt]...
                    );
                if filterindex ~= 0
                    header = {'Date(yyyy-mm-dd HH:MM:SS.FFF)'; 'Intensity'};
                    data = [cellstr(datestr(masterDeckDateNumData,'yyyy-mm-dd HH:MM:SS.FFF'))'; num2cell(masterDeckLightData')];
                    fid = fopen(fullfile(savePath, saveFileName),'w+');
                    % printing header
                    fprintf(fid,'%s\t',header{1:end-1});
                    fprintf(fid,'%s\n',header{end});
                    % printing the data
                    fprintf(fid,'%s\t%f\n', data{:});
                    fclose(fid);
                    msg = ['Deck cell master file ', saveFileName,...
                        ' was successfully exported to directory ', savePath];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                end
                cd(oldPath);
                
                set(DeckCellFig,'pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
            end
            
            function [] = loadCTD_cb(~,~)
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                ctdFilePicks = light_uipickfiles11();
                
                if ~isempty(ctdFilePicks)
                    set(CTDList,'String',ctdFilePicks,'Value',1);
                    currCTDFilePath = ctdFilePicks{1};
                    msg = [num2str(length(ctdFilePicks)), ' CTD file(s) loaded.'];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                end
                
                set(DeckCellFig,'Pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
            end
            
            function [] = tableCTD_cb(~,~)
                if isempty(currCTDFilePath)
                    msg = 'CTD table error: No CTD file is selected.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                % read in CTD file
                CTDHeader = {};
                CTDData = [];
                
                listVal = get(CTDList,'Value');
                listStr = get(CTDList,'String');
                currCTDFilePath = listStr{listVal};
                [~, fileName, fileExt] = fileparts(currCTDFilePath);
                
                if strcmpi(fileExt, '.cnv')
                    [CTDHeader CTDData] = readCTDFile_cnv(currCTDFilePath);
                    fileExtOK = 1;
                elseif strcmpi(fileExt, '.asc')
                    [CTDHeader CTDData] = readCTDFile_asc(currCTDFilePath);
                    fileExtOK = 1;
                else
                    msg = ['CTD file extension error: ', fileExt,...
                        ' is not a supported CTD file extension in the deck cell manager.',...
                        ' Only unaveraged .cnv and unaveraged .asc files should be processed.'];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    fileExtOK = 0;
                end
                
                if fileExtOK
                    if isempty(CTDData)
                        msg = ['CTD file read error: No CTD data was found in file ',...
                            [fileName fileExt]];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                        deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    else
                        % Deal with quirk in cell array representation that puts headers within
                        % double cells
                        if size(CTDHeader,2) == 1
                            CTDHeader = CTDHeader{:};
                        end
                        
                        % if a table is already open, delete it
                        dataTableH = findobj('Tag','dataTableFig');
                        if ~isempty(dataTableH)
                            delete(dataTableH);
                        end
                        
                        figWidth = screenSize(3)/3;
                        figHeight = screenSize(4)-6;
                        
                        dataTableFig = figure ('resize', 'on', 'name',['CTD Data Table: ' fileName],...
                            'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                            'units','characters', 'Position',[100 100 figWidth figHeight],...
                            'CreateFcn',{@movegui,'northeast'});
                        
                        defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
                        set(dataTableFig,'Color',defaultBackgroundColor);
                        
                        dataTable = uitable('Parent',dataTableFig, 'Units','Normalized',...
                            'Position',[0 0 1 1], 'RowName',[]);
                        
                        tableData = CTDData;
                        colNames = CTDHeader;
                        
                        set(dataTable,'Data',tableData, 'ColumnName',colNames);
                        
                        set(dataTableFig,'Visible','on');
                    end
                end
                
                set(DeckCellFig,'Pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
            end
            
            function [] = adjustCTD_OW_cb(~,~)
                % UNAVERAGED CNV FILES ONLY
                
                % check master data and ctd data
                if isempty(currMasterDeckFilePath)
                    msg = 'Adjust PAR error: No deck cell master file is loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                if isempty(ctdFilePicks)
                    msg = 'Adjust PAR error: No CTD files are loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                newOutputPath = '';
                choice = questdlg('Would you like to export adjusted CTD files to a separate directory?',...
                    'Adjust PAR (Overwrite)', ...
                    'Yes','No','Yes');
                % Handle response
                switch choice
                    case 'Yes'
                        startPath = '';
                        if ispref('lightgui','adjCTDPath')
                            startPath = getpref('lightgui','adjCTDPath');
                        end
                        % get new file path
                        [pathName] = uigetdir(startPath,'Choose a directory for adjusted CTD output');
                        if ischar(pathName)
                            newOutputPath = pathName;
                            setpref('lightgui','adjCTDPath',newOutputPath);
                        end
                    case 'No'
                        % do nothing
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                [~, masterFileName, masterFileExt] = fileparts(currMasterDeckFilePath);
                
                % adjust par in ctd data using master data
                for ctdNum = 1:length(ctdFilePicks)
                    set(CTDList,'Value',ctdNum);
                    ctdFullPath = ctdFilePicks{ctdNum};
                    [ctdPath, ctdName, ctdExt] = fileparts(ctdFullPath);
                    
                    msg = [ctdName, ctdExt];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'p_ctd');
                    
                    if ~strcmpi(ctdExt,'.cnv')
                        msg = ['Adjust PAR and CTD file extension error: The CTD file ',...
                            [ctdName ctdExt],...
                            ' does not contain a supported CTD file extension for overwrite adjustment.',...
                            ' Only unaveraged .cnv files can be adjusted and overwritten.'];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                        continue;
                    else
                        % read in ctd file data and column headers
                        [ctdHeader ctdData] = readCTDFile_cnv(ctdFullPath);
                        
                        % read in ctd file as string vector to save cnv header, determine
                        % start time, and and determine format spec for
                        % data for later fprintf
                        
                        % read in file as string vector
                        fileText = fileread(ctdFullPath);
                        
                        % find start time
                        startTimeBeg = strfind(fileText,'<startTime>');
                        startTimeEnd = strfind(fileText,'</startTime>');
                        startTime = fileText(startTimeBeg+11:startTimeEnd-1);
                        try
                            startTimeNum = datenum(startTime,'yyyy-mm-ddTHH:MM:SS');
                        catch
                            startTimeNum = [];
                        end
                        % older files have a different start time tag
                        if isempty(startTimeNum)
                            startTimeLine = regexp(fileText,'# start_time = \w{3} \d{2} \d{4} \d{2}:\d{2}:\d{2}', 'match');
                            startTimeCell = regexp(startTimeLine{:}, '\w{3} \d{2} \d{4} \d{2}:\d{2}:\d{2}', 'match');
                            startTimeStr = startTimeCell{:};
                            try
                                startTimeNum = datenum(startTimeStr,'mmm dd yyyy HH:MM:SS');
                            catch
                                startTimeNum = [];
                            end
                            if isempty(startTimeNum)
                                msg = ['Adjust PAR error: The start time for ',...
                                    [ctdName ctdExt], ' could not be located.'];
                                deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                                continue;
                            end
                        end
                        
                        headerEnd = strfind(fileText,'*END*');
                        
                        % save cnv header for later export
                        headerText = fileText(1:headerEnd+6);
                        
                        fileText = fileText(headerEnd+7:end);
                        
                        % select data lines using linefeed character
                        LF = sprintf ('\n');
                        LineEnds=strfind(fileText,LF);
                        LineBegs=strfind(fileText,LF)+1;
                        LineBegs=[1 LineBegs(1:end-1)]; % include first line and exclude last index
                        lineText = fileText(LineBegs(1):LineEnds(1));
                        clear fileText
                        
                        % analyze line of data to determine format spec for
                        % export
                        dataCell = regexp(lineText, '\S+', 'match');
                        fieldWidth = max(cellfun(@length, dataCell));
                        formatSpecCell = cell(1,length(dataCell));
                        for i = 1:length(dataCell)
                            data = dataCell{i};
                            precision = 0;
                            if strfind(data,'.')
                                convChar = 'f';
                                precision = regexp(data,'\.\d+','end')...
                                    - regexp(data,'\.\d+','start');
                            else
                                convChar = 'd';
                            end
                            if strfind(data,'e')
                                convChar = 'e';
                            end
                            if i < length(dataCell)
                                formatSpecCell{i} = [' %' num2str(fieldWidth) '.' num2str(precision) convChar];
                            else
                                formatSpecCell{i} = [' %' num2str(fieldWidth) '.' num2str(precision) convChar '\n'];
                            end
                        end
                        formatSpec = cell2mat(formatSpecCell);
                        
                        eTimeData = ctdData(:, strcmpi(ctdHeader,'times'));
                        parData = ctdData(:, strcmpi(ctdHeader,'par'));
                        
                        % match ctd start time to deck cell time
                        startTimeMasterInd = find(masterDeckDateNumData == startTimeNum, 1);
                        if isempty(startTimeMasterInd)
                            msg = ['Adjust PAR error: The start time from CTD file ',...
                                [ctdName ctdExt], ' could not be matched to a time in master file ', [masterFileName masterFileExt]];
                            deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                            continue;
                        else
                            % get approximate segment of master deck cell data
                            % corresponding to ctd data
                            eSeconds = ceil(eTimeData(end))+1;
                            endTimeMasterInd = startTimeMasterInd + eSeconds;
                            if endTimeMasterInd > length(masterDeckDateNumData)
                                msg = ['Adjust PAR error: There is not enough deck cell data in master file ',...
                                    [masterFileName masterFileExt], ' to match all CTD data in file ', [ctdName ctdExt]];
                                deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                                continue;
                            else
                                partMasterDateData = masterDeckDateNumData(startTimeMasterInd:endTimeMasterInd);
                                partMasterLightData = masterDeckLightData(startTimeMasterInd:endTimeMasterInd);
                                
                                % adjust PAR
                                parAdjData = adjustPAR(ctdName, ctdExt, parData, eTimeData, partMasterDateData, partMasterLightData);
                                
                                if ~isempty(parAdjData)
                                    % overwrite original PAR column
                                    ctdData(:, strcmpi(ctdHeader,'par')) = parAdjData;
                                    
                                    % export to .cnv file with identical formatting as
                                    % original file
                                    % On a Windows system, convert PC-style exponential notation
                                    % (three digits in the exponent) to UNIX-style notation (two digits)
                                    if ispc
                                        ctdData_str = sprintf(formatSpec, ctdData');
                                        ctdData_str = strrep(ctdData_str,'e+0','e+');
                                        ctdData_str = strrep(ctdData_str,'e-0','e-');
                                    end
                                    
                                    % write to separate directory if
                                    % specified
                                    if ~isempty(newOutputPath)
                                        ctdPath = newOutputPath;
                                    end
                                    
                                    outFullPath = fullfile(ctdPath, [ctdName 'PARADJtest' ctdExt]);
                                    fid = fopen(outFullPath,'w+');
                                    % print cnv header
                                    fprintf(fid, '%s', headerText);
                                    % print data
                                    if ispc
                                        fprintf(fid, '%s', ctdData_str);
                                    else
                                        fprintf(fid, formatSpec, ctdData');
                                    end
                                    fclose(fid);
                                    msg = ['PAR values in CTD file ',...
                                        [ctdName ctdExt], ' was overwritten with adjusted PAR values.',...
                                        ' The revised file was exported to: ', outFullPath];
                                    deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                                end
                            end
                        end
                    end
                end

                deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                
                set(DeckCellFig,'pointer','arrow');
                set(enabledH,'Enable','On');
                enabledH = [];
            end
            
            function [] = adjustCTD_AP_cb(~,~)
                % UNAVERAGED ASC FILES WITH HDR FILE ONLY
                % check master data and ctd data
                % adjust par in ctd data using master data
                % append adjusted par and deck intensity to ctd data
                % export ctd data
                
                if isempty(currMasterDeckFilePath)
                    msg = 'Adjust PAR error: No deck cell master file is loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                if isempty(ctdFilePicks)
                    msg = 'Adjust PAR error: No CTD files are loaded.';
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                    deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                    return;
                end
                
                newOutputPath = '';
                choice = questdlg('Would you like to export adjusted CTD files to a separate directory?',...
                    'Adjust PAR (Append)', ...
                    'Yes','No','Yes');
                % Handle response
                switch choice
                    case 'Yes'
                        startPath = '';
                        if ispref('lightgui','adjCTDPath')
                            startPath = getpref('lightgui','adjCTDPath');
                        end
                        % get new file path
                        [pathName] = uigetdir(startPath,'Choose a directory for adjusted CTD output');
                        if ischar(pathName)
                            newOutputPath = pathName;
                            setpref('lightgui','adjCTDPath',newOutputPath);
                        end
                    case 'No'
                        % do nothing
                end
                
                set(DeckCellFig,'pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(DeckCellFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                
                [~, masterFileName, masterFileExt] = fileparts(currMasterDeckFilePath);
                
                % adjust par in ctd data using master data
                for ctdNum = 1:length(ctdFilePicks)
                    set(CTDList,'Value',ctdNum);
                    ctdFullPath = ctdFilePicks{ctdNum};
                    [ctdPath, ctdName, ctdExt] = fileparts(ctdFullPath);
                    
                    msg = [ctdName, ctdExt];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'p_ctd');
                    
                    if ~strcmpi(ctdExt,'.asc')
                        msg = ['Adjust PAR and CTD file extension error: The CTD file ',...
                            [ctdName ctdExt],...
                            ' does not contain a supported CTD file extension for append adjustment.',...
                            ' Only unaveraged .asc files with .hdr files can be adjusted and appended.'];
                        deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                        continue;
                    else
                        hdrFullPath = fullfile(ctdPath,[ctdName '.hdr']);
                        if ~exist(hdrFullPath,'file')
                            msg = ['Adjust PAR error: The corresponding .hdr file for ',...
                                [ctdName ctdExt], ' could not be located. It is needed to determine',...
                                ' the CTD start time.'];
                            deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                            continue;
                        else
                            % read in ctd file data and column headers
                            [ctdHeader ctdData] = readCTDFile_asc(ctdFullPath);
                            
                            % read in hdr file as string vector to get
                            % start time
                            hdrText = fileread(hdrFullPath);
                            
                            % find start time
                            startTimeBeg = strfind(hdrText,'<startTime>');
                            startTimeEnd = strfind(hdrText,'</startTime>');
                            startTime = hdrText(startTimeBeg+11:startTimeEnd-1);
                            try
                                startTimeNum = datenum(startTime,'yyyy-mm-ddTHH:MM:SS');
                            catch
                                startTimeNum = [];
                            end
                            % older files have a different start time tag
                            if isempty(startTimeNum)
                                startTimeLine = regexp(hdrText,'# start_time = \w{3} \d{2} \d{4} \d{2}:\d{2}:\d{2}', 'match');
                                startTimeCell = regexp(startTimeLine{:}, '\w{3} \d{2} \d{4} \d{2}:\d{2}:\d{2}', 'match');
                                startTimeStr = startTimeCell{:};
                                try
                                    startTimeNum = datenum(startTimeStr,'mmm dd yyyy HH:MM:SS');
                                catch
                                    startTimeNum = [];
                                end
                                if isempty(startTimeNum)
                                    msg = ['Adjust PAR error: The start time for ',...
                                        [ctdName ctdExt], ' could not be located.'];
                                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                                    continue;
                                end
                            end
                            
                            
                            clear hdrText
                            
                            fileText = fileread(ctdFullPath);
                            
                            % select data lines using linefeed character
                            LF = sprintf ('\n');
                            LineEnds=strfind(fileText,LF);
                            LineBegs=strfind(fileText,LF)+1;
                            LineBegs=[1 LineBegs(1:end-1)]; % exclude first line (header) and exclude last index
                            LineEnds=LineEnds(1:end);
                            lineText = fileText(LineBegs(2):LineEnds(2));
                            clear fileText
                            
                            % analyze line of data to determine format spec for
                            % export
                            dataCell = regexp(lineText, '\S+', 'match');
                            fieldWidth = max(cellfun(@length, dataCell));
                            formatSpecCell = cell(1,length(dataCell));
                            for i = 1:length(dataCell)
                                data = dataCell{i};
                                precision = 0;
                                if strfind(data,'.')
                                    convChar = 'f';
                                    precision = regexp(data,'\.\d+','end')...
                                        - regexp(data,'\.\d+','start');
                                else
                                    convChar = 'd';
                                end
                                if strfind(data,'e')
                                    convChar = 'e';
                                end
                                formatSpecCell{i} = ['%' num2str(fieldWidth)...
                                    '.' num2str(precision) convChar];
                            end
                            
                            eTimeData = ctdData(:, strcmpi(ctdHeader,'times'));
                            parData = ctdData(:, strcmpi(ctdHeader,'par'));
                            
                            % match ctd start time to deck cell time
                            startTimeMasterInd = find(masterDeckDateNumData == startTimeNum, 1);
                            if isempty(startTimeMasterInd)
                                msg = ['Adjust PAR error: The start time from CTD file ',...
                                    [ctdName ctdExt], ' could not be matched to a time in master file ', [masterFileName masterFileExt]];
                                deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                                continue;
                            else
                                % get approximate segment of master deck cell data
                                % corresponding to ctd data
                                eSeconds = ceil(eTimeData(end))+1;
                                endTimeMasterInd = startTimeMasterInd + eSeconds;
                                if endTimeMasterInd > length(masterDeckDateNumData)
                                    msg = ['Adjust PAR error: There is not enough deck cell data in master file ',...
                                        [masterFileName masterFileExt], ' to match all CTD data in file ', [ctdName ctdExt]];
                                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                                    continue;
                                else
                                    partMasterDateData = masterDeckDateNumData(startTimeMasterInd:endTimeMasterInd);
                                    partMasterLightData = masterDeckLightData(startTimeMasterInd:endTimeMasterInd);
                                    
                                    % adjust PAR
                                    [parAdjData deckLightIntp] = adjustPAR(ctdName, ctdExt, parData, eTimeData, partMasterDateData, partMasterLightData);
                                    
                                    if ~isempty(parAdjData)
                                        % append par_adj and deck light columns to the right
                                        % of original par column
                                        parInd = find(strcmpi(ctdHeader, 'par'),1);
                                        ctdHeader = [ctdHeader(1:parInd) 'ParAdj' 'DeckLight' ctdHeader(parInd+1:end)];
                                        ctdData = [ctdData(:,1:parInd) parAdjData deckLightIntp ctdData(:,parInd+1:end)];
                                        formatSpecCell = [formatSpecCell(1:parInd) formatSpecCell{parInd} '%10.2f' formatSpecCell(parInd+1:end)];
                                        
                                        % export to .asc file with identical formatting as
                                        % original file
                                        adjFormatSpecCell = cell(1,length(formatSpecCell));
                                        for i = 1:length(formatSpecCell)
                                            if i < length(formatSpecCell)
                                                adjFormatSpecCell{i} = [' ' formatSpecCell{i}];
                                            else
                                                adjFormatSpecCell{i} = [' ' formatSpecCell{i} '\n'];
                                            end
                                        end
                                        adjFormatSpec = cell2mat(adjFormatSpecCell);
                                        
                                        % On a Windows system, convert PC-style exponential notation
                                        % (three digits in the exponent) to UNIX-style notation (two digits)
                                        if ispc
                                            ctdData_str = sprintf(adjFormatSpec, ctdData');
                                            ctdData_str = strrep(ctdData_str,'e+0','e+');
                                            ctdData_str = strrep(ctdData_str,'e-0','e-');
                                        end
                                        
                                        ctdHeader_outCell = cell(1,length(ctdHeader));
                                        for i = 1:length(ctdHeader)
                                            ctdHeader_outCell{i} = sprintf(' %10s',ctdHeader{i});
                                        end
                                        ctdHeader_str = cell2mat(ctdHeader_outCell);
                                        
                                        % write to separate directory if
                                        % specified
                                        if ~isempty(newOutputPath)
                                            ctdPath = newOutputPath;
                                        end
                                        
                                        outFullPath = fullfile(ctdPath, [ctdName 'PARADJtest' ctdExt]);
                                        fid = fopen(outFullPath,'w+');
                                        % print ctd header
                                        fprintf(fid, '%s\n', ctdHeader_str);
                                        % print data
                                        if ispc
                                            fprintf(fid, '%s', ctdData_str);
                                        else
                                            fprintf(fid, adjFormatSpec, ctdData');
                                        end
                                        fclose(fid);
                                        msg = ['Adjusted PAR values and deck cell intensity values',...
                                            ' were appended to CTD file ',...
                                            [ctdName ctdExt], '. The revised file was exported to: ', outFullPath];
                                        deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                                    end
                                end
                            end
                        end
                    end
                end

                deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
                
                set(DeckCellFig,'pointer','arrow');
                set(enabledH,'Enable','On');
                enabledH = [];
            end
            
            % helper function that adjusts PAR column of a ctd file using
            % deck cell data
            function [ctdPAR_adj deckLightIntp] = adjustPAR(ctdName, ctdExt, ctdPAR, ctdETime, deckDateNum, deckLight)
                % unaveraged ctd (elapsed time is 1/16 second intervals)
                % interpolate deck intensity into 1/16 second precision
                unAvgTimeStep = 1/16;
                ctdPAR_adj = [];
                if (ctdETime(end) - ctdETime(end-1)) >= (unAvgTimeStep*2)
                    msg = ['Adjust PAR error: The data in CTD file ',...
                        [ctdName ctdExt], ' does not appear to be unaveraged.',...
                        ' Unaveraged data is preferred for PAR adjustment.'];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'error');
                else
                    deckDateVec = datevec(deckDateNum);
                    deckETime = zeros(size(deckDateVec,1),1);
                    % normalize datenums by calculating elapsed seconds
                    % from first value
                    for rowNum = 1:size(deckDateVec,1)
                        deckETime(rowNum) = etime(deckDateVec(rowNum,:),deckDateVec(1,:));
                    end
                    % generate time column interpolated to unaveraged time step precision
                    deckETimeIntp = (0:unAvgTimeStep:deckETime(end))';
                    % interpolate intensity column using the above
                    % parameters
                    deckLightIntp = interp1q(deckETime, deckLight, deckETimeIntp);
                    % make length of vector match PAR column
                    deckLightIntp = deckLightIntp(1:length(ctdPAR));
                    % adjust PAR data by dividing ratio of deck light remaining
                    ctdPAR_adj = ctdPAR./(deckLightIntp/deckLightIntp(1));
                    msg = ['PAR adjustment was successful for CTD file ',...
                        [ctdName ctdExt]];
                    deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                end
            end
            
            function [] = selectCTD_cb(hObject, ~)
                files = get(hObject,'String');
                index_selected = get(hObject,'Value');
                file_selected = files{index_selected};
                currCTDFilePath = file_selected;
                [~, fileName fileExt] = fileparts(currCTDFilePath);
                msg = ['CTD file ', [fileName fileExt], ' was selected.'];
                deckMngLogText = appendLogText(deckMngLogText,msg,'success');
                deckMngLogText = updateLog(deckMngLogText,StatusTypeText);
            end
            
            function [] = resizeStatusBox_cb(hObject,~)
                statusPanelPos = get(StatusPanel,'Position');
                statusPanelHeight = statusPanelPos(4);
                minHeight = 0.15;
                
                if statusPanelHeight == minHeight
                    set(StatusPanel,'Position',[0.1 0.1 0.8 0.9]);
                    set(hObject,'String','<HTML>&#9660</HTML>');
                    set(hObject,'ToolTipString','Shrink log');
                    btnHeight = 0.15*minHeight;
                    boxHeight = 1-btnHeight;
                    set(hObject,'Position',[0 boxHeight 1 btnHeight]);
                    set(StatusTypeText,'Position',[0 0 1 boxHeight]);
                    % focus on invisible button to eliminate selection border
                    uicontrol(dcmGhostButton);
                else
                    set(StatusPanel,'Position',[0.1 0.1 0.8 minHeight]);
                    set(hObject,'String','<HTML>&#9650</HTML>');
                    set(hObject,'ToolTipString','Expand log');
                    btnHeight = 0.15;
                    boxHeight = 1-btnHeight;
                    set(hObject,'Position',[0 boxHeight 1 btnHeight]);
                    set(StatusTypeText,'Position',[0 0 1 boxHeight]);
                    % focus on invisible button to eliminate selection border
                    uicontrol(dcmGhostButton);
                end
            end
            
            function [] = close_cb(~, ~)
                delete(gcf);
            end
        end
    end


% display a new figure containing CTD data for all possible Y variables
    function[] = displayDataTable_tb(~, ~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        % Call nested function that displays pop-up window
        displayDataTable();
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
        
        function [] = displayDataTable()
            myData = guidata(mainFig);
            currDataType = myData.data.currCTDDataType;
            currVarsX = myData.data.currCTDVarsX;
            
            masterVars = myData.data.masterCTDVars.(currDataType);
            xData = masterVars.(currVarsX{1}).data;
            
            if isempty(xData)
                msg = 'CTD Table error: No data loaded.';
                mainLogText = appendLogText(mainLogText,msg,'error');
                mainLogText = updateLog(mainLogText,consoleBox);
            else
                % if a table is already open, delete it
                dataTableH = findobj('Tag','dataTableFig');
                if ~isempty(dataTableH)
                    delete(dataTableH);
                end
                
                figWidth = screenSize(3)/2;
                figHeight = screenSize(4)-6;
                dataTableFig = figure ('resize', 'on', 'name','Current CTD Data Table',...
                    'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                    'Tag', 'dataTableFig', 'units','characters', 'Position',[500 100 figWidth figHeight],...
                    'CreateFcn', {@movegui,'northeast'}, 'Tag','dataTableFig');
                
                defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
                set(dataTableFig,'Color',defaultBackgroundColor);
                
                dataTable = uitable('Parent',dataTableFig, 'Units','Normalized',...
                    'Position',[0 0 1 1], 'RowName',[], 'Tag','dataTable');
                
                % pre-allocate
                colNames = fieldnames(masterVars);
                tableData = zeros(length(xData), length(colNames));
                for varNum = 1:length(colNames)
                    data = masterVars.(colNames{varNum}).data;
                    if ~isempty(data)
                        tableData(:,varNum) = data;
                    end
                end
                set(dataTable,'Data',tableData, 'ColumnName',colNames);
                
                set(dataTableFig,'Visible','on');
            end
            
            clear myData
        end
    end

    function[] = saveDataToText_cb(~, ~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        [data, header, formatSpec] = prepDataForWrite();
        
        myData = guidata(mainFig);
        if ~isempty(data)
            currFullFile = myData.data.CTDFullFilePicks{myData.data.currCTDFileIndex};
            [ctdPath, ctdName, ctdExt] = fileparts(currFullFile);
            currFileName = [ctdName '.txt'];
            oldPath = pwd;
            cd(ctdPath);
            [filename, pathname, filterindex] = uiputfile({...
                '*.txt',  'TXT-files  (*.txt)'; ...
                }, ...
                'Save CTD File as',currFileName...
                );
            if filterindex ~= 0
                destFullPath = fullfile(pathname, filename);
                writeDataToText(data, header, formatSpec, destFullPath);
            end
            cd(oldPath);
            msg = [ctdName, ctdExt, ' was saved to ', destFullPath];
            mainLogText = appendLogText(mainLogText,msg,'success');
            mainLogText = updateLog(mainLogText,consoleBox);
        else
            msg = 'CTD data save error: no data available.';
            mainLogText = appendLogText(mainLogText,msg,'error');
            mainLogText = updateLog(mainLogText,consoleBox);
        end
        clear myData
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function[] = enableAutosave_cb(hObject,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        myData = guidata(mainFig);
        % change tooltipstring
        set(hObject,'ToolTipString','Disable autosave of CTD data to TXT');
        
        procCTDPath = '';
        choice = questdlg(['Would you like to autosave processed CTD files to a separate directory?',...
            ' Otherwise, each processed file will be saved in the original file''s location.'],...
            'CTD Save to TXT', ...
            'Yes','No','Yes');
        % Handle response
        switch choice
            case 'Yes'
                startPath = '';
                if ispref('lightgui','procCTDPath')
                    startPath = getpref('lightgui','procCTDPath');
                end
                % get new file path
                [pathName] = uigetdir(startPath,'Choose a directory for processed CTD output');
                if ischar(pathName)
                    procCTDPath = pathName;
                    setpref('lightgui','procCTDPath',procCTDPath);
                end
            case 'No'
                % do nothing
        end
        myData.data.procCTDPath = procCTDPath;
        
        % update log
        if isempty(procCTDPath)
            msg = 'Autosave enabled. Destination: original file path.';
        else
            msg = ['Autosave enabled. Destination: ',procCTDPath];
        end
        mainLogText = appendLogText(mainLogText,msg,'success');
        mainLogText = updateLog(mainLogText,consoleBox);
        
        % set flag that enables save routine in load files callback and
        % slider callback
        myData.data.autosaveOn = 1;
        
        guidata(mainFig,myData);
        clear myData
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function[] = disableAutosave_cb(hObject,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        myData = guidata(mainFig);
        % change tooltipstring
        set(hObject,'ToolTipString','Enable autosave of CTD data to TXT');
        % update log
        msg = 'Autosave disabled';
        mainLogText = appendLogText(mainLogText,msg,'success');
        mainLogText = updateLog(mainLogText,consoleBox);
        % set flag that enables save routine in load files callback and
        % slider callback
        myData.data.autosaveOn = 0;
        myData.data.procCTDPath = '';
        
        guidata(mainFig,myData);
        clear myData
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function[] = batchProcess_cb(~,~)
        % check file picks
        myData = guidata(mainFig);
        filePicks = myData.data.CTDFullFilePicks;
        if isempty(filePicks)
            msg = 'Batch process error: no files loaded';
            mainLogText = appendLogText(mainLogText,msg,'error');
            mainLogText = updateLog(mainLogText,consoleBox);
        else
            % create new figure listing file picks and destination directory
            figWidth = screenSize(3)/3;
            figHeight = screenSize(4)-6;
            cancelFlag = 0;
            destPath = 'Use original file''s path';
            
            batchFig = figure ( 'windowstyle', 'modal', 'resize', 'on', ...
                'name','Batch Process File Picks', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                'Tag', 'batchFigure', 'units','characters', 'Position',[100 100 figWidth figHeight],...
                'CreateFcn', {@movegui,'north'});
            
            set(batchFig,'Color',defaultBackgroundColor);
            
            % Proceed button
            uicontrol('Style','PushButton','Parent',batchFig, 'Units','Normalized','Position',[.25 .05 .15 .05],...
                'FontSize', 10,'String','Proceed','CallBack','uiresume(gcbf)');
            
            % Cancel button
            uicontrol('Style','PushButton','Parent',batchFig, 'Units','Normalized','Position',[.6 .05 .15 .05],...
                'FontSize', 10,'String','Cancel','CallBack',{@cancel_cb});
            
            % Destination directory path and text
            destPathPanel = uipanel('Parent',batchFig, 'units','normalized',...
                'Title','Destination Path', 'TitlePosition','centertop', 'Position',[0.05,0.85,0.9,0.09],...
                'Visible','off');
            
            destPathText = uicontrol('Style','listbox', 'Parent',destPathPanel,...
                'String',destPath, 'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                'Min',0, 'Max',2, 'FontSize',10, 'FontWeight','bold', 'Enable','inactive', 'Value',[]);
            
            set(destPathPanel,'Visible','on');
            
            % Button to change destination directory
            uicontrol('Style','pushbutton', 'Parent',batchFig,...
                'Units','normalized', 'Position',[0.35 0.8 0.3 0.05],...
                'FontSize',10, 'String','Change Path', 'CallBack',{@changePath_cb});
            
            % Listbox containing file picks
            filePicksList = uicontrol('Style','listbox', 'Parent',batchFig,...
                'String',filePicks, 'Units','normalized', 'Min',0, 'Max',2, 'Position',[0.05 0.15 0.9 0.6],...
                'FontSize',10, 'Enable','inactive', 'Value',[]);
            
            set(batchFig,'Visible','on');
            uiwait(batchFig);
            
            if ~cancelFlag
                set(batchFig,'Pointer','watch');
                % disable all enabled components while processing
                enabledH = findobj(batchFig,'Enable','on');
                set(enabledH,'Enable','off');
                pause(0.01)
                % iterate through file picks and preprocess and save each file
                msg = 'CTD BATCH PROCESS BEGIN';
                mainLogText = appendLogText(mainLogText,msg,'p_ctd');
                for fileNum = 1:length(filePicks)
                    set(filePicksList,'Value',fileNum);
                    fileFullPath = filePicks{fileNum};
                    [~,fileName,fileExt] = fileparts(fileFullPath);
                    msg = [fileName, fileExt];
                    mainLogText = appendLogText(mainLogText,msg,'p_ctd');
                    ppSuccess = preprocessCTDFile(filePicks{fileNum});
                    if ppSuccess
                        [data, header, formatSpec] = prepDataForWrite();
                        [ctdPath, ctdName, ctdExt] = fileparts(fileFullPath);
                        saveFileName = [ctdName '.txt'];
                        if strcmp(destPath,'Use original file''s path')
                            destFullPath = fullfile(ctdPath, saveFileName);
                        else
                            destFullPath = fullfile(destPath, saveFileName);
                        end
                        writeOK = writeDataToText(data, header, formatSpec, destFullPath);
                        if writeOK
                            msg = [ctdName, ctdExt, ' was autosaved to ', destFullPath];
                            mainLogText = appendLogText(mainLogText,msg,'success');
                        end
                    end
                end
                msg = 'CTD BATCH PROCESS END';
                mainLogText = appendLogText(mainLogText,msg,'p_ctd');
                mainLogText = updateLog(mainLogText,consoleBox);
                set(batchFig,'Pointer','arrow');
                set(enabledH,'Enable','on');
                enabledH = [];
            end
            
            clear myData
            try
                delete(batchFig);
            catch
            end
        end
        
        function [] = changePath_cb(~,~)
            if ispref('lightgui','procCTDPath')
                startPath = getpref('lightgui','procCTDPath');
            end
            % get new file path
            [pathName] = uigetdir(startPath,'Choose a directory for processed CTD output');
            if ischar(pathName)
                destPath = pathName;
                setpref('lightgui','procCTDPath',destPath);
            end
            set(destPathText,'String',destPath);
        end
        
        function [] = cancel_cb(~,~)
            cancelFlag = 1;
            uiresume(gcbf);
        end
    end

    function[] = showOnePercentDepth_cb(hObject, ~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        myData = guidata(mainFig);
        opdepth = myData.data.masterCTDVars.Z.OPDep.data;
        opdepth_adj = myData.data.masterCTDVars.Z.OPDep_adj.data;
        if isempty(opdepth)
            msg = 'Plot 1% depth error: 1% depth calculated from Ke not available.';
            mainLogText = appendLogText(mainLogText,msg,'error');
        end
        set(myData.handles.stemOPD, 'Visible','on');
        if isempty(opdepth_adj)
            msg = 'Plot 1% depth error: 1% depth calculated from Ke_adj not available.';
            mainLogText = appendLogText(mainLogText,msg,'error');
        end
        mainLogText = updateLog(mainLogText,consoleBox);
        set(myData.handles.stemOPD_adj, 'Visible','on');
        s = 'Hide one percent depth line(s) on plot.';
        set(hObject,'TooltipString',s);
        
        clear myData
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function[] = hideOnePercentDepth_cb(hObject, ~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        myData = guidata(mainFig);
        set(myData.handles.stemOPD, 'Visible','off');
        set(myData.handles.stemOPD_adj, 'Visible','off');
        s = sprintf('Display one percent depth line(s) on plot.\n Vertical dashed line = Ke derived.\n Vertical dotted line = Ke_adj derived.');
        set(hObject,'TooltipString',s);
        clear myData
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function[] = rotatePlot_cb(hObject, ~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        % rotate all axes
        myData = guidata(mainFig);
        for axNum = 0:20
            axisTagL = ['ax' num2str(axNum) 'L'];
            axisTagR = ['ax' num2str(axNum) 'R'];
            axisHandleL = myData.handles.(axisTagL);
            axisHandleR = myData.handles.(axisTagR);
            view(axisHandleL,[90 90]);
            view(axisHandleR,[90 90]);
            axpos = get(axisHandleL,'Position');
            % tweak axes position to accomodate new axes label location on
            % top, and the lack of a right side label
            axpos = axpos + [0.0 0.0 0.04 -0.05];
            set(axisHandleL,'Position',axpos);
            set(axisHandleR,'Position',axpos);
        end
        view(myData.handles.axOPD,[90 90]);
        set(myData.handles.axOPD,'Position',axpos);
        
        % replace west panel with x mod uicontrols 
        set(myData.handles.xMaxPanel,'Parent',westPanel,...
            'Position',[0.15 0.05 0.7 0.07]);
        set(myData.handles.forcedXMaxPanel,'Parent',westPanel,...
            'Position',[0.05 0.05+0.07 0.9 0.07]);
        set(myData.handles.mouseXPanel,'Parent',westPanel,...
            'Position',[0.05 0.5-0.07/2 0.9 0.07]);
        set(myData.handles.forcedXMinPanel,'Parent',westPanel,...
            'Position',[0.05 1-0.05-2*0.07 0.9 0.07]);
        set(myData.handles.xMinPanel,'Parent',westPanel,...
            'Position',[0.15 1-0.05-0.07 0.7 0.07]);
        
        % replace south panel with y mod uicontrols
        set(myData.handles.yMinPanel,'Parent',southPanel,...
            'Position',[0.05 0.1 0.07 0.9]);
        set(myData.handles.forcedYMinPanel,'Parent',southPanel,...
            'Position',[0.05+0.07 0.1 0.1 0.9]);
        set(myData.handles.forcedYMaxPanel,'Parent',southPanel,...
            'Position',[0.95-0.07-0.1 0.1 0.1 0.9]);
        set(myData.handles.yMaxPanel,'Parent',southPanel,...
            'Position',[0.95-0.07 0.1 0.07 0.9]);
        set(myData.handles.yAxMenuPanel,'Parent',southPanel,...
            'Position',[0.05+0.1+0.07 0.1 0.15 0.9]);
        set(myData.handles.autoYLim,'Parent',southPanel,...
            'Position',[0.05+0.1+0.07+0.15 0.1 0.04 0.75]);
        set(myData.handles.scanYLim,'Parent',southPanel,...
            'Position',[0.05+0.1+0.07+0.15+0.04 0.1 0.04 0.75]);
        set(myData.handles.saveYLim,'Parent',southPanel,...
            'Position',[0.05+0.1+0.07+0.15+2*0.04 0.1 0.04 0.75]);
        set(myData.handles.loadYLim,'Parent',southPanel,...
            'Position',[0.05+0.1+0.07+0.15+3*0.04 0.1 0.04 0.75]);
        
        % change axes selection panel title
        set(myData.handles.axSelPanel1,'Title','B/T');
        set(myData.handles.axSelPanel2,'Title','B/T');
        s = 'Rotate plot counterclockwise 90 degrees so that X values are horizontal.';
        set(hObject,'TooltipString',s);
        
        clear myData
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function[] = unrotatePlot_cb(hObject,~)
        set(mainFig,'Pointer','watch');
        % disable all enabled components while processing
        enabledH = findobj(mainFig,'Enable','on');
        set(enabledH,'Enable','off');
        pause(0.01)
        
        myData = guidata(mainFig);
        for axNum = 0:20
            axisTagL = ['ax' num2str(axNum) 'L'];
            axisTagR = ['ax' num2str(axNum) 'R'];
            axisHandleL = myData.handles.(axisTagL);
            axisHandleR = myData.handles.(axisTagR);
            view(axisHandleL,[0 90]);
            view(axisHandleR,[0 90]);
            axpos = get(axisHandleL,'Position');
            % tweak axes position to accomodate new axes label location on
            % top, and the lack of a right side label
            axpos = axpos + [0.0 0.0 -0.04 0.05];
            set(axisHandleL,'Position',axpos);
            set(axisHandleR,'Position',axpos);
            axpos = get(axisHandleL,'Position');
        end
        view(myData.handles.axOPD,[0 90]);
        set(myData.handles.axOPD,'Position',axpos);
        
        % replace south panel with x mod uicontrols
        set(myData.handles.xMinPanel,'Parent',southPanel,...
            'Position',[0.05 0.1 0.07 0.9]);
        set(myData.handles.forcedXMinPanel,'Parent',southPanel,...
            'Position',[0.05+0.07 0.1 0.1 0.9]);
        set(myData.handles.mouseXPanel,'Parent',southPanel,...
            'Position',[0.5-0.1/2 0.1 0.1 0.9]);
        set(myData.handles.forcedXMaxPanel,'Parent',southPanel,...
            'Position',[0.95-0.07-0.1 0.1 0.1 0.9]);
        set(myData.handles.xMaxPanel,'Parent',southPanel,...
            'Position',[0.95-0.07 0.1 0.07 0.9]);

        % replace west panel with y mod uicontrols
        set(myData.handles.yMinPanel,'Parent',westPanel,...
            'Position',[0.15 0.05 0.7 0.07]);
        set(myData.handles.forcedYMinPanel,'Parent',westPanel,...
            'Position',[0.05 0.05+0.07 0.9 0.07]);
        set(myData.handles.forcedYMaxPanel,'Parent',westPanel,...
            'Position',[0.05 0.95-2*0.07 0.9 0.07]);
        set(myData.handles.yMaxPanel,'Parent',westPanel,...
            'Position',[0.15 0.95-0.07 0.7 0.07]);
        set(myData.handles.yAxMenuPanel,'Parent',westPanel,...
            'Position',[0.05 0.95-3*0.07 0.9 0.07]);
        set(myData.handles.autoYLim,'Parent',westPanel,...
            'Position',[0.05 0.95-4*0.07-0.01 0.9/4 0.07]);
        set(myData.handles.scanYLim,'Parent',westPanel,...
            'Position',[0.05+0.9/4 0.95-4*0.07-0.01 0.9/4 0.07]);
        set(myData.handles.saveYLim,'Parent',westPanel,...
            'Position',[0.05+2*0.9/4 0.95-4*0.07-0.01 0.9/4 0.07]);
        set(myData.handles.loadYLim,'Parent',westPanel,...
            'Position',[0.05+3*0.9/4 0.95-4*0.07-0.01 0.9/4 0.07]);
        
        % change axes selection panel title
        set(myData.handles.axSelPanel1,'Title','L/R');
        set(myData.handles.axSelPanel2,'Title','L/R');
        s = 'Rotate plot clockwise 90 degrees so that Y values are vertical.';
        set(hObject,'TooltipString',s);
        
        clear myData
        
        set(mainFig,'Pointer','arrow');
        set(enabledH,'Enable','on');
        enabledH = [];
    end

    function[] = showGrid_cb(hObject,~)
        myData = guidata(mainFig);
        for i = 1:MAX_YVARS
            % get handle for left and right axes
            axLTag = ['ax' num2str(i) 'L'];
            axRTag = ['ax' num2str(i) 'R'];
            axLHandle = myData.handles.(axLTag);
            axRHandle = myData.handles.(axRTag);
            set(axLHandle,'XGrid','on', 'YGrid','on');
            set(axRHandle,'XGrid','on', 'YGrid','on');
        end
        set(hObject,'TooltipString','Hide X and Y axis grid lines.');
        clear myData
    end

    function[] = hideGrid_cb(hObject,~)
        myData = guidata(mainFig);
        for i = 1:MAX_YVARS
            % get handle for left and right axes
            axLTag = ['ax' num2str(i) 'L'];
            axRTag = ['ax' num2str(i) 'R'];
            axLHandle = myData.handles.(axLTag);
            axRHandle = myData.handles.(axRTag);
            set(axLHandle,'XGrid','off', 'YGrid','off');
            set(axRHandle,'XGrid','off', 'YGrid','off');
        end
        set(hObject,'TooltipString','Show X and Y axis grid lines.');
        clear myData
    end

% expand or shrink the size of the console box
    function[] = resizeConsole_cb(hObject,~)
        myData = guidata(mainFig);
        consolePanelPos = get(myData.handles.consolePanel,'Position');
        consolePanelHeight = consolePanelPos(4);
        minHeight = 0.15;
        if consolePanelHeight == minHeight
            set(myData.handles.consolePanel,'Position',[0 0 1 1]);
            set(hObject,'String','<HTML>&#9660</HTML>');
            set(hObject,'ToolTipString','Shrink Log');
            btnHeight = 0.15*minHeight;
            consoleHeight = 1-btnHeight;
            set(hObject,'Position',[0 consoleHeight 0.9 btnHeight]);
            set(myData.handles.exportLog,'Position',[0.9 consoleHeight 0.1 btnHeight]);
            set(myData.handles.consoleBox,'Position',[0 0 1 consoleHeight]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        else
            set(myData.handles.consolePanel,'Position',[0 0 1 0.15]);
            set(hObject,'String','<HTML>&#9650</HTML>');
            set(hObject,'ToolTipString','Expand log');
            btnHeight = 0.15;
            consoleHeight = 1-btnHeight;
            set(hObject,'Position',[0 consoleHeight 0.9 btnHeight]);
            set(myData.handles.exportLog,'Position',[0.9 consoleHeight 0.1 btnHeight]);
            set(myData.handles.consoleBox,'Position',[0 0 1 consoleHeight]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        end
        clear myData
    end

% export log in main console box to html file
    function[] = exportLog_cb(~,~)
        myData = guidata(mainFig);
        uicontrol(myData.handles.ghostButton);
        jScrollPane = findjobj(myData.handles.consoleBox); % findjobj is the time bottleneck, takes tenths of a second
        jViewPort = jScrollPane.getViewport;
        jEditbox = jViewPort.getComponent(0);
        % Ensure we have an HTML-ready editbox
        HTMLclassname = 'javax.swing.text.html.HTMLEditorKit';
        if ~isa(jEditbox.getEditorKit,HTMLclassname)
            jEditbox.setContentType('text/html');
        end
        % Place accumulated log text at bottom of the editbox
        currentHTML = char(jEditbox.getText);
        [logFileName, logPathName, filterindex] = uiputfile( ...
            {...
            '*.html',  'HTML-files  (*CTDWranglerLog.html)'; ...
            }, ...
            'Save window shot as',datestr(now,'yyyymmdd_HHMM')...
            );
        if filterindex ~= 0
            fid = fopen([logPathName logFileName],'w+');
            % printing header
            fprintf(fid,'%s',currentHTML);
            fclose(fid);
            msg = ['Log saved successfully to ', logPathName, logFileName];
            mainLogText = appendLogText(mainLogText,msg,'success');
            mainLogText = updateLog(mainLogText,consoleBox);
        end
    end

    function[] = editForcedXMin_cb(src,~)
        fxmin = get(src,'String');
        myData = guidata(mainFig);
        
        % if 'auto', don't have to validate
        if strcmpi(fxmin,'auto')
            fxmin = 'Auto';
        else
            fxmin = str2double(fxmin);
            if isnan(fxmin)
                fxmin = 'Auto';
                msg = 'Forced X Min must be a number, or ''Auto'' (case insensitive). Reverting to "Auto."';
                mainLogText = appendLogText(mainLogText,msg,'warning');
                mainLogText = updateLog(mainLogText,consoleBox);
            else
                % validate new value by comparing to displayed x max
                xmax = get(myData.handles.xMaxDisp,'String');
                % forced xmin < displayed xmax
                xmax = str2double(xmax);
                if fxmin >= xmax
                    fxmin = 'Auto';
                    msg = 'Forced X Min must be less than displayed X Max. Reverting to "Auto."';
                    mainLogText = appendLogText(mainLogText,msg,'warning');
                    mainLogText = updateLog(mainLogText,consoleBox);
                end
            end
        end
        
        set(src,'String',fxmin);
        if strcmpi(fxmin,'auto')
            myData.data.forcedXMin = [];
        else
            myData.data.forcedXMin = fxmin;
        end
        guidata(mainFig,myData);
        updatePlot();
        clear myData
    end

    function[] = editForcedXMax_cb(src,~)
        fxmax = get(src,'String');
        myData = guidata(mainFig);
        
        % if 'auto', don't have to validate
        if strcmpi(fxmax,'auto')
            fxmax = 'Auto';
        else
            fxmax = str2double(fxmax);
            if isnan(fxmax)
                msg = 'Forced X Max must be a number, or ''Auto'' (case insensitive). Reverting to "Auto."';
                mainLogText = appendLogText(mainLogText,msg,'warning');
                mainLogText = updateLog(mainLogText,consoleBox);
                fxmax = 'Auto';
            else
                % validate new value by comparing to displayed x min
                xmin = get(myData.handles.xMinDisp,'String');
                % forced xmax > displayed xmin
                xmin = str2double(xmin);
                if fxmax <= xmin
                    fxmax = 'Auto';
                    msg = 'Forced X Max must be greater than displayed X Min. Reverting to "Auto."';
                    mainLogText = appendLogText(mainLogText,msg,'warning');
                    mainLogText = updateLog(mainLogText,consoleBox);
                end
            end
        end
        
        set(src,'String',fxmax);
        if strcmpi(fxmax,'auto')
            myData.data.forcedXMax = [];
        else
            myData.data.forcedXMax = fxmax;
        end
        guidata(mainFig,myData);
        updatePlot();
        clear myData
    end

    function[] = yAxMenu_cb(hObject,~)
        myData = guidata(mainFig);
        % update value of y min/max disp boxes
        mval = get(hObject,'Value');
        mlist = get(hObject,'String');
        mstr = mlist{mval};
        axNumCell = regexp(mstr, '\d+', 'match'); % regexp match returns cell array
        axNum = str2double(axNumCell{1,1}); % convert str to number
        axH = ['ax' int2str(axNum) 'L'];
        axYlim = get(myData.handles.(axH),'Ylim');
        if  strcmp(mstr(end),'*')
            set(myData.handles.yMinDisp,'String','');
            set(myData.handles.yMaxDisp,'String','');
        else
            set(myData.handles.yMinDisp,'String',num2str(axYlim(1)));
            set(myData.handles.yMaxDisp,'String',num2str(axYlim(2)));
        end
        currDataType = myData.data.currCTDDataType;
        textTag = ['var' int2str(axNum) 'Text'];
        varName = get(myData.handles.(textTag),'String');
        fymin = myData.data.forcedYLims.(currDataType).(varName).FYMin;
        fymax = myData.data.forcedYLims.(currDataType).(varName).FYMax;
        if isempty(fymin)
            set(myData.handles.yMinEdit,'String','Auto');
        else
            set(myData.handles.yMinEdit,'String',num2str(fymin));
        end
        if isempty(fymax)
            set(myData.handles.yMaxEdit,'String','Auto');
        else
            set(myData.handles.yMaxEdit,'String',num2str(fymax));
        end
        clear myData
    end

    function[] = editForcedYMin_cb(src,~)
        fymin = get(src,'String');
        myData = guidata(mainFig);
        % if 'auto', don't have to validate
        if strcmpi(fymin,'auto')
            fymin = 'Auto';
        else
            fymin = str2double(fymin);
            if isnan(fymin)
                fymin = 'Auto';
                msg = 'Forced Y Min must be a number, or ''Auto'' (case insensitive). Reverting to "Auto."';
                mainLogText = appendLogText(mainLogText,msg,'warning');
                mainLogText = updateLog(mainLogText,consoleBox);
            else
                % validate new value by comparing to current y max
                ymax = get(myData.handles.yMaxDisp,'String');
                % forced ymin < ymax display
                ymax = str2double(ymax);
                if fymin >= ymax
                    fymin = 'Auto';
                    msg = 'Forced Y Min must be less than displayed Y Max. Reverting to "Auto."';
                    mainLogText = appendLogText(mainLogText,msg,'warning');
                    mainLogText = updateLog(mainLogText,consoleBox);
                end
            end
        end
        
        set(src,'String',fymin);
        dataType = myData.data.currCTDDataType;
        yvarList = get(myData.handles.yAxMenu,'String');
        val = get(myData.handles.yAxMenu,'Value');
        yvar = yvarList{val};
        % parse out var name from "Ax # - <varname>"
        yvar = yvar((strfind(yvar,'-')+2):end);
        if strcmpi(fymin,'auto')
            myData.data.forcedYLims.(dataType).(yvar).FYMin = [];
        else
            myData.data.forcedYLims.(dataType).(yvar).FYMin = fymin;
        end
        guidata(mainFig,myData);
        updatePlot();
        updateYModPanel();
        clear myData
    end

    function[] = editForcedYMax_cb(src,~)
        fymax = get(src,'String');
        myData = guidata(mainFig);
        % if 'auto', don't have to validate
        if strcmpi(fymax,'auto')
            fymax = 'Auto';
        else
            fymax = str2double(fymax);
            if isnan(fymax)
                fymax = 'Auto';
                msg = 'Forced Y Max must be a number, or ''Auto'' (case insensitive). Reverting to "Auto."';
                mainLogText = appendLogText(mainLogText,msg,'warning');
                mainLogText = updateLog(mainLogText,consoleBox);
            else
                % validate new value by comparing to displayed y min
                ymin = get(myData.handles.yMinDisp,'String');
                % forced ymax > ymin display
                ymin = str2double(ymin);
                if fymax <= ymin
                    fymax = 'Auto';
                    msg = 'Forced Y Max must be less than displayed Y Min. Reverting to "Auto."';
                    mainLogText = appendLogText(mainLogText,msg,'warning');
                    mainLogText = updateLog(mainLogText,consoleBox);
                end
            end
        end
        
        set(src,'String',fymax);
        dataType = myData.data.currCTDDataType;
        yvarList = get(myData.handles.yAxMenu,'String');
        val = get(myData.handles.yAxMenu,'Value');
        yvar = yvarList{val};
        % parse out var name from "Ax # - <varname>"
        yvar = yvar((strfind(yvar,'-')+2):end);
        if strcmpi(fymax,'auto')
            myData.data.forcedYLims.(dataType).(yvar).FYMax = [];
        else
            myData.data.forcedYLims.(dataType).(yvar).FYMax = fymax;
        end
        guidata(mainFig,myData);
        updatePlot();
        updateYModPanel();
        clear myData
    end

    function[] = autoYLim_cb(~,~)
        resetForcedYLims();
        updatePlot();
        updateYModPanel();
    end

    function[] = scanYLim_cb(~,~)
        % Have to scan all files in list and determine minimum and maximum value of all possible y-variables in each file
        % As we scan the files, the absolute minimum and maximum for each y-variable among all files is tracked.
        % The absolute mins and maxes are saved as guidata forcedYMin and forcedYMax for each variable.
        myData = guidata(mainFig);
        filePicks = myData.data.CTDFullFilePicks;
        currFileIndex = myData.data.currCTDFileIndex;
        clear myData
        if isempty(filePicks)
            % update log
            msg = 'Force Y Lim Error: No files loaded.';
            mainLogText = appendLogText(mainLogText,msg,'error');
            mainLogText = updateLog(mainLogText,consoleBox);
        else
            isCancelled = 0;
            myData = guidata(mainFig);
            fylims = myData.data.forcedYLims;
            % create waitbar
            waitBarH = waitbar(0,'Scanning file picks','Name','Scanning file picks...',...
                'CreateCancelBtn',...
                'setappdata(gcbf,''canceling'',1)',...
                'CloseRequestFcn',{@close_waitbar});
            setappdata(waitBarH,'canceling',0)
            for fileNum = 1:length(filePicks)
                % Check for waitbar cancel button press
                if getappdata(waitBarH,'canceling')
                    isCancelled = 1;
                    break;
                end
                % Report current estimate in the waitbar's message field
                [~,fileName,fileExt] = fileparts(filePicks{fileNum});
                waitbar(fileNum/length(filePicks),waitBarH,sprintf('Analyzing %s...',[fileName fileExt]))
                % Determine Ylims of a file
                ppSuccess = preprocessCTDFile(filePicks{fileNum});
                if ppSuccess
                    myData2 = guidata(mainFig);
                    dataType = myData2.data.currCTDDataType;
                    masterVars = myData2.data.masterCTDVars.(dataType);
                    yvarnames = fieldnames(fylims.(dataType));
                    for varNum = 1:length(yvarnames)
                        masterVarProps = masterVars.(yvarnames{varNum});
                        dataMax = max(masterVarProps.data);
                        dataMin = min(masterVarProps.data);
                        varMax = fylims.(dataType).(yvarnames{varNum}).FYMax;
                        varMin = fylims.(dataType).(yvarnames{varNum}).FYMin;
                        if isempty(varMax)
                            fylims.(dataType).(yvarnames{varNum}).FYMax = dataMax;
                        else
                            if dataMax > varMax
                                fylims.(dataType).(yvarnames{varNum}).FYMax = dataMax;
                            end
                        end
                        if isempty(varMin)
                            fylims.(dataType).(yvarnames{varNum}).FYMin = dataMin;
                        else
                            if dataMin < varMin
                                fylims.(dataType).(yvarnames{varNum}).FYMin = dataMin;
                            end
                        end
                    end
                    clear myData2
                end 
            end
            delete(waitBarH);
            if ~isCancelled
                myData.data.forcedYLims = fylims;
                guidata(mainFig,myData);
            end
            % re-preprocess current CTD file
            if strcmpi(myData.data.flistViewMode,'chronomode')
                ch = myData.data.chronoHash;
                currFile = filePicks{ch(currFileIndex)};
            else
                sh = myData.data.sequenHash;
                currFile = filePicks{sh(currFileIndex)};
            end
            preprocessCTDFile(currFile);
            updatePlot();
            updateYModPanel();
            clear myData
        end
        function[] = close_waitbar(~,~)
            delete(gcf);
        end
    end

    function[] = saveYLim_cb(~,~)
        myData = guidata(mainFig);
        fylims = myData.data.forcedYLims;
        [filename, pathname, filterindex] = uiputfile( ...
            {...
            '*.mat','MAT file (*.mat)'; ...
            }, ...
            'Save forced Y limits structure as'...
            );
        if filterindex ~= 0
            oldPath = pwd;
            cd(pathname);
            save(filename,'fylims');
            cd(oldPath);
            m = ['Forced Y limits structure was successfully saved to ',...
                fullfile(pathname, filename)];
            mainLogText = appendLogText(mainLogText,m,'success');
            mainLogText = updateLog(mainLogText,consoleBox);
        end
        clear myData
    end

    function[] = loadYLim_cb(~,~)
        myData = guidata(mainFig);
        [filename, pathname, filterIndex] = uigetfile( ...
            {'*.mat','MAT file (*.mat)'},...
            'Pick a MAT file containing Y limits structure'...
            );
        if filterIndex ~= 0
            oldPath = pwd;
            cd(pathname);
            load(filename,'fylims');
            if exist('fylims','var')
                fylims_new = myData.data.forcedYLims;
                % merge loaded Y limits into existing structure
                dt = fieldnames(fylims);
                for i = 1:length(dt)
                    yv = fieldnames(fylims.(dt{i}));
                    for jj = 1:length(yv)
                        fymin = fylims.(dt{i}).(yv{jj}).FYMin;
                        fymax = fylims.(dt{i}).(yv{jj}).FYMax;
                        fylims_new.(dt{i}).(yv{jj}).FYMin = fymin;
                        fylims_new.(dt{i}).(yv{jj}).FYMax = fymax;
                    end
                end
                myData.data.forcedYLims = fylims_new;
                guidata(mainFig,myData);
                updatePlot();
                updateYModPanel();
                m = ['Y limits structure in file ',...
                fullfile(pathname, filename), ' was successfully loaded ',...
                'and merged into existing structure.'];
                mainLogText = appendLogText(mainLogText,m,'success');
                mainLogText = updateLog(mainLogText,consoleBox);
            else
                m = ['No forced Y limits structure was found in file ',...
                fullfile(pathname, filename)];
                mainLogText = appendLogText(mainLogText,m,'error');
                mainLogText = updateLog(mainLogText,consoleBox);
            end
            cd(oldPath);
        end
        clear myData
    end

% restore or minimize south plot modification panel
    function[] = resizeSouthPanel_cb(hObject,~)
        myData = guidata(mainFig);
        sp_pos = get(southPanel,'Position');
        hp_pos = get(southPHidePanel,'Position');
        if strcmpi(get(southPanel,'Visible'),'off')
            set(southPanel,'Visible','on');
            set(hObject,'String','<HTML>&#9660</HTML>');
            set(hObject,'TooltipString','Hide Axis Modification panel');
            set(southPHidePanel,'Position',[hp_pos(1) hp_pos(2)+sp_pos(4) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        else
            set(southPanel,'Visible','off');
            set(hObject,'String','<HTML>&#9650</HTML>');
            set(hObject,'TooltipString','Show Axis Modification panel');
            set(southPHidePanel,'Position',[hp_pos(1) hp_pos(2)-sp_pos(4) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        end
        clear myData
        resizeMainFigPanels();
    end

% restore or minimize west plot modification panel
    function[] = resizeWestPanel_cb(hObject,~)
        myData = guidata(mainFig);
        wp_pos = get(westPanel,'Position');
        hp_pos = get(westPHidePanel,'Position');
        if strcmpi(get(westPanel,'Visible'),'off')
            set(westPanel,'Visible','on');
            set(hObject,'String','<HTML>&#9668</HTML>');
            set(hObject,'TooltipString','Hide Axis Modification panel');
            set(westPHidePanel,'Position',[hp_pos(1)+wp_pos(3) hp_pos(2) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        else
            set(westPanel,'Visible','off');
            set(hObject,'String','<HTML>&#9658</HTML>');
            set(hObject,'TooltipString','Show Axis Modification panel');
            set(westPHidePanel,'Position',[hp_pos(1)-wp_pos(3) hp_pos(2) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        end
        clear myData
        resizeMainFigPanels();
    end

% minimize or restore slider panel
    function[] = resizeSliderPanel_cb(hObject,~)
        myData = guidata(mainFig);
        slp_pos = get(sliderPanel,'Position');
        hp_pos = get(sliderPHidePanel,'Position');
        if strcmpi(get(sliderPanel,'Visible'),'off')
            set(sliderPanel,'Visible','on');
            set(hObject,'String','<HTML>&#9650</HTML>');
            set(hObject,'TooltipString','Hide slider panel');
            set(sliderPHidePanel,'Position',[hp_pos(1) slp_pos(2)-hp_pos(4) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        else
            set(sliderPanel,'Visible','off');
            set(hObject,'String','<HTML>&#9660</HTML>');
            set(hObject,'TooltipString','Show slider panel');
            set(sliderPHidePanel,'Position',[hp_pos(1) hp_pos(2)+slp_pos(4) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        end
        clear myData
        resizeMainFigPanels();
    end

% minimize or restore control panel
    function[] = resizeControlPanel_cb(hObject,~)
        myData = guidata(mainFig);
        cp_pos = get(controlPanel,'Position');
        hp_pos = get(controlPHidePanel,'Position');
        if strcmpi(get(controlPanel,'Visible'),'off')
            set(controlPanel,'Visible','on');
            set(hObject,'String','<HTML>&#9658</HTML>');
            set(hObject,'TooltipString','Hide control panel');
            set(controlPHidePanel,'Position',[hp_pos(1)-cp_pos(3) hp_pos(2) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        else
            set(myData.handles.controlPanel,'Visible','off');
            set(hObject,'String','<HTML>&#9668</HTML>');
            set(hObject,'TooltipString','Show control panel');
            set(controlPHidePanel,'Position',[hp_pos(1)+cp_pos(3) hp_pos(2) hp_pos(3) hp_pos(4)]);
            % focus on invisible button to eliminate selection border
            uicontrol(myData.handles.ghostButton);
        end
        clear myData
        resizeMainFigPanels();
    end

% restore mouse pointer to arrow and re-enable disabled components, in
% case of program crash
    function[] = restoreFig(hObject, evnt)
        if length(evnt.Modifier) == 1 && strcmp(evnt.Modifier{:},'control') && ...
                strcmpi(evnt.Key,'r')
            if strcmpi(get(hObject,'Pointer'),'watch')
                set(hObject,'Pointer','arrow');
                if exist('enabledH','var') && ~isempty(enabledH)
                    set(enabledH,'Enable','on');
                    enabledH = [];
                end
            end
        end
    end

    function[] = mainFigClose(~,~)
        selection = questdlg('Are you sure you want to quit and close all windows?',...
            'Quit CTDWrangler',...
            'Yes','No','No');
        switch selection,
            case 'Yes',
                delete(findall(0,'Type','figure'));
            case 'No'
                return
        end
    end
end