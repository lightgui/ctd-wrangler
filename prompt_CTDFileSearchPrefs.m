function [startDir startDate endDate fileFormat dataFormat chosenStations] = prompt_CTDFileSearchPrefs(defaultDir)
% Prompts the user for file preferences when searching the working
% directory for CTD files and returns 

prefFig = figure('visible','off','Units','Normalized','Position',[.3 .3 .3 .75],'NumberTitle','off',...
    'Name','File Preferences');
defaultBackgroundColor = get(0,'defaultUicontrolBackgroundColor');
set(prefFig,'Color',defaultBackgroundColor);
set(prefFig, 'WindowStyle', 'modal');

% Text description for working directory
h_panel_startDir = uipanel('title', 'Starting Directory', 'fontsize', 10,...
    'units', 'normalized', 'position', [0.05 0.8 0.9 0.15]);

if nargin ~= 1
    defaultDir = pwd;
end
% Text description for working directory
h_edit_startDir = uicontrol('parent', h_panel_startDir, 'style', 'edit', 'string', defaultDir,...
    'FontSize', 10,'units','normalized',...
    'background', 'w', 'position',[0.05 0.55 0.9 0.4]);

% Button to change working directory
h_btn_changeDir = uicontrol('parent', h_panel_startDir, 'Style','pushbutton',...
    'String','Change','FontSize', 10,'units','normalized',...
    'Position',[0.4 0.1 0.2 0.3],'Callback',{@callback_btn_changeDir});

% Drop-down menu to choose a sampling station for sequential plotting of
% all station-specific files within a date range
h_panel_station = uipanel('title', 'Station(s)', 'fontsize', 10,...
    'units', 'normalized', 'position', [0.05 0.45 0.4 0.35]);

stationNames = {'All Stations', 'Fox Point', 'Main Gap', 'Linnwood 10', 'Linnwood 20',...
    'Linnwood 30', 'Linnwood 40', 'Linnwood 50', 'Harbor N', 'Harbor S',...
    'Junction', 'Prins Willem', 'Green Can 6m', 'Green Can S', 'Green Can Deep',...
    'Green Can 30', 'Green Can 40', 'Green Can 50', 'East Reef Peak',...
    'Sheb Reef Peak', 'NE Reef Peak'};

h_list_stations = uicontrol ('parent', h_panel_station, 'style', 'listbox',...
    'units', 'normalized','position', [0.1 0.1 0.8 0.8],...
    'string', stationNames, 'value', 1,'backgroundcolor','w', 'fontsize', 10,...
    'max', 3, 'min', 1);

h_panel_dateRange = uipanel('title', 'Date Range', 'fontsize', 10,...
    'units', 'normalized', 'position', [0.5 0.45 0.45 0.35]);

h_txt_startDate = uicontrol('parent', h_panel_dateRange, 'Style','Text','Units','Normalized','Position',[.1 .75 .8 .15],...
    'FontSize', 9,'horizontalalignment', 'left', 'String','Start Date (mm-dd-yyyy):');

h_edit_startDate = uicontrol('parent', h_panel_dateRange, 'Style','Edit','Units','Normalized','Position',[.1 .55 .8 .15],...
    'FontSize', 10, 'string','01-01-1994', 'backgroundcolor', 'white');

h_txt_endDate = uicontrol('parent', h_panel_dateRange, 'Style','Text','Units','Normalized','Position',[.1 .2 .8 .15],...
    'FontSize', 9,'horizontalalignment', 'left', 'String','End Date (mm-dd-yyyy):');

h_edit_endDate = uicontrol('parent', h_panel_dateRange, 'Style','Edit','Units','Normalized','Position',[.1 .1 .8 .15],...
    'FontSize', 10,'string',datestr(date, 'mm-dd-yyyy'),'backgroundcolor', 'white');

align([h_txt_startDate h_edit_startDate h_txt_endDate h_edit_endDate], 'Center', 'Distribute');

h_panel_fileType = uipanel('title', 'File Type', 'fontsize', 10,...
    'units', 'normalized', 'position', [0.05 0.15 0.9 0.3]);

uicontrol('Style','PushButton','Units','Normalized','Position',[.25 .05 .14 .06],...
    'FontSize', 10,'String','Scan','CallBack','uiresume(gcbf)');

uicontrol('Style','PushButton','Units','Normalized','Position',[.6 .05 .14 .06],...
    'FontSize', 10,'String','Cancel','CallBack',{@callback_btn_cancel});

cancelFlag = 0;

% Create the button group.
h_btngrp_fileFormat = uibuttongroup('parent', h_panel_fileType, 'title', 'File Format',...
    'FontSize', 10,'visible','off','units','normalized','Position',[0.1 0.1 .3 0.9]);

% Create three radio buttons in the button group.
h_rdbtn_cnv = uicontrol('Style','radiobutton','String','.CNV','units','normalized',...
    'FontSize', 10,'position',[0.2 0.8 0.8 0.2],'parent',h_btngrp_fileFormat,'HandleVisibility','off');
uicontrol('Style','radiobutton','String','.XLS','units','normalized',...
    'FontSize', 10,'position',[0.2 0.45 0.8 0.2],'parent',h_btngrp_fileFormat,'HandleVisibility','off');
uicontrol('Style','radiobutton','String','.ASC','units','normalized',...
    'FontSize', 10,'position',[0.2 0.1 0.8 0.2],'parent',h_btngrp_fileFormat,'HandleVisibility','off');

% Initialize some button group properties.
set(h_btngrp_fileFormat,'SelectedObject',h_rdbtn_cnv);
set(h_btngrp_fileFormat,'Visible','on');

h_btngrp_dataFormat = uibuttongroup('parent', h_panel_fileType, 'title', 'Data Format',...
    'FontSize', 10,'units','normalized','Position',[0.45 0.25 0.45 0.6]);

uicontrol('Style','radiobutton','String','Depth','units','normalized',...
    'FontSize', 10,'position',[0.1 0.6 0.8 0.35],'parent',h_btngrp_dataFormat,'HandleVisibility','off');
uicontrol('Style','radiobutton','String','Pressure','units','normalized',...
    'FontSize', 10,'position',[0.1 0.1 0.8 0.35],'parent',h_btngrp_dataFormat,'HandleVisibility','off');

set(h_btngrp_dataFormat,'Visible','on');

movegui(prefFig, 'center');

set(prefFig,'Visible','on');

uiwait(prefFig);

if cancelFlag
    startDir = '';
    startDate = '';
    endDate = '';
    fileFormat = '';
    dataFormat = '';
    chosenStations = '';
else
    startDir = get(h_edit_startDir, 'string');
    startDate = get(h_edit_startDate, 'string');
    endDate = get(h_edit_endDate, 'string');
    fileFormat = get(get(h_btngrp_fileFormat, 'SelectedObject'),'string');
    dataFormat = get(get(h_btngrp_dataFormat,'SelectedObject'),'string');
    vals = get(h_list_stations,'value');
    stations = get(h_list_stations,'string');
    chosenStations = stations(vals);
end
close(prefFig);

    function [] = callback_btn_cancel(source, eventdata)
        cancelFlag = 1;
        uiresume(gcbf);
    end

    % Callback function for change directory button
    % Allows user to select a new starting directory for scan
    function [] = callback_btn_changeDir(hObject, eventdata)
        oldDir = get(h_edit_startDir, 'string');
        newDir = uigetdir(pwd,'Select new starting directory');
        if ischar(newDir) && strcmpi(oldDir, newDir) ~= 1
            set(h_edit_startDir, 'string', newDir);
        end
    end
end