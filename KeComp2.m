function  [lnPAR smKeMatrix] = KeComp(DepthAndPar)

                %asssumed input datacolumns:  [depth PAR ] (QUARTER METER DEPTH IS ASSUMED change!)
                %output datacolumns:  [lnPAR    Smoothed_interpolated_Ke]
                
                %asssumed input datacolumns:  [depth PAR ]
                %output datacolumns:  [lnPAR    Smoothed_interpolated_Ke]
                
                %Column 1 = depth       
                %Column 2 = PAR   
                %Column 3 = lnPAR  ( if exists)               
                %Column 4 = Ke
                %Column 5 = Interpolated Ke
                %Column 6 = 1% Depth (Piecewise)
                %Column 7 = 1% Depth (Linear)
                %Column 8 = Interpolated PAR
                %Column 9 = Smoothed PAR
                %Column 10 = Smoothed lnPAR
                %Column 11 = Smoothed Ke
                %Column 12 = Extrapolated Smoothed Ke
                %Column 13 = Smoothed 1% Depth (Piecewise)
                %Column 14 = Smoothed 1% Depth (Piecewise)
                
                %%
                %Calculate three point moving average slopes of lnPAR data
                DDataMatrix = DepthAndPar;
                lnPAR=log(DDataMatrix(:,end));
                DDataMatrix(:,end+1)=lnPAR;
                parMatrix = DDataMatrix(:,2);

                % Calculate Ke values
                DDataMatrix(isnan(DDataMatrix)) = 0;
                KeMatrix = NaN*DDataMatrix(:,1);
                %size(DDataMatrix),size(KeMatrix)

%                 KeMatrix=  .5*(DDataMatrix(3:end,2)-DDataMatrix(2:end-1,2))./(DDataMatrix(3:end,1)-DDataMatrix(2:end-1,1) ) ...
%                           +.5*(DDataMatrix(2:end-1,2)-DDataMatrix(1:end-2,2))./(DDataMatrix(2:end-1,1)-DDataMatrix(1:end-2,1) );

                
%                KeMatrix=.5*(DDataMatrix(3:end,2)-DDataMatrix(2:end-1,2))./(DDataMatrix(3:end,1)-DDataMatrix(2:end-1,1) ) ...
%                         +.5*(DDataMatrix(2:end-1,2)-DDataMatrix(1:end-2,2))./(DDataMatrix(2:end-1,1)-DDataMatrix(1:end-2,1) );
%                KeMatrix=[ KeMatrix(1); KeMatrix ;KeMatrix(end)];
               %
                
                for i = 2:size(DDataMatrix,1)-1
                    if DDataMatrix(i-1,end) ~= 0 && DDataMatrix(i,end) ~= 0 && DDataMatrix(i+1,end) ~= 0
                        Ke1 = (DDataMatrix(i,end) - DDataMatrix(i-1,end))/(DDataMatrix(i,1) - DDataMatrix(i-1,1));
                        Ke2 = (DDataMatrix(i+1,end) - DDataMatrix(i,end))/(DDataMatrix(i+1,1) - DDataMatrix(i,1));
                        Ke = (Ke1 + Ke2)/2;
                        if abs(Ke) < 0.0000001
                            Ke = 0;
                        end
                        KeMatrix(i,1) = Ke;
                    end
                end
                
                
               KeMatrix(KeMatrix == 0) = NaN;
               DDataMatrix(DDataMatrix == 0) = NaN;
                
                %Column 4 = Ke
                DDataMatrix = [DDataMatrix KeMatrix];
                
                %% Interpolate Ke column
                %Replace all NaN Ke values in data matrix with interpolated values
                %For first few rows, replace NaN with first actual number
                for i = 1:size(KeMatrix,1)
                    if ~isnan(KeMatrix(i,1))
                        if i>1
                            KeMatrix(1:i-1,:) = KeMatrix(i,:);
                            break;
                        else
                            break;
                        end
                    end
                end
                
                %If last row/s is/are NaN, replace with last actual number
                decIndex = size(KeMatrix,1);
                while decIndex >= 1
                    if ~isnan(KeMatrix(decIndex,1))
                        if decIndex < size(KeMatrix,1)
                            KeMatrix(decIndex+1:size(KeMatrix,1),:) = KeMatrix(decIndex);
                            break;
                        else
                            break;
                        end
                    end
                    decIndex = decIndex-1;
                end
                
                %KeMatrix
                               
                %For middle rows, replace NaN with interpolation
                %between bookends
                
                % I always need to know which elements are NaN,
                % and what size the array is for any method
                keSize = size(KeMatrix, 1);
                indNAN = isnan(KeMatrix(:));

                % list the nodes which are known, and which will
                % be interpolated
                nanList = find(indNAN);
                knownList = find(~indNAN);

                workList = unique([nanList;nanList - 1;nanList + 1]);
                workList(workList <= 1) = [];
                workList(workList >= keSize) = [];
                
                flagMatrix = [workList ismember(workList, nanList)];
                
                flagSize = size(flagMatrix,1);
                
                %Create zero matrix to hold bookends in two columns
                %Column 1 is starting point
                %Column 2 is ending point
                bookendMatrix = zeros(size(nanList,1),3);
                bookIndex = 1;
                
                for i=1:flagSize-1
                    if flagMatrix(i,2) == 0 && flagMatrix(i+1,2) == 1
                        startpt = flagMatrix(i,1);
                        bookendMatrix(bookIndex,1) = startpt;
                        i = i + 3;
                    elseif flagMatrix(i,2) == 1 && flagMatrix(i+1,2) == 0
                            endpt = flagMatrix(i+1,1);
                            bookendMatrix(bookIndex,2) = endpt;
                            bookIndex = bookIndex + 1;
                    end
                end
                
                bookendMatrix;
                lastRow = size(bookendMatrix,1);
                
                for i=1:size(bookendMatrix,1)
                    if bookendMatrix(i,1) == 0
                        lastRow = i-1;
                        break;
                    end
                end
                
                for i=1:lastRow            
                    ind1 = bookendMatrix(i,1);
                    ind2 = bookendMatrix(i,2);
                    slope = (KeMatrix(ind2) - KeMatrix(ind1))/(DDataMatrix(ind2,1)-DDataMatrix(ind1,1));
                    bookendMatrix(i,3) = slope;
                    for j=ind1+1:ind2-1
                        ke = slope*(DDataMatrix(j,1)-DDataMatrix(ind1,1))+KeMatrix(ind1);
                        KeMatrix(j,1) = ke;
                    end                        
                end
                
                %bookendMatrix;
                %KeMatrix;

                %Column 5 = Interpolated Ke
                DDataMatrix = [DDataMatrix KeMatrix];
                
                
                %% Calculate 1% Depth
 
                %Generate a triangular 0/1 matrix %upper triangular
                
                TriangleMatrix = zeros(size(KeMatrix,1),size(KeMatrix,1));
                for i = 1:size(KeMatrix,1)
                    for j = 1:i
                        TriangleMatrix(j,i) = 1;
                    end
                end
                
                
                %Calculate 1% depth from extinction coefficients
                %(piecewise)
                temp1 = (KeMatrix * 0.25)';
                
                logMatrix1 = temp1 * TriangleMatrix;
                
                DiffMatrix = logMatrix1 - ones(1,size(KeMatrix,1)) * log(0.01);
                
                index1 = max(find(DiffMatrix > 0));
                index2 = min(find(DiffMatrix < 0));
                
                
                if DiffMatrix(1,size(DiffMatrix,2)) > 0
                    depth1p = nan;
                    depth2p = nan;
                else
                    depth1p = DDataMatrix(index1,1);
                    depth2p = DDataMatrix(index2,1);
                end
                
                
                
                %Calculate 1% depth (linear)
                %Calculate the first rectangular area between 0m and 0.25m
                firstArea = KeMatrix(1,1) * 0.25;
                
                %Calculate subsequent trapezoidal areas 
                subMatrix = (KeMatrix(1:size(KeMatrix,1)-1,1)...
                    + KeMatrix(2:size(KeMatrix,1),1))' * 0.125;
                
                %First area and trapezoidal areas combined
                temp2 = [firstArea subMatrix(1,:)];
                
                %Matrix of accumulated summed areas
                logMatrix2 = temp2 * TriangleMatrix;
                
                DiffMatrix = logMatrix2 - ones(1,size(KeMatrix,1)) * log(0.01);
                
                index1 = max(find(DiffMatrix > 0));
                index2 = min(find(DiffMatrix < 0));
                
                if DiffMatrix(1,size(DiffMatrix,2)) > 0
                    depth1l = nan;
                    depth2l = nan;
                else
                    depth1l = DDataMatrix(index1,1);
                    depth2l = DDataMatrix(index2,1);
                end
                
                depthMatrix = zeros(size(DDataMatrix,1),2);
                depthMatrix(1,1) = depth1p;
                depthMatrix(2,1) = depth2p;
                depthMatrix(1,2) = depth1l;
                depthMatrix(2,2) = depth2l;
                
                %Column 6 = 1% Depth (Piecewise) %Riemann sum integration approximation
                %Column 7 = 1% Depth (Linear) %trapezoidal approximation
                DDataMatrix = [DDataMatrix depthMatrix(:,:)];
                
                %plot(DDataMatrix(:,1),temp1(:,:),'b',DDataMatrix(:,1),temp2(:,:),'r')
                
                %% Interpolate PAR column
                % Place values into column 8 of DDataMatrix               
                
                %parMatrix = DDataMatrix(:,2);
                
                startRow = min(find(~isnan(parMatrix)));
                
                %If last row/s is/are NaN, replace with last actual number
                decIndex = size(parMatrix,1);
                while decIndex >= 1
                    if ~isnan(parMatrix(decIndex,1))
                        if decIndex < size(parMatrix,1)
                            parMatrix(decIndex+1:size(parMatrix,1),:) = parMatrix(decIndex);
                            break;
                        else
                            break;
                        end
                    end
                    decIndex = decIndex-1;
                end
                
                
                %Replace NaN with extrapolation between bookends
                
                parSize = size(parMatrix,1);
                indNAN = isnan(parMatrix)   ;                            

                % list the nodes which are known, and which will
                % be interpolated
                nanList = find(indNAN);
                knownList = find(~indNAN);

                workList = unique([nanList;nanList - 1;nanList + 1]);
                workList(workList < 1) = [];
                workList(workList > parSize) = [];
                
                flagMatrix = [workList ismember(workList, nanList)];
                
                flagSize = size(flagMatrix,1);
                
                %Create zero matrix to hold bookends in two columns
                %Column 1 is starting point
                %Column 2 is ending point
                bookendMatrix = zeros(size(nanList,1),3);
                bookIndex = 1;
                
                for i=1:flagSize-1
                    if flagMatrix(i,2) == 0 && flagMatrix(i+1,2) == 1
                        startpt = flagMatrix(i,1);
                        bookendMatrix(bookIndex,1) = startpt;
                        i = i + 3;
                    elseif flagMatrix(i,2) == 1 && flagMatrix(i+1,2) == 0
                            endpt = flagMatrix(i+1,1);
                            bookendMatrix(bookIndex,2) = endpt;
                            bookIndex = bookIndex + 1;
                    end
                end
                
                %bookendMatrix;
                lastRow = size(bookendMatrix,1);
                
                for i=2:size(bookendMatrix,1)
                    if bookendMatrix(i,1) == 0
                        lastRow = i-1;
                        break;
                    end
                end
                
                for i=2:lastRow            
                    ind1 = bookendMatrix(i,1);
                    ind2 = bookendMatrix(i,2);
                    slope = (parMatrix(ind2) - parMatrix(ind1))/(DDataMatrix(ind2,1)-DDataMatrix(ind1,1));
                    bookendMatrix(i,3) = slope;
                    for j=ind1+1:ind2-1
                        par = slope*(DDataMatrix(j,1)-DDataMatrix(ind1,1))+parMatrix(ind1);
                        parMatrix(j,1) = par;
                    end                        
                end
                
                %bookendMatrix;

                %parMatrix;
                %Column 8 = 1% Interpolated PAR
                DDataMatrix = [DDataMatrix parMatrix];                
                
                
                %% Calculate five point moving averages of PAR
                % Place new values into column #9 of DDataMatrix
                smparMatrix = parMatrix(:,1);
                
                for i = startRow+2:size(DDataMatrix,1)-2
                    if parMatrix(i-2,1) ~= 0 && parMatrix(i-1,1) ~= 0 && parMatrix(i,1) ~= 0 &&...
                        parMatrix(i+1,1) ~= 0 && parMatrix(i+2,1) ~= 0
                        average = (parMatrix(i-2,1)+parMatrix(i-1,1)+parMatrix(i,1)+...
                            parMatrix(i+1,1)+parMatrix(i+2,1))/5;
                        smparMatrix(i,1) = average;
                    end
                end
                
                %Column 9 = 1% Smoothed PAR
                DDataMatrix = [DDataMatrix smparMatrix];
                
                %% Calculate natural log of smoothed PAR values
                % Insert values into column #10 of DDataMatrix
                
                lnparMatrix = log(smparMatrix(:,1));
                
                DDataMatrix = [DDataMatrix lnparMatrix];
                
                
                %% Calculate smoothed Ke values
                % Insert values into column #11 of DDataMatrix
                
                smKeMatrix = zeros(size(DDataMatrix,1),1);
                
                %Calculate three point moving average slopes of smoothed lnPAR data
                for i = 2:size(DDataMatrix,1)-1
                    if DDataMatrix(i-1,end) ~= 0 && DDataMatrix(i,end) ~= 0 && DDataMatrix(i+1,end) ~= 0
                        Ke1 = (DDataMatrix(i,end) - DDataMatrix(i-1,end))/(DDataMatrix(i,1) - DDataMatrix(i-1,1));
                        Ke2 = (DDataMatrix(i+1,end) - DDataMatrix(i,end))/(DDataMatrix(i+1,1) - DDataMatrix(i,1));
                        Ke = (Ke1 + Ke2)/2;
                        if abs(Ke) < 0.0000001
                            Ke = 0;
                        end
                        smKeMatrix(i,1) = Ke;
                    end
                end
                
                %smKeMatrix
                
                smKeMatrix(smKeMatrix == 0) = NaN;
                
                %Column 11 % Smoothed Ke
                DDataMatrix = [DDataMatrix smKeMatrix];
                
                
                %% Extrapolate unfilled smoothed Ke values
                % Insert values into column #12 of DDataMatrix
                
                %For first few rows, replace NaN with first actual number
                for i = 1:size(smKeMatrix,1)
                    if ~isnan(smKeMatrix(i,1))
                        if i > 1
                            smKeMatrix(1:i-1,1) = smKeMatrix(i,1);
                            break;
                        else
                            break;
                        end
                    end
                end
                
                %If last row/s is/are NaN, replace with last actual number
                decIndex = size(smKeMatrix,1);
                while decIndex >= 1
                    if ~isnan(smKeMatrix(decIndex,1))
                        if decIndex < size(smKeMatrix,1)
                            smKeMatrix(decIndex+1:size(smKeMatrix,1),:) = smKeMatrix(decIndex);
                            break;
                        else
                            break;
                        end
                    end
                    decIndex = decIndex-1;
                end
                
                %smKeMatrix;
                smKeMatrix';
                %Column 12 %    interpolate unfilled smoothed Ke values
                DDataMatrix = [DDataMatrix smKeMatrix];

                return
                
                %% Calculate 1% Depth from smoothed Ke
                 
                %Calculate 1% depth (piecewise)
                %Calculate rectangular areas for each Ke
                temp1 = (smKeMatrix * 0.25)';
                
                %Matrix of accumulated summed areas
                areaMatrix1 = temp1 * TriangleMatrix;
                
                diffMatrix1 = areaMatrix1 - ones(1,size(smKeMatrix,1)) * log(0.01);
                
                index1 = max(find(diffMatrix1 > 0));
                index2 = min(find(diffMatrix1 < 0));
                
                
                if diffMatrix1(1,size(diffMatrix1,2)) > 0
                    depth1p = nan;
                    depth2p = nan;
                else
                    depth1p = DDataMatrix(index1,1);
                    depth2p = DDataMatrix(index2,1);
                end               
                
                
                %Calculate 1% depth (linear)
                %Calculate the first rectangular area between 0m and 0.25m
                firstArea = smKeMatrix(1,1) * 0.25;
                
                %Calculate subsequent trapezoidal areas 
                subMatrix = (smKeMatrix(1:size(smKeMatrix,1)-1,1)...
                    + smKeMatrix(2:size(smKeMatrix,1),1))' * 0.125;
                
                %First area and trapezoidal areas combined
                temp2 = [firstArea subMatrix(1,:)];
                
                %Matrix of accumulated summed areas
                areaMatrix2 = temp2 * TriangleMatrix;
                
                diffMatrix2 = areaMatrix2 - ones(1,size(smKeMatrix,1)) * log(0.01);
                
                index1 = max(find(diffMatrix2 > 0));
                index2 = min(find(diffMatrix2 < 0));
                
                if diffMatrix2(1,size(diffMatrix2,2)) > 0
                    depth1l = nan;
                    depth2l = nan;
                else
                    depth1l = DDataMatrix(index1,1);
                    depth2l = DDataMatrix(index2,1);
                end
                
                depthMatrix2 = zeros(size(DDataMatrix,1),2);
                depthMatrix2(1,1) = depth1p;
                depthMatrix2(2,1) = depth2p;
                depthMatrix2(1,2) = depth1l;
                depthMatrix2(2,2) = depth2l;
                
                %Column 16 17 %    interpolate unfilled smoothed Ke values
                DDataMatrix = [DDataMatrix depthMatrix2(:,:)];
                
                
                  %% Write to CSV
                                
                  %Enter correct csv folder
                  %eval(['cd ' csvDir]);
                
                   %Export relevant data to csv file in csv folder
          %      if ~2==exist([FileName(1:end-4), '.csv'],'file'),
          %        csvwrite([FileName(1:end-4), '.csv'], DDataMatrix);
          %      end
                   %Go back to directory for xls files
                   %eval(['cd ' currentDir]);
                
                
           
           
           
end
      