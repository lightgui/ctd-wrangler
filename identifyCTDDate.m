function [ sampleDate_str sampleDate_num ] = identifyCTDDate( fileFullPath )
%identifyCTDDate( fileName ) Identifies the date of the CTD file
%   Use different methods to determine the date, based on the file format.

sampleDate_str = '';
sampleDate_num = [];
bsIndices = strfind(fileFullPath,'\');
fileName = fileFullPath(bsIndices(end)+1:end);
DATE_MIN = datenum('940101', 'yymmdd');
DATE_MAX = datenum(date,'dd-mmm-yyyy');

% ASC Format or TXT Format(assume HDR file exists in same directory)
if strcmpi(fileFullPath(end-3:end), '.asc') || strcmpi(fileFullPath(end-3:end), '.txt')
    fileFullPath = strcat(fileFullPath(1:end-3), 'hdr');
    try fileString = fileread(fileFullPath);
    catch error
        fileString = '';
    end
    % If HDR file doesn't exist, use file name date
    if isempty(fileString)
        [sampleDate_str sampleDate_num] = findFileDate(fileName);
    else
        [sampleDate_str sampleDate_num] = findHDRdate(fileString);
    end
    
% CNV Format (header exists in same file)
elseif strcmpi(fileName(end-3:end), '.cnv')
    % Some CNV files do not have date in yymmdd format in file name
    % but yyyy-mm-dd
    % TODO: do we account for these different formats or hope that
    % all files are in uniform format in future?
    % TODO: check date range?
    fileString = fileread(fileFullPath);
    [sampleDate_str sampleDate_num] = findHDRdate(fileString);
    
% XLS Format (header exists in HEADER tab)
% assume date in file name is correct because reading multiple XLS files takes too
% long
elseif strcmpi(fileName(end-3:end), '.xls')
%     [numeric text raw] = xlsread(fileFullPath,'HEADER','b1:b150');
%     fileString = '';
%     % horizontally concantenate each textual line in HEADER tab so we can
%     % use regexp on it later
%     for i = 1:size(raw,1)
%         if ischar(raw{i,1})
%             fileString = strcat(fileString, raw{i,1});
%         end
%     end
%     [sampleDate_str sampleDate_num] = findHDRdate(fileString);
    [sampleDate_str sampleDate_num] = findFileDate(fileName);
end

% If HDR file exists, then same method as CNV file
% Sampling Date - prefer cruise date->start time date->file name date
    function [date_str date_num] = findHDRdate(fileString)
        % Date parsed from cruise date (Russell write in)
        % Format: **Cruise: mm/dd/yyyy
        % one or more spaces between colon and month
        % Format: ** Cruise: GREEN CAN  25 MAR 2003
        % Format: ** Cruise:MONITORING FOX POINT 11 JUN 2003
        % Format: ** Cruise/Date:Fox Point Monitoring 11/09/2003
        
        CruiseDate_line = num2str(cell2mat(regexp(fileString, 'Cruise.+?\*','match')));
        CruiseDate_str = num2str(cell2mat(regexp(CruiseDate_line, '\d{2}/\d{2}/\d{4}','match')));
        formatFlag = 1;
        if isempty(CruiseDate_str)
            formatFlag = 2;
            CruiseDate_str = num2str(cell2mat(regexp(CruiseDate_line, '\d{2} \w{3} \d{4}','match')));
        end
        if isempty(CruiseDate_str)
            formatFlag = 3;
            CruiseDate_str = num2str(cell2mat(regexp(CruiseDate_line, '\d{2}/\d{2}/\d{2}','match')));
        end
        if ~isempty(CruiseDate_str)
            if formatFlag == 1
                try
                    CruiseDate_num = datenum(CruiseDate_str, 'mm/dd/yyyy');
                    CruiseDate_print = datestr(CruiseDate_num, 'dd-mmm-yyyy');
                catch error
                    CruiseDate_num = 1;
                    CruiseDate_print = 'N/A';
                end
                
            elseif formatFlag == 2
                try
                    CruiseDate_num = datenum(CruiseDate_str, 'dd mmm yyyy');
                    CruiseDate_print = datestr(CruiseDate_num, 'dd-mmm-yyyy');
                catch error
                    CruiseDate_num = 1;
                    CruiseDate_print = 'N/A';
                end
                
            elseif formatFlag == 3
                try
                    CruiseDate_num = datenum(CruiseDate_str, 'mm/dd/yy');
                    CruiseDate_print = datestr(CruiseDate_num, 'dd-mmm-yyyy');
                catch error
                    CruiseDate_num = 1;
                    CruiseDate_print = 'N/A';
                end
            end
        else
            CruiseDate_num = 1;
            CruiseDate_print = 'N/A';
        end
        
        FinalDate_num = CruiseDate_num;
        FinalDate_print = CruiseDate_print;
        
        % If no cruise date found
        if strcmpi(CruiseDate_print, 'N/A') == 1
            % Date parsed from start time
            StartDate_line = num2str(cell2mat(regexp(fileString,'# start_time = \w{3} \d{2} \d{4} \d{2}:\d{2}:\d{2}', 'match')));
            StartDate_str = num2str(cell2mat(regexp(StartDate_line, '\w{3} \d{2} \d{4}', 'match')));
            try
                StartDate_num = datenum(StartDate_str, 'mmm dd yyyy');
                StartDate_print = datestr(StartDate_num, 'dd-mmm-yyyy');
            catch error
                StartDate_num = 1;
                StartDate_print = 'N/A';
            end
            if isempty(StartDate_num)
                StartDate_num = 1;
                StartDate_print = 'N/A';
            end
            FinalDate_num = StartDate_num;
            FinalDate_print = StartDate_print;
            
            % If start time date is out of range
            if StartDate_num < DATE_MIN || StartDate_num > DATE_MAX
                % Date parsed from file name
                FileDate_str = regexp(fileName,'\d{6}','match');
                try
                    FileDate_num = datenum(FileDate_str, 'yymmdd');
                    FileDate_print = datestr(FileDate_num, 'dd-mmm-yyyy');
                catch error
                    FileDate_num = 1;
                    FileDate_print = 'N/A';
                end
                FinalDate_num = FileDate_num;
                FinalDate_print = FileDate_print;
            end
        end
        date_num = FinalDate_num;
        date_str = FinalDate_print;
    end

    function[date_str, date_num] = findFileDate(fname)
        % ID date from file name
        % Case 1: date is saved in yymmdd format
        fileDate_str = regexp(fname,'\d{6}','match');
        if isempty(fileDate_str) || length(fileDate_str) > 1
            % Case 2: date is saved in yyyy-mm-dd format
            fileDate_str = regexp(fname,'\d{4}-\d{2}-\d{2}','match');
            if isempty(fileDate_str) || length(fileDate_str) > 1
                fileDate_num = -1;
                fileDate_print = 'N/A';
            else
                try
                    fileDate_num = datenum(fileDate_str, 'yyyy-mm-dd');
                    fileDate_print = datestr(fileDate_num, 'dd-mmm-yyyy');
                catch
                    fileDate_num = -1;
                    fileDate_print = 'N/A';
                end
            end
        else
            try
                fileDate_num = datenum(fileDate_str, 'yymmdd');
                fileDate_print = datestr(fileDate_num, 'dd-mmm-yyyy');
            catch
                fileDate_num = -1;
                fileDate_print = 'N/A';
            end
        end
        
        date_str = fileDate_print;
        date_num = fileDate_num;
    end
end

