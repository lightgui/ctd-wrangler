function [] = lightgui45_17_4()
%transparency
%http://blogs.mathworks.com/steve/2009/02/18/image-overlay-using-transparency/

%http://blogs.mathworks.com/loren/2010/11/18/deploying-standalone-applications/
%First of all, I would like to give my feedback about the first answer from Matt Whitaker.
%Instead of using a splash screen , I am using mcc with the flags -R followed by -startmsg and -completemsg . 


% I = checkerboard(20);
%         figure, imshow(I)

% rgb = imread('peppers.png');
% imshow(rgb);
% I = rgb2gray(rgb);
% hold on
% h = imshow(I); % Save the handle; we'll need it later
% hold off 
% 
% [M,N] = size(I);
% block_size = 50;
% P = ceil(M / block_size);
% Q = ceil(N / block_size);
% alpha_data = checkerboard(block_size, P, Q) > 0;
% alpha_data = alpha_data(1:M, 1:N);
% set(h, 'AlphaData', alpha_data);


% Author:  Istvan Lauko
% Date:  1/15/2013


% UIExamples!!
%# uisetcolor uisetfont propedit plotedit uigetfile uigetdir uiputfile
%uisave uiload umtoggle uiinspect (Y. Altman in) findjobj (Y. Altman in)
%# uipushtool uitoolbar uitoggletool uicontrol uiputfile uitable uipanel uibuttongroup uimenu uiimport axes

%%axes enable your GUI to display graphics such as graphs and images using commands such as: 
%# plot, surf, line, bar, polar, pie, contour, and mesh.

%http://www.mathworks.com/help/matlab/creating_guis/examples-programming-gui-components.html?s_tid=doc_12b#f16-1003354
%http://www.mathworks.com/help/matlab/creating_guis/adding-components-to-a-programmatic-gui.html#f15-1000752

%Select an ActiveX Control dialog
%h = actxcontrolselect
%http://www.mathworks.com/help/matlab/matlab_external/creating-com-objects.html#f106314
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%             
% hText = text(.5,.5,'Hello World');
% C = uisetcolor(hText, 'Set Text Color')
%             
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%             
%splash screen             
% im = imread('russellport05.jpg');  %# A sample image to display
% jimg = im2java(im);
% frame = javax.swing.JFrame;
% frame.setUndecorated(true);
% icon = javax.swing.ImageIcon(jimg);
% label = javax.swing.JLabel(icon);
% frame.getContentPane.add(label);
% frame.pack;
% imgSize = size(im);
% frame.setSize(imgSize(2),imgSize(1));
% screenSize = get(0,'ScreenSize');  %# Get the screen size from the root object
% frame.setLocation((screenSize(3)-imgSize(2))/2,...  %# Center on the screen
%                   (screenSize(4)-imgSize(1))/2);
% frame.show;  %# You can hide it again with frame.hide
% pause(3);
% frame.hide;


% R = input('What is the pass thought? Figure it out and type it here:  ','s');
% 
% if str2num(datestr(now,'yy'))==13,  
%    if strcmp(R,'I am Dr Cool, so you dont be the Fool!'), YY='You got it!', else  YY='No it is not!', end
% elseif str2num(datestr(now,'yy'))==14, 'go ',
%    if strcmp(R,'I am Dr Kirk, so you dont be the Jerk!'), YY='You got it!', else  YY='No it is not!', end
% elseif str2num(datestr(now,'yy'))==15, 'go ',
%    if strcmp(R,'I am Dr Stress, so you dont be the Ass!'), YY='You got it!', else  YY='No it is not!', end
% elseif str2num(datestr(now,'yy'))==16, 'go ',
%    if strcmp(R,'I am Dr Chimp, so you dont be the Whimp!'), YY='You got it!', else  YY='No it is not!', end
% elseif str2num(datestr(now,'yy'))==17, 'go ',
%    if strcmp(R,'I am Dr Quick, so you dont be the Stick!'), YY='You got it!', else  YY='No it is not!', end
% else
%    if strcmp(R,'I am Dr Cape, so you dont be that Ape!'), YY='You got it!', else  YY='No it is not!', end 
% end
% AppropriateResponse=YY;
% if strcmp(AppropriateResponse,'No it is not!'), return; end


%      h = waitbar(perc,'Please wait... checking digital copyrights');
%    while  ii<100  & statusKEK
%     robotMouse0;
%     %pause(0.05);
%     perc=ii/100;
%     if statusKEK
%        pause(0.03);
%       %[statusKEK,CheckIstanceKEK] = system('tasklist /v | findstr /c:"lajkoocsi_kekmama_small44passwd2DomokosLauko2"');
%     else
%     %pause(0.01);
%     end
%     waitbar(perc,h );
%    ii=ii+1;
%    end
%    close(h)
    %#splash('russellport05.jpg',3)
    %#pause(3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%             
%             
%             
%             function color_gui
% fh = figure('Position',[250 250 250 150],'Toolbar','none');
% th = uitoolbar('Parent',fh);
% pth = uipushtool('Parent',th,'Cdata',rand(20,20,3),...
%                  'ClickedCallback',@color_callback);
% %-------------------------------------------------------
%     function color_callback(hObject,eventdata)
%     color = uisetcolor(fh,'Pick a color');
%     end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function toggle_plots
% counter = 0;
% fh = figure('Position',[250 250 300 340],'Toolbar','none');
% ah = axes('Parent',fh,'Units','pixels',...
%           'Position',[35 85 230 230]);
% th = uitoolbar('Parent',fh);
% tth = uitoggletool('Parent',th,'Cdata',rand(20,20,3),...
%                  'OnCallback',@surf_callback,...
%                  'OffCallback',@mesh_callback,...
%                  'ClickedCallback',@counter_callback);
% sth = uicontrol('Style','text','String','Counter: ',...
%                 'Position',[35 20 45 20]);
% cth = uicontrol('Style','text','String',num2str(counter),...
%                 'Position',[85 20 30 20]);
% %-------------------------------------------------------
%     function counter_callback(hObject,eventdata)
%     counter = counter + 1;
%     set(cth,'String',num2str(counter))
%     end
% %-------------------------------------------------------
%     function surf_callback(hObject,eventdata)
%     surf(ah,peaks(35));
%     end
% %-------------------------------------------------------
%     function mesh_callback(hObject,eventdata)
%     mesh(ah,peaks(35));
%     end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [file,path] = uiputfile('animinit.m','Save file name');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% To add an image to a push button (or togglebutton), assign the button's CData property an m-by-n-by-3 array of RGB values
% img(:,:,1) = rand(16,64);
% img(:,:,2) = rand(16,64);
% img(:,:,3) = rand(16,64);
% pbh = uicontrol(fh,'Style','pushbutton',...
%                 'Position',[50 20 100 45],...
%                 'CData',img);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% show the images
% Im = imread(files{k});
% AxesH = axes('Units', 'pixels', 'position', [0.5, 10, 400, 260], 'Visible', 'off');
% image('Parent', axesH, 'CData', Im); % add other property-value pairs as needed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global ax1L ax2L  ax3L  ax4L ax5L ax6L ax7L ax8L
global ax1R ax2R  ax3R  ax4R ax5R ax6R ax7R ax8R
global h1_L h2_L  h3_L  h4_L h5_L h6_L h7_L h8_L 
global h1_R h2_R  h3_R  h4_R h5_R h6_R h7_R h8_R
%global B1_LbarsL B2_LbarsL B3m_LbarsL B3M_LbarsL B4_LbarsL B5_LbarsL B6_LbarsL B7_LbarsL B8_LbarsL
%global B1_LbarsR B2_LbarsR B3m_LbarsR B3M_LbarsR B4_LbarsR B5_LbarsR B6_LbarsR B7_LbarsR B8_LbarsR
%global B1_RbarsL B2_RbarsL B3m_RbarsL B3M_RbarsL B4_RbarsL B5_RbarsL B6_RbarsL B7_RbarsL B8_RbarsL
%global B1_RbarsR B2_RbarsR B3m_RbarsR B3M_RbarsR B4_RbarsR B5_RbarsR B6_RbarsR B7_RbarsR B8_RbarsR
global minmaxAxV3 minmaxAxV4 minmaxAxTicks
global MINpV3Bp2 MAXpV3Bp2
global MINpV4Bp2 MAXpV4Bp2
global VarChoice1 VarChoice2 VarChoice3 VarChoice4 VarChoice5 VarChoice6 VarChoice7 VarChoice8
global CTDfilename CTDpathname SingleFileCall
global CTDloadDirname CTDloadExt CTDExt
global D Dfig
global Cseq NoUseCseq NoUseCseqWeak
global CAx1  CAx2 CAx3 CAx4  CAx5  CAx6 CAx7 CAx8
global CLine1 CLine2 CLine3 CLine4 CLine5 CLine6 CLine7 CLine8
global CTDData CTDDataHeader CTDDataHeaderUnit CTDDataAxFontWgt 
global LAxisRadioSeq RAxisRadioSeq ZeroRadioSeq LPlotSeq RPlotSeq MinMaxPlotSeq
global Sl sliderLineDots DataTime SDataTime SDataTimeInd DataTimeInd ViewBefore
global sliderAx BlueK BlueKSTEP  currentSliderValue TIMEslider STEPslider sub3M
global ListReturn ListReturnPre Viewswitch
%global maxtabKe mintabKe IndRangeTabDownKe jdKe IndRangeTabUpKe juKe deltapercKe
global maxtabV3m mintabV3m IndRangeTabDownV3m jdV3m IndRangeTabUpV3m juV3m deltapercV3m
global maxtabV3M mintabV3M IndRangeTabDownV3M jdV3M IndRangeTabUpV3M juV3M deltapercV3M
global maxtabV4m mintabV4m IndRangeTabDownV4m jdV4m IndRangeTabUpV4m juV4m deltapercV4m
global maxtabV4M mintabV4M IndRangeTabDownV4M jdV4M IndRangeTabUpV4M juV4M deltapercV4M
global XX1V3m XX1V3M XX2V3m XX2V3M XX1V4m XX1V4M XX2V4m XX2V4M

CTDFileName='';
CTDpathname='';
DataTime=[]; 
ListReturn=[];
CTDloadDirname=pwd;
CTDloadExt='XLS';
SingleFileCall=0;
currentSliderValue=1;
TIMEslider=1; STEPslider=~TIMEslider;
%STEPslider=1, TIMEslider=~STEPslider,
ListReturn={};  ListReturnPre={};
Viewswitch=0;
ViewBefore='TIME';
sub3M=0;
CTDDataHeader=[{'Var0'} {'Var1'} {'Var2'} {'Var3'} {'Var4'} {'Var5'} {'Var6'} {'Var7'} {'Var8'}  ];   
%[  {depFM} {'ln(PAR)')} {'FlS'} {'CStarTr0'} {'Oxygen Sat.'} {'t090C'} {'Ke'} {'ph'} {'Cond'} ];
CTDDataHeaderUnit=[{'Var0Unit'} {'Var1Unit'} {'Var2Unit'} {'Var3Unit'} {'Var4Unit'} {'Var5Unit'} {'Var6Unit'} {'Var7Unit'} {'Var8Unit'}  ];
%[  {m} {'ln(PAR)')} {'FlS'} {'%'} {'mg/l'} {'deg C'} {'unitless'} {'PH'} {'uS/cm'} ];
CTDDataAxFontWgt= [{'Bold'} {'Bold'} {'Bold'} {'Bold'} {'Bold'} {'Bold'} {'Bold'} {'Bold'} {'Bold'}  ];
%CTDDataHeader{1}

                    % # name 1 = depFM: Depth [fresh water, m]
                    % # name 2 = t090C: Temperature [ITS-90, deg C]
                    % # name 3 = v4: Voltage 4 %or FLS %or FlECO-AFL for fluorescence
                    % # name 4 = CStarTr0: Beam Transmission, WET Labs C-Star [%]
                    % # name 5 = c0uS/cm: Conductivity [uS/cm]
                    % # name 6 = specc: Specific Conductance [uS/cm]
                    % # name 7 = par: PAR/Irradiance, Biospherical/Licor
                    % # name 8 = oxsolMg/L: Oxygen Saturation, Garcia & Gordon [mg/l]
                    % # name 9 = ph: pH
                    % # name 10 = orp: Oxidation Reduction Potential [mV]
                    % # name 11 = wl0: RS-232 WET Labs raw counts 0
                    % # name 12 = wl1: RS-232 WET Labs raw counts 1
                    % # name 13 = wl2: RS-232 WET Labs raw counts 2
                    % # name 14 = timeS: Time, Elapsed [seconds]



%GUI LAYOUT

%GUI figure
Dfig    = dialog ( 'windowstyle', 'normal', 'resize', 'on', ...
                    'name','CTDWrangler', 'visible' ,'off'  );

                
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
%set icon                
javaFrame = get(Dfig,'JavaFrame');
        drawnow                
        pause(1)
%         javaFrame.setFigureIcon(javax.swing.ImageIcon('russellport05.jpg'));
        javaFrame.setFigureIcon(javax.swing.ImageIcon('water-icon.png'));
         set(Dfig, 'visible', 'on')                    

         
         
shiftup=0.04; shiftupButtonGr=0.016; shiftleft=0.005; shiftright=0.02;
         
         
%plotedit(Dfig,'on');                    
                    
% D.VOnOff = uicontrol (  'parent', Dfig, ...
%               'style', 'pushbutton', 'units', 'normalized','position', [0.9 0.9 0.1/2 0.1/2],... 
%               'string', 'Off','BackgroundColor' , 'r','fontunits', 'normalized', 'fontsize', 0.5, ...
%               'callback',{@VOnOff_pb_call});


% img(:,:,1) = rand(16,64);
% img(:,:,2) = rand(16,64);
% img(:,:,3) = rand(16,64);
% pbh = uicontrol(fh,'Style','pushbutton',...
%                 'Position',[50 20 100 45],...
%                 'CData',img);
img1(:,:,1) = ones(64,64)-fliplr(triu(ones(64,64)));
img1(:,:,2) = ones(64,64)-fliplr(triu(ones(64,64)));
img1(:,:,3) = ones(64,64)-fliplr(triu(ones(64,64)));
img1(:,:,3) = 1;
IMG1=img1; IMG2=1-img1; IMG2(:,:,3)=fliplr(1-triu(ones(64,64)));
img2=img1;   img2(:,:,1)=0;
IMG3=img2; IMG4=1-img2;
img3=img1;   img3(:,:,2)=1;
IMG5=img3; IMG5=1-img2;
img4=img1;   img4(:,:,3)=0;
IMG6=img4;

D.VOnOff = uicontrol (  'parent', Dfig, ...
              'style', 'pushbutton', 'units', 'normalized','position', [0.9+shiftright 0.9 0.1/2 0.1/2],... 
              'string', 'Snap','BackgroundColor' , 'y','CData',IMG4,'fontunits', 'normalized', 'fontsize', 0.5, ...
              'callback',{@VOnOff_pb_call});
FList=[];
D.FList = uicontrol (  'parent', Dfig, ...
              'style', 'togglebutton', 'units', 'normalized','position', [0.83+shiftright 0.9 0.1/2 0.1/2],... 
              'string', 'FList','BackgroundColor' , 'b','fontunits', 'normalized', 'fontsize', 0.5, ...
              'CData',IMG1,'callback',{@FList_pb_call});

 sub3METER = [ ...
    3 3 3 3 3 3 3 3 3 3 3 3 3 3 3;
 	3 3 3 3 3 3 3 3 3 3 3 3 3 3 3;
 	3 3 3 3 3 3 3 3 3 3 3 3 3 3 3;
    3 3 1 2 2 2 2 2 2 2 2 2 2 2 1;
 	3 3 1 2 2 2 2 2 2 2 2 2 2 2 1;
 	3 3 1 2 2 2 1 2 2 2 2 2 2 2 1;
 	3 3 1 1 1 1 1 1 1 2 1 2 1 2 1;
 	3 3 1 2 2 2 1 2 2 2 2 2 2 2 1;
 	3 3 1 2 2 2 2 2 2 2 2 2 2 2 1;
 	3 3 1 2 2 2 2 2 2 2 2 2 2 2 1;
 	3 3 1 1 2 2 2 2 2 2 2 2 2 2 1;
    3 3 3 3 3 3 3 3 3 3 3 3 3 3 3;
 	3 3 3 3 3 3 3 3 3 3 3 3 3 3 3;
 	3 3 3 3 3 3 3 3 3 3 3 3 3 3 3];

cmap = [0 0 0;255 240 160;255 255 255]/255;
sub3METER_im = ind2rgb(double(sub3METER),cmap);
sub3METER_but = uicontrol('Style','pushbutton',...
    'units', 'normalized',...
    'position', [0.007 +0.03-0.1/8 0.045 0.15/4],  ...
    'fontunits', 'normalized', ...
    'parent', Dfig,  ...
	'Callback',@sub3METER_cb,...
    'string', '3m|',...
	'ToolTip','View Sub 3 Meters Data');
	%'CData',sub3METER_im,...
    %'FontSize',6,... 
set(sub3METER_but','Enable','on')
ButBKGrndC=get(sub3METER_but,'BackgroundColor');
%set(sub3METER_but,'BackgroundColor','w')

FList=[];
D.FList = uicontrol (  'parent', Dfig, ...
              'style', 'togglebutton', 'units', 'normalized','position', [0.83+shiftright 0.9 0.1/2 0.1/2],... 
              'string', 'FList','BackgroundColor' , 'b','fontunits', 'normalized', 'fontsize', 0.5, ...
              'CData',IMG1,'callback',{@FList_pb_call});

%	'Position',[240 350 24 24],...
            
  TimeChoice = [ ...
    3 3 3 3 3 1 1 1 1 1 3 3 3 3 3;
 	3 3 3 3 3 1 1 1 1 1 3 3 3 3 3;
 	3 3 3 1 1 1 2 2 2 1 1 1 3 3 3;
    3 3 1 1 2 2 2 2 2 2 2 1 1 3 3;
 	3 3 1 2 2 2 2 2 2 2 1 2 1 3 3;
 	3 1 1 2 2 2 2 2 1 2 2 2 1 1 3;
 	3 1 2 2 2 2 2 1 2 2 2 2 2 1 3;
 	3 1 2 2 2 2 2 1 2 2 2 2 2 1 3;
 	3 1 1 2 2 2 2 1 2 2 2 2 1 1 3;
 	3 3 1 2 2 2 2 1 2 2 2 2 1 3 3;
 	3 3 1 1 2 2 2 1 2 2 2 1 1 3 3;
    3 3 3 1 1 1 2 2 2 1 1 1 3 3 3;
 	3 3 3 3 3 1 1 1 1 1 3 3 3 3 3;
    3 3 3 3 3 3 3 3 3 3 3 3 3 3 3];
 
cmap = [0 0 0;255 240 160;255 255 255]/255;
TimeChoice_im = ind2rgb(double(TimeChoice),cmap);
TimeChoice_but = uicontrol('Style','pushbutton',...
    'units', 'normalized',...
    'position', [0.007 0.9-0.03-0.1/8+shiftup 0.015 0.1/4],  ...
    'fontunits', 'normalized', ...
    'parent', Dfig,  ...
	'CData',TimeChoice_im,...
	'Callback',@Time_Choice_cb,...
	'ToolTip','Selected File-list viewed in a timeline');
set(TimeChoice_but','Enable','on')
ButBKGrndC=get(TimeChoice_but,'BackgroundColor');
set(TimeChoice_but,'BackgroundColor','w')

%	'Position',[240 350 24 24],...

ListChoice = [ ...
	 3 3 1 1 1 1 1 1 1 1 1 3;
	 3 1 1 2 2 2 2 2 2 2 1 3;
     3 1 2 1 1 1 1 1 1 2 1 3;
	 3 1 2 2 2 2 2 2 2 2 1 3;
	 3 1 2 1 1 1 1 1 1 2 1 3;
	 3 1 2 2 2 2 2 2 2 2 1 3;
	 3 1 2 1 1 1 1 1 1 2 1 3;
	 3 1 2 2 2 2 2 2 2 2 1 3;
	 3 1 2 1 1 1 1 1 1 2 1 3;
	 3 1 2 2 2 2 2 2 2 2 1 3;
     3 1 2 1 1 1 1 1 1 2 1 3;
	 3 1 2 2 2 2 2 2 2 1 1 3;
     3 1 1 1 1 1 1 1 1 1 3 3];
 
cmap = [0 0 0;255 240 160;255 255 255]/255;
ListChoice_im = ind2rgb(double(ListChoice),cmap);
ListChoice_but = uicontrol('Style','pushbutton',...
    'units', 'normalized',...
    'position', [0.03 0.9-0.03-0.1/8+shiftup 0.015 0.1/4],  ...
                 'fontunits', 'normalized', ...
	'CData',ListChoice_im,...
	'parent', Dfig,  ...
	'Callback',@List_Choice_cb,...
	'ToolTip','Selected File-list viewed as an ordered list');
set(ListChoice_but','Enable','on')
%ButBKGrndC=get(ListChoice_but,'BackgroundColor');
set(ListChoice_but,'BackgroundColor',ButBKGrndC);

%'KeyPressFcn',@keypressmisc,...
	




% D.ScreenSwitch4 = uicontrol (  'parent', Dfig, ...
%           'style', 'togglebutton', 'units', 'normalized','position', [0.85-shiftleft 0.17-(-5.8)*shiftup 0.1/2 0.1/2],... 
%           'string', 'TAB4','BackgroundColor' , 'g','fontunits', 'normalized', 'fontsize', 0.5, ...
%           'CData',IMG4,'callback',{@Rowplot_pb_call});
% 
% 
% D.ScreenSwitch3 = uicontrol (  'parent', Dfig, ...
%           'style', 'togglebutton', 'units', 'normalized','position', [0.85-shiftleft 0.09-(-5.8)*shiftup 0.1/2 0.1/2],... 
%           'string', 'TAB3','BackgroundColor' , 'g','fontunits', 'normalized', 'fontsize', 0.5, ...
%           'CData',IMG3,'callback',{@Rowplot_pb_call});
% 
% D.ScreenSwitch2 = uicontrol (  'parent', Dfig, ...
%           'style', 'togglebutton', 'units', 'normalized','position', [0.93-shiftleft 0.17-(-5.8)*shiftup 0.1/2 0.1/2],... 
%           'string', 'TAB2','BackgroundColor' , 'g','fontunits', 'normalized', 'fontsize', 0.5, ...
%           'CData',IMG2,'callback',{@Rowplot_pb_call});
% 
% D.ScreenSwitch1 = uicontrol (  'parent', Dfig, ...
%           'style', 'togglebutton', 'units', 'normalized','position', [0.93-shiftleft 0.09-(-5.8)*shiftup 0.1/2 0.1/2],... 
%           'string', 'TAB1','BackgroundColor' , 'g','fontunits', 'normalized', 'fontsize', 0.5, ...
%           'CData',IMG1,'callback',{@Rowplot_pb_call});
% 
% D.ScreenSwitch5 = uicontrol (  'parent', Dfig, ...
%           'style', 'togglebutton', 'units', 'normalized','position', [0.89-shiftleft 0.13-(-5.8)*shiftup 0.1/2 0.1/2],... 
%           'string', 'Chk Depth','BackgroundColor' , 'g','fontunits', 'normalized', 'fontsize', 0.3, ...
%           'CData',IMG5,'callback',{@Rowplot_pb_call});

D.ScreenSwitch4 = uicontrol (  'parent', Dfig, ...
              'style', 'togglebutton', 'units', 'normalized','position', [0.85-shiftleft 0.17-(-5.8)*shiftup 2.6*0.1/2 0.1/2],... 
              'string', 'DeckCell Mng','BackgroundColor' , 'y','fontunits', 'normalized', 'fontsize', 0.3, ...
              'CData',IMG4,'callback',{@DeckCellMng_pb_call},'visible','on'); 
          

D.ScreenSwitch3 = uicontrol (  'parent', Dfig, ...
              'style', 'togglebutton', 'units', 'normalized','position', [0.85-shiftleft 0.09-(-5.8)*shiftup-0.03 0.1/2 0.1/2],... 
              'string', 'Chk Depth','BackgroundColor' , 'b','fontunits', 'normalized', 'fontsize', 0.3, ...
              'CData',IMG3,'callback',{@Rowplot_pb_call});

D.ScreenSwitch2 = uicontrol (  'parent', Dfig, ...
              'style', 'togglebutton', 'units', 'normalized','position', [0.98-shiftleft 0.17-(-5.8)*shiftup 0.1/2 0.1/2],... 
              'string', 'Chk Depth','BackgroundColor' , 'y','fontunits', 'normalized', 'fontsize', 0.3, ...
              'CData',IMG2,'callback',{@Rowplot_pb_call},'visible','off');
          
D.ScreenSwitch1 = uicontrol (  'parent', Dfig, ...
              'style', 'togglebutton', 'units', 'normalized','position', [0.93-shiftleft 0.09-(-5.8)*shiftup-0.03 0.1/2 0.1/2],... 
              'string', 'Chk DepthXX','BackgroundColor' , 'b','fontunits', 'normalized', 'fontsize', 0.3, ...
              'CData',IMG1,'callback',{@Rowplot_pb_call});
          
D.ScreenSwitch5 = uicontrol (  'parent', Dfig, ...
              'style', 'togglebutton', 'units', 'normalized','position', [0.89-shiftleft 0.13-(-5.8)*shiftup-0.03 0.1/2 0.1/2],... 
              'string', 'Chk Depth','fontunits', 'normalized', 'fontsize', 0.3, ...
              'callback',{@Rowplot_pb_call});

%FLIST
          
D.VCTDfileTxt = uicontrol ( 'parent', Dfig,  ...
                'style', 'text', 'units', 'normalized','position', [0.01 0.9-0.1/8+shiftup 0.09 0.1/2],  ...
                'string', 'CTD File:', 'fontunits', 'normalized', 'fontsize', 0.5);

D.VCTDfile = uicontrol ( 'parent', Dfig,  ...
              'BackgroundColor' , [0.991569 0.993725 0.9912],'style','togglebutton','units','normalized',...
              'position', [0.1 0.9+shiftup 0.7 0.1/2],'string','Select A Single File (or try the FileLists)','fontunits','normalized','fontsize', 0.5,...
              'callback',{@SingleFileLoad_callback});

%setting plotting space and background            
figure(1001)            
[D.ax,h0_1,h0_2] = plotyy(0,0,0,0,'plot');
    %set(get(D.ax(1),'Ylabel'),'color','n')
    %set(get(D.ax(2),'Ylabel'),'color','n')
    set(D.ax(1),'Parent',Dfig,'units','normalized','position',[0.06 0.09+shiftup 0.7 0.7]); %axis([0 7 -1 1])
    set(D.ax(2),'Parent',Dfig,'units','normalized','position',[0.06 0.09+shiftup 0.7 0.7]); %axis([0 7 -1 1])
    set(D.ax(1),'Xcolor','w','Ycolor','w','Ytick',[],'Xtick',[],'YAxisLocation','Left','visible','on')
    set(D.ax(2),'Xcolor','w','Ycolor','w','Ytick',[],'Xtick',[],'YAxisLocation','Right','visible','on')
    set(h0_1,'Color','w')
    set(h0_2,'Color','w')
delete(figure(1001))    



%PLOT initial CONTENT!!!!! ( done here -- figure initialization)
        %COLOR PURPLE!!
        %plot(1:100,sin(1:100),'color',1/255*[148 0 211])

        %  Varseq = []                       %variables plotted
         delta=.7; gamm1=0.7;gamm2=0.4;
         %Cseq=[[0 0 1];[0 1 0 ];[0 1 1];[1 0 0];[1 0 1];[1 1 0]; delta*[gamm1 gamm1 gamm1]; [gamm2/2 gamm2 gamm2]]; %Colorseq
         Cseq=[[0 0 1];[0 1 0 ];[0 1 1];[1 0 0];[1 0 1];[1 0.5/delta 0.2/delta]; delta*[gamm1 gamm1 gamm1]; [gamm2/2 gamm2 gamm2]]; %Colorseq %%replace Yellow to Orange
         %[1 0.5 0.2]
         
         %NoUseCseq=[[delta delta 1];[delta 1 delta ];[delta 1 1];[1 delta delta];[1 delta 1];[1 1 delta]; [gamm1 gamm1 gamm1];[gamm2/2 gamm2 gamm2]/delta];%%replace Yellow to Orange
         NoUseCseq=[[delta delta 1];[delta 1 delta ];[delta 1 1];[1 delta delta];[1 delta 1];[1 0.5/delta 0.2/delta]+ (1-[1 0.5/delta 0.2/delta])/2; [gamm1 gamm1 gamm1];[gamm2/2 gamm2 gamm2]/delta];
         NoUseCseqWeak=[[delta delta 1];[delta 1 delta ];[delta 1 1];[1 delta delta];[1 delta 1];[1 0.5/delta 0.2/delta]+ (1-[1 0.5/delta 0.2/delta])/2; [gamm1 gamm1 gamm1];[gamm2/2 gamm2 gamm2]/delta];
        % Colorseq=['b';'y';'r';'m';'c';'g']
        % NoUseColorseq=['b';'y';'r';'m';'c';'g']
        % Axisseq=['L ';' R';'off';'off';'off';'off']; %'LR','L ',' R','off'
        % Unitseq=['L ';' R';'off';'off';'off';'off']; %'LR','L ',' R','off'
         LAxisRadioSeq=[1 0 0 0 0 0 0 0]; %Left Axis Button Settings
         RAxisRadioSeq=[1 0 0 0 0 0 0 0]; %Right Axis Button Settings
         ZeroRadioSeq=[0 0 0 0 0 0 0 0];
         LPlotSeq=[1 1 1 1 1 1 1 1];
         RPlotSeq=[1 1 1 1 1 1 1 1];%ones(1,8);
         MinMaxPlotSeq=[1 1 1 1 ];

         
%Axiscolors
CAx1=Cseq(1,:);   CAx2=Cseq(2,:);   CAx3=Cseq(3,:);   CAx4=Cseq(4,:);   CAx5=Cseq(5,:);   CAx6=Cseq(6,:);   CAx7=Cseq(7,:);   CAx8=Cseq(8,:);
%plotColors
CLine1=Cseq(1,:); CLine2=Cseq(2,:); CLine3=Cseq(3,:); CLine4=Cseq(4,:); CLine5=Cseq(5,:); CLine6=Cseq(6,:); CLine7=Cseq(7,:); CLine8=Cseq(8,:);


% %---------------------------
%    barsL=patch([0 0 1 1],[1 0 0 1],[0 0 1]),
%    set(barsL, 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax1L)
%    barsR=patch([0 0 1 1],[1 0 0 1],[0 0 1]),
%    set(barsR, 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax1R)
% 
% %---------------------------


%minmax -----------------------------

% minmaxAx = axes('Position',[0.06 0.05 0.7 0.08],'Parent',Dfig,'units','normalized',...
%            'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
%            'XGrid','off','YGrid','off','Box','off','yticklabel',[],...
%            'Ycolor',[0.9198 0.9198 0.9198],'YAxisLocation','Left' ,'Visible','on','fontunits', 'normalized','fontsize', 1.0);
      

GreyBGColor=0.93980*[1 1 1];
minmaxAxV3 = axes('Position',[0.06 0.05 0.7 0.78],'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor',GreyBGColor,...
           'XGrid','off','YGrid','off','Box','off','yticklabel',[],...
           'Ycolor',GreyBGColor,'YAxisLocation','Left' ,'Visible','on','fontunits', 'normalized','fontsize', 1.0);

minmaxAxV4 = axes('Position',[0.06 0.05 0.7 0.78],'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor',GreyBGColor,...
           'XGrid','off','YGrid','off','Box','off','yticklabel',[],...
           'Ycolor',GreyBGColor,'YAxisLocation','Left' ,'Visible','on','fontunits', 'normalized','fontsize', 1.0);
       
minmaxAxTicks = axes('Position',[0.06 0.05 0.7 0.01],'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off','yticklabel',[],...
           'Ycolor',GreyBGColor,'YAxisLocation','Left' ,'Visible','off','fontunits', 'normalized','fontsize', 1.0);

%why is this needed??
%LabelLine=line([.1 .3 .5],0*[.1 .3 .5],'Color', 'k' , 'LineStyle', '.' , 'Parent' ,...
%                minmaxAxTicks, 'Visible' , 'off' );

%set(minmaxAxTicks,'XTick',[0.2 0.3 0.7, 0.71],'xticklabel',{'0.2'; '0.3'; '0.7'} )
%set(minmaxAx2,'XTicklabelMode', 'auto')
%get(minmaxAx2)

set(minmaxAxV3,'XTick',[],'YTick',[])
set(minmaxAxV4,'XTick',[],'YTick',[])
%set(minmaxAx,'XTick',[0.2 0.3 0.7, 0.9],'YTick',[],'xticklabel',[0.2 0.3 0.7]' )

figure(1001)    
cmapcool=colormap(cool(88));
cmapcopper=colormap(copper(88));
delete(figure(1001))    

cmapSize=32;

dmapcool=cmapcool(1:32,:);
dmapcopper=cmapcopper(89-33+1:88,:);

%colormap(minmaxAxV3,dmapcool);
colormap(minmaxAxV3,[dmapcool ; dmapcopper]);
%%
% 
%    
% 

colormap(minmaxAxV4,[dmapcool ; dmapcopper]);
%colormap(minmaxAxV4,dmapcopper);

%colorbar
%B1_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax1L);


% XMISS (Var3) MINS
%verticals
singleline=1;
if singleline
    MINpV3Bp2 = patch([0.45*ones(1,cmapSize) nan],[[0:(cmapSize-1)]/(cmapSize-1) nan], [(1:cmapSize) nan]+1,...
                  'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'off');
    MINpV4Bp2 = patch([0.45*ones(1,cmapSize) nan],[[0:(cmapSize-1)]/(cmapSize-1) nan], [(1:cmapSize) nan]+1+cmapSize,...
                  'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'off');
else
    MINpV3Bph = patch( [  [0.5*ones(1,cmapSize) nan]' ; [0.4*ones(1,cmapSize) nan]' ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan]' ; [[0:(cmapSize-1)]/(cmapSize-1) nan]' ], ...
                   [ [1:cmapSize nan]'+1; [1:cmapSize nan]'+1  ]   , 'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'off');
    MINpV4Bpv = patch( [  [0.5*ones(1,cmapSize) nan] ; [0.4*ones(1,cmapSize) nan] ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan] ; [[0:(cmapSize-1)]/(cmapSize-1) nan] ], ...
                   [ ([1:cmapSize nan])+1; ([1:cmapSize nan])+1  ]+cmapSize , 'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'off');

    MINpV3Bph = patch( [  [0.5*ones(1,cmapSize) nan]' ; [0.4*ones(1,cmapSize) nan]' ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan]' ; [[0:(cmapSize-1)]/(cmapSize-1) nan]' ], ...
                   [ [1:cmapSize nan]'+1; [1:cmapSize nan]'+1  ]   , 'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'off');
    MINpV4Bpv = patch( [  [0.5*ones(1,cmapSize) nan] ; [0.4*ones(1,cmapSize) nan] ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan] ; [[0:(cmapSize-1)]/(cmapSize-1) nan] ], ...
                   [ ([1:cmapSize nan])+1; ([1:cmapSize nan])+1  ]+cmapSize , 'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'off');

end
% XMISS MAXS
%horizontals
if singleline
    MAXpV3Bp2 = patch([0.25*ones(1,cmapSize) nan],[[0:(cmapSize-1)]/(cmapSize-1) nan], [fliplr(0:cmapSize-1) nan]+1,...
                  'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'off');
    MAXpV4Bp2 = patch([0.25*ones(1,cmapSize) nan],[[0:(cmapSize-1)]/(cmapSize-1) nan], [fliplr(0:cmapSize-1) nan]+1+cmapSize,...
                  'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'off');
else
    MAXpV3Bph = patch( [  [0.2*ones(1,cmapSize) nan]' ; [0.3*ones(1,cmapSize) nan]' ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan]' ; [[0:(cmapSize-1)]/(cmapSize-1) nan]' ], ...
                   [ fliplr([0:cmapSize-1 nan]')+1; fliplr([1:cmapSize nan]')+1  ] , 'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'off');
    MAXpV3Bpv = patch( [  [0.2*ones(1,cmapSize) nan] ; [0.3*ones(1,cmapSize) nan] ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan] ; [[0:(cmapSize-1)]/(cmapSize-1) nan] ], ...
                   [ fliplr([0:cmapSize-1 nan])+1; fliplr([0:cmapSize-1 nan])+1  ]   , 'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'off');

    MAXpV4Bph = patch( [  [0.2*ones(1,cmapSize) nan]' ; [0.3*ones(1,cmapSize) nan]' ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan]' ; [[0:(cmapSize-1)]/(cmapSize-1) nan]' ], ...
                   [ fliplr([1:cmapSize nan]')+1; fliplr([1:cmapSize nan]')+1+cmapSize  ] , 'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'off');
    MAXpV4Bpv = patch( [  [0.2*ones(1,cmapSize) nan] ; [0.3*ones(1,cmapSize) nan] ],...
                  [ [[0:(cmapSize-1)]/(cmapSize-1) nan] ; [[0:(cmapSize-1)]/(cmapSize-1) nan] ], ...
                   [ fliplr([0:cmapSize-1 nan] )+1; fliplr([0:cmapSize-1 nan] )+1+cmapSize  ]   , 'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'off');
end



% MAXpBpv = patch( [  [0.5*ones(1,cmapSize) nan] ; [0.3*ones(1,cmapSize) nan] ],...
%               [ [[0:(cmapSize-1)]/(cmapSize-1) nan] ; [[0:(cmapSize-1)]/(cmapSize-1) nan] ], ...
%                [ fliplr([1:cmapSize nan])+1; fliplr([1:cmapSize nan])+1  ] , 'EdgeColor', 'interp','Parent', minmaxAx,'visible', 'on');

%single line
%pBp2 = patch([0.2*ones(1,cmapSize) nan],[[0:(cmapSize-1)]/(cmapSize-1) nan], [fliplr(1:cmapSize) nan]+1,...
%              'EdgeColor', 'interp','Parent', minmaxAx,'visible', 'off');

          
set(minmaxAxV3,'YLim',[0 1])
set(minmaxAxV4,'YLim',[0 1])

xticks=[0.2 0.3 0.5];
xtickslab=cellstr(num2str(xticks'));
set(minmaxAxTicks,'XTick',xticks)

set(minmaxAxTicks,'XTicklabel',xtickslab)





%var1
ax1L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx1,'YAxisLocation','Left' ,'Visible','off');
h1_L=line(0,0, 'Color', CLine1, 'LineStyle', '-', 'Marker', '.', 'Parent',...
          ax1L, 'Visible','off');
hold(ax1L,'on')      
set(get(ax1L,'Ylabel'),'String',[CTDDataHeader{1+1}, ' (' CTDDataHeaderUnit{1+1} ')'],'FontWeight', CTDDataAxFontWgt{1+1})
set(get(ax1L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax1L,'Ylabel'),'String','ln(PAR) (unit)','FontWeight', 'Bold')
%set(get(ax1L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
%B1_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax1L);
%B1_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax1L);
hold(ax1L,'off')      

ax1R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx1,'YAxisLocation','Right' ,'Visible','off');
h1_R=line(0,0, 'Color', CLine1, 'LineStyle', '-', 'Marker', '.', 'Parent',... 
           ax1R, 'Visible','off');
hold(ax1R,'on')      
set(get(ax1R,'Ylabel'),'String',[CTDDataHeader{1+1}, ' (' CTDDataHeaderUnit{1+1} ')'],'FontWeight', CTDDataAxFontWgt{1+1})
set(get(ax1R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax1R,'Ylabel'),'String','ln(PAR) (unit)','FontWeight', 'Bold')
%set(get(ax1R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')

%B1_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax1R);
%B1_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax1R);
hold(ax1R,'off')      

%var2
ax2L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx2,'YAxisLocation','Left' ,'Visible','off');
h2_L=line(0,0, 'Color', CLine2, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax2L, 'Visible','off');
hold(ax2L,'on')      
set(get(ax2L,'Ylabel'),'String',[CTDDataHeader{2+1}, ' (' CTDDataHeaderUnit{2+1} ')'],'FontWeight', CTDDataAxFontWgt{2+1})
set(get(ax2L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax2L,'Ylabel'),'String','FLUOR (unit)','FontWeight', 'Bold')
%set(get(ax2L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
%B2_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax2L);
%B2_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax2L);
hold(ax2L,'off')      

ax2R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx2,'YAxisLocation','Right' ,'Visible','off');
h2_R=line(0,0, 'Color', CLine2, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax2R, 'Visible','off');
hold(ax2R,'on')      
set(get(ax2R,'Ylabel'),'String',[CTDDataHeader{2+1}, ' (' CTDDataHeaderUnit{2+1} ')'],'FontWeight', CTDDataAxFontWgt{2+1})
set(get(ax2R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax2R,'Ylabel'),'String','FLUOR (unit)','FontWeight', 'Bold')
%set(get(ax2R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
%B2_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax2R);
%B2_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax2R);
hold(ax2L,'off')      

%var3
ax3L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx3,'YAxisLocation','Left' ,'Visible','off');
h3_L=line(0,0, 'Color', CLine3, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax3L, 'Visible','off');
set(get(ax3L,'Ylabel'),'String',[CTDDataHeader{3+1}, ' (' CTDDataHeaderUnit{3+1} ')'],'FontWeight', CTDDataAxFontWgt{3+1})
set(get(ax3L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax3L,'Ylabel'),'String','XMISS ( % )','FontWeight', 'Bold')
%set(get(ax3L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax3L,'on')      
%B3m_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3L);
%B3m_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3L);
%B3M_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3L);
%B3M_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3L);
hold(ax3L,'off')      

ax3R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx3,'YAxisLocation','Right' ,'Visible','off');
h3_R=line(0,0, 'Color', CLine3, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax3R, 'Visible','off');
set(get(ax3R,'Ylabel'),'String',[CTDDataHeader{3+1}, ' (' CTDDataHeaderUnit{3+1} ')'],'FontWeight', CTDDataAxFontWgt{3+1})
set(get(ax3R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax3R,'Ylabel'),'String','XMISS ( % )','FontWeight', 'Bold')
%set(get(ax3R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax3R,'on')      
%B3m_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3R);
%B3m_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3R);
%B3M_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3R);
%B3M_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax3R);
hold(ax3R,'off')      

%var4
ax4L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx4,'YAxisLocation','Left' ,'Visible','off');
h4_L=line(0,0, 'Color', CLine4, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax4L, 'Visible','off');
set(get(ax4L,'Ylabel'),'String',[CTDDataHeader{4+1}, ' (' CTDDataHeaderUnit{4+1} ')'],'FontWeight', CTDDataAxFontWgt{4+1})
set(get(ax4L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax4L,'Ylabel'),'String','T090C ( deg C )','FontWeight', 'Bold')
%set(get(ax4L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax4L,'on')      
%B4_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax4L);
%B4_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax4L);
hold(ax4L,'off')      

ax4R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx4,'YAxisLocation','Right' ,'Visible','off');
h4_R=line(0,0, 'Color', CLine4, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax4R, 'Visible','off');
set(get(ax4R,'Ylabel'),'String',[CTDDataHeader{4+1}, ' (' CTDDataHeaderUnit{4+1} ')'],'FontWeight', CTDDataAxFontWgt{4+1})
set(get(ax4R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax4R,'Ylabel'),'String','T090C ( deg C )','FontWeight', 'Bold')
%set(get(ax4R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax4R,'on')      
%B4_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax4R);
%B4_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax4R);
hold(ax4R,'off')      

%var5
ax5L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx5,'YAxisLocation','Left' ,'Visible','off');
h5_L=line(0,0, 'Color', CLine5, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax5L, 'Visible','off');
set(get(ax5L,'Ylabel'),'String',[CTDDataHeader{5+1}, ' (' CTDDataHeaderUnit{5+1} ')'],'FontWeight', CTDDataAxFontWgt{5+1})
set(get(ax5L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax5L,'Ylabel'),'String','DO2 (unit)','FontWeight', 'Bold')
%set(get(ax5L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax5L,'on')      
%B5_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax5L);
%B5_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax5L);
hold(ax5L,'off')      

ax5R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx5,'YAxisLocation','Right' ,'Visible','off');
h5_R=line(0,0, 'Color', CLine5, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax5R, 'Visible','off');
set(get(ax5R,'Ylabel'),'String',[CTDDataHeader{5+1}, ' (' CTDDataHeaderUnit{5+1} ')'],'FontWeight', CTDDataAxFontWgt{5+1})
set(get(ax5R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax5R,'Ylabel'),'String','DO2 (unit)','FontWeight', 'Bold')
%set(get(ax5R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax5R,'on')      
%B5_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax5R);
%B5_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax5R);
hold(ax5R,'off')      

%var6
ax6L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx6,'YAxisLocation','Left' ,'Visible','off');
h6_L=line(0,0, 'Color', CLine6, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax6L, 'Visible','off');
set(get(ax6L,'Ylabel'),'String',[CTDDataHeader{6+1}, ' (' CTDDataHeaderUnit{6+1} ')'],'FontWeight', CTDDataAxFontWgt{6+1})
set(get(ax6L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax6L,'Ylabel'),'String','Ke (dimensionless)','FontWeight', 'Bold')
%set(get(ax6L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax6L,'on')      
%B6_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax6L);
%B6_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax6L);
hold(ax6L,'off')      

ax6R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx6,'YAxisLocation','Right' ,'Visible','off');
h6_R=line(0,0, 'Color', CLine6, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax6R, 'Visible','off');
set(get(ax6R,'Ylabel'),'String',[CTDDataHeader{6+1}, ' (' CTDDataHeaderUnit{6+1} ')'],'FontWeight', CTDDataAxFontWgt{6+1})
set(get(ax6R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax6R,'Ylabel'),'String','Ke (dimensionless)','FontWeight', 'Bold')
%set(get(ax6R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax6R,'on')      
%B6_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax6R);
%B6_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax6R);
hold(ax6R,'off')      
           
            
%var7
ax7L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx7,'YAxisLocation','Left' ,'Visible','off');
h7_L=line(0,0, 'Color', CLine7, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax7L, 'Visible','off');
set(get(ax7L,'Ylabel'),'String',[CTDDataHeader{7+1}, ' (' CTDDataHeaderUnit{7+1} ')'],'FontWeight', CTDDataAxFontWgt{7+1})
set(get(ax7L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax7L,'Ylabel'),'String','Var 7 (unit)','FontWeight', 'Bold')
%set(get(ax7L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax7L,'on')      
%B7_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax7L);
%B7_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax7L);
hold(ax7L,'off')      

ax7R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx7,'YAxisLocation','Right' ,'Visible','off');
h7_R=line(0,0, 'Color', CLine7, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax7R, 'Visible','off');
set(get(ax7R,'Ylabel'),'String',[CTDDataHeader{7+1}, ' (' CTDDataHeaderUnit{7+1} ')'],'FontWeight', CTDDataAxFontWgt{7+1})
set(get(ax7R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax7R,'Ylabel'),'String','Var 7 (unit)','FontWeight', 'Bold')
%set(get(ax7R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax7R,'on')      
%B7_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax7R);
%B7_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax7R);
hold(ax7R,'off')      

%var8
ax8L = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx8,'YAxisLocation','Left' ,'Visible','off');
h8_L=line(0,0, 'Color', CLine8, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax6L, 'Visible','off');
set(get(ax8L,'Ylabel'),'String',[CTDDataHeader{8+1}, ' (' CTDDataHeaderUnit{8+1} ')'],'FontWeight', CTDDataAxFontWgt{8+1})
set(get(ax8L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax8L,'Ylabel'),'String','Var 8 (unit)','FontWeight', 'Bold')
%set(get(ax8L,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax8L,'on')      
%B8_LbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax8L);
%B8_LbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax8L);
hold(ax8L,'off')      

ax8R = axes('Position',get(D.ax(1),'Position'),'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off',...
           'Ycolor',CAx8,'YAxisLocation','Right' ,'Visible','off');
h8_R=line(0,0, 'Color', CLine8, 'LineStyle', '-', 'Marker', '.', 'Parent',...
           ax8R, 'Visible','off');
set(get(ax8R,'Ylabel'),'String',[CTDDataHeader{8+1}, ' (' CTDDataHeaderUnit{8+1} ')'],'FontWeight', CTDDataAxFontWgt{8+1})
set(get(ax8R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
%set(get(ax8R,'Ylabel'),'String','Var 8 (unit)','FontWeight', 'Bold')
%set(get(ax8R,'Xlabel'),'String','Depth (m)','FontWeight', 'Bold')
hold(ax8R,'on')      
%B8_RbarsL=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax8R);
%B8_RbarsR=patch([0 0 1 1],[1 0 0 1],[0 0 1], 'EdgeColor','none','facealpha',0.2,'Visible','off','Parent',ax8R);
hold(ax8R,'off')      

%SLIDER -----------------------------

sliderAx = axes('Position',[0.06 0.85+shiftup 0.75 0.01],'Parent',Dfig,'units','normalized',...
           'XAxisLocation','bottom','YAxisLocation','left','Color','none','XColor','k',...
           'XGrid','off','YGrid','off','Box','off','yticklabel',[],...
           'Ycolor',[0.94 0.94 0.94],'YAxisLocation','Left' ,'Visible','on','fontunits', 'normalized','fontsize', 1.9);

%t = (1994:1:2012)'; % Time interval      
% x = 19610807;
% d = datestr(x)
% d = 24-Jul-3692
%
%datestr(datenum(sprintf('%d',x),'yyyymmdd'),'dd-mm-yyyy')

DateVector = datevec(datestr(now+30)); %slider is on 1994 til now
t = (1994:1:DateVector(1))'; % Time interval
       
tD=datenum(t,1,1); % Time interval in datenum format
sliderLine=line(tD,0*tD,'Color',[0.991569 0.993725 0.9912] , 'LineStyle', '-', 'Parent',...
                sliderAx, 'Visible','off');

NumTicks = 5;
%L = get(sliderAx,'XLim');
set(sliderAx,'XTick',linspace(tD(1),tD(end),NumTicks))
datetick(sliderAx,'x','mmmyy','keepticks') 
%xticklabel_rotate([],45,[],'Fontsize',10)
      
% collect dates of data in the current directory      
%DataTime =[ datenum(2001,12,19) datenum(2003,12,19) datenum(2007,12,19) datenum(2009,12,19)];
files = dir( '*CTD*.csv' ); %find CTD csv files in directory
%     name     date    bytes   isdir
%  Files = strvcat( files.name );
[CTDFILES FI]=sortrows(strvcat( files.name ));
yy=str2num(CTDFILES(FI(1:end),1:2));  mm=str2num(CTDFILES(FI(1:end),3:4));   dd=str2num(CTDFILES(FI(1:end),5:6)); 
yy(find(yy>=50))=yy(find(yy>=50))-100;
yy=yy+2000;
DataTime=datenum([yy mm dd]);

sliderLineDots=line(DataTime,0*DataTime, 'Color','r' , 'LineStyle', 'none', 'Marker', '.','Parent',...
          sliderAx, 'Visible','on');
BlueK=3;
sliderLineDots2=line(DataTime(BlueK),0*DataTime(BlueK), 'Color','b' ,'LineStyle', 'none', 'Marker', '.','Parent',...
          sliderAx, 'Visible','on');
SmallSTEP=0; 
LargeSTEP=10*SmallSTEP;
Sl = uicontrol('BackgroundColor' , [0.991569 0.993725 0.9912],'Parent', Dfig,'Style','slider', 'Value',5, 'Min',1,'units', 'normalized',...
        'Max',100, 'SliderStep',[SmallSTEP LargeSTEP], ...
        'Position',[0.06 0.87+shiftup 0.75 0.012], 'Callback',@slider_callback); 
sliderMin=1;
sliderMax=100;
currentSliderValue=1;
% Sl = uicontrol('BackgroundColor' , [0.991569 0.993725 0.9912],'Parent', Dfig,'Style','slider', 'Value',DataTime(3)-DataTime(3), 'Min',0,'units', 'normalized',...
%         'Max',Datatime(end)-DataTime(1), 'SliderStep',[1 10], ...
%         'Position',[0.06 0.87 0.75 0.012], 'Callback',@slider_callback); 

%hTxt = uicontrol('Parent', Dfig,'Style','text', 'Position',[0.02 0.8 0.01 0.04],'fontsize', 36, 'String','0','units', 'normalized', 'fontunits', 'normalized')
%hTxt = uicontrol('Parent', Dfig,'Style','text','units', 'normalized', 'fontunits', 'normalized','fontsize', 0.5,...
%    'Position',[0.02 0.87 0.01 0.04], 'String','0');


 
%------------------------------------


%D.BtnGrpTxt = uicontrol ( 'parent', Dfig,  ...
%                'style', 'text', 'units', 'normalized',  ...
%                'position', [.7-.02 0.8-.02  0.1*1.5 0.1/2],  ...
%                'string', 'Variable', 'fontunits', 'normalized', 'fontsize', 0.5);

%vertical
Entr=8; Base=.05; Gap=(1- 2*Base)/Entr;
%horizontal
Horbase=0.84; GapW=[]; Col1W=0.09;Col2W=0.025;Col3W=0.025;
%Vertbase=0.43; Verthight=0.4;
VertTop=.8;
Vertbase=VertTop-(Entr)*Base; Verthight=Entr*(Base);

D.BtnGrp = uibuttongroup('Position',[Horbase Vertbase+shiftup+shiftupButtonGr Col1W Verthight],...
                         'Title',{'Var.'},'TitlePosition','centertop','Bordertype','None',...
                         'Units','Normalized','Parent',Dfig);
set(D.BtnGrp,'SelectionChangeFcn',@selchbk_call);
%set(D.BtnGrp,'SelectedObject',[]);  % No selection
    %For the SelectionChangeFcn callback, selcbk, the source and event data structure arguments 
    %are available only if selcbk is called using a function handle. See SelectionChangeFcn for more information.
            
    VarChoice1 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
             'units', 'normalized','Position',[.01  Base+7*Gap  .99  .15],'BackgroundColor' , CLine1,...
             'fontunits', 'normalized', 'fontsize', .5,'String','lnPAR', ...
             'callback',{@VarChoice_check_call});
    VarChoice2 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
             'units', 'normalized','Position',[.01  Base+6*Gap  .99  .15],'BackgroundColor' , CLine2,...
             'fontunits', 'normalized', 'fontsize', .5,'String','FLUOR', ...
             'callback',{@VarChoice_check_call});
    VarChoice3 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp,...
             'units', 'normalized','Position',[.01  Base+5*Gap  .99 .15],'BackgroundColor' , CLine3,...
             'fontunits', 'normalized', 'fontsize', .5,'String','XMISS', ...
             'callback',{@VarChoice_check_call});
    VarChoice4 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
             'units', 'normalized','Position',[.01  Base+4*Gap .99 .15],'BackgroundColor' , CLine4,...
             'fontunits', 'normalized', 'fontsize', .5,'String','T090C', ...
             'callback',{@VarChoice_check_call});
    VarChoice5 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
             'units', 'normalized','Position',[.01  Base+3*Gap .99 .15],'BackgroundColor' , CLine5,...
             'fontunits', 'normalized', 'fontsize', .5,'String','DO2', ...
             'callback',{@VarChoice_check_call});    
    VarChoice6 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
             'units', 'normalized','Position',[.01  Base+2*Gap .99 .15],'BackgroundColor' , CLine6,...
             'fontunits', 'normalized', 'fontsize', .5,'String','Ke', ...
             'callback',{@VarChoice_check_call});    
%     VarChoice6 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
%              'units', 'normalized','Position',[.01  Base+2*Gap .99 .15],'BackgroundColor' , CLine6,...
%              'fontunits', 'normalized', 'fontsize', .5,'String','KeExSm', ...
%              'callback',{@VarChoice_check_call});    
    VarChoice7 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
             'units', 'normalized','Position',[.01  Base+1*Gap .99 .15],'BackgroundColor' , CLine7,...
             'fontunits', 'normalized', 'fontsize', .5,'String','Var 7', ...
             'callback',{@VarChoice_check_call});    
%     VarChoice7 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
%              'units', 'normalized','Position',[.01  Base+1*Gap .99 .15],'BackgroundColor' , CLine7,...
%              'fontunits', 'normalized', 'fontsize', .5,'String','Ke', ...
%              'callback',{@VarChoice_check_call});    
    VarChoice8 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
             'units', 'normalized','Position',[.01  Base+0*Gap .99 .15],'BackgroundColor' , CLine8,...
             'fontunits', 'normalized', 'fontsize', .5,'String','Var 8', ...
             'callback',{@VarChoice_check_call});    
%     VarChoice8 = uicontrol('Style','checkbox', 'Parent',D.BtnGrp, ...
%              'units', 'normalized','Position',[.01  Base+0*Gap .99 .15],'BackgroundColor' , CLine8,...
%              'fontunits', 'normalized', 'fontsize', .5,'String','KeInt', ...
%              'callback',{@VarChoice_check_call});    
    

    %set(D.BtnGrp,'SelectionChangeFcn',@selchbk_call);
    %set(D.BtnGrp,'SelectedObject',[]);  % No selection
    %For the SelectionChangeFcn callback, selcbk, the source and event data structure arguments 
    %are available only if selcbk is called using a function handle. See SelectionChangeFcn for more information.

D.BtnGrpAxL = uibuttongroup('Position',[Horbase+Col1W Vertbase+shiftup+shiftupButtonGr Col2W Verthight],...
                            'Title',{'L'},'TitlePosition','centertop','Bordertype','None',...
                            'Units','Normalized','Parent',Dfig);
set(D.BtnGrpAxL,'SelectionChangeFcn',@selchbk_callAxL);
%set(D.BtnGrpAxL,'SelectedObject',[]);  % No selection
    %For the SelectionChangeFcn callback, selcbk, the source and event data structure arguments 
    %are available only if selcbk is called using a function handle. See SelectionChangeFcn for more information.
    
            
    D.AxisChoice1AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL, ...
             'units', 'normalized','Position',[.01  Base+7*Gap  .99  .15],...
             'BackgroundColor' , CLine1,'fontunits', 'normalized', 'fontsize', .5, 'Tag','1');
    D.AxisChoice2AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL, ...
             'units', 'normalized','Position',[.01  Base+6*Gap  .99  .15],...
             'BackgroundColor' , CLine2,'fontunits', 'normalized', 'fontsize', .5, 'Tag','2');
    D.AxisChoice3AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL,...
             'units', 'normalized','Position',[.01  Base+5*Gap  .99 .15],...
             'BackgroundColor' , CLine3,'fontunits', 'normalized', 'fontsize', .5, 'Tag','3');
    D.AxisChoice4AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL, ...
             'units', 'normalized','Position',[.01  Base+4*Gap .99 .15],...
             'BackgroundColor' , CLine4,'fontunits', 'normalized', 'fontsize', .5, 'Tag','4');
    D.AxisChoice5AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL, ...
             'units', 'normalized','Position',[.01  Base+3*Gap .99 .15],...
             'BackgroundColor' , CLine5,'fontunits', 'normalized', 'fontsize', .5, 'Tag','5');    
    D.AxisChoice6AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL, ...
             'units', 'normalized','Position',[.01  Base+2*Gap .99 .15],...
             'BackgroundColor' , CLine6,'fontunits', 'normalized', 'fontsize', .5, 'Tag','6');    
    D.AxisChoice7AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL, ...
             'units', 'normalized','Position',[.01  Base+1*Gap .99 .15],...
             'BackgroundColor' , CLine7,'fontunits', 'normalized', 'fontsize', .5, 'Tag','7');    
    D.AxisChoice8AxL = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxL, ...
             'units', 'normalized','Position',[.01  Base+0*Gap .99 .15],...
             'BackgroundColor' , CLine8,'fontunits', 'normalized', 'fontsize', .5, 'Tag','8');    
         
D.BtnGrpAxR = uibuttongroup('Position',[Horbase+Col1W+Col2W Vertbase+shiftup+shiftupButtonGr Col3W Verthight],...
                            'Title',{'R'},'TitlePosition','centertop','Bordertype','None',...
                            'Units','Normalized','Parent',Dfig);
set(D.BtnGrpAxR,'SelectionChangeFcn',@selchbk_callAxR);
%set(D.BtnGrpAxR,'SelectedObject',[]);  % No selection
    %For the SelectionChangeFcn callback, selcbk, the source and event data structure arguments 
    %are available only if selcbk is called using a function handle. See SelectionChangeFcn for more information.
    
            
    D.AxisChoice1AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR, ...
             'units', 'normalized','Position',[.01  Base+7*Gap  .99  .15],...
             'BackgroundColor' , CLine1,'fontunits', 'normalized', 'fontsize', .5, 'Tag','1');
    D.AxisChoice2AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR, ...
             'units', 'normalized','Position',[.01  Base+6*Gap  .99  .15],...
             'BackgroundColor' , CLine2,'fontunits', 'normalized', 'fontsize', .5, 'Tag','2');
    D.AxisChoice3AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR,...
             'units', 'normalized','Position',[.01  Base+5*Gap  .99 .15],...
             'BackgroundColor' , CLine3,'fontunits', 'normalized', 'fontsize', .5, 'Tag','3');
    D.AxisChoice4AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR, ...
             'units', 'normalized','Position',[.01  Base+4*Gap .99 .15],...
             'BackgroundColor' , CLine4,'fontunits', 'normalized', 'fontsize', .5, 'Tag','4');
    D.AxisChoice5AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR, ...
             'units', 'normalized','Position',[.01  Base+3*Gap .99 .15],...
             'BackgroundColor' , CLine5,'fontunits', 'normalized', 'fontsize', .5, 'Tag','5');  
    D.AxisChoice6AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR, ...
             'units', 'normalized','Position',[.01  Base+2*Gap .99 .15],...
             'BackgroundColor' , CLine6,'fontunits', 'normalized', 'fontsize', .5, 'Tag','6');    
    D.AxisChoice7AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR, ...
             'units', 'normalized','Position',[.01  Base+1*Gap .99 .15],...
             'BackgroundColor' , CLine7,'fontunits', 'normalized', 'fontsize', .5, 'Tag','7');    
    D.AxisChoice8AxR = uicontrol('Style','Radio', 'Parent',D.BtnGrpAxR, ...
             'units', 'normalized','Position',[.01  Base+0*Gap .99 .15],...
             'BackgroundColor' , CLine8,'fontunits', 'normalized', 'fontsize', .5, 'Tag','8');    
         

%For the SelectionChangeFcn callback, selcbk, the source and event data structure 
%arguments are available only if selcbk is called using a function handle. 
%See SelectionChangeFcn for more information.

% %vertical
Entr=4; Base=.08; Gap=(1- Base)/Entr;
Vstep=.08;
% %horizontal
Horbase=0.84; GapW=[]; Col1W=0.13;Col2W=0.025;Col3W=0.025;
%Vertbase=0.2; Verthight=0.2;
VertTop=.25;
Vertbase=VertTop-(Entr)*.8*Base; Verthight=Entr*(.8*Base);

D.MinMaxBtnGrp = uibuttongroup('Position',[Horbase Vertbase+shiftupButtonGr Col1W Verthight],...
                         'Title',{'Noted Local Extrema'},'TitlePosition','centertop','Bordertype','None',...
                         'Units','Normalized','Parent',Dfig);
set(D.MinMaxBtnGrp,'SelectionChangeFcn',@MinMaxselchbk_call);
%set(D.BtnGrp,'SelectedObject',[]);  % No selection
    %For the SelectionChangeFcn callback, selcbk, the source and event data structure arguments 
    %are available only if selcbk is called using a function handle. See SelectionChangeFcn for more information.
            
    MinVar3 = uicontrol('Style','checkbox', 'Parent',D.MinMaxBtnGrp, ...
             'units', 'normalized','Position',[.01  .8-0*Gap*.8  .99  .15],'BackgroundColor' , CLine3,...
             'fontunits', 'normalized', 'fontsize', .5,'String','MIN XMISS', ...
             'callback',{@MinMaxVar_check_call});
    MaxVar3 = uicontrol('Style','checkbox', 'Parent',D.MinMaxBtnGrp, ...
             'units', 'normalized','Position',[.01  .8-1*Gap*.8  .99  .15],'BackgroundColor' , CLine3,...
             'fontunits', 'normalized', 'fontsize', .5,'String','MAX XMISS', ...
             'callback',{@MinMaxVar_check_call});
    MinVar4 = uicontrol('Style','checkbox', 'Parent',D.MinMaxBtnGrp,...
             'units', 'normalized','Position',[.01  .8-2*Gap*.8  .99 .15],'BackgroundColor' , CLine6,...
             'fontunits', 'normalized', 'fontsize', .5,'String','MIN Ke', ...
             'callback',{@MinMaxVar_check_call});
    MaxVar4 = uicontrol('Style','checkbox', 'Parent',D.MinMaxBtnGrp, ...
             'units', 'normalized','Position',[.01  .8-3*Gap*.8 .99 .15],'BackgroundColor' , CLine6,...
             'fontunits', 'normalized', 'fontsize', .5,'String','MAX Ke', ...
             'callback',{@MinMaxVar_check_call});
    

    
%%Callback functions    
    function [] = SingleFileLoad_callback(source,eventdata)
    % Right
    %axiR
    %str2num(get(eventdata.NewValue,'Tag'));
    %str2num(get(eventdata.OldValue,'Tag'));
    %str2num(get(eventdata.NewValue,'Tag'))==8;
    %['set(ax' get(eventdata.NewValue,'Tag') 'R,''Visible'',''on'');']
    %['set(ax' get(eventdata.OldValue,'Tag') 'R,''Visible'',''off'');']
%      eval(['set(ax' get(eventdata.OldValue,'Tag') 'R,''Visible'',''off'');']); 
%      eval(['set(ax' get(eventdata.NewValue,'Tag') 'R,''Visible'',''on'');']);
%        RAxisRadioSeq=ZeroRadioSeq;
%        RAxisRadioSeq(str2num(get(eventdata.NewValue,'Tag')))=1; %Right Axis Button Settings
    %'boo'
    %sliderLineDots2=line(DataTime(2),0*DataTime(2), 'Color','b' , 'LineStyle', '.', 'Parent',...
    %      sliderAx, 'Visible','on');
      
    %set(sliderLineDots2,'Color','r')
    SingleFileCall=1;
    VCTDfile_pb_call
    end
          
    function [] = VOnOff_pb_call(varargin) %snap button
    %global CTDfilename, CTDpathname
    % Callback for the pushbutton.
        if strcmp(get(D.VOnOff,'string'),'Off'),
            set(D.VOnOff,'string','On','BackgroundColor' , 'g')
        else
            set(D.VOnOff,'string','Off','BackgroundColor' , 'r')
            %blurFigure(Dfig, 'on');
            %pause(3);
            %blurFigure(Dfig, 'off');
        end
            set(D.VOnOff,'string','Snap','BackgroundColor' , 'y','CData',IMG4)
        %f = getframe (S.fh, [150 75 S.SCR(3)+1 S.SCR(4)+1]) ;
        %C = frame2im(f);
        %close(S.fh)
        %IMAGESAVE
        %set(figureHandle, 'Visible', 'on');
        frame = getframe(Dfig);
        %set(figureHandle,'Visible','off');
        imagen = frame.cdata;
        pause(0.02)
       
%%screenshot        
%         imagenS(:,:,1) =imagen(round(size(imagen,1)*9/64:end),1:round(size(imagen,2)*15/18), 1);
%         imagenS(:,:,2) =imagen(round(size(imagen,1)*9/64:end),1:round(size(imagen,2)*15/18), 2);
%         imagenS(:,:,3) =imagen(round(size(imagen,1)*9/64:end),1:round(size(imagen,2)*15/18), 3);
%partial screenshot only
        imagenS(:,:,1) = imagen(round(size(imagen,1)*9/64:end),:, 1);
        imagenS(:,:,2) = imagen(round(size(imagen,1)*9/64:end),:, 2);
        imagenS(:,:,3) = imagen(round(size(imagen,1)*9/64:end),:, 3);

        imagenC=imrotate90s(imagenS,-90);

        pause(0.02)
       
        %setupsave
        %imName=get(D.VCTDfile,'string'); %write as CTDfilename.png
        Dotloc=strfind(CTDfilename,'.');
        imName=CTDfilename(1:Dotloc-1);
        
     %Landscape       
        imNamepng=[ imName 'Landscape.png'];
        imNamejpg=[ imName 'Landscape.jpg'];
        imNamegif=[ imName 'Landscape.gif'];
       % Save the image to disk
       [SaveimFilename, SaveimPathname, filterindex] = uiputfile( ...
       {...
        '*.png',  'PNG-files  (*.png)'; ...
        '*.jpg',  'JPEG-files (*.jpg)'; ...
        '*.gif',  'GIF files  (*.gif)'; ...
        '*.*',    'All Files  (*.*)'...
        }, ...
        'Save window shot as',imNamepng...
        );
    
        %filterindex;
        %SaveimFilename;
        %SaveimPathname;
        if filterindex>0
           imwrite(imagen,  [SaveimPathname  SaveimFilename ]);
           %imwrite(imagenS,  [SaveimPathname  SaveimFilename ]);
           %imwrite(imagenC,  [CTDpathname  SaveimFilename]);
        end
     %Landscape  saved       
     %Portrait       
        imNamepng=[ imName 'Portrait.png'];
        imNamejpg=[ imName 'Portrait.jpg'];
        imNamegif=[ imName 'Portrait.gif'];
       % Save the image to disk
       [SaveimFilename, SaveimPathname, filterindex] = uiputfile( ...
       {...
        '*.png',  'PNG-files  (*.png)'; ...
        '*.jpg',  'JPEG-files (*.jpg)'; ...
        '*.gif',  'GIF files  (*.gif)'; ...
        '*.*',    'All Files  (*.*)'...
        }, ...
        'Save window shot as',imNamepng...
        );
    
        %filterindex;
        %SaveimFilename;
        %SaveimPathname;
        if filterindex>0
           %imwrite(imagenS,  [CTDpathname  SaveimFilename ]);
           imwrite(imagenC,  [SaveimPathname  SaveimFilename]);
           %imwrite(imagen,  [SaveimPathname  SaveimFilename]);
        end
     %Portrait  saved     
        
        
        
        
        clear imagen imagenS imagenC

        
       %saveImage(Dfig,[CTDfilename(1:end-4) '.png'])
    end

    function [] = FList_pb_call(varargin)
    % Callback for the FList pushbutton.
        if Viewswitch
            %Viewswitch=0;
            %ListReturnPre
        else
            ListReturnPre=light_uipickfiles8_7;
        end
          
        if  isa(ListReturnPre,'double'),
         'double';
         %'muu'
        elseif length(ListReturnPre)==0 
            %length(ListReturnPre)==0 & 0==ListReturnPre(1)
            %isequaln(ListReturnPre,{0})
         %'boo'   
        %elseif length(ListReturn)==1
        elseif length(ListReturnPre)>=1 ,
           ListReturn=ListReturnPre;
           FlistLen=length(ListReturn);
           %SingleFileCall=0;
           DataTime=datenum(0*[1:length(ListReturn)]);
            for jj=1:length(ListReturn)
                LRChar= char(ListReturn(jj));
                %UU=dir(char(ListReturn(jj)));
                bslashes=findstr( LRChar,'\');
                LRChar(bslashes(end)+1:end);
                yy=str2num(LRChar(bslashes(end)+[1 2]));  mm=str2num(LRChar(bslashes(end)+[3 4]));   dd=str2num(LRChar(bslashes(end)+[5 6])); 
                yy(find(yy>=50))=yy(find(yy>=50))-100;
                yy=yy+2000;
                %[yy mm dd];
                DataTime(jj)=datenum([yy mm dd]);
            end
                %DataTime
                %TIMEslider
                %'most1'
                %L = get(sliderAx,'XLim');
            [SDataTime SDataTimeInd]=sort( DataTime );%siz=size(DT),DT
            [NoUseInd ListReturnInd]=sort( SDataTimeInd );%siz=size(DT),DT
                %Use:  ListReturn(ListReturnInd( BlueKSTEP)) (timeind seqind conv)   BlueK=ListReturnInd( BlueKSTEP )
                %Use:  ListReturn(SDataTimeInd( BlueK))      (seqind timeind conv)   BlueKSTEP=SDataTimeInd( BlueK)
                % % % % uu is DataTime                 
                % % % %UU is SDataTime                
                % % % uu=[4.1  1.1  5.1  7.1  9.1  11.1  3.1  45.1  17.1]
                % % % % uu =     4     1     5     7     9    11     3    45    17
                % % % [UU,UUind]=sort(uu);
                % % % UUind
                % % % [IND, uuind]=sort(UUind);
                % % % uuind
                % % % 
                % % % uu(UUind) %( UU=uu(UUind) )
                % % % % ans =     1     3     4     5     7     9    11    17    45
                % % % 
                % % % UU(uuind)  %( uu=UU(uuind) )
                % % % % ans =     4     1     5     7     9    11     3    45    17
                % % % 
                % % % uu3=uuind(3), UU3=UUind(3)  
                % % % 

            if TIMEslider
                NumTicks = 6;
                if Viewswitch
                    if strcmp(ViewBefore,'STEP')
                        BlueK=ListReturnInd( BlueKSTEP );
                        %currentSliderValue=1; SDataTime(BlueK)
                    %elseif strcmp(ViewBefore'TIME')
                    end
                    Viewswitch=0;
                    %ListReturnPre
                else
                    BlueK=1; currentSliderValue=1;
                end
                if exist('sliderLineDots'),  delete(sliderLineDots); clear('sliderLineDots'); end
                if exist('sliderLineDots2'),  delete(sliderLineDots2); clear('sliderLineDots2'); end
                if exist('sliderLine'),  delete(sliderLine); clear('sliderLine'); end
                sliderLine=line(DataTime,0*DataTime,'Color',[0.991569 0.993725 0.9912] , 'LineStyle', '-', 'Parent',...
                          sliderAx, 'Visible','off');

                      
                      
                      
                sliderMin=SDataTime(1);
                set(Sl, 'Min',SDataTime(1)-1) % sliderMin should be 1
                sliderMax=SDataTime(end); 
                set(Sl, 'Max',SDataTime(end)+1) % sliderMax should be length(ListReturn)
                sliderVal=SDataTime(BlueK);
                set(Sl, 'Value',sliderVal)
                %sliderStep= 
                %'most2'
                if length(SDataTime)==1,
                    SmallSTEP=0.01; 
                    LargeSTEP=10*SmallSTEP;
                elseif length(SDataTime)>1,
                    if min(SDataTime(2:end)-SDataTime(1:end-1))>0
                        SmallSTEP=min(SDataTime(2:end)-SDataTime(1:end-1))/(SDataTime(end)-SDataTime(1)+3);
                        LargeSTEP=10*SmallSTEP;
                    elseif max(SDataTime(2:end)-SDataTime(1:end-1))>0
                        SDataTimeDiff=SDataTime(2:end)-SDataTime(1:end-1);
                        minposSDataTime=min(SDataTimeDiff(find(SDataTimeDiff)));
                        SmallSTEP=minposSDataTime/(SDataTime(end)-SDataTime(1)+3);
                        LargeSTEP=10*SmallSTEP;
                    end
                end
                
                set(Sl, 'SliderStep',[SmallSTEP   LargeSTEP/100 ]) % correct
%                 Sl = uicontrol('BackgroundColor' , [0.991569 0.993725 0.9912],'Parent', Dfig,'Style','slider', 'Value',5, 'Min',1,'units', 'normalized',...
%                 'Max',100, 'SliderStep',[1 10]./100, ...
%                 'Position',[0.06 0.87+shiftup 0.75 0.012], 'Callback',@slider_callback); 

                %SDataTime
                if SDataTime(1)<SDataTime(end)
                   set(sliderAx,'XTick',linspace(SDataTime(1),SDataTime(end),NumTicks))
                   %set(sliderAx,'XTick',[DT(1)-1 unique(DT))
                else
                   %set(sliderAx,'XTick',unique(DT))
                   %set(sliderAx,'XTick',linspace(DT(1),DT(end),NumTicks))
                   set(sliderAx,'XTick',linspace(SDataTime(1)-datenum(0,0,2),SDataTime(end)+datenum(0,0,2),5) ) 
                end
                unique(SDataTime);
                datetick(sliderAx,'x','mmmyy','keepticks')
                %xticklabel_rotate([],45,[],'Fontsize',10)
      
                sliderLineDots=line(DataTime,0*DataTime, 'Color','r' , 'LineStyle', 'none', 'Marker', '.','Markersize',5,'Parent',...
                          sliderAx, 'Visible','on');
                sliderLineDots2=line(SDataTime(BlueK),0*SDataTime(BlueK), 'Color','b' ,'LineStyle', 'none', 'Marker', '.','Markersize',10,'Parent',...
                         sliderAx, 'Visible','on');
               %'most3'
                VCTDfile_pb_call;
            elseif STEPslider
                if Viewswitch
                    if strcmp(ViewBefore,'TIME')
                        BlueKSTEP=SDataTimeInd( BlueK);
                        currentSliderValue=1;
                    end
                    Viewswitch=0;
                    %ListReturnPre
                else
                    BlueKSTEP=1; currentSliderValue=1;
                end
                if exist('sliderLineDots'),  delete(sliderLineDots); clear('sliderLineDots'); end
                if exist('sliderLineDots2'),  delete(sliderLineDots2); clear('sliderLineDots2'); end
                if exist('sliderLine'),  delete(sliderLine); clear('sliderLine'); end
                sliderLine=line(1:length(ListReturn),0*(1:length(ListReturn)),'Color',[0.991569 0.993725 0.9912] , 'LineStyle', '-', 'Parent',...
                          sliderAx, 'Visible','off');
                      
                NumTicks = length(ListReturn);
                %1:ceil(NumTicks/50):NumTicks
                %L = get(sliderAx,'XLim');
                %DT=sort( DataTime );
                
                sliderMinSTEP=1;
                set(Sl, 'Min',1 ) % sliderMin should be 1
                sliderMaxSTEP=length(ListReturn);
                set(Sl, 'Max', length(ListReturn)) % sliderMax should be length(ListReturn)
                sliderValSTEP= BlueKSTEP;
                set(Sl, 'Value',sliderValSTEP)
                sliderStepSTEP=[1/length(ListReturn),3*1/length(ListReturn)/30];
                set(Sl, 'SliderStep',sliderStepSTEP) % correct
%                 Sl = uicontrol('BackgroundColor' , [0.991569 0.993725 0.9912],'Parent', Dfig,'Style','slider', 'Value',5, 'Min',1,'units', 'normalized',...
%                 'Max',100, 'SliderStep',[1 10]./100, ...
%                 'Position',[0.06 0.87+shiftup 0.75 0.012], 'Callback',@slider_callback); 

                %
                %set(sliderAx, 'Visible','off')
                %get(sliderAx)
                %'booboo'
                if 1<length(ListReturn)
                   set(sliderAx,'XTickLabelMode','auto')
                   %set(sliderAx,'XTick',linspace(1,length(ListReturn),NumTicks),'Xlim',[1 length(ListReturn)])
                   set(sliderAx,'XTick',1:ceil(NumTicks/40):NumTicks,'Xlim',[1 length(ListReturn)])
                   
                   %'beeboo'
                   %get(sliderAx)
                else
                   set(sliderAx,'XTick',linspace(1-1,length(ListReturn)+1,length(ListReturn)+2),'Xlim',[0 length(ListReturn)+1] ) 
                   %'boobee'
                end
%                datetick(sliderAx,'x','mmmyy','keepticks'); 
                sliderLineDots=line(1:length(ListReturn),0*(1:length(ListReturn)), 'Color','r' , 'LineStyle', 'none', 'Marker', '.','Markersize',5,...
                               'Parent', sliderAx, 'Visible','on');
                sliderLineDots2=line(BlueKSTEP,0*BlueKSTEP, 'Color','b' ,'LineStyle', 'none', 'Marker', '.', 'Markersize',12,...
                               'Parent', sliderAx, 'Visible','on');
                VCTDfile_pb_call;
                
            end
        else
           %'no file in the FileList selection' 
        end
    end

    function [] = VCTDfile_pb_call(varargin)
    %  global CTDfilename, CTDpathname
    %  global ax1L ax2L  ax3L  ax4L ax5L ax6L ax1R ax2R  ax3R  ax4R ax5R ax6R
    %  global h1_L h2_L  h3_L  h4_L h5_L h6_L h1_R h2_R  h3_R  h4_R h5_R h6_R

     %TIMEslider
     %STEPslider
     
    % Callback for the text.
        
        %UIPICKFILES instead!!
     if SingleFileCall
         
        readit=0; %indicator to see of fileselection was read successfully
        currDir=pwd;
        cd(CTDloadDirname); %change to last load directory
        
        %change to last load extension
        CTDExt=CTDloadExt;
        switch lower(CTDExt)
          case 'xls', 
                FirstLoadType=['*.' lower(CTDExt)]; FirstLoadPrompt='CDT file Excel (*.xls)';
          case 'csv',
                FirstLoadType=['*.' lower(CTDExt)]; FirstLoadPrompt='Comma separated CTD data (*.csv)';
          otherwise   
                FirstLoadType='*.*'; FirstLoadPrompt='All files (*.*)';
        end
        
        [CTDfilename, CTDpathname] = uigetfile( ...
          {FirstLoadType,FirstLoadPrompt;...
          '*.xls','CDT file Excel (*.xls)';...
           '*.csv','Comma separated CTD data (*.csv)';...
           '*.*',  'All files (*.*)'}, ...
           'Pick a file'...
                 );       
        if isequal(CTDfilename,0) || isequal(CTDpathname,0)
           ;
        else
            DotsInFName=findstr(CTDfilename,'.');
            CTDloadDirname=CTDpathname; 
            CTDExt=upper(CTDfilename(DotsInFName(end)+1:end));
            CTDloadExt=CTDExt;   
            CTDActExt=CTDExt;
        end
        SingleFileCall=0;
        %CTDfilename, 
        %CTDpathname,
        cd(currDir);
     elseif length(ListReturn)
                [SLR SLRInd]=sort(ListReturn);
                if TIMEslider
                  LRChar= char(ListReturn(SDataTimeInd(BlueK)));
                elseif STEPslider
                  LRChar= char(ListReturn(BlueKSTEP));
                end
                %UU=dir(char(ListReturn(jj)));
                bslashesLR=findstr( LRChar,'\');
                alldotsLR=findstr( LRChar,'.');
                LRChar(bslashesLR(end)+1:end);
                yy=str2num(LRChar(bslashesLR(end)+[1 2]));
                mm=str2num(LRChar(bslashesLR(end)+[3 4]));
                dd=str2num(LRChar(bslashesLR(end)+[5 6])); 
                yy(find(yy>=50))=yy(find(yy>=50))-100;
                yy=yy+2000;
                CTDfilename=LRChar(bslashesLR(end)+1:end);
                CTDpathname=LRChar(1:bslashesLR(end));
                CTDActExt=LRChar(alldotsLR(end)+1:end);
                

     end
    
    
        
        if ~isequal(CTDfilename,0) & ~isequal(CTDpathname,0) %length(CTDfilename)>1%strcmp(get(D.VOnOff,'string'),'Off'),

            readit=0;
           %Read in file....
%instead of this (old call)
% % % % % %             if strcmp(upper(CTDActExt),'CSV')
% % % % % %                 ReadCTDData=csvread([CTDpathname CTDfilename]); readit=1;
% % % % % %             elseif strcmp(upper(CTDActExt),'XLS')
% % % % % %                [xlSTATUS,xlSHEETS,xlFORMAT] = xlsfinfo([CTDpathname CTDfilename]);
% % % % % %                sheetValid = any(strcmp(xlSHEETS, 'CTDQTRM'));
% % % % % %                if  isempty(xlFORMAT), errordlg('Excel for Windows is not available on this system','Could not read file');
% % % % % %                elseif ~sheetValid,  errordlg('Selected file does not have a CTDQTRM Tab','File Error - Read unsuccessful');
% % % % % %                elseif sheetValid,
% % % % % %                    ReadCTDData=LightCodeImport_xls(CTDpathname,CTDfilename); readit=1;
% % % % % %                end
% % % % % %             end
%instead of this
%build in this
% Some Sample Headers:
%  (QTRM tab)SampHeaderXLS=         {                 'DEPTHm' 'T090C' 'FLUOR' 'XMISS' 'C0uS/cm' 'SPCOND'  'PAR' 'DO2mg' 'pH' 'ORP' 'lnPAR'};
%            SampHeaderCNVwTime=    {           'prdM' 'depFM' 't090C' 'v4'  'CStarTr0' 'c0uS/cm' 'specc' 'par' 'oxsolMg/L' 'ph' 'orp' 'wl0' 'wl1' 'wl2' 'timeS'  'flag'};
%            SampHeaderASC_QTRZ=    {'POST1990'        'DepFM' 'T090C'         'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsatMg/L' 'Ph' 'Orp' 'Nbin' 'Flag'}; 
%            SampHeaderASC_NEW_2013={                  'DepFM' 'T090C' 'V4'  'CStarTr0' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'Wl0' 'Wl1' 'Wl2' 'Flag'};                
%       SampHeaderASC_NEW_2013Times={ 'POST1900'       'DepFM' 'T090C' 'V4'  'CStarTr0' 'C0uS/cm'  Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'TimeS' 'Wl0' 'Wl1'  'Wl2' 'Nbin' 'Flag'}
%            SampHeaderASC_QTRDBN2= {'POST1990' 'PrSM' 'DepFM' 'T090C'         'Xmiss' 'Specc' 'E' 'E10^-8' 'N^2' 'N' 'Flag'};                

%            ... POST1990 prdM PrSM  and Flag are being  removed from  headers in readCTDFile_allFormats

           SampHeaderXLS=           {'DEPTHm' 'T090C' 'FLUOR' 'XMISS' 'C0uS/cm' 'SPCOND'    'PAR'        'DO2mg'   'pH'        'ORP'    'lnPAR'};
           XLSUnits  =              [  {'m'}  {'deg C'} {'FlS'} {'%'} {'uS/cm'} {'uS/cm'} {'mE/m^2/sec'} {'mg/l'} {'unitless'} {'mV'} {'log(mE/m^2/sec)'} ];
           
           SampHeaderASC_QTRDBN2_1= {'DepFM' 'T090C'       'Xmiss' 'Specc' 'E' 'E10^-8' 'N^2' 'N'};                
           SampHeaderASC_QTRDBN2_2= {'DepFM' 'T090C' 'V4'  'Xmiss' 'Specc' 'E' 'E10^-8' 'N^2' 'N'};
           SampHeaderASC_QTRDBN2_3= {'DepFM' 'T090C' 'FlS' 'Xmiss' 'Specc' 'E' 'E10^-8' 'N^2' 'N'};
           DBN2Units              = [{'m'} {'deg C'} {'FlS'} {'%'} {'uS/cm'} {'what1'} {'what2'} {'what3'} {'what4'} ];
           
           SampHeaderASC_QTRZ_1=    {'DepFM' 'T090C'       'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsatMg/L' 'Ph' 'Orp' 'Nbin'}; 
           SampHeaderASC_QTRZ_2=    {'DepFM' 'T090C' 'V4'  'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsatMg/L' 'Ph' 'Orp' 'Nbin'};
           SampHeaderASC_QTRZ_3=    {'DepFM' 'T090C' 'FLS' 'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsatMg/L' 'Ph' 'Orp' 'Nbin'}; 
           SampHeaderASC_QTRZ_4=    {'DepFM' 'T090C'       'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'Nbin'}; 
           SampHeaderASC_QTRZ_5=    {'DepFM' 'T090C' 'V4'  'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'Nbin'};
           SampHeaderASC_QTRZ_6=    {'DepFM' 'T090C' 'FLS' 'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'Nbin'}; 
           QTRZUnits           =   [ {'m'} {'deg C'} {'FlS'} {'%'} {'uS/cm'} {'uS/cm'} {'mE/m^2/sec'} {'mg/l'} {'unitless'} {'mV'} {'unitless'} ];

           SampHeaderCNVwTime=    {'depFM' 't090C' 'v4' 'CStarTr0' 'c0uS/cm' 'specc' 'par' 'oxsolMg/L' 'ph' 'orp' 'wl0' 'wl1' 'wl2' 'timeS'};
           CNVwTimeUnits     =    [ {'m'} {'deg C'} {'FlS'} {'%'} {'uS/cm'} {'uS/cm'} {'mE/m^2/sec'} {'mg/l'} {'unitless'} {'mV'} {'wh1'} {'wh2'} {'wh3'} {'sec'} ];
           
           
           SampHeaderASC_NEW_2013={'DepFM' 'T090C' 'V4' 'CStarTr0' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'Wl0' 'Wl1' 'Wl2' 'Nbin'};                
           ASC_NEW_2013Units     =   [ {'m'} {'deg C'} {'FlS'} {'%'} {'uS/cm'} {'uS/cm'} {'mE/m^2/sec'} {'mg/l'} {'unitless'} {'mV'} {'wh1'} {'wh2'} {'wh3'} {'unitless'}];

           SampHeaderASC_NEW_2013Times=     {'DepFM' 'T090C' 'V4' 'CStarTr0' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'TimeS' 'Wl0' 'Wl1'  'Wl2' 'Nbin'};
           ASC_NEW_2013TimesUnits=   [ {'m'} {'deg C'} {'FlS'} {'%'} {'uS/cm'} {'uS/cm'} {'mE/m^2/sec'} {'mg/l'} {'unitless'} {'mV'} {'sec'} {'wh1'} {'wh2'} {'wh3'} {'unitless'} ];

           fileFormat = CTDfilename(end-3:end);
           if strcmpi(fileFormat, '.XLS') == 1 % if reading from XLS file, need to specify tab to read from
                CTDFullPath=[CTDpathname CTDfilename];
                %Have to deal with other xls sheets and file formats
                [CTDHeaders ReadaCTDData FluorHeaderEntry XmissHeaderEntry PARHeaderEntry] = readCTDFile_allFormats2_1(CTDFullPath, 'CTDQTRM');
                    %FluorHeaderEntry, XmissHeaderEntry
                    % Sample Header xls qtrm:
                    %'DEPTHm'    'T090C'    'FLUOR'   'XMISS'    'C0uS/cm'    'SPCOND'     'PAR'    'DO2mg'    'pH'    'ORP'    'lnPAR'
                    % do not use lnPAR directly as  not fully reliable,
                    % compute from PAR instad
                    %SampHeaderXLS={'DEPTHm'    'T090C'    'FLUOR'    'XMISS'    'C0uS/cm'    'SPCOND'     'PAR'    'DO2mg'    'pH'    'ORP'    'lnPAR'};
                    % # name 0 = depFM: Depth [fresh water, m]
                    % # name 1 = t090C: Temperature [ITS-90, deg C]
                    % # name 2 = v4: Voltage 4
                    % # name 3 = xmiss: Beam Transmission, Chelsea/Seatech/Wetlab CStar [%]
                    % # name 4 = c0uS/cm: Conductivity [uS/cm]
                    % # name 5 = specc: Specific Conductance [uS/cm]
                    % # name 6 = par: PAR/Irradiance, Biospherical/Licor
                    % # name 7 = oxsatMg/L: Oxygen Saturation [mg/l]
                    % # name 8 = ph: pH
                    % # name 9 = orp: Oxidation Reduction Potential [mV]
                    % # name 10 = nbin: number of scans per bin
                    % # name 11 = flag: flag
                %If expected xls header is met
                if isequal(lower(CTDHeaders),lower(SampHeaderXLS)),
                  CTDDataHeader=CTDHeaders;
                  CTDDataHeaderUnit=XLSUnits;
                  ReadCTDData=ReadaCTDData;
                  %FileInfo = {CTDFullPath identifyCTDStation(CTDFullPath) identifyCTDDate(CTDFullPath)} ;
                  %compose ReadCTDData 
                  %{'DEPTHm' 'T090C' 'FLUOR' 'XMISS' 'C0uS/cm' 'SPCOND'  'PAR' 'DO2mg' 'pH' 'ORP' 'lnPAR'} Ke
                       %old call%KeCmp2=LightCodeImport_xls(CTDpathname,CTDfilename);
                       %old call%ReadCTDData(:,end+1)=KeCmp2(:,end);
                  [LNPAR KeV]=KeComp2(ReadCTDData(:,[1 7]));  
                  ReadCTDData(:,end+1)=KeV;%KeComp2(ReadCTDData(:,[1 7]));
                  ReadCTDData(end-2:end,end)=NaN;
                  CTDDataHeader{end+1}='Ke';
                  CTDDataHeaderUnit{end+1}='unitless';
                  %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke'  ('ORP'   'C0uS/cm')
                  CTDData=ReadCTDData(:,[1 11 3 4 2 8 12 10 5]);
                  CTDData(:,[7 ])=-CTDData(:,[7 ]); %DO2 is not in here
                  CTDDataHeader=CTDDataHeader([1 11 3 4 2 8 12 10 5]);
                  CTDDataHeaderUnit=CTDDataHeaderUnit([1 11 3 4 2 8 12 10 5]);
                  ThereIsKe=1;
                  readit=1;
                  set(VarChoice1,'String',CTDDataHeader{2})
                  set(VarChoice2,'String',CTDDataHeader{3})
                  set(VarChoice3,'String',CTDDataHeader{4})
                  set(VarChoice4,'String',CTDDataHeader{5})
                  set(VarChoice5,'String',CTDDataHeader{6})
                  set(VarChoice6,'String',CTDDataHeader{7})
                  set(VarChoice7,'String',CTDDataHeader{8})
                  set(VarChoice8,'String',CTDDataHeader{9})
                  CTDData(find(CTDData<0 & -eps<CTDData))=NaN;
                else 
                  errordlg('Selected file does not have a CTDQTRM Tab','File Error - Read unsuccessful, File header is not conforming ');
                end
                

           elseif strcmpi(fileFormat, '.ASC') == 1 | strcmpi(fileFormat, '.CNV') == 1
                 
                 CTDFullPath=[CTDpathname CTDfilename];
                 [CTDHeaders ReadaCTDData FluorHeaderEntry XmissHeaderEntry PARHeaderEntry] = readCTDFile_allFormats2_1([CTDpathname CTDfilename]);
                 %FluorHeaderEntry, XmissHeaderEntry      
% CTDHeaders
% SampHeaderASC_QTRZ_2
% lower(CTDHeaders),
% lower(SampHeaderASC_QTRZ_2)
% class(CTDHeaders)
% class(SampHeaderASC_QTRZ_2)
% isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRZ_2))

                 if  isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRDBN2_1)) | ...
                         isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRDBN2_2)) | ...
                         isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRDBN2_3))
                    % Sample Header ASC QTRDBN2:
                    % already removed            % # name 0 = PrSM: Pressure, Strain Gauge [db]
                    %SampHeaderASC_QTRDBN2={'DepFM' 'T090C' ''v4'' 'Xmiss' 'Specc' 'E' 'E10^-8' 'N^2' 'N'}                 
                    %If expected xls header is met
                  CTDDataHeader=CTDHeaders;
                  CTDDataHeaderUnit=XLSUnits;
                    ReadCTDData=ReadaCTDData;
                    %FileInfo = {CTDFullPath identifyCTDStation(CTDFullPath) identifyCTDDate(CTDFullPath)}; 
                       %not   %ReadCTDData(:,end+1)=log(ReadCTDData(:,7)); 
                       %not   %ReadCTDData(:,end+1)=KeComp(ReadCTDData(:,[1 7]));
                    %{'DepFM' 'T090C' 'Xmiss' 'Specc' 'E' 'E10^-8' 'N^2' 'N'}
                    CTDData=ReadCTDData(:,[1 2 3 4 5 7 8 8 8]); 
                  CTDDataHeader=CTDDataHeader([1 2 3 4 5 7 8 8 8]);
                  CTDDataHeaderUnit=CTDDataHeaderUnit([1 2 3 4 5 7 8 8 8]);
                    %compose ReadCTDData   
                    ThereIsKe=0;
                    readit=1;
                    set(VarChoice1,'String',CTDDataHeader{2})
                    set(VarChoice2,'String',CTDDataHeader{3})
                    set(VarChoice3,'String',CTDDataHeader{4})
                    set(VarChoice4,'String',CTDDataHeader{5})
                    set(VarChoice5,'String',CTDDataHeader{6})
                    set(VarChoice6,'String',CTDDataHeader{7})
                    set(VarChoice7,'String',CTDDataHeader{8})
                    set(VarChoice8,'String',CTDDataHeader{9})
                    CTDData(find(CTDData<0 & -eps<CTDData))=NaN;
                 elseif isequal(lower(CTDHeaders),lower(SampHeaderCNVwTime))
                    %lnPAR Ke ADD
                    % Sample Header CNV WITH ELAPSED TIME
                    %SampHeaderCNVwTime={'depFM' 't090C' 'v4' 'CStarTr0' 'c0uS/cm' 'specc' 'par' 'oxsolMg/L' 'ph' 'orp' 'wl0' 'wl1' 'wl2' 'timeS'};
                    % already removed            % # name 0 = prdM: Pressure, Strain Gauge [db]
                    % # name 1 = depFM: Depth [fresh water, m]
                    % # name 2 = t090C: Temperature [ITS-90, deg C]
                    % # name 3 = v4: Voltage 4 %or FLS %or FlECO-AFL for fluorescence
                    % # name 4 = CStarTr0: Beam Transmission, WET Labs C-Star [%]
                    % # name 5 = c0uS/cm: Conductivity [uS/cm]
                    % # name 6 = specc: Specific Conductance [uS/cm]
                    % # name 7 = par: PAR/Irradiance, Biospherical/Licor
                    % # name 8 = oxsolMg/L: Oxygen Saturation, Garcia & Gordon [mg/l]
                    % # name 9 = ph: pH
                    % # name 10 = orp: Oxidation Reduction Potential [mV]
                    % # name 11 = wl0: RS-232 WET Labs raw counts 0
                    % # name 12 = wl1: RS-232 WET Labs raw counts 1
                    % # name 13 = wl2: RS-232 WET Labs raw counts 2
                    % # name 14 = timeS: Time, Elapsed [seconds]
                    % already removed            % # name 15 = flag:  0.000e+00
                    %{'depFM' 't090C' 'v4' 'CStarTr0' 'c0uS/cm' 'specc' 'par' 'oxsolMg/L' 'ph' 'orp' 'wl0' 'wl1' 'wl2' 'timeS'}
                  CTDDataHeader=CTDHeaders;
                  CTDDataHeaderUnit=XLSUnits;
                    ReadCTDData=ReadaCTDData;
                    %FileInfo = {CTDFullPath identifyCTDStation(CTDFullPath) identifyCTDDate(CTDFullPath)}; 
                    %{'depFM' 't090C' 'v4' 'CStarTr0' 'c0uS/cm' 'specc' 'par' 'oxsolMg/L' 'ph' 'orp' 'wl0' 'wl1' 'wl2' 'timeS'} lnPAR Ke
                         [LNPAR KeV]=KeComp2(ReadCTDData(:,[1 7]));  
                    ReadCTDData(:,end+1:end+2)=[LNPAR KeV];
                    ReadCTDData(end-2:end,end)=NaN;
                  CTDDataHeader(end+1:end+2)=[ {'lnPAR'} {'Ke'} ];
                  CTDDataHeaderUnit(end+1:end+2)=[ {'mE/m^2/sec'} {'unitless'} ];
                    %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke'
                    %CTDData=ReadCTDData(:,[1 15 3 4 2 8 16]); 
                    CTDData=ReadCTDData(:,[1 (end-1) 3 4 2 8 end 5 9]); 
                    CTDData(:,7)=-CTDData(:,7); %Ke flipped
                    % Add in conductivity and PH at the end
                    %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke' 'C0uS/cm' 'Ph'
                  CTDDataHeader=CTDDataHeader([1 (end-1) 3 4 2 8 end 5 9]);
                  CTDDataHeaderUnit=CTDDataHeaderUnit([1 (end-1) 3 4 2 8 end 5 9]);
                                %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke' 'C0uS/cm' 'Ph'
                                % % %CTDData(:,end+1:end+2)=ReadCTDData(:,[5 9]);
                                % % %CTDDataHeader(:,end+1:end+2)=CTDDataHeader([1 (end-1) 3 4 2 8 end]);
                                % % %CTDDataHeaderUnit(:,end+1:end+2)=CTDDataHeaderUnit([1 (end-1) 3 4 2 8 end]);
                    ThereIsKe=1;
                    readit=1;
                    set(VarChoice1,'String',CTDDataHeader{2})
                    set(VarChoice2,'String',CTDDataHeader{3})
                    set(VarChoice3,'String',CTDDataHeader{4})
                    set(VarChoice4,'String',CTDDataHeader{5})
                    set(VarChoice5,'String',CTDDataHeader{6})
                    set(VarChoice6,'String',CTDDataHeader{7})
                    set(VarChoice7,'String',CTDDataHeader{8})
                    set(VarChoice8,'String',CTDDataHeader{9})
                    CTDData(find(CTDData<0 & -eps<CTDData))=NaN;

                 elseif  isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRZ_1)) | ...
                         isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRZ_2)) | ...
                         isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRZ_3)) | ...
                         isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRZ_4)) | ...
                         isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRZ_5)) | ...
                         isequal(lower(CTDHeaders),lower(SampHeaderASC_QTRZ_6))
                    % Sample Header ASC QTRZ
                    %SampHeaderASC_QTRZ={'DepFM' 'T090C' ''v4'' 'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsatMg/L' 'Ph' 'Orp' 'Nbin'}  lnPAR Ke 
                  CTDDataHeader=CTDHeaders;
                  CTDDataHeaderUnit=XLSUnits;
                    ReadCTDData=ReadaCTDData;
                    % %ReadCTDData(:,end+1)=log(ReadCTDData(:,7)); 
                    %FileInfo = {CTDFullPath identifyCTDStation(CTDFullPath) identifyCTDDate(CTDFullPath)}; 
                    %{'DepFM' ''v4'' 'T090C' 'Xmiss' 'C0uS/cm' 'Specc' 'Par' 'OxsatMg/L' 'Ph' 'Orp' 'Nbin'}  lnPAR Ke 
                         [LNPAR KeV]=KeComp2(ReadCTDData(:,[1 7]));  
                    ReadCTDData(:,end+1:end+2)=[LNPAR KeV];
                    ReadCTDData(end-2:end,end)=NaN;
                  CTDDataHeader(end+1:end+2)=[ {'lnPAR'} {'Ke'} ];
                  CTDDataHeaderUnit(end+1:end+2)=[ {'mE/m^2/sec'} {'unitless'} ];
                    %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke'
                    %CTDData=ReadCTDData(:,[1 12 3 4 2 8 13]); 
                    CTDData=ReadCTDData(:,[1 (end-1) 3 4 2 8 end 5 9]); 
                    CTDData(:,7)=-CTDData(:,7); %Ke flipped
                    % Add in conductivity and PH
                    %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke' 'C0uS/cm' 'Ph'
                  CTDDataHeader=CTDDataHeader([1 (end-1) 3 4 2 8 end 5 9]);
                  CTDDataHeaderUnit=CTDDataHeaderUnit([1 (end-1) 3 4 2 8 end 5 9]);
                    %compose ReadCTDData                                          
                    ThereIsKe=1;
                    readit=1;                 
                    set(VarChoice1,'String',CTDDataHeader{2})
                    set(VarChoice2,'String',CTDDataHeader{3})
                    set(VarChoice3,'String',CTDDataHeader{4})
                    set(VarChoice4,'String',CTDDataHeader{5})
                    set(VarChoice5,'String',CTDDataHeader{6})
                    set(VarChoice6,'String',CTDDataHeader{7})
                    set(VarChoice7,'String',CTDDataHeader{8})
                    set(VarChoice8,'String',CTDDataHeader{9})
                    CTDData(find(CTDData<0 & -eps<CTDData))=NaN;

                 elseif isequal(lower(CTDHeaders),lower(SampHeaderASC_NEW_2013))|...
                        isequal(lower(CTDHeaders),lower(SampHeaderASC_NEW_2013Times)) 
                    % Sample Header ASC NEW 2013
                    %SampHeaderASC_NEW_2013={'DepFM' 'T090C' 'V4' 'CStarTr0' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'Wl0' 'Wl1' 'Wl2'} lnPAR Ke 
                  CTDDataHeader=CTDHeaders;
                  CTDDataHeaderUnit=XLSUnits;
                    ReadCTDData=ReadaCTDData;
                    %  %ReadCTDData(:,end+1)=log(ReadCTDData(:,7)); 
                    %FileInfo = {CTDFullPath identifyCTDStation(CTDFullPath) identifyCTDDate(CTDFullPath)}; 
                    %{'DepFM' 'T090C' 'V4' 'CStarTr0' 'C0uS/cm' 'Specc' 'Par' 'OxsolMg/L' 'Ph' 'Orp' 'Wl0' 'Wl1' 'Wl2'} lnPAR Ke 
                         [LNPAR KeV]=KeComp2(ReadCTDData(:,[1 7]));  
                    ReadCTDData(:,end+1:end+2)=[LNPAR KeV];
                    ReadCTDData(end-2:end,end)=NaN;
                  CTDDataHeader(end+1:end+2)=[ {'lnPAR'} {'Ke'} ];
                  CTDDataHeaderUnit(end+1:end+2)=[ {'mE/m^2/sec'} {'unitless'} ];
                    %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke'
                    %CTDData=ReadCTDData(:,[1 14      3 4 2 8 15 ]); 
                    CTDData=ReadCTDData(:,[1 (end-1) 3 4 2 8 end 5 9]); 
                    CTDData(:,7)=-CTDData(:,7); %Ke flipped
                    % Add in conductivity and PH
                    %'DEPTHm' 'ln(PAR)' 'FLUOR' 'XMISS' 'T090C' 'DO2mg' 'Ke' 'C0uS/cm' 'Ph'
                  CTDDataHeader=CTDDataHeader([1 (end-1) 3 4 2 8 end 5 9]);
                  CTDDataHeaderUnit=CTDDataHeaderUnit([1 (end-1) 3 4 2 8 end 5 9]);
                    %compose ReadCTDData    
                    ThereIsKe=1;
                    readit=1;  
                    set(VarChoice1,'String',CTDDataHeader{2})
                    set(VarChoice2,'String',CTDDataHeader{3})
                    set(VarChoice3,'String',CTDDataHeader{4})
                    set(VarChoice4,'String',CTDDataHeader{5})
                    set(VarChoice5,'String',CTDDataHeader{6})
                    set(VarChoice6,'String',CTDDataHeader{7})
                    set(VarChoice7,'String',CTDDataHeader{8})
                    set(VarChoice8,'String',CTDDataHeader{9})
                    CTDData(find(CTDData<0 & -eps<CTDData))=NaN;
                 else
                    errordlg('File is not recognized as CTD Data. File header is not conforming. (NoAverage Data is not diplayed here.) ');
                 end
           else
               errordlg('Selected file is not recognized as CTD Data (based on file extension).');
           end
%build of this
            
            
          %if file was properly read do this   
          if  readit
           set(D.VCTDfile,'string',[CTDpathname CTDfilename],'value',0)


%             deltapercKe;
%             [maxtabKe, mintabKe, IndRangeTabDownKe, jdKe, IndRangeTabUpKe, juKe]=...
%                            PeaksAndValleys(-CTDData(:,7), deltapercKe, CTDData(:,1));
%             mintabKe=mintabKe*[1 0;0 -1];           

                           
            %AND 

            %PLOT Background!!!!! (no its done at GUI initialization)          


            %PLOT  CONTENT!!!!! ( done here -- figure initialization)
 
            % Variables plotted
            % Colorseq=['b';'y';'r';'m';'c';'g']
            % Axisseq=['L ';' R';'off';'off';'off';'off']; %'LR','L ',' R','off'
            % Unitseq=['L ';' R';'off';'off';'off';'off']; %'LR','L ',' R','off'

            CAx1=Cseq(1,:);   CAx2=Cseq(2,:);   CAx3=Cseq(3,:);   CAx4=Cseq(4,:);   CAx5=Cseq(5,:);   CAx6=Cseq(6,:);
            CLine1=Cseq(1,:); CLine2=Cseq(2,:); CLine3=Cseq(3,:); CLine4=Cseq(4,:); CLine5=Cseq(5,:); CLine6=Cseq(6,:);

            %pause,
            %                set(minmaxAxV3,'Xlim',[0 floor(max(CTDData(:,1)))+1])

            %var1
            if  LAxisRadioSeq(1), set(ax1L,'Visible','on'); else  set(ax1L,'Visible','off');  end
            if exist('h1_L'),  delete(h1_L); clear('h1_L'); end
            h1_L=line(CTDData(:,1),CTDData(:,2), 'Color', CLine1, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                      ax1L, 'Visible','on');
            set(get(ax1L,'Ylabel'),'String',[CTDDataHeader{1+1}, ' (' CTDDataHeaderUnit{1+1} ')'],'FontWeight', CTDDataAxFontWgt{1+1})
            set(get(ax1L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax1L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])           
            if  RAxisRadioSeq(1), set(ax1R,'Visible','on'); else set(ax1R,'Visible','off'); end
            if exist('h1_R'),  delete(h1_R); clear('h1_R'); end
            h1_R=line(CTDData(:,1),CTDData(:,2), 'Color', CLine1, 'LineStyle', '-', 'Marker', '.', 'Parent',... 
                       ax1R, 'Visible','on');
            set(get(ax1R,'Ylabel'),'String',[CTDDataHeader{1+1}, ' (' CTDDataHeaderUnit{1+1} ')'],'FontWeight', CTDDataAxFontWgt{1+1})
            set(get(ax1R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax1R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            
           %----------
            if RPlotSeq(1)
             set(VarChoice1,'value',1);
             CBk1=Cseq(1,:);      set(VarChoice1,'BackgroundColor' , CBk1)
             set(h1_L,'Visible','on'); set(h1_R,'Visible','on');
            else
             set(VarChoice1,'value',0);
             CBk1=NoUseCseq(1,:); set(VarChoice1,'BackgroundColor' , CBk1)
             set(h1_L,'Visible','off'); set(h1_R,'Visible','off');
            end
           %----------
            
            %var2
            if  LAxisRadioSeq(2), set(ax2L,'Visible','on'); else set(ax2L,'Visible','off'); end
            %set(ax2L,'Visible','off');
            if exist('h2_L'),  delete(h2_L); clear('h2_L'); end
            h2_L=line(CTDData(:,1),CTDData(:,3), 'Color', CLine2, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax2L, 'Visible','on');
            set(get(ax2L,'Ylabel'),'String',[CTDDataHeader{2+1}, ' (' CTDDataHeaderUnit{2+1} ')'],'FontWeight', CTDDataAxFontWgt{2+1})
            set(get(ax2L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax2L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            if  RAxisRadioSeq(2), set(ax2R,'Visible','on'); else set(ax2R,'Visible','off'); end
            %set(ax2R,'Visible','off');
            if exist('h2_R'),  delete(h2_R); clear('h2_R'); end
            h2_R=line(CTDData(:,1),CTDData(:,3), 'Color', CLine2, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax2R, 'Visible','on');
            set(get(ax2R,'Ylabel'),'String',[CTDDataHeader{2+1}, ' (' CTDDataHeaderUnit{2+1} ')'],'FontWeight', CTDDataAxFontWgt{2+1})
            set(get(ax2R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax2R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            %----------
            if RPlotSeq(2)
             set(VarChoice2,'value',1);
             CBk2=Cseq(2,:);      set(VarChoice2,'BackgroundColor' , CBk2)
             set(h2_L,'Visible','on'); set(h2_R,'Visible','on');
            else
             set(VarChoice2,'value',0);
             CBk2=NoUseCseq(2,:); set(VarChoice2,'BackgroundColor' , CBk2)
             set(h2_L,'Visible','off'); set(h2_R,'Visible','off');
            end
           %----------
            
            %var3
            if  LAxisRadioSeq(3), set(ax3L,'Visible','on'); else set(ax3L,'Visible','off'); end
            %set(ax3L,'Visible','off');
            if exist('h3_L'),  delete(h3_L); clear('h3_L'); end
            h3_L=line(CTDData(:,1),CTDData(:,4), 'Color', CLine3, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax3L, 'Visible','on');
            set(get(ax3L,'Ylabel'),'String',[CTDDataHeader{3+1}, ' (' CTDDataHeaderUnit{3+1} ')'],'FontWeight', CTDDataAxFontWgt{3+1})
            set(get(ax3L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax3L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            hold(ax3L,'on')      
            if  RAxisRadioSeq(3), set(ax3R,'Visible','on'); else set(ax3R,'Visible','off'); end
            %set(ax3R,'Visible','off');
            if exist('h3_R'),  delete(h3_R); clear('h3_R'); end
            h3_R=line(CTDData(:,1),CTDData(:,4), 'Color', CLine3, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax3R, 'Visible','on');
            set(get(ax3R,'Ylabel'),'String',[CTDDataHeader{3+1}, ' (' CTDDataHeaderUnit{3+1} ')'],'FontWeight', CTDDataAxFontWgt{3+1})
            set(get(ax3R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax3R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            hold(ax3R,'on')      

            %+-------------------------
            %----------
      if ~strcmpi(XmissHeaderEntry,'empty') & ~strcmpi(PARHeaderEntry,'empty')
          %if none of the Xmiss and Par columns are missing
          
          set(MinVar3,'Enable','on');
          set(MinVar4,'Enable','on');
          set(MaxVar3,'Enable','on');
          set(MaxVar4,'Enable','on');
          set(minmaxAxV3,'Visible','on');
          set(minmaxAxV4,'Visible','on');
          set(minmaxAxTicks,'Visible','on');
          set(MINpV3Bp2,'Visible','on');
          set(MINpV4Bp2,'Visible','on');
          set(MAXpV3Bp2,'Visible','on');
          set(MAXpV4Bp2,'Visible','on');

          %minmaxAxTicks

            %V3m
            deltapercV3m=6;
            stripwidth=0; stripwidth=floor(stripwidth);

            [maxtabV3m, mintabV3m, IndRangeTabDownV3m, jdV3m, IndRangeTabUpV3m, juV3m, NotNaNIndV3m]=...
                           PeaksAndValleys2_6(CTDData(:,4), deltapercV3m, CTDData(:,1));
            
          if prod([size(mintabV3m) size(maxtabV3m)])   %if normal mintab maxtab case 
            if mintabV3m(1,1)<maxtabV3m(1,1) kkAdjV3m=0; else kkAdjV3m=1; end  % only for valeys
            if mintabV3m(end,1)>maxtabV3m(end,1), skipLastV3m=1; else skipLastV3m=0; end, 

            
            XX1V3m=[];YYV3m=[]; cdata1V3m=[];
            XX2V3m=[];         cdata2V3m=[];
            for kk=1:(size(mintabV3m,1)-skipLastV3m)
                 if  IndRangeTabDownV3m(kk,2)> IndRangeTabUpV3m(kk,1), jj=kk+1; else jj=kk; end
                  mu=1;
                  XX1V3m(:,kk)=[CTDData(max(1,NotNaNIndV3m(IndRangeTabDownV3m(kk,2)-stripwidth)),1)...
                               CTDData(max(1,NotNaNIndV3m(IndRangeTabDownV3m(kk,2)-stripwidth)),1)...
                               CTDData(NotNaNIndV3m(IndRangeTabDownV3m(kk,2)),1)...
                               CTDData(NotNaNIndV3m(IndRangeTabDownV3m(kk,2)),1)                     ]';
                  YL=ylim(ax3R);
                  YYV3m(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata1V3m(1,kk,1:3)=[1 1 1   ]; cdata1V3m(2,kk,1:3)=[1 1 1   ]; 
                  cdata1V3m(3,kk,1:3)=[0 0 1*mu]; cdata1V3m(4,kk,1:3)=[0 0 1*mu];
                  %             h1=patch(XX1,YY1,cdata1),
                  %             set(h1, 'EdgeColor','none','facealpha',.2)
                  xlen=size(CTDData,1);%xlen=length(x);
                  XX2V3m(:,kk)=[CTDData(NotNaNIndV3m(IndRangeTabUpV3m(jj,1)),1)...
                               CTDData(NotNaNIndV3m(IndRangeTabUpV3m(jj,1)),1)...
                               CTDData(min(xlen,NotNaNIndV3m(IndRangeTabUpV3m(jj,1)+stripwidth)),1)...
                               CTDData(min(xlen,NotNaNIndV3m(IndRangeTabUpV3m(jj,1)+stripwidth)),1)   ]';
                  %YL=ylim(ax3R);
                  %YY2V3m(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata2V3m(1,kk,1:3)=[0 0 1*mu];
                  cdata2V3m(2,kk,1:3)=[0 0 1*mu];
                  cdata2V3m(3,kk,1:3)=[1 1 1];
                  cdata2V3m(4,kk,1:3)=[1 1 1];
                  %            h2=patch(XX2,YY2,cdata2),
                  %            set(h2, 'EdgeColor','none','facealpha',.2)
            end
          else    %if not normal mintab maxtab case 
            XX1V3m=[];YYV3m=[]; cdata1V3m=[];
            XX2V3m=[];         cdata2V3m=[];
            YL=ylim(ax3R);
            xlen=size(CTDData,1);%xlen=length(x);
          end
            %V3M
            deltapercV3M=6;
            stripwidth=0; stripwidth=floor(stripwidth);

            [maxtabV3M, mintabV3M, IndRangeTabDownV3M, jdV3M, IndRangeTabUpV3M, juV3M, NotNaNIndV3M]=...
                           PeaksAndValleys2_6(-CTDData(:,4), deltapercV3M, CTDData(:,1));
            
          if prod([size(mintabV3M) size(maxtabV3M)])   %if normal mintab maxtab case 
            if mintabV3M(1,1)<maxtabV3M(1,1) kkAdjV3M=0; else kkAdjV3M=1; end  % only for valeys
            if mintabV3M(end,1)>maxtabV3M(end,1), skipLastV3M=1; else skipLastV3M=0; end, 

            
            XX1V3M=[];YYV3M=[]; cdata1V3M=[];
            XX2V3M=[];         cdata2V3M=[];
            for kk=1:(size(mintabV3M,1)-skipLastV3M)
                 if  IndRangeTabDownV3M(kk,2)> IndRangeTabUpV3M(kk,1), jj=kk+1; else jj=kk; end
                  mu=1;
                  XX1V3M(:,kk)=[CTDData(max(1,NotNaNIndV3M(IndRangeTabDownV3M(kk,2)-stripwidth)),1)...
                               CTDData(max(1,NotNaNIndV3M(IndRangeTabDownV3M(kk,2)-stripwidth)),1)...
                               CTDData(NotNaNIndV3M(IndRangeTabDownV3M(kk,2)),1)...
                               CTDData(NotNaNIndV3M(IndRangeTabDownV3M(kk,2)),1)                     ]';
                  YL=ylim(ax3R);
                  YYV3M(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata1V3M(1,kk,1:3)=[1 1 1   ]; cdata1V3M(2,kk,1:3)=[1 1 1   ]; 
                  cdata1V3M(3,kk,1:3)=[1*mu 0 0 ]; cdata1V3M(4,kk,1:3)=[1*mu 0 0 ];
                  %             h1=patch(XX1,YY1,cdata1),
                  %             set(h1, 'EdgeColor','none','facealpha',.2)
                  xlen=size(CTDData,1);%xlen=length(x);
                  XX2V3M(:,kk)=[CTDData(NotNaNIndV3M(IndRangeTabUpV3M(jj,1)),1)...
                               CTDData(NotNaNIndV3M(IndRangeTabUpV3M(jj,1)),1)...
                               CTDData(min(xlen,NotNaNIndV3M(IndRangeTabUpV3M(jj,1)+stripwidth)),1)...
                               CTDData(min(xlen,NotNaNIndV3M(IndRangeTabUpV3M(jj,1)+stripwidth)),1)   ]';
                  %YL=ylim(ax3R);
                  %YY2V3M(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata2V3M(1,kk,1:3)=[1*mu 0 0 ];
                  cdata2V3M(2,kk,1:3)=[1*mu 0 0 ];
                  cdata2V3M(3,kk,1:3)=[1 1 1];
                  cdata2V3M(4,kk,1:3)=[1 1 1];
                  %            h2=patch(XX2,YY2,cdata2),
                  %            set(h2, 'EdgeColor','none','facealpha',.2)
            end
          else    %if not normal mintab maxtab case 
            XX1V3M=[];YYV3M=[]; cdata1V3M=[];
            XX2V3M=[];         cdata2V3M=[];
            YL=ylim(ax3R);
            xlen=size(CTDData,1);%xlen=length(x);
          end
            %-----V3Mm-----

            %+-------------------------
            %V4m
            deltapercV4m=6;
            stripwidth=0; stripwidth=floor(stripwidth);

            [maxtabV4m, mintabV4m, IndRangeTabDownV4m, jdV4m, IndRangeTabUpV4m, juV4m, NotNaNIndV4m]=...
                           PeaksAndValleys2_6(CTDData(:,7), deltapercV4m, CTDData(:,1));

          if prod([size(mintabV4m) size(maxtabV4m)])   %if normal mintab maxtab case 
            if mintabV4m(1,1)<maxtabV4m(1,1) kkAdjV4m=0; else kkAdjV4m=1; end  % only for valeys
            if mintabV4m(end,1)>maxtabV4m(end,1), skipLastV4m=1; else skipLastV4m=0; end, 

            
            XX1V4m=[];YYV4m=[]; cdata1V4m=[];
            XX2V4m=[];         cdata2V4m=[];
            for kk=1:(size(mintabV4m,1)-skipLastV4m)
                 if  IndRangeTabDownV4m(kk,2)> IndRangeTabUpV4m(kk,1), jj=kk+1; else jj=kk; end
                  mu=1;
                  XX1V4m(:,kk)=[CTDData(max(1,NotNaNIndV4m(IndRangeTabDownV4m(kk,2)-stripwidth)),1)...
                               CTDData(max(1,NotNaNIndV4m(IndRangeTabDownV4m(kk,2)-stripwidth)),1)...
                               CTDData(NotNaNIndV4m(IndRangeTabDownV4m(kk,2)),1)...
                               CTDData(NotNaNIndV4m(IndRangeTabDownV4m(kk,2)),1)                     ]';
                  YL=ylim(ax3R);
                  YYV4m(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata1V4m(1,kk,1:3)=[1 1 1   ]; cdata1V4m(2,kk,1:3)=[1 1 1   ]; 
                  cdata1V4m(3,kk,1:3)=[0 0 1*mu]; cdata1V4m(4,kk,1:3)=[0 0 1*mu];
                  %             h1=patch(XX1,YY1,cdata1),
                  %             set(h1, 'EdgeColor','none','facealpha',.2)
                  xlen=size(CTDData,1);%xlen=length(x);
                  XX2V4m(:,kk)=[CTDData(NotNaNIndV4m(IndRangeTabUpV4m(jj,1)),1)...
                               CTDData(NotNaNIndV4m(IndRangeTabUpV4m(jj,1)),1)...
                               CTDData(min(xlen,NotNaNIndV4m(IndRangeTabUpV4m(jj,1)+stripwidth)),1)...
                               CTDData(min(xlen,NotNaNIndV4m(IndRangeTabUpV4m(jj,1)+stripwidth)),1)   ]';
                  %YL=ylim(ax3R);
                  %YY2V4m(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata2V4m(1,kk,1:3)=[0 0 1*mu];
                  cdata2V4m(2,kk,1:3)=[0 0 1*mu];
                  cdata2V4m(3,kk,1:3)=[1 1 1];
                  cdata2V4m(4,kk,1:3)=[1 1 1];
                  %            h2=patch(XX2,YY2,cdata2),
                  %            set(h2, 'EdgeColor','none','facealpha',.2)
            end
          else    %if not normal mintab maxtab case 
            XX1V4m=[];YYV4m=[]; cdata1V4m=[];
            XX2V4m=[];         cdata2V4m=[];
            YL=ylim(ax3R);
            xlen=size(CTDData,1);%xlen=length(x);
          end
            %V4M
            deltapercV4M=6;
            stripwidth=0; stripwidth=floor(stripwidth);

            [maxtabV4M, mintabV4M, IndRangeTabDownV4M, jdV4M, IndRangeTabUpV4M, juV4M, NotNaNIndV4M]=...
                           PeaksAndValleys2_6(-CTDData(:,7), deltapercV4M, CTDData(:,1));

          if prod([size(mintabV4M) size(maxtabV4M)])   %if normal mintab maxtab case 
            if mintabV4M(1,1)<maxtabV4M(1,1) kkAdjV4M=0; else kkAdjV4M=1; end  % only for valeys
            if mintabV4M(end,1)>maxtabV4M(end,1), skipLastV4M=1; else skipLastV4M=0; end, 

            
            XX1V4M=[];YYV4M=[]; cdata1V4M=[];
            XX2V4M=[];         cdata2V4M=[];
            for kk=1:(size(mintabV4M,1)-skipLastV4M)
                 if  IndRangeTabDownV4M(kk,2)> IndRangeTabUpV4M(kk,1), jj=kk+1; else jj=kk; end
                  mu=1;
                  XX1V4M(:,kk)=[CTDData(max(1,NotNaNIndV4M(IndRangeTabDownV4M(kk,2)-stripwidth)),1)...
                               CTDData(max(1,NotNaNIndV4M(IndRangeTabDownV4M(kk,2)-stripwidth)),1)...
                               CTDData(NotNaNIndV4M(IndRangeTabDownV4M(kk,2)),1)...
                               CTDData(NotNaNIndV4M(IndRangeTabDownV4M(kk,2)),1)                     ]';
                  YL=ylim(ax3R);
                  YYV4M(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata1V4M(1,kk,1:3)=[1 1 1   ]; cdata1V4M(2,kk,1:3)=[1 1 1   ]; 
                  cdata1V4M(3,kk,1:3)=[1*mu 0 0 ]; cdata1V4M(4,kk,1:3)=[1*mu 0 0 ];
                  %             h1=patch(XX1,YY1,cdata1),
                  %             set(h1, 'EdgeColor','none','facealpha',.2)
                  xlen=size(CTDData,1);%xlen=length(x);
                  XX2V4M(:,kk)=[CTDData(NotNaNIndV4M(IndRangeTabUpV4M(jj,1)),1)...
                               CTDData(NotNaNIndV4M(IndRangeTabUpV4M(jj,1)),1)...
                               CTDData(min(xlen,NotNaNIndV4M(IndRangeTabUpV4M(jj,1)+stripwidth)),1)...
                               CTDData(min(xlen,NotNaNIndV4M(IndRangeTabUpV4M(jj,1)+stripwidth)),1)   ]';
                  %YL=ylim(ax3R);
                  %YY2V4M(:,kk)=[YL(2) YL(1) YL(1) YL(2) ]';
                  cdata2V4M(1,kk,1:3)=[1*mu 0 0 ];
                  cdata2V4M(2,kk,1:3)=[1*mu 0 0 ];
                  cdata2V4M(3,kk,1:3)=[1 1 1];
                  cdata2V4M(4,kk,1:3)=[1 1 1];
                  %            h2=patch(XX2,YY2,cdata2),
                  %            set(h2, 'EdgeColor','none','facealpha',.2)
            end
          else    %if not normal mintab maxtab case 
            XX1V4M=[];YYV4M=[]; cdata1V4M=[];
            XX2V4M=[];         cdata2V4M=[];
            YL=ylim(ax3R);
            xlen=size(CTDData,1);%xlen=length(x);
          end
            %--V4Mm--------
            
            %m
            if stripwidth==0

                %if exist('B3m_LbarsL'),  delete(B3m_LbarsL); clear('B3m_LbarsL'); end
                %B3m_LbarsL=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3m_LbarsR'),  delete(B3m_LbarsR); clear('B3m_LbarsR'); end
                %B3m_LbarsR=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %hold(ax3L,'off')      

                %if exist('B3m_RbarsL'),  delete(B3m_RbarsL); clear('B3m_RbarsL'); end
                %B3m_RbarsL=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3m_RbarsR'),  delete(B3m_RbarsR); clear('B3m_RbarsR'); end
                %B3m_RbarsR=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %hold(ax3R,'off')      

                
                %XX1V3M(4,:)
                %XX1V3m(4,:)
                
                %V3
                if exist('MINpV3Bp2'),  delete(MINpV3Bp2); clear('MINpV3Bp2'); end
                %cmapSize=29;
              if prod([size(mintabV3m) size(maxtabV3m) size(XX1V3m)])   %if normal mintab maxtab case 
                ZZ1V3m= ((1+0*XX1V3m(4,:))'*[fliplr(0:cmapSize-1) nan])'; 
                ZZ1V3m = min(cmapSize,round((cmapSize-1)*(ZZ1V3m-1)/(2*cmapSize-1))+1); 
                MINpV3Bp2 = patch((XX1V3m(4,:)'*[ones(1,cmapSize) nan])',((1+0*XX1V3m(4,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ZZ1V3m,...
                'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'on');
                %MINpV3Bp2 = patch((XX1V3m(4,:)'*[ones(1,cmapSize) nan])',((1+0*XX1V3m(4,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ((1+0*XX1V3m(4,:))'*[fliplr(1:cmapSize) nan]+1)',...
                %'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'on');
              else
                MINpV3Bp2 = patch([],[],[],'Parent', minmaxAxV3,'visible', 'on');
              end
              
                %V4

                if exist('MINpV4Bp2'),  delete(MINpV4Bp2); clear('MINpV4Bp2'); end
                %cmapSize=29;
              if prod([size(mintabV4m) size(maxtabV4m) size(XX1V4m)])   %if normal mintab maxtab case 
                ZZ1V4m= ((1+0*XX1V4m(4,:))'*[fliplr(0:cmapSize-1) nan])'; 
                ZZ1V4m = min(cmapSize,round((cmapSize-1)*(ZZ1V4m-1)/(2*cmapSize-1))+1); 
                MINpV4Bp2 = patch((XX1V4m(4,:)'*[ones(1,cmapSize) nan])',((1+0*XX1V4m(4,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ZZ1V4m+cmapSize,...
                'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'on');
                %MINpV4Bp2 = patch((XX1V4m(4,:)'*[ones(1,cmapSize) nan])',((1+0*XX1V4m(4,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ((1+0*XX1V4m(4,:))'*[fliplr(1:cmapSize) nan]+1+cmapSize)',...
                %'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'on');
              else
                MINpV4Bp2 = patch([],[],[],'Parent', minmaxAxV4,'visible', 'on');
              end
                
                hold( minmaxAxV3,'on')
                hold( minmaxAxV4,'on')
                
                %if exist('B3m_LbarsL'),  delete(B3m_LbarsL); clear('B3m_LbarsL'); end
                %B3m_LbarsL=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3m_LbarsR'),  delete(B3m_LbarsR); clear('B3m_LbarsR'); end
                %B3m_LbarsR=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %hold(ax3L,'off')      

                %if exist('B3m_RbarsL'),  delete(B3m_RbarsL); clear('B3m_RbarsL'); end
                %B3m_RbarsL=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3m_RbarsR'),  delete(B3m_RbarsR); clear('B3m_RbarsR'); end
                %B3m_RbarsR=line(XX2V3m(1:2,:),YYV3m(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %hold(ax3R,'off')      
            elseif stripwidth>0      
                %NoUseCseq(3,:)
                %if exist('B3m_LbarsL'),  delete(B3m_LbarsL); clear('B3m_LbarsL'); end
                %B3m_LbarsL=patch(XX1V3m,YYV3m,cdata1V3m,'visible','on','EdgeColor','none','Parent',ax3L);
                %%set(B3m_LbarsL, 'facealpha',.2)
                %if exist('B3m_LbarsR'),  delete(B3m_LbarsR); clear('B3m_LbarsR'); end
                %B3m_LbarsR=patch(XX2V3m,YYV3m,cdata2V3m,'visible','on','EdgeColor','none','Parent',ax3L );
                %%set(B3m_LbarsR,'facealpha',.2)
                hold(ax3L,'off')      

                %if exist('B3m_RbarsL'),  delete(B3m_RbarsL); clear('B3m_RbarsL'); end
                %B3m_RbarsL=patch(XX1V3m,YYV3m,cdata1V3m,'visible','on', 'EdgeColor','none','Parent',ax3R);
                %%set(B3m_LbarsL,'facealpha',.2)
                %if exist('B3m_RbarsR'),  delete(B3m_RbarsR); clear('B3m_RbarsR'); end
                %B3m_RbarsR=patch(XX2V3m,YYV3m,cdata2V3m,'visible','on', 'EdgeColor','none','Parent',ax3R);
                %%set(B3m_LbarsR, 'facealpha',.2)
                hold(ax3R,'off')
            elseif stripwidth<0
                error('maxmin bar widths are negative')
            end
            
            %M
            if stripwidth==0
                
                %if exist('B3M_LbarsL'),  delete(B3M_LbarsL); clear('B3M_LbarsL'); end
                %%B3M_LbarsL=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %%        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3M_LbarsR'),  delete(B3M_LbarsR); clear('B3M_LbarsR'); end
                %%B3M_LbarsR=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %%        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %%hold(ax3L,'off')      

                %if exist('B3M_RbarsL'),  delete(B3M_RbarsL); clear('B3M_RbarsL'); end
                %%B3M_RbarsL=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %%        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3M_RbarsR'),  delete(B3M_RbarsR); clear('B3M_RbarsR'); end
                %%B3M_RbarsR=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %%        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %%hold(ax3R,'off')
                
                
                
                %V3
                if exist('MAXpV3Bp2'),  delete(MAXpV3Bp2); clear('MAXpV3Bp2'); end
                %cmapSize=29;
              if prod([size(mintabV3M) size(maxtabV3M) size(XX2V3M)])   %if normal mintab maxtab case 
                ZZ2V3M= ((1+0*XX2V3M(1,:))'*[(0:cmapSize-1) nan])'; 
                ZZ2V3M = min(cmapSize,round((cmapSize-1)*(ZZ2V3M-1)/(2*cmapSize-1))+1); 
                MAXpV3Bp2 = patch((XX2V3M(1,:)'*[ones(1,cmapSize) nan])',((1+0*XX2V3M(1,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ZZ2V3M,...
                'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'on');
                %MAXpV3Bp2 = patch((XX2V3M(1,:)'*[ones(1,cmapSize) nan])',((1+0*XX2V3M(1,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ((1+0*XX2V3M(1,:))'*[(1:cmapSize) nan]+1)',...
                %'EdgeColor', 'interp','Parent', minmaxAxV3,'visible', 'on');
                caxis(minmaxAxV3);
              else
                MAXpV3Bp2 = patch([],[],[],'Parent', minmaxAxV3,'visible', 'on');
              end
              
                %V4
                
                
                if exist('MAXpV4Bp2'),  delete(MAXpV4Bp2); clear('MAXpV4Bp2'); end
                %cmapSize=29;
              if prod([size(mintabV4M) size(maxtabV4M) size(XX2V4M)])   %if normal mintab maxtab case 
                ZZ2V4M= ((1+0*XX2V4M(1,:))'*[(0:cmapSize-1) nan])'; 
                ZZ2V4M = min(cmapSize,round((cmapSize-1)*(ZZ2V4M-1)/(2*cmapSize-1))+1); 
                MAXpV4Bp2 = patch((XX2V4M(1,:)'*[ones(1,cmapSize) nan])',((1+0*XX2V4M(1,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ZZ2V4M+cmapSize,...
                'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'on');
                %MAXpV4Bp2 = patch((XX2V3M(1,:)'*[ones(1,cmapSize) nan])',((1+0*XX2V3M(1,:))'*[[0:(cmapSize-1)]/(cmapSize-1) nan])', ( ((1+0*XX2V3M(1,:))'*[(1:cmapSize) nan]+1)+ cmapSize )',...
                %'EdgeColor', 'interp','Parent', minmaxAxV4,'visible', 'on');
                caxis(minmaxAxV4,[0 2*cmapSize-1]);
              else
                MAXpV4Bp2 = patch([],[],[],'Parent', minmaxAxV4,'visible', 'on');
              end
            
                %hold( minmaxAxV3,'off')
                %hold( minmaxAxV4,'off')

                %'boo',floor(max(CTDData(:,1)))+1
                %'bee',max(XX2V4M(1,:))
                set(minmaxAxV3,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
                set(minmaxAxV3,'visible','on')
                set(minmaxAxV4,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
                set(minmaxAxV4,'visible','on')
                set(minmaxAxTicks,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
                set(minmaxAxTicks,'visible','on')

                 

             xticks=[];
             
             if   get(MinVar3,'value')==1
                   VisLine='on';  MinMaxCBk1=Cseq(3,:);      MinMaxPlotSeq(1)=1; 
                   if prod([size(mintabV3m) size(maxtabV3m) size(XX1V3m) ])   %if normal mintab maxtab case 
                     xticks=[xticks XX1V3m(4,:)];  %IDESZURD
                   end
             else
                   VisLine='off'; MinMaxCBk1=NoUseCseq(3,:); MinMaxPlotSeq(1)=0; 
             end
             set(MINpV3Bp2,'Visible',VisLine); 
             set(MinVar3,'BackgroundColor' , MinMaxCBk1)
             
             if   get(MaxVar3,'value')==1
                   VisLine='on';  MinMaxCBk2=Cseq(3,:);      MinMaxPlotSeq(2)=1; 
                   if prod([size(mintabV3M) size(maxtabV3M) size(XX2V3M)])   %if normal mintab maxtab case 
                     xticks=[xticks XX2V3M(4,:)];   %IDESZURD
                   end
             else
                   VisLine='off'; MinMaxCBk2=NoUseCseq(3,:); MinMaxPlotSeq(2)=0; 
             end
             set(MAXpV3Bp2,'Visible',VisLine); 
             set(MaxVar3,'BackgroundColor' , MinMaxCBk2)
             
             if   get(MinVar4,'value')==1
                   VisLine='on';  MinMaxCBk3=Cseq(6,:);      MinMaxPlotSeq(3)=1; 
                   if prod([size(mintabV4m) size(maxtabV4m) size(XX1V4m)])   %if normal mintab maxtab case 
                     xticks=[xticks  XX1V4m(4,:)];  %IDESZURD
                   end
             else
                   VisLine='off'; MinMaxCBk3=NoUseCseq(6,:); MinMaxPlotSeq(3)=0; 
             end
             set(MINpV4Bp2,'Visible',VisLine); 
             set(MinVar4,'BackgroundColor' , MinMaxCBk3)

             if   get(MaxVar4,'value')==1
                   VisLine='on';  MinMaxCBk4=Cseq(6,:);      MinMaxPlotSeq(4)=1; 
                   if prod([size(mintabV4M) size(maxtabV4M)  size(XX2V4m)])   %if normal mintab maxtab case 
                      xticks=[xticks  XX2V4M(1,:)];  %IDESZURD
                   end
             else
                   VisLine='off'; MinMaxCBk4=NoUseCseq(6,:); MinMaxPlotSeq(4)=0; 
             end
             set(MAXpV4Bp2,'Visible',VisLine); 
             set(MaxVar4,'BackgroundColor' , MinMaxCBk4)

%              set(MINpV3Bp2,'Visible',VisLine); 
%              set(MinVar3,'BackgroundColor' , MinMaxCBk1)
%              set(MAXpV3Bp2,'Visible',VisLine); 
%              set(MaxVar3,'BackgroundColor' , MinMaxCBk2)
%              set(MINpV4Bp2,'Visible',VisLine); 
%              set(MinVar4,'BackgroundColor' , MinMaxCBk3)
%              set(MAXpV4Bp2,'Visible',VisLine); 
%              set(MaxVar4,'BackgroundColor' , MinMaxCBk4)

             xticks= unique(xticks);
             xtickslab=cellstr(num2str(xticks'));
             set(minmaxAxTicks,'XTick',xticks)
             set(minmaxAxTicks,'XTicklabel',xtickslab)

%***************************

                %%%magnify(Dfig)
                %figure(4)
                %ginput_ax(ax1L, 1)
                
                %if exist('B3M_LbarsL'),  delete(B3M_LbarsL); clear('B3M_LbarsL'); end
                %B3M_LbarsL=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3M_LbarsR'),  delete(B3M_LbarsR); clear('B3M_LbarsR'); end
                %B3M_LbarsR=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3L, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %hold(ax3L,'off')      

                %if exist('B3M_RbarsL'),  delete(B3M_RbarsL); clear('B3M_RbarsL'); end
                %B3M_RbarsL=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %if exist('B3M_RbarsR'),  delete(B3M_RbarsR); clear('B3M_RbarsR'); end
                %B3M_RbarsR=line(XX2V3M(1:2,:),YYV3M(1:2,:), 'Color', 'b' , 'LineStyle', '-', 'Parent',...
                %        ax3R, 'LineWidth', .5, 'Visible','on','Color', NoUseCseq(3,:));
                %hold(ax3R,'off')      
            elseif stripwidth>0      
                %NoUseCseq(3,:)
                %if exist('B3M_LbarsL'),  delete(B3M_LbarsL); clear('B3M_LbarsL'); end
                %B3M_LbarsL=patch(XX1V3M,YYV3M,cdata1V3M,'visible','on','EdgeColor','none','Parent',ax3L);
                %%set(B3M_LbarsL, 'facealpha',.2)
                %if exist('B3M_LbarsR'),  delete(B3M_LbarsR); clear('B3M_LbarsR'); end
                %B3M_LbarsR=patch(XX2V3M,YYV3M,cdata2V3M,'visible','on','EdgeColor','none','Parent',ax3L );
                %%set(B3M_LbarsR,'facealpha',.2)
                %hold(ax3L,'off')      

                %if exist('B3M_RbarsL'),  delete(B3M_RbarsL); clear('B3M_RbarsL'); end
                %B3M_RbarsL=patch(XX1V3M,YYV3M,cdata1V3M,'visible','on', 'EdgeColor','none','Parent',ax3R);
                %%set(B3M_LbarsL,'facealpha',.2)
                %if exist('B3M_RbarsR'),  delete(B3M_RbarsR); clear('B3M_RbarsR'); end
                %B3M_RbarsR=patch(XX2V3M,YYV3M,cdata2V3M,'visible','on', 'EdgeColor','none','Parent',ax3R);
                %%set(B3M_LbarsR, 'facealpha',.2)
                %hold(ax3R,'off')
            elseif stripwidth<0
                error('maxmin bar widths are negative')
            end
      else %if PAR or XMISS is missing 
          set(MinVar3,'Enable','off');
          set(MinVar4,'Enable','off');
          set(MaxVar3,'Enable','off');
          set(MaxVar4,'Enable','off');
          set(minmaxAxV3,'Visible','off');
          set(minmaxAxV4,'Visible','off');
          set(minmaxAxTicks,'Visible','off');
          set(MINpV3Bp2,'Visible','off');
          set(MINpV4Bp2,'Visible','off');
          set(MAXpV3Bp2,'Visible','off');
          set(MAXpV4Bp2,'Visible','off');
          %minmaxAxTicks
      end
      
            %+-------------------------
            if RPlotSeq(3)
             set(VarChoice3,'value',1);
             CBk3=Cseq(3,:);      set(VarChoice3,'BackgroundColor' , CBk3)
             set(h3_L,'Visible','on'); set(h3_R,'Visible','on');
         %    set(B3m_LbarsL,'Visible','on'); set(B3m_LbarsR,'Visible','on');
         %    set(B3m_RbarsL,'Visible','on'); set(B3m_RbarsR,'Visible','on');
         %    set(B3M_LbarsL,'Visible','on'); set(B3M_LbarsR,'Visible','on');
         %    set(B3M_RbarsL,'Visible','on'); set(B3M_RbarsR,'Visible','on');
            else
             set(VarChoice3,'value',0);
             CBk3=NoUseCseq(3,:); set(VarChoice3,'BackgroundColor' , CBk3)
             set(h3_L,'Visible','off'); set(h3_R,'Visible','off');
          %   set(B3m_LbarsL,'Visible','off'); set(B3m_LbarsR,'Visible','off');
          %   set(B3m_RbarsL,'Visible','off'); set(B3m_RbarsR,'Visible','off');
          %   set(B3M_LbarsL,'Visible','off'); set(B3M_LbarsR,'Visible','off');
          %   set(B3M_RbarsL,'Visible','off'); set(B3M_RbarsR,'Visible','off');
            end
           %----------
            
            %var4
            if  LAxisRadioSeq(4), set(ax4L,'Visible','on'); else set(ax4L,'Visible','off'); end
            %set(ax4L,'Visible','off');
            if exist('h4_L'),  delete(h4_L); clear('h4_L'); end
            h4_L=line(CTDData(:,1),CTDData(:,5), 'Color', CLine4, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax4L, 'Visible','on');
            set(get(ax4L,'Ylabel'),'String',[CTDDataHeader{4+1}, ' (' CTDDataHeaderUnit{4+1} ')'],'FontWeight', CTDDataAxFontWgt{4+1})
            set(get(ax4L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax4L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            if  RAxisRadioSeq(4), set(ax4R,'Visible','on'); else set(ax4R,'Visible','off'); end
            %set(ax4R,'Visible','off');
            if exist('h4_R'),  delete(h4_R); clear('h4_R'); end
            h4_R=line(CTDData(:,1),CTDData(:,5), 'Color', CLine4, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax4R, 'Visible','on');
            set(get(ax4R,'Ylabel'),'String',[CTDDataHeader{4+1}, ' (' CTDDataHeaderUnit{4+1} ')'],'FontWeight', CTDDataAxFontWgt{4+1})
            set(get(ax4R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax4R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            %----------
            if RPlotSeq(4)
             set(VarChoice4,'value',1);
             CBk4=Cseq(4,:);      set(VarChoice4,'BackgroundColor' , CBk4)
             set(h4_L,'Visible','on'); set(h4_R,'Visible','on');
            else
             set(VarChoice4,'value',0);
             CBk4=NoUseCseq(4,:); set(VarChoice4,'BackgroundColor' , CBk4)
             set(h4_L,'Visible','off'); set(h4_R,'Visible','off');
            end
           %----------
            
            %var5
            if  LAxisRadioSeq(5), set(ax5L,'Visible','on'); else set(ax5L,'Visible','off'); end
            %set(ax5L,'Visible','off');
            if exist('h5_L'),  delete(h5_L); clear('h5_L'); end
            h5_L=line(CTDData(:,1),CTDData(:,6), 'Color', CLine5, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax5L, 'Visible','on');
            set(get(ax5L,'Ylabel'),'String',[CTDDataHeader{5+1}, ' (' CTDDataHeaderUnit{5+1} ')'],'FontWeight', CTDDataAxFontWgt{5+1})
            set(get(ax5L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax5L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            if  RAxisRadioSeq(5), set(ax5R,'Visible','on'); else set(ax5R,'Visible','off'); end
            %set(ax5R,'Visible','off');
            if exist('h5_R'),  delete(h5_R); clear('h5_R'); end
            h5_R=line(CTDData(:,1),CTDData(:,6), 'Color', CLine5, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax5R, 'Visible','on');
            set(get(ax5R,'Ylabel'),'String',[CTDDataHeader{5+1}, ' (' CTDDataHeaderUnit{5+1} ')'],'FontWeight', CTDDataAxFontWgt{5+1})
            set(get(ax5R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax5R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            %----------
            if RPlotSeq(5)
             set(VarChoice5,'value',1);
             CBk5=Cseq(5,:);      set(VarChoice5,'BackgroundColor' , CBk5)
             set(h5_L,'Visible','on'); set(h5_R,'Visible','on');
            else
             set(VarChoice5,'value',0);
             CBk5=NoUseCseq(5,:); set(VarChoice5,'BackgroundColor' , CBk5)
             set(h5_L,'Visible','off'); set(h5_R,'Visible','off');
            end
           %----------
            
            %var6
            if  LAxisRadioSeq(6), set(ax6L,'Visible','on'); else set(ax6L,'Visible','off'); end
            %set(ax6L,'Visible','off');
            if exist('h6_L'),  delete(h6_L); clear('h6_L'); end
            h6_L=line(CTDData(:,1),CTDData(:,7), 'Color', CLine6, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax6L, 'Visible','on');
            set(get(ax6L,'Ylabel'),'String',[CTDDataHeader{6+1}, ' (' CTDDataHeaderUnit{6+1} ')'],'FontWeight', CTDDataAxFontWgt{6+1})
            set(get(ax6L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax6L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            if  RAxisRadioSeq(6), set(ax6R,'Visible','on'); else set(ax6R,'Visible','off'); end
            %set(ax6R,'Visible','off');
            if exist('h6_R'),  delete(h6_R); clear('h6_R'); end
            h6_R=line(CTDData(:,1),CTDData(:,7), 'Color', CLine6, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax6R, 'Visible','on');
            set(get(ax6R,'Ylabel'),'String',[CTDDataHeader{6+1}, ' (' CTDDataHeaderUnit{6+1} ')'],'FontWeight', CTDDataAxFontWgt{6+1})
            set(get(ax6R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax6R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            %----------
            if RPlotSeq(6)
             set(VarChoice6,'value',1);
             CBk6=Cseq(6,:);      set(VarChoice6,'BackgroundColor' , CBk6)
             set(h6_L,'Visible','on'); set(h6_R,'Visible','on');
            else
             set(VarChoice6,'value',0);
             CBk6=NoUseCseq(6,:); set(VarChoice6,'BackgroundColor' , CBk6)
             set(h6_L,'Visible','off'); set(h6_R,'Visible','off');
            end
           %----------
           
            %var7
            if  LAxisRadioSeq(7), set(ax7L,'Visible','on'); else set(ax7L,'Visible','off'); end
            %set(ax7L,'Visible','off');
            if exist('h7_L'),  delete(h7_L); clear('h7_L'); end
            h7_L=line(CTDData(:,1),CTDData(:,8), 'Color', CLine7, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax7L, 'Visible','off');
            set(get(ax7L,'Ylabel'),'String',[CTDDataHeader{7+1}, ' (' CTDDataHeaderUnit{7+1} ')'],'FontWeight', CTDDataAxFontWgt{7+1})
            set(get(ax7L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax7L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            if  RAxisRadioSeq(7), set(ax7R,'Visible','off'); else set(ax7R,'Visible','off'); end
            %set(ax7R,'Visible','off');
            if exist('h7_R'),  delete(h7_R); clear('h7_R'); end
            h7_R=line(CTDData(:,1),CTDData(:,8), 'Color', CLine7, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax7R, 'Visible','off');
            set(get(ax7R,'Ylabel'),'String',[CTDDataHeader{7+1}, ' (' CTDDataHeaderUnit{7+1} ')'],'FontWeight', CTDDataAxFontWgt{7+1})
            set(get(ax7R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax7R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            %----------
            if RPlotSeq(7)
             set(VarChoice7,'value',1);
             CBk7=Cseq(7,:);      set(VarChoice7,'BackgroundColor' , CBk7)
             set(h7_L,'Visible','on'); set(h7_R,'Visible','on');
            else
             set(VarChoice7,'value',0);
             CBk7=NoUseCseq(7,:); set(VarChoice7,'BackgroundColor' , CBk7)
             set(h7_L,'Visible','off'); set(h7_R,'Visible','off');
            end
           %----------

            %var8
            if  LAxisRadioSeq(8), set(ax8L,'Visible','on'); else set(ax8L,'Visible','off'); end
            %set(ax8L,'Visible','off');
            if exist('h8_L'),  delete(h8_L); clear('h8_L'); end
            h8_L=line(CTDData(:,1),CTDData(:,9), 'Color', CLine8, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax8L, 'Visible','off');
            set(get(ax8L,'Ylabel'),'String',[CTDDataHeader{8+1}, ' (' CTDDataHeaderUnit{8+1} ')'],'FontWeight', CTDDataAxFontWgt{8+1})
            set(get(ax8L,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax8L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            if  RAxisRadioSeq(8), set(ax8R,'Visible','on'); else set(ax8R,'Visible','off'); end
            %set(ax8R,'Visible','off');
            if exist('h8_R'),  delete(h8_R); clear('h8_R'); end
            h8_R=line(CTDData(:,1),CTDData(:,9), 'Color', CLine8, 'LineStyle', '-', 'Marker', '.', 'Parent',...
                       ax8R, 'Visible','off');
            set(get(ax8R,'Ylabel'),'String',[CTDDataHeader{8+1}, ' (' CTDDataHeaderUnit{8+1} ')'],'FontWeight', CTDDataAxFontWgt{8+1})
            set(get(ax8R,'Xlabel'),'String',[CTDDataHeader{1},   ' (' CTDDataHeaderUnit{1}   ')'],'FontWeight', CTDDataAxFontWgt{1})
            set(ax8R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
            %----------
            if RPlotSeq(8)
             set(VarChoice8,'value',1);
             CBk8=Cseq(8,:);      set(VarChoice8,'BackgroundColor' , CBk8)
             set(h8_L,'Visible','on'); set(h8_R,'Visible','on');
            else
             set(VarChoice8,'value',0);
             CBk8=NoUseCseq(8,:); set(VarChoice8,'BackgroundColor' , CBk8)
             set(h8_L,'Visible','off'); set(h8_R,'Visible','off');
            end
           %----------
            
            CTDFileName=get(D.VCTDfile,'string');
%             set(ax1R,'Visible','off'),
        %readit==0 not successful read
        else
            if length(CTDFileName)<2
              set(D.VCTDfile,'string','Please select a CTD datafile to load','value',0)
            end
            
        end %readit
      end
        
        %%%%%f = getframe (S.fh, [150 75 S.SCR(3)+1 S.SCR(4)+1]) ;
        %%%%%C = frame2im(f);
        %%%%%close(S.fh)
    end

    function [] = VarChoice_check_call(varargin)
        % Visualization variable selection Callback 
            
         if   get(VarChoice1,'value')==1
                           VisLine='on';  CBk1=Cseq(1,:);  
                           RPlotSeq(1)=1; LPlotSeq(1)=1;
         else
                           VisLine='off'; CBk1=NoUseCseq(1,:); 
                           RPlotSeq(1)=0; LPlotSeq(1)=0;
         end
         set(h1_L,'Visible',VisLine); set(h1_R,'Visible',VisLine);
         set(VarChoice1,'BackgroundColor' , CBk1)
         
         if   get(VarChoice2,'value')==1
                           VisLine='on';  CBk2=Cseq(2,:);
                           RPlotSeq(2)=1; LPlotSeq(2)=1;
         else
                           VisLine='off';  CBk2=NoUseCseq(2,:);
                           RPlotSeq(2)=0; LPlotSeq(2)=0;
         end
         set(h2_L,'Visible',VisLine); set(h2_R,'Visible',VisLine);
         set(VarChoice2,'BackgroundColor' , CBk2)
         
         if   get(VarChoice3,'value')==1
                           VisLine='on';  CBk3=Cseq(3,:);
                           RPlotSeq(3)=1; LPlotSeq(3)=1;
         else
                           VisLine='off'; CBk3=NoUseCseq(3,:);
                           RPlotSeq(3)=0; LPlotSeq(3)=0;
         end
         set(h3_L,'Visible',VisLine); set(h3_R,'Visible',VisLine);
         %set(B3m_LbarsL,'Visible',VisLine); set(B3m_LbarsR,'Visible',VisLine);
         %set(B3m_RbarsL,'Visible',VisLine); set(B3m_RbarsR,'Visible',VisLine);
         %set(B3M_LbarsL,'Visible',VisLine); set(B3M_LbarsR,'Visible',VisLine);
         %set(B3M_RbarsL,'Visible',VisLine); set(B3M_RbarsR,'Visible',VisLine);
         set(VarChoice3,'BackgroundColor' , CBk3)
         
         if   get(VarChoice4,'value')==1
                           VisLine='on';  CBk4=Cseq(4,:);
                           RPlotSeq(4)=1; LPlotSeq(4)=1;
         else
                           VisLine='off'; CBk4=NoUseCseq(4,:);
                           RPlotSeq(4)=0; LPlotSeq(4)=0;
         end
         set(h4_L,'Visible',VisLine); set(h4_R,'Visible',VisLine);
         set(VarChoice4,'BackgroundColor' , CBk4)
         
         if   get(VarChoice5,'value')==1
                           VisLine='on';  CBk5=Cseq(5,:);
                           RPlotSeq(5)=1; LPlotSeq(5)=1;
         else
                           VisLine='off'; CBk5=NoUseCseq(5,:);
                           RPlotSeq(5)=0; LPlotSeq(5)=0;
         end
         set(h5_L,'Visible',VisLine); set(h5_R,'Visible',VisLine);
         set(VarChoice5,'BackgroundColor' , CBk5)

         if   get(VarChoice6,'value')==1
                           VisLine='on';  CBk6=Cseq(6,:);
                           RPlotSeq(6)=1; LPlotSeq(6)=1;
         else
                           VisLine='off'; CBk6=NoUseCseq(6,:);
                           RPlotSeq(6)=0; LPlotSeq(6)=0;
         end
         set(h6_L,'Visible',VisLine); set(h6_R,'Visible',VisLine);
         set(VarChoice6,'BackgroundColor' , CBk6)

         if   get(VarChoice7,'value')==1
                           VisLine='on';  CBk7=Cseq(7,:);
                           RPlotSeq(7)=1; LPlotSeq(7)=1;
         else
                           VisLine='off'; CBk7=NoUseCseq(7,:);
                           RPlotSeq(7)=0; LPlotSeq(7)=0;
         end
         set(h7_L,'Visible',VisLine); set(h7_R,'Visible',VisLine);
         set(VarChoice7,'BackgroundColor' , CBk7)

         if   get(VarChoice8,'value')==1
                           VisLine='on';  CBk8=Cseq(8,:);
                           RPlotSeq(8)=1; LPlotSeq(8)=1;
         else
                           VisLine='off'; CBk8=NoUseCseq(8,:);
                           RPlotSeq(8)=0; LPlotSeq(8)=0;
         end
         set(h8_L,'Visible',VisLine); set(h8_R,'Visible',VisLine);
         set(VarChoice8,'BackgroundColor' , CBk8)
    end

    function [] = MinMaxVar_check_call(varargin)
        % Visualization variable selection Callback 
        xticks=[];
        if   get(MinVar3,'value')==1
                           VisLine='on';  MinMaxCBk1=Cseq(3,:);  
                           MinMaxPlotSeq(1)=1; % RPlotSeq(1)=1; LPlotSeq(1)=1;
                           if prod([size(mintabV3m) size(maxtabV3m) size(XX1V3m)])   %if normal mintab maxtab case 
                             xticks=[xticks XX1V3m(4,:)];
                           end
        else
                           VisLine='off'; MinMaxCBk1=NoUseCseq(3,:); 
                           MinMaxPlotSeq(1)=0; % RPlotSeq(1)=0; LPlotSeq(1)=0;
         end
         set(MINpV3Bp2,'Visible',VisLine); %set(h1_L,'Visible',VisLine); %set(h1_L,'Visible',VisLine); set(h1_R,'Visible',VisLine);
         set(MinVar3,'BackgroundColor' , MinMaxCBk1)
          
         if   get(MaxVar3,'value')==1
                           VisLine='on';  MinMaxCBk2=Cseq(3,:);
                           MinMaxPlotSeq(2)=1; % RPlotSeq(2)=1; LPlotSeq(2)=1;
                           if prod([size(mintabV3M) size(maxtabV3M) size(XX2V3M)])   %if normal mintab maxtab case 
                             xticks=[xticks XX2V3M(4,:)];
                           end
         else
                           VisLine='off';  MinMaxCBk2=NoUseCseq(3,:);
                           MinMaxPlotSeq(2)=0; % RPlotSeq(2)=0; LPlotSeq(2)=0;
         end
         set(MAXpV3Bp2,'Visible',VisLine); %set(h2_L,'Visible',VisLine); set(h2_R,'Visible',VisLine);
         set(MaxVar3,'BackgroundColor' , MinMaxCBk2)
         
         if   get(MinVar4,'value')==1
                           VisLine='on';  MinMaxCBk3=Cseq(6,:);
                           MinMaxPlotSeq(3)=1; % RPlotSeq(3)=1; LPlotSeq(3)=1;
                           if prod([size(mintabV4m) size(maxtabV4m) size(XX1V4m)])   %if normal mintab maxtab case 
                             xticks=[xticks  XX1V4m(4,:)];
                           end
         else
                           VisLine='off'; MinMaxCBk3=NoUseCseq(6,:);
                           MinMaxPlotSeq(3)=0; % RPlotSeq(3)=0; LPlotSeq(3)=0;
         end
         set(MINpV4Bp2,'Visible',VisLine); %set(h3_L,'Visible',VisLine); set(h3_R,'Visible',VisLine);
         set(MinVar4,'BackgroundColor' , MinMaxCBk3)
         
         if   get(MaxVar4,'value')==1
                           VisLine='on';  MinMaxCBk4=Cseq(6,:);
                           MinMaxPlotSeq(4)=1; % RPlotSeq(4)=1; LPlotSeq(4)=1;
                           if prod([size(mintabV4M) size(maxtabV4M) size(XX2V4M)])   %if normal mintab maxtab case 
                             xticks=[xticks  XX2V4M(1,:)];
                           end
         else
                           VisLine='off'; MinMaxCBk4=NoUseCseq(6,:);
                           MinMaxPlotSeq(4)=0; % RPlotSeq(4)=0; LPlotSeq(4)=0;
         end
         set(MAXpV4Bp2,'Visible',VisLine); %set(h4_L,'Visible',VisLine); set(h4_R,'Visible',VisLine);
         set(MaxVar4,'BackgroundColor' , MinMaxCBk4)
         
         xticks= unique(xticks);
         xtickslab=cellstr(num2str(xticks'));
         set(minmaxAxTicks,'XTick',xticks)
         set(minmaxAxTicks,'XTicklabel',xtickslab)

    end




    %D.BtnGrp radiobuttongroups 'SelectionChangeFcn' :@selchbk_call
    %function selchbk_call(D.BtnGrp,eventdata)

    function selchbk_call(source,eventdata)
        %disp(source);
        %disp([eventdata.EventName,'  ',... 
        %     get(eventdata.OldValue,'String'),'  ', ...
        %     get(eventdata.NewValue,'String')]);
        %disp(get(get(source,'SelectedObject'),'String'));
 
        %plot the necessary axis
    
                %Column 1 = depth       %CTDQTRM Col2
                %Column 2 = temperature       %CTDQTRM Col3
                %Column 3 = fluorescence       %CTDQTRM Col4
                %Column 4 = transmission       %CTDQTRM Col4
                %Column 5 = PAR       %CTDQTRM Col8
                %Column 6 = lnPAR       %CTDQTRM Col12
                %Column 7 = Ke
                %Column 8 = Interpolated Ke
                %Column 9 = 1% Depth (Piecewise)
                %Column 10 = 1% Depth (Linear)
                %Column 11 = Interpolated PAR
                %Column 12 = Smoothed PAR
                %Column 13 = Smoothed lnPAR
                %Column 14 = Smoothed Ke
                %Column 15 = Extrapolated Smoothed Ke
                %Column 16 = Smoothed 1% Depth (Piecewise)
                %Column 17 = Smoothed 1% Depth (Piecewise)
          DDDDD=get(source,'SelectedObject')
          %get(get(source,'SelectedObject'),'Tag')
          switch get(get(source,'SelectedObject'),'Tag') 
            case 'lnPAR',  res = 1; 'put Eventdata_new value on left & Eventdata_old value on right'
            case 'FLUOR',  res = 2;
            case 'XMISS',  res = 3;
            case 'T090C',  res = 4;
            case 'DO2',  res = 5;
            case 'Ke',  res = 6;
            case 'Var 7',  res = 7;
            case 'Var 8',  res = 8;
%             case 'Ke (interp, smoothed)',  res = 1; %put Eventdata_new value on left & Eventdata_old value on right
%             case 'Fluoresence',  res = 2;
%             case 'Transmission',  res = 3;
%             case 'Chlorophyl',  res = 4;
%             case 'ln(PAR) (smoothed - after loopedit)',  res = 5;
%             case 'Temperature',  res = 6;
            otherwise, res = '';
          end
        res;
    end

    function MinMaxselchbk_call(source,eventdata)
        %disp(source);
        %disp([eventdata.EventName,'  ',... 
        %     get(eventdata.OldValue,'String'),'  ', ...
        %     get(eventdata.NewValue,'String')]);
        %disp(get(get(source,'SelectedObject'),'String'));
 
        %plot the necessary axis
    
                %Column 1 = depth       %CTDQTRM Col2
                %Column 2 = temperature       %CTDQTRM Col3
                %Column 3 = fluorescence       %CTDQTRM Col4
                %Column 4 = transmission       %CTDQTRM Col4
                %Column 5 = PAR       %CTDQTRM Col8
                %Column 6 = lnPAR       %CTDQTRM Col12
                %Column 7 = Ke
                %Column 8 = Interpolated Ke
                %Column 9 = 1% Depth (Piecewise)
                %Column 10 = 1% Depth (Linear)
                %Column 11 = Interpolated PAR
                %Column 12 = Smoothed PAR
                %Column 13 = Smoothed lnPAR
                %Column 14 = Smoothed Ke
                %Column 15 = Extrapolated Smoothed Ke
                %Column 16 = Smoothed 1% Depth (Piecewise)
                %Column 17 = Smoothed 1% Depth (Piecewise)
          get(get(source,'SelectedObject'),'Tag')
          switch get(get(source,'SelectedObject'),'Tag') 
            case 'MIN XMISS',  res = 1; %put Eventdata_new value on left & Eventdata_old value on right
            case 'MAX XMISS',  res = 2;
            case 'MIN Ke',  res = 3;
            case 'MAX Ke',  res = 4;
%             case 'Ke (interp, smoothed)',  res = 1; %put Eventdata_new value on left & Eventdata_old value on right
%             case 'Fluoresence',  res = 2;
%             case 'Transmission',  res = 3;
%             case 'Chlorophyl',  res = 4;
%             case 'ln(PAR) (smoothed - after loopedit)',  res = 5;
%             case 'Temperature',  res = 6;
            otherwise, res = '';
          end
          res;
    end


    function [] = selchbk_callAxL(source,eventdata)
    % Left
    %axiL
        %if get(eventdata.NewValue,'Tag')<7
        %['set(ax' get(eventdata.NewValue,'Tag') 'L,''Visible'',''on'');']
        %['set(ax' get(eventdata.OldValue,'Tag') 'L,''Visible'',''off'');']
         eval(['set(ax' get(eventdata.OldValue,'Tag') 'L,''Visible'',''off'');']); 
         eval(['set(ax' get(eventdata.NewValue,'Tag') 'L,''Visible'',''on'');']);
          LAxisRadioSeq=ZeroRadioSeq;
          LAxisRadioSeq(str2num(get(eventdata.NewValue,'Tag')))=1; %Left Axis Button Settings

        %end
    end

	
    function [] = selchbk_callAxR(source,eventdata)
        % Right
        %if get(eventdata.NewValue,'Tag')<7
        %axiR
        %str2num(get(eventdata.NewValue,'Tag'));
        %str2num(get(eventdata.OldValue,'Tag'));
        %str2num(get(eventdata.NewValue,'Tag'))==8;
        %['set(ax' get(eventdata.NewValue,'Tag') 'R,''Visible'',''on'');']
        %['set(ax' get(eventdata.OldValue,'Tag') 'R,''Visible'',''off'');']
         eval(['set(ax' get(eventdata.OldValue,'Tag') 'R,''Visible'',''off'');']); 
         eval(['set(ax' get(eventdata.NewValue,'Tag') 'R,''Visible'',''on'');']);
           RAxisRadioSeq=ZeroRadioSeq;
           RAxisRadioSeq(str2num(get(eventdata.NewValue,'Tag')))=1; %Right Axis Button Settings
        %end
    end


    function [] = Rowplot_pb_call(source,eventdata)
        % Right
        %axiR
        depthchk=ginput(1);
        %!!!!!!!datacursormode and ancestor!!!!!!!!! TRY INSTEAD
        
        %set(ax3L,'Xlim',[0 floor(max(CTDData(:,1)))+1])
        if   0<depthchk(1) & depthchk(1)<floor(max(CTDData(:,1)))+1 ,
            STR=num2str(round(depthchk(1)*4)/4);  set(D.ScreenSwitch5,'string',[STR ' m'],'value',0)
        end
        %str2num(get(eventdata.NewValue,'Tag'));
        %str2num(get(eventdata.OldValue,'Tag'));
        %str2num(get(eventdata.NewValue,'Tag'))==8;
        %['set(ax' get(eventdata.NewValue,'Tag') 'R,''Visible'',''on'');']
        %['set(ax' get(eventdata.OldValue,'Tag') 'R,''Visible'',''off'');']
        % eval(['set(ax' get(eventdata.OldValue,'Tag') 'R,''Visible'',''off'');']); 
        % eval(['set(ax' get(eventdata.NewValue,'Tag') 'R,''Visible'',''on'');']);
        %   RAxisRadioSeq=ZeroRadioSeq;
        %   RAxisRadioSeq(str2num(get(eventdata.NewValue,'Tag')))=1; %Right Axis Button Settings
    end

    function [] = List_Choice_cb(source,eventdata)
       if  STEPslider
         ViewBefore='STEP';
       elseif TIMEslider
         ViewBefore='TIME';
       end
        TIMEslider=0; STEPslider=1;
       set(TimeChoice_but,'BackgroundColor',ButBKGrndC);
       set(ListChoice_but,'BackgroundColor','w');
       Viewswitch=1; 
       FList_pb_call;
    end

    function [] = sub3METER_cb(source,eventdata)
       if sub3M==1, 
           sub3M=0;
           set(sub3METER_but,'BackgroundColor',ButBKGrndC);
       elseif sub3M==0, 
           sub3M=1;
           set(sub3METER_but,'BackgroundColor','w');
       end
       set(ax1L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax1R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax2L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax2R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax3L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax3R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax4L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax4R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax5L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax5R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax6L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax6R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax7L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax7R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax8L,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(ax8R,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
       set(minmaxAxV3,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
                %set(minmaxAxV3,'visible','on')
       set(minmaxAxV4,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
                %set(minmaxAxV4,'visible','on')
       set(minmaxAxTicks,'Xlim',[3*sub3M floor(max(CTDData(:,1)))+1])
                %set(minmaxAxTicks,'visible','on')

    end

    function [] = Time_Choice_cb(source,eventdata)
       if  STEPslider
         ViewBefore='STEP';
       elseif TIMEslider
         ViewBefore='TIME';
       end
       TIMEslider=1; STEPslider=0;
       set(ListChoice_but,'BackgroundColor',ButBKGrndC);
       set(TimeChoice_but,'BackgroundColor','w');
       Viewswitch=1;
       FList_pb_call;
    end


    function [] = slider_callback(hObject,eventdata)
        %'mano'
        if TIMEslider
            %SDataTime
            %in Timeslider BlueK indexes sorted DataTime
            oldTIMESliderValue=SDataTime(BlueK);
            OldBlueK=BlueK;
            %sliderMin= get(hObject, 'Min') % correct
            %sliderMax = get(hObject, 'Max') % correct
            %format long
            sliderVal = get(hObject, 'Value');
            %format
            sliderStep = get(hObject, 'SliderStep'); % correct
            if oldTIMESliderValue<sliderVal ,
    %          SortedDistPosInd=find(SDataTime-sliderVal>0);
              if abs(sliderVal-oldTIMESliderValue)>1.5*sliderStep(1) & sliderVal<sliderMax
                  SortedDistPosInd=find(SDataTime-sliderVal>0);
                  SDataTimeDiff=SDataTime(2:end)-SDataTime(1:end-1);
                  if SDataTimeDiff(OldBlueK),
                      BlueK=SortedDistPosInd(1);
                  else
                      BlueK=min(BlueK+1,length(SDataTime));
                  end
                  
                  set(hObject, 'Value', SDataTime(BlueK))
                  %SDataTime(BlueK)
                  %newTIMESliderValue=SDataTime(BlueK)
                  
              else
                  BlueK=min(BlueK+1,length(SDataTime));
                  set(hObject, 'Value', SDataTime(BlueK))
                  %SDataTime
                  %newTIMESliderValue=SDataTime(BlueK)
                  
              end
            elseif oldTIMESliderValue>sliderVal ,
              if abs(oldTIMESliderValue-sliderVal)>1.5*sliderStep(1) & sliderVal>sliderMin
                  %sliderVal-SDataTime
                  SortedDistPosInd=find(sliderVal-SDataTime>0);
                  SDataTimeDiff=SDataTime(2:end)-SDataTime(1:end-1);
                  if SDataTimeDiff(OldBlueK-1),
                      BlueK=SortedDistPosInd(end);
                  else
                      BlueK=max(BlueK-1,1);
                  end
                  
                  set(hObject, 'Value', SDataTime(BlueK))
                  %SDataTime
                  %%'ide'
                  %newTIMESliderValue=SDataTime(BlueK)
              else
                  BlueK=max(BlueK-1,1);
                  set(hObject, 'Value', SDataTime(BlueK))
                  %SDataTime
                  %newTIMESliderValue=SDataTime(BlueK)
              end
            else
              %sliderVal==oldTIMESliderValue 
              %'no slider change and'
              %BlueK
            end
            %'baa',
            OldBlueK==BlueK;
            if ~ (OldBlueK==BlueK),
                %'was here'
               set(sliderLineDots2,'Xdata',SDataTime(BlueK) )
               VCTDfile_pb_call;
            end
        elseif STEPslider 
            oldSTEPSliderValue=BlueKSTEP;
            OldBlueKSTEP=BlueKSTEP;
            %only file's index is plotted in given file list (everything is pos. integer)
            sliderMinSTEP = get(hObject, 'Min'); % sliderMin should be 1
            sliderMaxSTEP = get(hObject, 'Max'); % sliderMax should be length(ListReturn)
            sliderValSTEP = get(hObject, 'Value');
            sliderStepSTEP = get(hObject, 'SliderStep'); % correct
            if oldSTEPSliderValue<sliderValSTEP & oldSTEPSliderValue<sliderMaxSTEP,
              if abs(oldSTEPSliderValue-sliderValSTEP)>1.5*sliderStepSTEP(1)
                  BlueKSTEP=min(max(oldSTEPSliderValue+1,round(sliderValSTEP) ), sliderMaxSTEP);%,'#1'
                 %[SortedDist SortedDistInd]=sort(DataTime-sliderval);
                 % SortedDistPosInd=find(SortedDist>0);
                 % BlueK=SortedDistInd(SortedDistPosInd(1))
                 % %BlueK=max(oldTIMESliderValue+1,round(sliderVal) );
              else
                  BlueKSTEP=min(BlueKSTEP+1,sliderMaxSTEP);%,'#2'
                  

              end
            elseif oldSTEPSliderValue>sliderValSTEP & oldSTEPSliderValue>sliderMinSTEP,
              if abs(oldSTEPSliderValue-sliderValSTEP)>1.5*sliderStepSTEP(1)
                   BlueKSTEP=max(min(oldSTEPSliderValue-1,round(sliderValSTEP) ),1);%,'#3'
                  %[SortedDist SortedDistInd]=sort(sliderval-DataTime);
                  % SortedDistPosInd=find(SortedDist>0);
                  % BlueK=SortedDistInd(SortedDistPosInd(1))
                  % %BlueK=min(oldTIMESliderValue-1,round(sliderVal) );
              else
                  BlueKSTEP=max(BlueKSTEP-1,sliderMinSTEP);%, '#4'
              end
            else
              %  ',hoho'
            end
            oldSTEPSliderValue=BlueKSTEP;
            if ~(OldBlueKSTEP==BlueKSTEP);
               set(sliderLineDots2,'Xdata',BlueKSTEP )
               VCTDfile_pb_call;
            end
            
        end
        
        
    end

% Callback function for "DeckCellMng" button
    %function [] = setVariables_cb(hObject, eventData)
    function [] = DeckCellMng_pb_call(hObject, eventData)
        % Call nested function that displays pop-up window
        DeckCellMng();
        
        function [] = DeckCellMng()
            % Allows user to set what variables and how many variables to display
            % in the lightgui variable selection panel
            
            DeckDataMaster=[];   
            DateDataMaster=[];
            figWidth = 500;
            figHeight = 600;
            cancelFlag = 0;
            
            %myData = guidata(mainFig);
            %currDataType = myData.data.currCTDDataType;
            %currVarsY = myData.data.currCTDVarsY;
            %currVarsX = myData.data.currCTDVarsX;
            
            DeckCellFig = figure ( 'windowstyle', 'modal', 'resize', 'off', ...
                'name','Manage DeckCell data', 'visible' ,'off', 'MenuBar', 'none', 'NumberTitle', 'off',...
                'Tag', 'DeckCellFigure', 'units','pixels', 'Position',[100 100 figWidth figHeight],...
                'CreateFcn', {@movegui,'center'});
            
            % Done button
            uicontrol('Style','PushButton','Units','Normalized','Position',[.25 .03 .15 .05],...
                'FontSize', 10,'String','Done','CallBack','uiresume(gcbf)');
            
            % Cancel button
            uicontrol('Style','PushButton','Units','Normalized','Position',[.6 .03 .15 .05],...
                'FontSize', 11,'String','Cancel','CallBack',{@callback_btn_cancel});
            
            % Action panel and text 
            ActionPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','Light Forcing', 'TitlePosition','centertop', 'Position',[0.3,0.88,0.4,0.1],...
                'Visible','off');
            StatusPanel = uipanel('Parent',DeckCellFig, 'units','normalized',...
                'Title','Status Msg:', 'TitlePosition','lefttop', 'Position',[0.05,0.1,0.85,0.09],...
                'Visible','off');
            ActionTypeText = uicontrol('Style','text', 'Parent',ActionPanel,...
                'String','Manage DeckCell Data', 'Units','normalized', 'Position',[0.0 0.1 1.0 .8],...
                'FontSize',10);%, 'FontWeight','bold');
            StatusTypeText = uicontrol('Style','text', 'Parent',StatusPanel,...
                'String','                                                                                                              ',...
                'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                'FontSize',6);%, 'FontWeight','bold');
            %get(StatusTypeText)
            set(ActionPanel,'Visible','on');
            set(StatusPanel,'Visible','on');
            
            % create a panel containing variable text and drop-down menu
            function[] = createActionPanel(pos, textStr, ActionTag, CallbackTag,TooltipTag)
                singleActionPanel = uipanel('Parent',DeckCellFig, 'Units','normalized', 'Position',pos);
                uicontrol('Style','text', 'Parent',singleActionPanel, 'String',textStr,...
                    'Units','normalized', 'Position', [0.05 0 0.3 1], 'FontSize',12);
                uicontrol('Style','pushbutton', 'Parent',singleActionPanel,...
                    'String',{ActionTag}, 'Value',1, 'Tag', ActionTag,...
                    'Units','normalized', 'Position',[0.55 0.1 0.35 .8], 'FontSize',12,...
                    'FontWeight','bold', 'Callback', CallbackTag,'ToolTip',TooltipTag);
            end
            
            
            %DeckCell module functionalities:
            
            %X Display light data for dates compiled file
            %X Display light data from selected source files
            %X Check light data (files) against data in a  compiled/master file
            %X Add light data (files) to compiled/master file
            %X Concatenate light data (from master file) to selected  (*.cnv files?)
            %X Export data from master file (to *.txt?)
            
            textStrings = {'View data in Master file',...
                           'View data in Source files',...
                           'Check Source against Master',...
                           'Add Source to Master',...
                           'Adjust PAR using Master',...
                           'Export Master file'};%CTD data
            ActionTags =  {'View M', 'View S', 'Check', 'Add', 'Adjust','Export'};
            CallbackTags ={@DCViewM_cb, @DCViewS_cb, @DCCheck_cb, @DCAdd_cb, @DCAdjust_cb,@DCExport_cb};
            TooltipTags=  {'View data in Master file (plots)',...
                           'View selected Deck light data file content (plot)',...
                           'Check Deck light data file against Master file',...
                           'Add Deck light data file(s) (*.txt) to Deck light Master file',...
                           sprintf('Using Deck light measurements from Master, and raw CTD data (from *.cnv format files),\nPAR values are adjusted to factor out light forcing variations, \nand the adjusted CTD Data is saved to *PADJ.cnv file'),...
                           'Export Deck light Master file content into *.txt file'};

            %Layout
            % iteratively create single act panels
            pos = [0.1 0.75 0.8 0.1];
            for i = 1:6
                createActionPanel(pos, textStrings{i}, ActionTags{i},CallbackTags{i},TooltipTags{i});
                %CallbackTags{i}
                pos = pos + [0 -0.11 0 0];
            end
            
            set(DeckCellFig,'Visible','on');
            uiwait(DeckCellFig);
            
            if ~cancelFlag
               % save selected variables to guidata's currCTDVarsX and currCTDVarsY
%                myData.data.currCTDVarsX = currVarsX;
%                myData.data.currCTDVarsY = currVarsY;
%                % save selected variables to preferred defaults
%                if strcmpi(currDataType,'Z')
%                    myData.data.preferredCTDVarsZY = currVarsY;
%                    myData.data.preferredCTDVarsZX = currVarsX;
%                elseif strcmpi(currDataType,'N2')
%                    myData.data.preferredCTDVarsN2Y = currVarsY;
%                    myData.data.preferredCTDVarsN2X = currVarsX;
%                end
%                guidata(mainFig, myData);
               
               % update variable selection panel
%               updateVarSelPanel();
            end
            
%           clear myData
            close(DeckCellFig);
                        
            %DeckCell Mngmnt callback functions
            %@DCViewM_cb, @DCViewS_cb, @DCCheck_cb, @DCAdd_cb, @DCAdjust_cb,@DCExport_cb
         
            function [] = DCViewM_cb(source, eventdata)
                boo=1
                cancelFlag = 0;
                    DeckCellMasterDir=prefdir;
                    nowDir=pwd;
                    cd(DeckCellMasterDir);
                    yearStr='2014'
                    MastFile=['DeckCellMaster', yearStr,'.mat']
                    try
                        Mdata =load(MastFile); 'MM';
                        DeckDataMaster=Mdata.DeckDataMaster;
                        DateDataMaster=Mdata.DateDataMaster;
                        %insert current data in master file if needed
                        %FirstTimeStampMaster=DateDataMaster(1); 
                        %LastTimeStampMaster=DateDataMaster(end);
                        %cd(nowDir);
                        %getting rid of duplicate lines
                        figure(51)
                        size(DateDataMaster),
                        size(DeckDataMaster),
                        plot(DateDataMaster,DeckDataMaster(:,end),'b.')    
                        xlabel('Time (hour:minute)')
                        datetick('x','HH:MM')
                        title(['Master Deck Light Data: '  datestr(DateDataMaster(1)) ' -- '   datestr(DateDataMaster(end))  ';  FILE: ' MastFile])
                        set(StatusTypeText,'String',...
                            '                                                                          ',...
                            'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                            'FontSize',6)
                        pause(1);
                        %picName = strcat(datestr(DateData(1),'yymmdd'),'DeckCellLight_',fileName(1:end-4));
                    catch
                        %disp(strcat('Could not load ', saveFile, '. Creating new file instead.'));
                        set(StatusTypeText,'String',...
                            strcat('Could not load Masterfile ', MastFile, '. Try to add Source file ( .txt Deck-datafile) to Master.'),...                                                                                        ',...
                            'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                            'FontSize',6)
                        pause(1);
                    end
                    cd(nowDir);
                %uiresume(gcbf);
            end
            
            function [] = DCViewS_cb(source, eventdata)
              %boo=2
              cancelFlag = 0;
              cancelFlag = 0;
              [fileName, folderPath] = uigetfile( ...
                  {...
                  '*.txt','DeckCell Data (*.txt)';...
                   '*.*',  'All files (*.*)'}, ...
                   'Pick a file'...
                         );       
              filePath = [folderPath fileName];
              if isequal(fileName,0) || isequal(folderPath,0)
                   ;
              else

                    LF=sprintf ('\n'); CR=sprintf ('\r'); Tab=sprintf ('\t');

                    %read in file
                    %filePath
                    fileText = fileread(filePath);
                    %select data lines using linefeed character
                    LineEnds=strfind(fileText,LF);
                    LineBegs=strfind(fileText,LF)+1; 
                    LineBegs=[1 LineBegs(1:end-1)]; %include first line and exclude last index
                    % fill up DeckData
                    % Format:(LiCorcode(1s only) , Time(datenum), Time(datestr), Intensity )
%                    DateData=[];
                    DateData=zeros(1:length(LineBegs),2);
                    DeckData=zeros(1:length(LineBegs),2);

                    lineStoreNum=0;
                    for lineNum=1:length(LineBegs)
                        lineText = fileText(LineBegs(lineNum):LineEnds(lineNum));
                        LTabs=strfind(lineText,Tab);
                        %if length(LTabs)>1                            
                         if length(LTabs)==2                            
                            if str2double(lineText(1))==1
                                lineStoreNum=lineStoreNum+1;
                                try
                                  column1txt = lineText(1); % control flag
                                  column2txt = lineText(LTabs(1)+1:LTabs(2)-1); % timestamp
                                  column3txt = lineText(LTabs(2)+1:end); % intensity
                                  %if length(Ltabs)>2
                                  %    column3txt = lineText(LTabs(2)+1:LTabs(3)-1); % intensity
                                  %else
                                  %    column3txt = lineText(LTabs(2)+1:end); % intensity
                                  %end
                                  DeckData(lineStoreNum,1)=str2double(column1txt); % save control flag
                                  timeNum=datenum(column2txt,'yyyy-mm-dd HH:MM:SS'); % save timestamp in datenum form for comparison purposes
                                  timeStr=datestr(timeNum,'yyyy-mm-dd HH:MM:SS.FFF'); % convert datenum to datestr (millisecond precision)
                                  %timeStr=datestr(timeNum,'yyyy-mm-dd HH:MM:SS'); % convert datenum to datestr (millisecond precision)
                                  DeckData(lineStoreNum,2)=timeNum;
                                  DateData(lineStoreNum,1)=timeNum;
                                  DeckData(lineStoreNum,3:8)=datevec(timeStr,'yyyy-mm-dd HH:MM:SS.FFF');
                                  %DeckData(lineNum,3:8)=datevec(timeStr,'yyyy-mm-dd HH:MM:SS');
                                  if isempty(str2double(column3txt))
                                     DeckData(lineStoreNum,9)=NaN;
                                  else
                                     DeckData(lineStoreNum,9)=str2double(column3txt); % save intensity data
                                  end
                                catch
                                  DeckLightErrorLine=lineNum  
                                  lineStoreNum=lineStoreNum-1;
                                  continue;    
                                end
                            end
                         end
                    end
 %                   if size(DeckData,1)
 %                      DMax=max(DeckData(:,9));
 %                      DMaxRow=find( DeckData(:,9)==max(DeckData(:,9)) );
 %                   end
                    %getting rid of contrl lines in data
                    DeckData=DeckData((DateData>0),:);
                    DateData=DateData(DateData>0);

                    DateData=DateData( ~isnan(DeckData(:,end)) );
                    DeckData=DeckData( ~isnan(DeckData(:,end)) ,:);

                    %getting rid of duplicate lines
                    [Y,IN]=unique(DateData);
                    DateData=DateData(IN);
                    DeckData=DeckData(IN,:);

                   %plot file content (light)
                    figure(50)
                         plot(DateData,DeckData(:,end),'r.')    
                         xlabel('Time (hour:minute)')
                         datetick('x','HH:MM')
                         title(['Deck Light Data: '  datestr(DateData(1)) ' -- '   datestr(DateData(end))  ';  FILE: ' fileName])
                    %save picture?
                    %picName = strcat(datestr(DateData(1),'yymmdd'),'DeckCellLight_',fileName(1:end-4));

              end
              %uiresume(gcbf);
            end
            
            function [] = DCCheck_cb(source, eventdata)
                %boo=3
                cancelFlag = 0;
                %uiresume(gcbf);
            end
            function [] = DCAdd_cb(source, eventdata)
                %boo=4
                cancelFlag = 0;
                
                LF=sprintf ('\n'); CR=sprintf ('\r'); Tab=sprintf ('\t');

                %save file names
                saveFile = 'DeckCellMaster.mat';
                %deckMasterFile= 'DeckCellMaster.mat';
                %saveFileXLS = 'DeckCellMaster.xls';
                %saveFileHeader = 'DeckCellMasterwHeader.asci';
                DeckCellMasterDir=prefdir;
                nowDir=pwd;
                FileHeader = {'Date(yyyy-mm-dd HH:MM:SS.FFF)' 'Intensity'};

                %Compile selected Neeskay deck cell light data into a Deck Cell Lightdata Masterfile***')
                % Select  Neeskay deck cell light data directory and files (LiCor logfiles .txt extention) ')
                %           from cruise log data to add to the Deck Cell Lightdata Master File )')
                 
                %select field LiCor files containing light data for procelineNuming
                files = uipickfiles('prompt',' Select LiCor deck-cell  light intensity files to add to MasterFile and to plot and view (*.txt) ',...
                                    'out','struct' );
                if isstruct(files)
                    %procelineNum field files
                    for fileNum=1:length(files)
                        %file name with path
                        filePath=files(fileNum).name;
                        backSlashes=strfind(filePath,'\');
                        %file name without path
                        fileName=filePath(backSlashes(end)+1:end);
                        %path of folder containing file
                        folderPath=filePath(1:backSlashes(end)-1);
                        %read in file
                        fileText = fileread(filePath);
                        %select data lines using linefeed character
                        LineEnds=strfind(fileText,LF);
                        LineBegs=strfind(fileText,LF)+1; 
                        LineBegs=[1 LineBegs(1:end-1)]; %include first line and exclude last index
                        % fill up DeckData
                        % Format:(LiCorcode(1s only) , Time(datenum), Time(datestr), Intensity )
                        DateData=[]; DeckData=[];

                        lineStoreNum=0;
                        for lineNum=1:length(LineBegs)
                            lineText = fileText(LineBegs(lineNum):LineEnds(lineNum));
                            LTabs=strfind(lineText,Tab);
                             if length(LTabs)>1                            
                             %if length(LTabs)==2                            
                                if str2double(lineText(1))==1
                                    lineStoreNum=lineStoreNum+1;
                                    try
                                      column1txt = lineText(1); % control flag
                                      column2txt = lineText(LTabs(1)+1:LTabs(2)-1); % timestamp
                                      column3txt = lineText(LTabs(2)+1:end); % intensity
                                      %if length(Ltabs)>2
                                      %    column3txt = lineText(LTabs(2)+1:LTabs(3)-1); % intensity
                                      %else
                                      %    column3txt = lineText(LTabs(2)+1:end); % intensity
                                      %end
                                      DeckData(lineStoreNum,1)=str2double(column1txt); % save control flag
                                      timeNum=datenum(column2txt,'yyyy-mm-dd HH:MM:SS'); % save timestamp in datenum form for comparison purposes
                                      timeStr=datestr(timeNum,'yyyy-mm-dd HH:MM:SS.FFF'); % convert datenum to datestr (millisecond precision)
                                      %timeStr=datestr(timeNum,'yyyy-mm-dd HH:MM:SS'); % convert datenum to datestr (millisecond precision)
                                      yearStr=timeStr(1:4);
                                      DeckData(lineStoreNum,2)=timeNum;
                                      DateData(lineStoreNum,1)=timeNum;
                                      DeckData(lineStoreNum,3:8)=datevec(timeStr,'yyyy-mm-dd HH:MM:SS.FFF');
                                      %DeckData(lineNum,3:8)=datevec(timeStr,'yyyy-mm-dd HH:MM:SS');
                                      if isempty(str2double(column3txt))
                                         DeckData(lineStoreNum,9)=NaN;
                                      else
                                         DeckData(lineStoreNum,9)=str2double(column3txt); % save intensity data
                                      end
                                    catch
                                      DeckLightErrorLine=lineNum  %reporting problem lines
                                      lineStoreNum=lineStoreNum-1;
                                      continue;    
                                    end
                                  end
                            end
                        end
                        if size(DeckData,1)>0
                            DMax=max(DeckData(:,9));
                            DMaxRow=find( DeckData(:,9)==max(DeckData(:,9)) );
                        else
                            'Empty datafile'
                        end
                        %getting rid of contrl lines in data
                        DeckData=DeckData((DateData>0),:);
                        DateData=DateData(DateData>0);

                        DateData=DateData( ~isnan(DeckData(:,end)) );
                        DeckData=DeckData( ~isnan(DeckData(:,end)) ,:);

                        %getting rid of duplicate lines
                        [Y,IN]=unique(DateData);
                        DateData=DateData(IN);
                        DeckData=DeckData(IN,:);

                        %change current directory to folder containing file, so pictures are
                        %saved in correct folder
                        cd(folderPath)

    %                    %save plot picture
    %                   print('-djpeg90',picName);

                        %Note current file time stamp for later comparisons
                        FirstTimeStamp=DateData(1); LastTimeStamp=DateData(end);

                        % Load master file. If doesn't exist, save current data into new file.
                        %cd ('C:\Users\yangxie\Documents\LMPAR2.0\DeckCell');
                        %cd ('C:\myfiles\pisti\cuhel\LightcodeXie\NewFolder\130725SRdata\130725DeckCell');

                        cd(DeckCellMasterDir);
                        if size(DateData,1)
                            %deckMasterFile=['DeckCellMaster', yearStr,'.mat'];
                            saveFile=['DeckCellMaster', yearStr,'.mat'];

                            try
                                %loadDeck=load(deckMasterFile);
                                loadDeck=load(saveFile);
                                DeckDataMaster=loadDeck.DeckDataMaster;
                                DateDataMaster=loadDeck.DateDataMaster;
                                %load(saveFile);
                            catch
                                %disp(strcat('Could not load ', saveFile, '. Creating new file instead.'));
                                set(StatusTypeText,'String',...
                                    strcat('Could not load Masterfile', saveFile, '. Creating new file instead.'),...                                                                                        ',...
                                    'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                                    'FontSize',6)
                                pause(1);
                                DeckDataMaster = DeckData;
                                DateDataMaster = DateData;
                                save(saveFile, 'DeckDataMaster', 'DateDataMaster');
                            end

                            %insert current data in master file if needed
                            FirstTimeStampMaster=DateDataMaster(1); LastTimeStampMaster=DateDataMaster(end);
                            if FirstTimeStampMaster>LastTimeStamp %insert sample data in front of master data
                                 DeckDataMaster=[DeckData; DeckDataMaster];
                                 DateDataMaster=[DateData; DateDataMaster];
                                 save(saveFile, 'DeckDataMaster', 'DateDataMaster');
                            elseif FirstTimeStamp>LastTimeStampMaster %insert sample data after master data
                                 DeckDataMaster=[DeckDataMaster; DeckData];
                                 DateDataMaster=[DateDataMaster; DateData];
                                 save(saveFile, 'DeckDataMaster', 'DateDataMaster');
                            else 
                                 DeckDataMaster=[DeckDataMaster; DeckData]; %insert sample data after master data and resort
                                 DateDataMaster=[DateDataMaster; DateData];
                                 [Y,IN]=unique(DateDataMaster);
                                 DateDataMaster=DateDataMaster(IN);
                                 DeckDataMaster=DeckDataMaster(IN,:);
                                 save(saveFile, 'DeckDataMaster', 'DateDataMaster');
                            end

                            set(StatusTypeText,'String',...
                                'Masterfile is Updated                                                                                         ',...
                                'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                                'FontSize',6,'ForegroundColor','r')
                            pause(.2);
                            set(StatusTypeText,'String',...
                                ['Updating DeckCell  Masterfile ... processing file  ', fileName ,'                                             '],...
                                'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                                'FontSize',6,'ForegroundColor','r')
                            pause(1);
                            if fileNum==length(files)
                                set(StatusTypeText,'String',...
                                    '                                                                                                              ',...
                                    'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                                    'FontSize',6,'ForegroundColor','r')
                                pause(.2);
                                set(StatusTypeText,'String',...
                                    'Masterfile is Updated                                                                                         ',...
                                    'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                                    'FontSize',6,'ForegroundColor','r')
                                pause(2);
                                set(StatusTypeText,'String',...
                                    '                                                                                                              ',...
                                    'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                                    'FontSize',6)
                            end
                        else
                            %Empty datafile.   No update to Masterfile. 
                            set(StatusTypeText,'String',...
                                'Empty datafile.   No update to Masterfile.                                                                    ',...
                                'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                                'FontSize',6,'ForegroundColor','r')
                            pause(1);
                        end

                        
                        cd(nowDir);
                    end
                end               
                cd(nowDir);
                %uiresume(gcbf);
            end
            function [] = DCAdjust_cb(source, eventdata)
                %boo=5
                cancelFlag = 0;
                LF=sprintf ('\n'); CR=sprintf ('\r'); Tab=sprintf ('\t'); SPC=sprintf(' ');
                deckMasterFile = 'DeckCellMaster.mat';
                %saveFile = 'DeckCellMaster.mat';
                %saveFileXLS = 'DeckCellMaster.xls';
                %saveFileHeader = 'DeckCellMasterwHeader.asci';
                DeckCellMasterDir=prefdir;
                nowDir=pwd;
                %select CTD  files for PAR adjustment
                files = uipickfiles('FilterSpec', '*.cnv','prompt',' Select unprocessed CNV files (*.cnv) for PAR Adjustment using Deck light data','out','struct' );
                for fileNum=1:length(files)
    
                    %file name with path
                    filePath=files(fileNum).name;
                    backSlashes=strfind(filePath,'\');

                    %file name without path & folder path
                    folderPath=filePath(1:backSlashes(end));
                    fileName=filePath(backSlashes(end)+1:end);

                    %read in file
                    fileText = fileread(filePath);

                    %find start time
                    startTimeBeg = strfind(fileText,'<startTime>');
                    startTimeEnd = strfind(fileText,'</startTime>');
                    startTime = fileText(startTimeBeg+11:startTimeEnd-1)
                    startTimeNum = datenum(startTime,'yyyy-mm-ddTHH:MM:SS');

                    %find end of header
                    headerEnd = strfind(fileText,'*END*');
                    headerText=fileText(1:headerEnd+6);
                    fileText = fileText(headerEnd+7:end);

                    %select data lines using linefeed character
                    LineEnds=strfind(fileText,LF);
                    LineBegs=strfind(fileText,LF)+1;
                    LineBegs=[1 LineBegs(1:end-1)]; %include first line and exclude last index

                    %fill up CTD Data (columns are separated by spaces)

                    %     # name 1 = prdM: Pressure, Strain Gauge [db]
                    %     # name 2 = depFM: Depth [fresh water, m]
                    %     # name 3 = t090C: Temperature [ITS-90, deg C]
                    %     # name 4 = v4: Voltage 4
                    %     # name 5 = CStarTr0: Beam Transmission, WET Labs C-Star [%]
                    %     # name 6 = c0uS/cm: Conductivity [uS/cm]
                    %     # name 7 = specc: Specific Conductance [uS/cm]
                    %     # name 8 = par: PAR/Irradiance, Biospherical/Licor
                    %     # name 9 = oxsolMg/L: Oxygen Saturation, Garcia & Gordon [mg/l]
                    %     # name 10 = ph: pH
                    %     # name 11 = orp: Oxidation Reduction Potential [mV]
                    %     # name 12 = wl0: RS-232 WET Labs raw counts 0
                    %     # name 13 = wl1: RS-232 WET Labs raw counts 1
                    %     # name 14 = wl2: RS-232 WET Labs raw counts 2
                    %     # name 15 = timeS: Time, Elapsed [seconds]
                    %     # name 16 = flag:  0.000e+00

                    clear CTDRawCNVData
                    'clear CTDRawCNVData'
                    for lineNum=1:length(LineBegs)
                        lineText = fileText(LineBegs(lineNum):LineEnds(lineNum));
                        LSpaces=strfind(lineText,SPC);
                        if length(LSpaces)>1
                            colNum = 1;
                            for spaceIndex=1:length(LSpaces)-1 %find starting indices of values by marking out contiguous spaces
                                spaceBeg = LSpaces(spaceIndex+1);
                                spaceEnd = LSpaces(spaceIndex);
                                if (spaceBeg-spaceEnd)>1 %extract values between spaces and add to CTDRawCNVData columnwise
                                    valueBeg = spaceEnd+1;
                                    valueEnd = spaceBeg-1;
                                    if colNum==8 & lineNum<100, V8B=valueBeg; V8E=valueEnd; end
                                    value = lineText(valueBeg:valueEnd);
                                    CTDRawCNVData(lineNum,colNum)=str2double(value);
                                    colNum = colNum + 1;
                                end
                            end
                            %append last column value
                            value = lineText(LSpaces(end)+1:end);
                            CTDRawCNVData(lineNum,colNum)=str2double(value);
                        end
                    end
                    %V8B,V8E, 8th column beg and end
                    
                    %     # name 1 = prdM: Pressure, Strain Gauge [db]
                    %     # name 2 = depFM: Depth [fresh water, m]
                    %     # name 3 = t090C: Temperature [ITS-90, deg C]
                    %     # name 4 = v4: Voltage 4
                    %     # name 5 = CStarTr0: Beam Transmission, WET Labs C-Star [%]
                    %     # name 6 = c0uS/cm: Conductivity [uS/cm]
                    %     # name 7 = specc: Specific Conductance [uS/cm]
                    %     # name 8 = par: PAR/Irradiance, Biospherical/Licor
                    %     # name 9 = oxsolMg/L: Oxygen Saturation, Garcia & Gordon [mg/l]
                    %     # name 10 = ph: pH
                    %     # name 11 = orp: Oxidation Reduction Potential [mV]
                    %     # name 12 = wl0: RS-232 WET Labs raw counts 0
                    %     # name 13 = wl1: RS-232 WET Labs raw counts 1
                    %     # name 14 = wl2: RS-232 WET Labs raw counts 2
                    %     # name 15 = timeS: Time, Elapsed [seconds]
                    %     # name 16 = flag:  0.000e+00
                    %     # name 17 = deck: interpolated deck cell readings
                    %     # name 18 = AdjPARCol: Adjusted PAR (using DeckCell measurements, PAR data is normalized  wr. to initial Deck value) 

                    % Load master deck cell .mat file

                    cd(DeckCellMasterDir);
                    startTimeYearStr=startTime(1:4);
                    deckMasterFile = ['DeckCellMaster', startTimeYearStr ,'.mat']

                    
                  if exist(deckMasterFile,'file')==2
                    loadDeck=load(deckMasterFile);
                    DeckDataMaster=loadDeck.DeckDataMaster;
                    size(DeckDataMaster)
                    DateDataMaster=loadDeck.DateDataMaster;
                    
                    % Locate corresponding time in DeckDataMaster
                    startTimeIndex = find(DeckDataMaster(:,2)==startTimeNum),

                    % Interpolate deck readings for time period (linear)
                    %samplesPerSec = 16;
                    intDeckCol = zeros(size(CTDRawCNVData,1),1); % pre-allocate memory for column %17

%                    ctdRow = 1;
%                    deckRow = startTimeIndex;
%                     while ctdRow <= size(CTDRawCNVData,1)
%                         slope = (DeckDataMaster(deckRow+1,9) - DeckDataMaster(deckRow,9)) / 16.0;
%                         while mod(ctdRow,16) > 0 && ctdRow <= size(CTDRawCNVData,1)
%                             intDeckCol(ctdRow,1) = DeckDataMaster(deckRow,9) + (mod(ctdRow - 1,16)) * slope;
%                             ctdRow = ctdRow + 1;
%                         end
%                         if mod(ctdRow,16) == 0 && ctdRow <= size(CTDRawCNVData,1)
%                             intDeckCol(ctdRow,1) = DeckDataMaster(deckRow,9) + (mod(ctdRow - 1,16)) * slope;
%                             deckRow = deckRow + 1;
%                             ctdRow = ctdRow + 1;
%                         end
%                     end

                    'egy'

                    size(CTDRawCNVData,1)
                    %samplesPerSec = 16;
                    ctdRow = 1;
                    deckRow = startTimeIndex;

                    MaxDDMS=0;
                    while ctdRow <= size(CTDRawCNVData,1)
                        DeckDataMasterStep=round((DeckDataMaster(deckRow+1,2) - DeckDataMaster(deckRow,2))*24*3600); %in seconds
                        MaxDDMS=max(MaxDDMS,DeckDataMasterStep);
                        slope = (DeckDataMaster(deckRow+1,9) - DeckDataMaster(deckRow,9)) /  DeckDataMasterStep /16.0;
                        %ctdRow%%%
                        while mod(ctdRow,16*DeckDataMasterStep) > 0 & ctdRow <= size(CTDRawCNVData,1)
                            intDeckCol(ctdRow,1) = DeckDataMaster(deckRow,9) + (mod(ctdRow - 1,16*DeckDataMasterStep)) * slope;
                            ctdRow = ctdRow + 1;
                        end
                        if mod(ctdRow,16*DeckDataMasterStep) == 0 & ctdRow <= size(CTDRawCNVData,1)
                            intDeckCol(ctdRow,1) = DeckDataMaster(deckRow,9) + (mod(ctdRow - 1,16*DeckDataMasterStep)) * slope;
                            deckRow = deckRow + 1;%%%
                            ctdRow = ctdRow + 1;%%%%
                        end
                    end        
                    MaxDDMS

                    'ketto'
                    
                    % intDeckCol

                    %Par Adjustment using deckcell light
                    %intDeckCol/intDeckCol(1)
                    AdjPARCol = CTDRawCNVData(:,8)./( intDeckCol/intDeckCol(1) ) ;

                    %Adjusting Data for output
                    for lineNum=1:length(LineBegs)
                        lineText = fileText(LineBegs(lineNum):LineEnds(lineNum));
                        lineText(V8B:V8E)=sprintf('%0.4d',double(AdjPARCol(lineNum))+.000001); %+.000001 correction -- we are in trouble when we print integers!
                        fileText(LineBegs(lineNum):LineEnds(lineNum))=lineText;
                    end
                    
                    %Save adjusted data
                    cd(folderPath) %change back to directory to save
                    %fileTextOut=[headerText fileText]; % contents to save
                    SaveAdjfileName=[fileName(1:end-4) 'PARADJ.cnv'];
                    fid = fopen( SaveAdjfileName,'w'); %Open file. Note that this will discard existing data!
                    if fid ~= -1
                        fprintf(fid,'%s',[headerText fileText]);       %# Print the file content (string)
                        fclose(fid);                     %# Close the file
                        set(StatusTypeText,'String',...
                            ['PAR column adjustment for raw CNV file' fileName 'has been finished'] ,...
                            'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                            'FontSize',6,'ForegroundColor','r')
                        pause(1);
                        set(StatusTypeText,'String',...
                            ['Adjusted CNV file' SaveAdjfileName 'has been saved'] ,...
                            'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                            'FontSize',6,'ForegroundColor','r')
                        pause(2);
                        set(StatusTypeText,'String',...
                            ['                                                                 '] ,...
                            'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                            'FontSize',6,'ForegroundColor','r')
                        pause(2);
                    end
                    
                    'harom'
                    
                    %Back to original Dir
                    cd(nowDir);
                    %***********************
                    %CTDRawCNVData = [CTDRawCNVData intDeckCol];
                  else
                     errordlg('Full DeckCell data is not available in Masterfile for this CTD cast. Try loading Deckcell data to Master!','DC-Adjustment not possible.');    
                  end %end of DeckcellMaster check (if)

                end %end of fileList loop

                %uiresume(gcbf);
            end
            function [] = DCExport_cb(source, eventdata)
                %boo=6
                cancelFlag = 0;
                %save file names
                saveFile = 'DeckCellMaster.mat';
                saveFileXLS = 'DeckCellMaster.xls';
                saveFileHeader = 'DeckCellMasterwHeader.asci';
                DeckCellMasterDir=prefdir;
                nowDir=pwd;
                cd(DeckCellMasterDir);

                FileHeader = {'Date(yyyy-mm-dd HH:MM:SS.FFF)' 'Intensity'};

                %save master file with headers in txt format
                try
                    loadDeck=load(saveFile);
                    DeckDataMaster =loadDeck.DeckDataMaster;
                    DateDataMaster =loadDeck.DateDataMaster;
                catch
                    %disp(strcat('Could not load ', saveFile, '. Creating new file instead.'));
                    set(StatusTypeText,'String',...
                        strcat('Could not load Masterfile', saveFile, '. Creating new file instead.'),...                                                                                        ',...
                        'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                        'FontSize',6)
                    pause(2);
                    DeckDataMaster = DeckData;
                    DateDataMaster = DateData;
                    save(saveFile, 'DeckDataMaster', 'DateDataMaster');
                end

                cd(nowDir);
                %load(saveFile);
                saveData = DeckDataMaster(:,3:end);
                dateCell = cellstr(datestr(saveData(:,1:6),'yyyy-mm-dd HH:MM:SS.FFF'));
                %dateCell = cellstr(datestr(saveData(:,1:6),'yyyy-mm-dd HH:MM:SS'));
                intensityCell = cellstr(num2str(saveData(:,7)));

                saveCell = [FileHeader; dateCell intensityCell];
                ex2 = cellfun(@ex_func, saveCell, 'UniformOutput', 0);
                size_ex2 = cellfun(@length,ex2,'UniformOutput',0);
                str_length = max(max(cell2mat(size_ex2)));
                ex3 = cellfun(@(x) ex_func2(x,str_length),ex2,'uniformoutput',0);
                ex4 = cell2mat(ex3);
                DCMname=['DeckCellMaster', datestr(now,29) '.txt'];
                [SaveDeckMasterFilename, SaveDeckMasterPathname, filterindex] = uiputfile( ...
                   {...
                    '*.txt',  'TXT-files  (*.txt)'; ...
                    }, ...
                    'Export DeckCell Master File as',DCMname...
                    );

                if filterindex>0
                    %fid = fopen('DeckCellMaster.txt','w+');
                    fid = fopen([SaveDeckMasterPathname SaveDeckMasterFilename ],'w+');
                    for i = 1:size(ex4,1)
                        fprintf(fid,'%s\n',ex4(i,:));
                    end
                    fclose(fid);
                    cd(nowDir);
                    set(StatusTypeText,'String',...
                        ['DeckCell Masterfile has been exported to' SaveDeckMasterPathname SaveDeckMasterFilename ] ,...
                        'Units','normalized', 'Position',[0.0 0.0 1.0 1.0],...
                        'FontSize',6,'ForegroundColor','r')
    %                   'Manage DeckCell                                                                                               ',...
                    pause(2);
                end
                cd(nowDir);
                clear DeckDataMaster
                clear DateDataMaster
                %uiresume(gcbf);
            end
            function [] = callback_btn_cancel(source, eventdata)
                %boo='CANCEL'
                cancelFlag = 1;
                uiresume(gcbf);
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%return

