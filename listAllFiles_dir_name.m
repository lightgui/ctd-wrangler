function [fileDirs fileNames] = listAllFiles_dir_name(dirName)

  dirData = dir(dirName);      % Get the data for the current directory
  dirIndex = [dirData.isdir];  % Find the index for directories
  fileNames = {dirData(~dirIndex).name}';  % Get a list of the files
  % Fill cell array for directory corresponding to file names
  fileDirs = repmat({dirName},[size(fileNames,1) 1]);
  subDirs = {dirData(dirIndex).name};  % Get a list of the subdirectories
  validIndex = ~ismember(subDirs,{'.','..'});  % Find index of subdirectories
                                               %   that are not '.' or '..'
  for iDir = find(validIndex)                  % Loop over valid subdirectories
    nextDir = fullfile(dirName,subDirs{iDir});    % Get the subdirectory path
    [fdirs fnames] = listAllFiles_dir_name(nextDir); % Recursively call function
    fileDirs = [fileDirs; fdirs]; % Append results of recursive calls
    fileNames = [fileNames; fnames];
  end
end