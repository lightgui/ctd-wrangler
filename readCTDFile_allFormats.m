function [ CTDHeader1 CTDData1 CTDHeader2 CTDData2 ] = readCTDFile_allFormats(filePath)

% Ability to read the following types of files:
% 1. XLS CTD Data
% 2. ASC CTD Data (old format)
% 3. ASC CTD Data (new format)
% 4. CNV CTD Data with/without elapsed time

CTDHeader1 = {};
CTDData1 = [];
CTDHeader2 = {};
CTDData2 = [];

fileExt = filePath(end-3:end);

if strcmpi(fileExt, '.cnv')
    [CTDHeader1 CTDData1] = readCTDFile_cnv(filePath);
elseif strcmpi(fileExt, '.asc') || strcmpi(fileExt, '.txt')
    [CTDHeader1 CTDData1] = readCTDFile_asc(filePath);
elseif strcmpi(fileExt, '.xls') 
    [CTDHeader1 CTDData1 CTDHeader2 CTDData2] = readCTDFile_xls(filePath);
else
    return;
end

% Deal with quirk in cell array representation that puts headers within
% double cells
if size(CTDHeader1,2) == 1
    CTDHeader1 = CTDHeader1{:};
end
if size(CTDHeader2,2) == 1
    CTDHeader2 = CTDHeader2{:};
end

% Sample Headers:
% XLS CTDQTRM Tab:
% 'DEPTHm'      'T090C'   'FLUOR'       'XMISS'     'C0uS/cm'   'SPCOND'    'PAR'   'DO2mg'        'pH'    'ORP'    'lnPAR'

% ASC QTRDBN2:
% 'POST1990'    'PrSM'    'DepFM'    'T090C'       'Xmiss'    'Specc'    'E'    'E10^-8'    'N^2'    'N'    'Flag'
% 'POST1990'    'PrSM'    'DepFM'    'T090C' 'V4'  'Xmiss'    'Specc'    'E'    'E10^-8'    'N^2'    'N'    'Flag'
% 'POST1990'    'PrSM'    'DepFM'    'T090C' 'FlS' 'Xmiss'    'Specc'    'E'    'E10^-8'    'N^2'    'N'    'Flag'

% ASC QTRZ
% 'POST1990'    'DepFM'    'T090C'       'Xmiss'    'C0uS/cm'    'Specc'    'Par'    'OxsatMg/L'    'Ph'    'Orp'    'Nbin'    'Flag'
% 'POST1990'    'DepFM'    'T090C' 'FlS' 'Xmiss'    'C0uS/cm'    'Specc'    'Par'    'OxsatMg/L'    'Ph'    'Orp'    'Nbin'    'Flag'
% 'POST1990'    'DepFM'    'T090C' 'V4'  'Xmiss'    'C0uS/cm'    'Specc'    'Par'    'OxsatMg/L'    'Ph'    'Orp'    'Nbin'    'Flag'

% ASC NEW 2013
%          'DepFM'    'T090C' 'V4'  'CStarTr0' 'C0uS/cm'    'Specc'    'Par'    'OxsolMg/L' 'Ph' 'Orp' 'Wl0''Wl1''Wl2''Flag'

% CNV WITH ELAPSED TIME
% 'prdM'   'depFM'    't090C' 'v4'  'CStarTr0' 'c0uS/cm'    'specc'    'par'    'oxsolMg/L' 'ph' 'orp' 'wl0''wl1''wl2''timeS'  'flag'
end

