function[] = structTest()
black =     [0.0000    0.0000   0.0000]; %Black
vYellow =   [1.0000    0.7020   0.0000]; %Vivid Yellow
strPurple = [0.5020    0.2431   0.4588]; %Strong Purple
vOrange =   [1.0000    0.4078   0.0000]; %Vivid Orange
vlBlue =    [0.6510    0.7412   0.8431]; %Very Light Blue
vRed =      [0.7569    0.0000   0.1255]; %Vivid Red
grYellow =  [0.8078    0.6353   0.3843]; %Grayish Yellow
mGray =     [0.5059    0.4392   0.4000]; %Medium Gray
vGreen =    [0.0000    0.4902   0.2039]; %Vivid Green
sPPink =    [0.9647    0.4627   0.5569]; %Strong Purplish Pink
strBlue =   [0.0000    0.3255   0.5412]; %Strong Blue
strYPink =  [1.0000    0.4784   0.3608]; %Strong Yellowish Pink
strViolet = [0.3255    0.2157   0.4784]; %Strong Violet
vOYellow =  [1.0000    0.5569   0.0000]; %Vivid Orange Yellow
strPRed =   [0.7020    0.1569   0.3176]; %Strong Purplish Red
vGYellow =  [0.9569    0.7843   0.0000]; %Vivid Greenish Yellow
strRBrown = [0.4980    0.0941   0.0510]; %Strong Reddish Brown
vYGreen =   [0.5765    0.6667   0.0000]; %Vivid Yellowish Green
dYBrown =   [0.3490    0.2000   0.0824]; %Deep Yellowish Brown
vROrange =  [0.9451    0.2275   0.0745]; %Vivid Reddish Orange
dOGreen =   [0.1373    0.1725   0.0863]; %Dark Olive Green

%% Initialize variable aliases and variable units

% Store variables in a hierarchical structure
% Struct for individual variables contain helpful fields for plotting,
% identification, and data storage
Post1900 = struct(...
    'aliases', {{'post1900', 'post1990'}},...
    'unit', 'days',...
    'data', []);

Depth = struct(...
    'aliases', {{'depfm', 'depthfm', 'depthm'}},...
    'unit', 'm',...
    'data', []);

PressureD = struct(...
    'aliases', {{'prdm'}},...
    'unit', 'db',...
    'data', []);

PressureS = struct(...
    'aliases', {{'prsm', 'press'}},...
    'unit', 'db',...
    'data', []);

Temp = struct(...
    'aliases', {{'t090c', 'tempc'}},...
    'unit', 'deg C',...
    'data', [],...
    'color', black,...
    'lineStyle', '-',...
    'marker', 'none');

Fluor = struct(...
    'aliases', {{'v4', 'fluor', 'fls', 'fleco-afl', 'fluorv'}},...
    'unit', '??',...
    'data', [],...
    'color', strPurple,...
    'lineStyle', '-',...
    'marker', 'none');

W10 = struct(...
    'aliases', {{'w10'}},...
    'unit', '??',...
    'data', [],...
    'color', strPurple,...
    'lineStyle', 'none',...
    'marker', '+');

W11 = struct(...
    'aliases', {{'w11'}},...
    'unit', '??',...
    'data', [],...
    'color', strPurple,...
    'lineStyle', 'none',...
    'marker', 'o');

W12 = struct(...
    'aliases', {{'w12'}},...
    'unit', '%',...
    'data', [],...
    'color', strPurple,...
    'lineStyle', 'none',...
    'marker', '*');

Xmiss = struct(...
    'aliases', {{'xmiss' 'cstartr0'}},...
    'unit', '%',...
    'data', [],...
    'color', vOrange,...
    'lineStyle', '-',...
    'marker', '.');

Cond = struct(...
    'aliases', {{'cond', 'c0us/cm'}},...
    'unit', 'uS/cm',...
    'data', [],...
    'color', vlBlue,...
    'lineStyle', '-',...
    'marker', '.');

Specc = struct(...
    'aliases', {{'specc', 'spcond'}},...
    'unit', 'uS/cm',...
    'data', [],...
    'color', mGray,...
    'lineStyle', '-',...
    'marker', '.');

PAR = struct(...
    'aliases', {{'par'}},...
    'unit', 'mE/m^2/sec',...
    'data', [],...
    'color', vRed,...
    'lineStyle', 'none',...
    'marker', '+');

PAR_adj = struct(...
    'aliases', {{'par_adj'}},...
    'unit', 'mE/m^2/sec',...
    'data', [],...
    'color', vRed,...
    'lineStyle', 'none',...
    'marker', 'o');

lnPAR = struct(...
    'aliases', {{'lnpar'}},...
    'unit', 'unitless',...
    'data', [],...
    'color', vRed,...
    'lineStyle', '-',...
    'marker', 'none');

lnPAR_adj = struct(...
    'aliases', {{'lnpar_adj'}},...
    'unit', 'unitless',...
    'data', [],...
    'color', vRed,...
    'lineStyle', '--',...
    'marker', 'none');

Ke = struct(...
    'aliases', {{'ke'}},...
    'unit', 'unitless',...
    'data', [],...
    'color', vGreen,...
    'lineStyle', '-',...
    'marker', '+');

Ke_adj = struct(...
    'aliases', {{'ke_adj'}},...
    'unit', 'unitless',...
    'data', [],...
    'color', vGreen,...
    'lineStyle', '-',...
    'marker', 'o');

Oxsat = struct(...
    'aliases', {{'oxsat', 'oxsatmg/l', 'do2mg', 'oxsolmg/l'}},...
    'unit', 'mg/L',...
    'data', [],...
    'color', sPPink,...
    'lineStyle', '-',...
    'marker', 'none');

pH = struct(...
    'aliases', {{'ph'}},...
    'unit', 'unitless',...
    'data', [],...
    'color', strBlue,...
    'lineStyle', '-',...
    'marker', 'none');

ORP = struct(...
    'aliases', {{'orp'}},...
    'unit', 'mV',...
    'data', [],...
    'color', strYPink,...
    'lineStyle', '-',...
    'marker', 'none');

Nbin = struct(...
    'aliases', {{'nbin'}},...
    'unit', 'bins',...
    'data', []);

TimeS = struct(...
    'aliases', {{'times'}},...
    'unit', 'seconds',...
    'data', []);

Flag = struct(...
    'aliases', {{'flag'}},...
    'unit', 'unitless',...
    'data', []);

E = struct(...
    'aliases', {{'e'}},...
    'unit', '??',...
    'data', [],...
    'color', strViolet,...
    'lineStyle', '-',...
    'marker', 'none');

E108 = struct(...
    'aliases', {{'e10^-8'}},...
    'unit', '??',...
    'data', [],...
    'color', vOYellow,...
    'lineStyle', '-',...
    'marker', 'none');

N2 = struct(...
    'aliases', {{'n^2'}},...
    'unit', '??',...
    'data', [],...
    'color', strPRed,...
    'lineStyle', '-',...
    'marker', 'none');

N = struct(...
    'aliases', {{'n'}},...
    'unit', '??',...
    'data', [],...
    'color', vGYellow,...
    'lineStyle', '-',...
    'marker', 'none');

% Variables are stored as "leaves" in a tree-like structure, according to
% their data-type, whether they will be plotted, and whether they are a
% dependent(Y) or independent(X) variable

masterCTDVars.Z.plot.Y = struct('Temp',Temp, 'Fluor',Fluor, 'W10',W10,...
    'W11',W11, 'W12',W12, 'Xmiss',Xmiss, 'Cond',Cond,...
    'Specc',Specc, 'PAR',PAR, 'PAR_adj',PAR_adj, 'lnPAR',lnPAR, 'lnPAR_adj',lnPAR_adj,...
    'Ke',Ke, 'Ke_adj',Ke_adj, 'Oxsat',Oxsat, 'pH',pH, 'ORP',ORP);

masterCTDVars.Z.plot.X = struct('PressureD',PressureD, 'Depth',Depth);

masterCTDVars.Z.nonPlot = struct('Post1900',Post1900, 'Nbin',Nbin, 'TimeS',TimeS, 'Flag',Flag);

masterCTDVars.N2.plot.Y = struct('Temp',Temp, 'Fluor',Fluor, 'Xmiss',Xmiss,...
    'Specc',Specc, 'E',E, 'E108',E108, 'N2',N2, 'N',N);

masterCTDVars.N2.plot.X = struct('PressureS',PressureS);

masterCTDVars.N2.nonPlot = struct('Post1900',Post1900, 'Flag',Flag);

    function[] = getAllVarStructs(rootStruct, dataType, isPlot, varType)
        switch nargin
            case 1
                startStruct = rootStruct;
            case 2
                startStruct = rootStruct.dataType;
            case 3
                startStruct = rootStruct.dataType.isPlot;
            case 4
                startStruct = rootStruct.dataType.isPlot.varType;
            otherwise
                return
        end
    end
end